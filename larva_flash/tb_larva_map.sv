`include "defines.svh"
// 12.582912 MHz

import defines_pkg::*;

module tb_larva_map();
    //-------------------------------------------------------------------------------------//
    //--------------------------------- Global things -------------------------------------//
    //-------------------------------------------------------------------------------------//
    spi_io spi();
    generic_bus_io#(.ADR_WIDTH(24), .DATA_WIDTH(8)) tb_data_bus();
	wishbone_io tb_wbm();
    typedef enum {ST_OPERATION, ST_ADR, ST_GET_DATA, ST_SET_DATA, ST_UNKNOWN} state_e;
    state_e state;
    typedef enum bit [7:0] {WRITE_OP = 8'h02, READ_OP = 8'h03} operation_e;
    operation_e op;
    bit clk, nrst, clk_x2;
    logic tb_next;
    logic [7:0] tb_oper, tb_data;
    logic [8:0] tb_burst_len;
    logic [7:0] data_arr[];
	logic [15:0] rd_adr;
	
	// WB signals
	logic [31:0] tb_wb_data;
	
	// Map signals
	logic [7:0] tb_map_data;
	logic [3:0] tb_map_mode;
 	logic tb_map_nempty, tb_map_rrq, tb_map_enable;
	
	
	always #39.736 clk = ~clk; // 12.582912 MHz
	always #19.868 clk_x2 = ~clk_x2; // 12.582912 MHz
	
    initial begin
        nrst <= 0;
        tb_data_bus.reset_master();
		tb_wbm.reset_master();
        spi.reset_slave();
		tb_oper <= '0;
		tb_burst_len <= '0;
		tb_data <= '0;
		rd_adr <= '0;
		
		// Map section
		tb_map_enable <= 1'b0;
		tb_map_mode <= '0;
		
		// WB section
		tb_wb_data <= '0;
		
        #397.36 
        nrst <= 1;   
        data_arr = new [5] ('{8'h81, 8'h33, 8'h11, 8'h15, 8'h32});
        // write_data(24'h800001, data_arr);
		// read_data(24'h800001, 5);
		wb_write(16'd0, 32'h77_010000, 4'hf);
		wb_write(16'd0, 32'h88_010001, 4'hf);
		wb_write(16'd0, 32'h99_010002, 4'hf);
		wb_write(16'd0, 32'hAA_010003, 4'hf);
		wb_write(16'd0, 32'hBB_010004, 4'hf);
		
		wb_write(16'd1, 32'h00_010000, 4'hf);
		
		wb_read(16'd0, 4'hf);
		wb_read(16'd0, 4'hf);
		wb_read(16'd0, 4'hf);
		wb_read(16'd0, 4'hf);
		wb_read(16'd0, 4'hf);
		tb_map_enable <= 1'b1;
		
		
    end
    
    larva_map_reader larvaMapReader(.clk(clk), .nrst(nrst),
                        .map_data(tb_map_data), 
                        .map_nempty(tb_map_nempty),
                        .map_rrq(tb_map_rrq),
						.map_enable(tb_map_enable),
                        .map_mode(tb_map_mode),
						.map_wbs(tb_wbm),
						.spi(spi));                     
    task automatic write_data(logic [23:0] adr,
                              const ref [7:0] data[]); 
        automatic int bl = $size(data);
        automatic int cnt = 0;
        if (bl > 0 && bl < 256) begin        
            while (tb_data_bus.busy) begin
                @(posedge clk);
            end
            tb_oper <= WRITE_OP;
            tb_burst_len <= bl[8:0];
            tb_data_bus.adr <= adr;
            tb_data_bus.dat_mosi <= data[cnt];
            tb_data_bus.we <= 1'b1;
            while (cnt != tb_burst_len - 1) begin
				@(negedge tb_next);
                cnt++;
                tb_data_bus.dat_mosi <= data[cnt];
            end
			tb_data_bus.we <= 1'b0;
        end
        else begin 
            $display("Input variable \"bl\" should be greater than zero and less than 256, but it's %0d", bl);
        end
    endtask: write_data
	
    task automatic read_data(logic [23:0] adr, int burst); 
        automatic int cnt = 0; 
		while (tb_data_bus.busy) begin
			@(posedge clk);
		end
		tb_oper <= READ_OP;
		tb_burst_len <= '0;
		tb_data_bus.adr <= adr;
		tb_data_bus.re <= 1'b1;
		if (burst > 0) begin
			while (cnt != burst) begin
				@(posedge tb_next);
				cnt++;
			end
			tb_data_bus.re <= 1'b0;
		end		
		else begin 
			$display("Input variable \"burst\" should be greater than zero %0d", burst);
		end
    endtask: read_data 

    logic [4:0] spi_cnt;
    logic spi_sck_d, spi_ncs_d;
    logic sck_rise, sck_down, ncs_down;
    logic [7:0] spi_operation, mem_data, mem_q;
    logic [23:0] spi_adr;
	logic [15:0] mem_adr;
	logic mem_wr;

    always_ff @(posedge clk_x2 or negedge nrst) begin
        if (!nrst) begin
            spi_cnt <= '0;
            spi_operation <= '0;
            spi_adr <= '0;
            mem_data <= '0;
            state <= ST_OPERATION;
            spi_sck_d <= 1'b0;
            spi_ncs_d <= 1'b0;
			mem_adr <= '0;
			mem_wr <= 1'b0;
        end
        else begin
			mem_wr <= 1'b0;
            spi_sck_d <= spi.sck;
            spi_ncs_d <= spi.ncs;

            if (!spi.ncs) begin
                unique case (state)
                    ST_OPERATION: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            spi_operation[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
                                state <= ST_ADR;
                                spi_cnt <= 5'd23;
                            end
                        end
                    end
                    ST_ADR: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            spi_adr[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
								spi_cnt <= 5'd7;                                
                                unique case (spi_operation)
                                    WRITE_OP: begin
                                        state <= ST_GET_DATA;
                                    end
                                    READ_OP: begin
                                        state <= ST_SET_DATA;
                                    end
                                    default: begin
                                        state <= ST_UNKNOWN;
                                    end
                                endcase    
                            end
                        end
                    end
                    ST_GET_DATA: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            mem_data[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
                                state <= ST_GET_DATA;
                                spi_cnt <= 5'd7;
								mem_wr <= 1'b1;
                            end
                        end
						if (mem_wr) begin
							spi_adr <= spi_adr + 1'b1;
						end
                    end
					ST_SET_DATA: begin
						if (sck_down) begin // 
							spi_cnt <= spi_cnt - 1'b1;
							spi.miso <= mem_q[spi_cnt];
							if (spi_cnt == 5'd0) begin
                                state <= ST_SET_DATA;
                                spi_cnt <= 5'd7;
								spi_adr <= spi_adr + 1'b1;
                            end
						end
					end
					ST_UNKNOWN: begin
						state <= ST_UNKNOWN;
					end
                endcase
            end
            else begin
                state <= ST_OPERATION;
				spi_cnt <= 7'd7;
            end
        end
    end
    assign sck_rise = (spi.sck && ~spi_sck_d);
    assign sck_down = (~spi.sck && spi_sck_d);
    assign ncs_down = ~spi.ncs && spi_ncs_d;
	
	logic [7:0] map_mem[0:256];
    always_ff @(posedge clk_x2 or negedge nrst) begin
		if (!nrst) begin
			mem_q <= '0;
		end
		else begin
			mem_q <= map_mem[spi_adr[7:0]];
			if (mem_wr) begin
				map_mem[spi_adr[7:0]] <= mem_data;
				mem_q <= mem_data;
			end
		end
	end
	
	task wb_write(input bit [15:0] adr, bit [31:0] data, bit [3:0] sel);
        while (tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_we <= 1'b1;
        tb_wbm.wbm_stb <= 1'b1;
        tb_wbm.wbm_cyc <= 1'b1;
        tb_wbm.wbm_sel <= sel;
        tb_wbm.wbm_adr <= adr;
        tb_wbm.wbm_dat <= data;
        while (!tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_stb <= 1'b0;
        tb_wbm.wbm_cyc <= 1'b0;
    endtask: wb_write

    task wb_read(input bit [15:0] adr, bit [3:0] sel);
        while (tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_we <= 1'b0;
        tb_wbm.wbm_stb <= 1'b1;
        tb_wbm.wbm_cyc <= 1'b1;
        tb_wbm.wbm_sel <= sel;
        tb_wbm.wbm_adr <= adr;
        while (!tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wb_data <= tb_wbm.wbs_dat;
        tb_wbm.wbm_stb <= 1'b0;
        tb_wbm.wbm_cyc <= 1'b0;
    endtask: wb_read
	
endmodule: tb_larva_map