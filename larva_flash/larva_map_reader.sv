/*
 * Модуль "larva_map_reader" для проекта ft75_main.
 * FSM, читающий из микросхемы флэш-памяти EPCQ16A "карту опроса" для выбранного режима работы.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::EPCQ_WRITE;
import defines_pkg::EPCQ_READ;
module larva_map_reader(input bit clk, nrst, // clk is 12.582912 MHz
                        // Fifo map-interface
                        output logic [7:0] map_data, 
                        output logic map_nempty,
                        input logic map_rrq, map_enable,
                        input logic [3:0] map_mode,
						wishbone_io.slave map_wbs,
						spi_io.master spi);
    
    localparam logic [23:0] MODE_0_ADDR = `EPCQ_BASE_MODE_ADR; // 0x010000 Режим 1
    localparam logic [23:0] MODE_1_ADDR = MODE_0_ADDR + `M01_SIZE + 16'd4096; // 0x012000 Режим 2
    localparam logic [23:0] MODE_2_ADDR = MODE_1_ADDR + `M02_SIZE + 16'd4096; // 0x015000 Режим 3
    localparam logic [23:0] MODE_3_ADDR = MODE_2_ADDR + `M04_SIZE + 16'd4096; // 0x01A000 Режим 4
    localparam logic [23:0] MODE_4_ADDR = MODE_3_ADDR + `M08_SIZE + 16'd4096; // 0x023000 Режим 5
    localparam logic [23:0] MODE_5_ADDR = MODE_4_ADDR + `M16_SIZE + 16'd4096; // 0x034000 Режим 6
    localparam logic [23:0] MODE_6_ADDR = MODE_5_ADDR + `M16_SIZE + 16'd4096; // 0x045000 Режим 7
    localparam logic [23:0] MODE_7_ADDR = MODE_6_ADDR + `M16_SIZE + 16'd4096; // 0x056000 Режим 8
    localparam logic [23:0] MODE_8_ADDR = MODE_7_ADDR + `M16_SIZE + 16'd4096; // 0x067000 Режим 9
    localparam logic [23:0] MODE_9_ADDR = MODE_8_ADDR + `M16_SIZE + 16'd4096; // 0x078000 Режим 10 // Для проверки на старом КПА
    localparam logic [23:0] MODE_10_ADDR = MODE_9_ADDR + `M08_SIZE + 16'd4096; // 0x081000 Режим 11
    
    generic_bus_io#(.ADR_WIDTH(24), .MOSI_WIDTH(8), .MISO_WIDTH(8)) data_bus();
    typedef enum bit {ST_SET_READ, ST_GET_DATA} state_e;
    state_e state;
    logic [23:0] mode_addr;
    logic [16:0] mode_size;
    logic ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            mode_addr <= '0;
            mode_size <= '0;
            ena <= 1'b0;
        end
        else begin
            if (map_enable && !ena) begin
                ena <= 1'b1;
                unique case (map_mode)
                    4'd0: begin // M01 - 4096 words in a cycle
                        mode_addr <= MODE_0_ADDR;
                        mode_size <= `M01_SIZE;
                    end
                    4'd1: begin // M02 - 8192 words in a cycle
                        mode_addr <= MODE_1_ADDR;
                        mode_size <= `M02_SIZE;
                    end
                    4'd2: begin // M04 - 16384 words in a cycle
                        mode_addr <= MODE_2_ADDR;
                        mode_size <= `M04_SIZE;
                    end
                    4'd3: begin // M08 - 32768 words in a cycle
                        mode_addr <= MODE_3_ADDR;
                        mode_size <= `M08_SIZE;
                    end
                    4'd4: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_4_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                    4'd5: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_5_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                    4'd6: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_6_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                    4'd7: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_7_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                    4'd8: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_8_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                    4'd9: begin // M08 - 32768 words in a cycle
                        mode_addr <= MODE_9_ADDR;
                        mode_size <= `M08_SIZE;
                    end
                    default: begin // M16 - 65536 words in a cycle
                        mode_addr <= MODE_10_ADDR;
                        mode_size <= `M16_SIZE;
                    end
                endcase
            end
        end
    end
    
	// Fifo signals
    logic fifo_pause;
    logic fifo_full, fifo_empty, fifo_wrq;
    logic [9:0] fifo_usedw;
    logic [15:0] rd_cnt;
	logic [8:0] burst_len;
    assign map_nempty = ~fifo_empty;
	assign burst_len = (ena) ? '0 : 9'd1;
	// WB signals
	logic wb_rd, wb_wr, wb_done;
	logic [23:0] wb_spi_wr_adr, wb_spi_rd_adr;
	logic [7:0] wb_spi_wr_data;
	// EPCQ signals
	logic [7:0] op, debug_data;
	logic nb, op_done;
	logic was_busy;

	typedef enum {WB_DEBUG_IDLE, WB_DEBUG_OP} wb_debug_state_e;
	wb_debug_state_e wb_debug_state;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
			// Fifo signals 
			data_bus.re <= 1'b0;
            data_bus.we <= 1'b0;
            data_bus.dat_mosi <= '0;
            data_bus.adr <= '0;			
            fifo_pause <= 1'b0;
            fifo_wrq <= 1'b0;
            rd_cnt <= '0;
            state <= ST_SET_READ;	
			// WB signals
			wb_done <= 1'b0;	
			// EPCQ signals
			op <= '0;
			op_done <= 1'b0;
			debug_data <= '0;
			was_busy <= 1'b0;
            wb_debug_state <= WB_DEBUG_IDLE;
        end
        else begin
            fifo_wrq <= 1'b0;
            if (ena && !wb_wr && !wb_rd) begin
                unique case (state)
                    ST_SET_READ: begin
						op <= EPCQ_READ;
                        if (!fifo_full && !fifo_pause) begin
                            data_bus.re <= 1'b1;
                            data_bus.adr <= mode_addr + rd_cnt;
							state <= ST_GET_DATA;
                        end
                        else begin
                            fifo_pause <= 1'b1;
                            data_bus.re <= 1'b0;
                            if (nb) begin
                                fifo_wrq <= 1'b1;
								if (rd_cnt != mode_size - 1'b1) begin
									rd_cnt <= rd_cnt + 1'b1;
								end
								else begin
									rd_cnt <= '0;
								end
                            end
                        end
                    end
                    ST_GET_DATA: begin
                        if (nb) begin
                            fifo_wrq <= 1'b1;
                            if (rd_cnt != mode_size - 1'b1) begin
                                rd_cnt <= rd_cnt + 1'b1;
                                data_bus.adr <= mode_addr + rd_cnt;
                            end
                            else begin
                                rd_cnt <= '0;
                            end
                            state <= ST_SET_READ;
                        end
                        if (rd_cnt == mode_size - 1'b1) begin
                            data_bus.re <= 1'b0;
                        end
                    end
                endcase
            end
            
            if (!fifo_usedw[9]) begin
                fifo_pause <= 1'b0;
            end
			if (data_bus.busy && !ena) begin // WB debug only code
				was_busy <= 1'b1;
				data_bus.re <= 1'b0;
				data_bus.we <= 1'b0;
			end
			else begin 
				was_busy <= 1'b0;
			end
			wb_done <= 1'b0;
			op_done <= 1'b0;
			unique case (wb_debug_state)
				WB_DEBUG_IDLE: begin
					data_bus.dat_mosi <= wb_spi_wr_data;
					if (wb_wr && !op_done) begin
						data_bus.adr <= wb_spi_wr_adr;
						op <= EPCQ_WRITE;
						data_bus.we <= 1'b1;
						wb_done <= 1'b1;
						wb_debug_state <= WB_DEBUG_OP;
					end
					if (wb_rd && !op_done) begin
						data_bus.adr <= wb_spi_rd_adr;	
						op <= EPCQ_READ;
						data_bus.re <= 1'b1;
						wb_debug_state <= WB_DEBUG_OP;
					end
				end
				WB_DEBUG_OP: begin
					if (!data_bus.busy && was_busy) begin
						op_done <= 1'b1;
						debug_data <= data_bus.dat_miso;
						if (wb_rd) begin
							wb_done <= 1'b1;
						end
						wb_debug_state <= WB_DEBUG_IDLE;
					end
				end
			endcase
        end
    end

	// Debug section
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            map_wbs.wbs_ack <= 1'b0;
            map_wbs.wbs_dat <= '0;
			wb_rd <= 1'b0;
			wb_wr <= 1'b0;
			wb_spi_wr_adr <= '0;
			wb_spi_rd_adr <= '0;
			wb_spi_wr_data <= '0;
        end
        else begin
			map_wbs.wbs_ack <= 1'b0;
            if (map_wbs.wbm_stb && map_wbs.wbm_cyc && !map_wbs.wbs_ack) begin
                if (map_wbs.wbm_we) begin
					unique case (map_wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
						`WB_ADR_INT_WIDTH'd0: begin // Запись флэш-памяти. В слове 32'hAA_BBBBBB: AA - байт записи, BBBBBB  - адрес записи
							if (map_wbs.wbm_sel == 4'hf && !ena) begin
								if (!wb_wr) begin
									{wb_spi_wr_data, wb_spi_wr_adr} <= map_wbs.wbm_dat;
									wb_wr <= 1'b1;
								end
							end
							else begin
								map_wbs.wbs_ack <= 1'b1;
							end
						end
						`WB_ADR_INT_WIDTH'd1: begin // Установка адреса для чтения флэш-памяти. 24 разряда.
							if (map_wbs.wbm_sel[2:0] == 3'h7 && !ena) begin
								wb_spi_rd_adr <= map_wbs.wbm_dat[23:0];
								map_wbs.wbs_ack <= 1'b1;
							end
						end
					endcase
                end 
                else begin // Чтение флэш-памяти по адресу wb_spi_rd_adr, после чтения адрес увеличивается на 1.
					if ((map_wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == `WB_ADR_INT_WIDTH'd0) && (map_wbs.wbm_sel[0]) && !ena) begin
						if (!wb_wr) begin
							wb_rd <= 1'b1;
						end
                    end
					else begin
						map_wbs.wbs_ack <= 1'b1;
					end
                end
            end
			
			if (wb_done) begin
				map_wbs.wbs_ack <= 1'b1;
				map_wbs.wbs_dat <= {24'd0, debug_data};
			end
			
			if (op_done) begin
				wb_wr <= 1'b0;
				wb_rd <= 1'b0;
				if (wb_rd) begin
					wb_spi_rd_adr <= wb_spi_rd_adr + 1'b1;
				end
			end
		end
	end
	   
    larva_map_fifo larvaMapFifo(.clock(clk), .aclr(~nrst), .data(data_bus.dat_miso), .rdreq(map_rrq), .wrreq(fifo_wrq), .almost_full(fifo_full), .empty(fifo_empty), .q(map_data), .usedw(fifo_usedw), .full());
    larva_epcq16a_phy larvaEPCQ16A_Phy(.clk(clk), .nrst(nrst), .operation(op), .burst_len(burst_len), .next_byte(nb), .data_bus(data_bus.slave), .spi(spi));
endmodule: larva_map_reader