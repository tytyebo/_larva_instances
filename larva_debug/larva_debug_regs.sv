/*
 * Модуль larva_debug_regs содержит тестовые регистры, подключенные к шине WB.
 * Адреса от Base до Base + 10'h3ff возвращают в качестве данных 10 разрядный адрес.
 * Адрес Base + 12'h401 содержит все единицы.
 * Адрес Base + 12'h402 содержит 10 разрядный счетчик, считающий с ноля.
 * Остальные адреса содержат ноли.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
module larva_debug_regs(input bit nrst, clk,
                        wishbone_io.slave wbs);

    logic [9:0] debug_cnt;
    logic [31:0] test_reg, version;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
            debug_cnt <= '0;
            test_reg <= '0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                if (wbs.wbm_we) begin
                    if (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == `WB_ADR_INT_WIDTH'h403) begin
                        test_reg <= wbs.wbm_dat;
                    end
                end
                else begin
                    if (wbs.wbm_sel[1:0] == 2'b11) begin
                        if (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] >= 0 && wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] <= 10'h3ff) begin
                            // wbs.wbs_dat <= {22'd0, wbs.wbm_adr[9:0]};
                            wbs.wbs_dat <= {24'd0, wbs.wbm_adr[7:0]};
                        end
                        else begin
                            case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                            // `WB_ADR_INT_WIDTH'h0:   // Все ноли
                            `WB_ADR_INT_WIDTH'h401: begin // Все единицы
                                wbs.wbs_dat <= {22'd0, 10'h3ff};
                            end
                            `WB_ADR_INT_WIDTH'h402: begin // Счётчик
                                wbs.wbs_dat <= {22'd0, debug_cnt};
                                debug_cnt <= debug_cnt + 1'b1;
                            end
                            `WB_ADR_INT_WIDTH'h403: begin // Тестовый регистр
                                wbs.wbs_dat <= test_reg;
                            end
                            `WB_ADR_INT_WIDTH'h404: begin // Версия прошивки
                                wbs.wbs_dat <= version;
                            end
                            default: begin
                                wbs.wbs_dat <= '0;
                            end
                        endcase
                        end
                    end
                end
                wbs.wbs_ack <= 1'b1;
            end
        end
    end
    version_reg versionReg(version);
endmodule: larva_debug_regs