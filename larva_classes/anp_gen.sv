/*
 * Класс anp_gen для работы с DUT larva_anp.
 * Эмитирует 5 АЦП, формирует случайные данные аналоговых датчиков.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef ANP_GEN_INCLUDE
    `define ANP_GEN_INCLUDE
    // `include "defines.svh"
    import classes_pkg::vAnpTest_t;
    class anp_gen; 
        static bit [5:0] data_ena;
        mailbox mbx[0:7];  
        vAnpTest_t anp;
        logic [2:0] mux;
        bit [5:0][15:0] data;
        local real emr = 0.0006103515625; // Voltage
        // Конструктор класса anp_gen, реализующий связь с проектом larva_anp через интерфейсы Spi.
        extern function new(input vAnpTest_t anp, mailbox mbx[0:7]);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод-сериализатор данных АЦП по Spi
        extern task send_answer(int n);
        // Метод-генератор данных АЦП
        extern task gen_data(int n);
        extern task run;
        
    endclass: anp_gen
    
    function anp_gen::new(input vAnpTest_t anp, mailbox mbx[0:7]);
        this.anp = anp;
        this.mbx[0:7] = mbx[0:7];
    endfunction
    
    task anp_gen::init;
        anp.miso = '0;
        mux = '1;
        data = '0;
        data_ena = 6'b000001;
    endtask: init
    
    task anp_gen::send_answer(int n);
        // $display("@%0t: %m n mux/anp.mux data %d %h %h %h", $time, n, mux[, anp.mux, data[n]);
        for (int i = 15; i >= 0; i--) begin
            @(negedge anp.sck[n]);
            anp.miso[2 * n] = data[2 * n][i];
            anp.miso[2 * n + 1] = data[2 * n + 1][i];
        end
    endtask: send_answer

   task anp_gen::gen_data(int n);
        automatic real u_in = 0;
        // @(negedge anp.ncs[n]);
        if (mux != anp.mux) begin
            mux = anp.mux;
            // u_in = ($urandom_range(8124, 0));
            u_in = 995; // АНП-1, АНП-Б
            u_in = u_in - 795; // АНП-1, АНП-Б
            u_in = 2.2485 - (u_in / 1000) * 2.1029 * 160 / 1160; // АНП-1, АНП-Б
            u_in = 2.296;
            // u_in = 30000; // АНП-2
            // u_in = u_in - 15000; // АНП-2
            // u_in = 1.2884 - (u_in / 1000) * 1.1013 * 68 / 1068; // АНП-2
            
           //  u_in = 30000; // АНП-3
            // u_in = 2.2618 - (u_in / 1000) * 0.9534 * 68 / 1068; // АНП-3
            
            if (u_in < 0) begin
                data[2 * n] = '0;
            end
            else begin
                if (u_in > 2.5) begin
                    data[2 * n] = '1;
                end
                else begin
                    data[2 * n] = 12'hcb5;// u_in / emr;
                end
            end
            $display("@%0t: %m data %h real %f", $time, data[2 * n], u_in);
            data[2 * n + 1] = ($urandom_range(25, 1)) / (emr * 1000);
            if (data_ena[n]) begin
                mbx[mux].put(data[n][11:0]);
            end
                // $display("@%0t: %m mux %d data[n][11:0] %h", $time, mux, data[n][11:0]);
                
            /* if (data_ena[i + 1]) begin
                mbx.put(data[i + 1][11:0]);
            end*/ 

        end
    endtask: gen_data
    
    task anp_gen::run;
        fork
            forever begin
                gen_data(0);
                send_answer(0);
            end
            /*forever begin
                send_answer(1);
            end
            forever begin
                send_answer(2);
            end
            forever begin
                send_answer(3);
            end
            forever begin
                send_answer(4);
            end
            forever begin
                send_answer(5);
            end*/
        join_none
    endtask: run
`endif