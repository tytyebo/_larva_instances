/*
 * Класс pc_chanel для работы с DUT larva_digital.
 * Эмитирует передачу информации по параллельному каналу.
 *
 * Made by NotBlacksmithMax.
 * ЗИТЦ.
 * 2020
 */
`ifndef PARALL_CHAN_INCLUDE
    `define PARALL_CHAN_INCLUDE
    import classes_pkg::vPcTest_t;
    class pc_gen;
        vPcTest_t pc;
        mailbox mbx;
        bit [9:0] data;
        // Конструктор класса pс_gen, реализующий связь с проектом larva_anp через параллельный канал.
        extern function new(input vPcTest_t pc, mailbox mbx);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод-сериализатор данных параллельному каналу
        extern task send_answer(int n);
        // Метод-генератор данных
        extern task gen_data;
        extern task run;
    endclass: pc_gen
    
    function pc_gen::new(input vPcTest_t pc, mailbox mbx);
        this.pc = pc;
        this.mbx = mbx;
        data = 10'd0;
    endfunction
    
    task pc_gen::init;
        pc.prk = '0;
    endtask: init
    
    task pc_gen::send_answer(int n);
        // $display("@%0t: %m n mux/anp.mux data %d %h %h %h", $time, n, mux[n], anp.mux[n], data[n]);
        @(posedge pc.req);
        pc.prk = data;
        @(negedge pc.req);
        pc.prk = '0;
    endtask: send_answer
    
    task pc_gen::gen_data;
        data = $urandom_range(1023);
        mbx.put(data);
    endtask: gen_data
    
    task pc_gen::run;
        fork
            forever begin
                gen_data();
                send_answer(0);
            end
        join_none
    endtask: run
`endif