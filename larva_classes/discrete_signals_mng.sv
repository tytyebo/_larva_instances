/*
 * Класс discrete_signals_mng - мэнеджер разовых команд.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2021
 * email: kuznetsov_ms@org.miet.ru
 */

`ifndef DISCRETE_SIGNALS_MNG_INCLUDE
    `define DISCRETE_SIGNALS_MNG_INCLUDE
    import classes_pkg::vDisSigTest_t;
    class discrete_signals_mng#(parameter int LIFETIME = 5000); // LIFETIME refer to `timescale in the project
        vDisSigTest_t signals;
        // Конструктор класса xmtr
        extern function new(input vDisSigTest_t signals);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод run, имитирующий посылку по RS-232 от передатчика
        extern task run_after(input int time_by_timescale);
    endclass: discrete_signals_mng
    
    function discrete_signals_mng::new(input vDisSigTest_t signals);
        this.signals = signals;
    endfunction
    
    task discrete_signals_mng::init;
        signals.nrecord <= 1'b1;
    endtask: init

    task discrete_signals_mng::run_after(input int time_by_timescale); // refer to `timescale in the project
        fork
            #time_by_timescale;
            signals.nrecord <= ~signals.nrecord;
            #LIFETIME;
            signals.nrecord <= ~signals.nrecord;
        join_none
    endtask: run_after
`endif