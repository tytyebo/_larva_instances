/*
 * Класс orbit_checker для работы с DUT larva_main.
 * Проверяет слова Орбиты полученные от класса orbit_monitor. Сохраняет данные в файл.
 * Подключается к классу orbit_monitor с помощью mailbox для удвоенных слов Орбиты logic [23:0].
 * collect_cycle - осуществляет сортировку и преобразование удвоенных слов Орбиты в слова-структуры Орбиты orbitWord_s.
 * check_frame - проверяет кадры Орбиты на правильность служебной информации (разработка в процессе)
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef ORBIT_CHECKER_INCLUDE
    `define ORBIT_CHECKER_INCLUDE
    `include "defines.svh"
    import defines_pkg::bool_e;
    import defines_pkg::TRUE;
    import defines_pkg::FALSE;
    import classes_pkg::orbitWord_s;
    import classes_pkg::orbitType_e;
    import classes_pkg::SYCN_WORD;
    import classes_pkg::DATA_WORD;
    import classes_pkg::SERVICE_WORD;
    import classes_pkg::orbitService_s;
    import classes_pkg::anp_checker;
    import defines_pkg::CALIB_CH0_LOW;
    import defines_pkg::CALIB_CH0_HIGH;
    import defines_pkg::CALIB_CH1_ZERO;
    import defines_pkg::CALIB_CH1_LOW;
    import defines_pkg::CALIB_CH1_HIGH;
    class orbit_checker;
        mailbox pipe_scb, cycle_scb, map_scb;
        anp_checker anp_chk[0:7];
        bit [3:0] mode;
        int sync_period;
        local bit [/*9*/7:0] checking_cnt;
        bool_e mute;
        
        local bit [1:0] cycle_cnt = '0;
        local bit [7:0] anp0_cha_f = `ANP0_CHA_F;
        local bit [9:0] mil0_cnt = `MIL0_DEBUG_CONTS;
        local bit [9:0] mil1_cnt = `MIL1_DEBUG_CONTS;
        local bit [9:0] mil2_cnt = `MIL2_DEBUG_CONTS;
        local bit [9:0] pc0_cnt = `DIG_PC0_BASE;
        local bit [9:0] pc1_cnt = `DIG_PC1_BASE;
        local bit [9:0] rs_cnt = `DIG_RS_BASE;
        local bit [9:0] sc_cnt = `DIG_SC_BASE;
        local bit [9:0] calib_ch0 = CALIB_CH0_LOW, calib_ch0_1 = '0;
        local bit [9:0] calib_ch1 = CALIB_CH1_ZERO;
        local bit calib_ch1_sw = 1'b0;
        local bit [9:0] anp1_cnt = '0;
        local bit [9:0] anp1b_cnt = '0;
        local bit [9:0] anp2_cnt = '0;
        local bit [9:0] anp3_cnt = '0;
        local bit [9:0] anpb_cnt = '0;
        local bit [9:0] mem_queue[$];
        local int mem_cnt = 0;
        local bit [255:0][15:0] mem_map;

        // Конструктор класса. Принимает mailbox mbx с удвоенными словами Орбиты и код mode режима работы.
        extern function new(mailbox mbx_pipe, bit [3:0] mode, mailbox mbx_map, mailbox mbx_anp[0:7]);
        // Private метод, преобразующий удвоенные слова в одиночные данные
        extern local function logic [11:0] undouble(logic [23:0] data);
        // Загрузка mif-файла с байтовыми ключами
        extern local function void load_mif(string fname);
        // Сброс в начальное состояне
        extern task init;
        // Запуск проверки кадров Орбиты
        extern task run;
        // Метод-конвертор удвоенных слов Орбиты в слова Орбиты структуры orbitWord_s
        extern task collect_cycle;
        // Метод, запускающий конвертацию и проверку слов Орбиты
        extern task collect;
        // Метод, проверяющий служебную информацию в кадре Орбиты (в процессе разработки)
        extern task check_frame;
        // Метод для проверки данных в словах Орбиты при использовании счётчика в качестве источника данных
        extern task check_counter(input logic [9:0] data);
        // Метод для проверки данных в словах Орбиты при использовании режима самоконтроля
        extern task check_selftest(input logic [9:0] data);
    endclass: orbit_checker

    // Конструктор класса. Принимает mailbox mbx с удвоенными словами Орбиты и код mode режима работы.
    function orbit_checker::new(mailbox mbx_pipe, bit [3:0] mode, mailbox mbx_map, mailbox mbx_anp[0:7]);
        pipe_scb = mbx_pipe;
        map_scb = mbx_map;
        this.mode = mode;
        for (int i = 0; i < 8; i++) begin 
            anp_chk[i] = new(mbx_anp[i], anp0_cha_f[i], "ANP0 CH_A", i);
        end
    endfunction
    
    // Private метод, преобразующий удвоенные слова в одиночные данные
    function logic [11:0] orbit_checker::undouble(logic [23:0] data);
        for (int i = 0; i < 12; i++) begin
            if (data[2 * i] == data[2 * i + 1]) begin
                undouble[i] = data[2 * i];
            end
            else begin
                undouble[i] = 1'bx;
            end
        end
    endfunction: undouble
    // Загрузка mif-файла с байтовыми ключами
    function void orbit_checker::load_mif(string fname);
            
        automatic int file, code, num = -1;
        automatic bit [7:0] cdata;
        automatic bit [15:0] adr;
        automatic string str;
        
        file = $fopen(fname, "r");
        while (!$feof(file)) begin
            code = $fgets(str, file);
            code = $sscanf(str, "%d %c %h", num, cdata, adr);
            if (num > -1) begin
                mem_map[num] = adr;
            end
        end
        $fclose(file);
    endfunction: load_mif
    // Сброс в начальное состояне
    task orbit_checker::init;
        cycle_scb = new();
        checking_cnt = '0;
        mute = TRUE;
        unique case(mode)
            4'd0: begin
                sync_period = 2; // sync_period - период следования синхроимпульса - 1 раз в 2 слова.
            end
            4'd1: begin
                sync_period = 4;
            end
            4'd2: begin
                sync_period = 8;
            end
            4'd3: begin
                sync_period = 16;
            end
            default: begin
                sync_period = 32;
            end
        endcase
        for (int i = 0; i < 8; i++) begin
            anp_chk[i].init();
        end
        load_mif("larva_orbit_mem.mif");
    endtask: init
    
    // Запуск проверки кадров Орбиты
    task orbit_checker::run;
        collect();
    endtask: run

    // Метод-конвертор удвоенных слов Орбиты в слова Орбиты структуры orbitWord_s
    task orbit_checker::collect_cycle;
        automatic orbitWord_s orb_w;
        automatic logic [11:0] tmp_dat;
        automatic int words_cnt, m16_cnt;
        automatic bit part;
        automatic logic [23:0] dword;
        words_cnt = 0;
        m16_cnt = 0;
        part = 1'b0;
        for (int i = 0; i < 4096 * sync_period; i++) begin
            // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid);
            if (mode < 4) begin
                pipe_scb.get(dword);
                tmp_dat = undouble(dword);
            end
            else begin
                pipe_scb.get(tmp_dat);
            end
            orb_w.\type = DATA_WORD;
            orb_w.service = tmp_dat[11];
            orb_w.data = tmp_dat[10:1];
            if (mode < 4) begin         
                if (words_cnt == 0) begin
                    part = 1'b1;
                    orb_w.\type = SYCN_WORD;
                    if (dword[23:22] == 2'b10) begin
                        orb_w.valid = TRUE;
                    end 
                    else begin
                        orb_w.valid = FALSE;
                    end   
                end
            end
            else begin
                if (!words_cnt[0]) begin
                    part = 1'b1;
                    orb_w.\type = SYCN_WORD;
                    // 0111_1000_z100_1101 - 0x784D
                    if (words_cnt == 0 || words_cnt == 10 || words_cnt == 12 || words_cnt == 14 || words_cnt == 20 || words_cnt == 22 || words_cnt == 28) begin
                        if (!tmp_dat[11]) begin
                            orb_w.valid = TRUE;
                        end 
                        else begin
                            orb_w.valid = FALSE;
                        end  
                    end
                    if (words_cnt == 2 || words_cnt == 4 || words_cnt == 6 || words_cnt == 8 || words_cnt == 18 || words_cnt == 24 || words_cnt == 26 || words_cnt == 30) begin
                        if (tmp_dat[11]) begin
                            orb_w.valid = TRUE;
                        end 
                        else begin
                            orb_w.valid = FALSE;
                        end  
                    end
                end
            end

            if (words_cnt == (sync_period / 2)) begin
                part = 1'b0;    
                orb_w.\type = SERVICE_WORD;
                orb_w.service = tmp_dat[11];
                if (tmp_dat[11] == 1'bx) begin
                    orb_w.valid = FALSE;
                end
                else begin
                    orb_w.valid = TRUE;
                end
            end
            
            if (tmp_dat[0] == 1'bx) begin
                orb_w.parity = FALSE;
                orb_w.valid = FALSE;
            end
            else begin
                if ((tmp_dat[0] != ^tmp_dat[10:1])) begin
                    orb_w.parity = TRUE;
                end
                else begin
                    orb_w.parity = FALSE;
                end
            end
            cycle_scb.put(orb_w);
            words_cnt++;
            if (words_cnt == sync_period) begin
                words_cnt = 0;
            end
            // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid);
        end
    endtask: collect_cycle

    // Метод, запускающий конвертацию и проверку слов Орбиты
    task orbit_checker::collect;
        fork: collecting
            forever begin
                collect_cycle();
            end
            forever begin
                check_frame();
            end
        join_none
    endtask: collect
    
    // Метод, проверяющий служебную информацию в кадре Орбиты (в процессе разработки)
    task orbit_checker::check_frame;
        automatic orbitWord_s orb_w;
        automatic orbitService_s service_w;
        automatic int phrases_cnt = 0;
        automatic bit [6:0] group_number, groups_cnt = '0;
        automatic logic [7:0] group_label, trans_temp, transmitter_voltage;
        automatic logic [15:0] local_time_label;
        automatic logic [3:0] transmitter_power;
        for (int i = 0; i < 4 * 4096 * (sync_period / 2); i++) begin // 4096 кол-во фраз в цикле
            cycle_scb.get(orb_w);
            // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid);
            if (orb_w.valid == TRUE) begin
                if (orb_w.\type == SERVICE_WORD) begin
                    // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid); // Uncomment for debug
                    if (phrases_cnt >= 0 && phrases_cnt < 7) begin // формируется маркер номера группы
                        group_number = { group_number[5:0], orb_w.service };
                        if (phrases_cnt == 6) begin
                            if (group_number != groups_cnt) begin  // проверяется маркер номера группы
                                $display("@%0t: %m ERROR: Wrong Group Number in the frame. Presented Group Number is 7'b%b, expected Group Number is 7'b%b.", $time, group_number, groups_cnt);
                                $finish;
                            end
                            else begin
                                // $display("@%0t: %m Presented Group Number is 7'b%b.", $time, group_number);
                            end
                        end
                    end
                    
                    if (phrases_cnt == 7) begin  // проверяется маркер кадра
                        if (groups_cnt == '0) begin
                            if (orb_w.service != 1'b1) begin
                                $display("@%0t: %m ERROR: Wrong Frame Label. Presented Frame Label is 1'b%b, expected Frame Label is 1'b1.", $time, orb_w.service);
                                $finish;
                            end
                            else begin
                                $display("@%0t: %m Presented  Frame Label is 1'b%b.", $time, orb_w.service);
                            end
                        end
                        else begin
                            if (orb_w.service != 1'b0) begin
                                $display("@%0t: %m ERROR: Wrong Frame Label. Presented Frame Label is 1'b%b, expected Frame Label is 1'b0.", $time, orb_w.service);
                                $finish;
                            end
                            else begin
                                // $display("@%0t: %m Presented  Frame Label is 1'b%b.", $time, orb_w.service);
                            end
                        end                          
                    end
                    
                    if (phrases_cnt >= 8 && phrases_cnt < 24) begin // формируется МТВ
                        local_time_label = { local_time_label[14:0], orb_w.service };
                        if (phrases_cnt == 23) begin // проверяется МТВ
                            if (groups_cnt[4:0] == '0) begin
                                 $display("@%0t: %m Presented Local Timer Label is %d%d%d%d.", $time, local_time_label[15:12], local_time_label[11:8], local_time_label[7:4], local_time_label[3:0]);
                            end
                        end
                    end
                    
                    if (phrases_cnt >= 56 && phrases_cnt < 64) begin // формируется маркер группы
                        group_label = { group_label[6:0], orb_w.service };
                        if (phrases_cnt == 63) begin // проверяется маркер группы
                            if (groups_cnt[4:0] != '1) begin
                                if (group_label != 8'b01110010) begin
                                    $display("@%0t: %m ERROR: Wrong Group Label in the frame. Presented Group Label is 8'b%b, expected Group Label is 8'b01110010.", $time, group_label);
                                    $finish;
                                end
                                else begin
                                    // $display("@%0t: %m Presented Group Label is 8'b%b.", $time, group_label);
                                end
                            end
                            else begin
                                if (group_label != 8'b10001101) begin
                                    $display("@%0t: %m ERROR: Wrong Group Label in the frame. Presented Group Label is 8'b%b, expected Group Label is 8'b10001101.", $time, group_label);
                                    $finish;
                                end
                                else begin
                                    $display("@%0t: %m Presented Group Label is 8'b%b.", $time, group_label);
                                end
                            end
                        end
                    end
                    
                    if ((groups_cnt == 7'd2) && (phrases_cnt >= 36 && phrases_cnt < 40)) begin
                        transmitter_power = { transmitter_power[2:0], orb_w.service };
                        if (phrases_cnt == 39) begin 
                            $display("@%0t: %m Presented Transmitter Power is 4'h%h.", $time, transmitter_power);
                        end
                    end
                    if ((groups_cnt == 7'd3) && (phrases_cnt >= 16 && phrases_cnt < 24)) begin
                        trans_temp = { trans_temp[6:0], orb_w.service };
                        if (phrases_cnt == 23) begin 
                            $display("@%0t: %m Presented Transmitter Temperature is 8'h%h.", $time, trans_temp);
                        end
                    end
                    if ((groups_cnt == 7'd3) && (phrases_cnt >= 32 && phrases_cnt < 40)) begin
                        transmitter_voltage = { transmitter_voltage[6:0], orb_w.service };
                        if (phrases_cnt == 39) begin 
                            $display("@%0t: %m Presented Transmitter Voltage is 8'h%h.", $time, transmitter_voltage);
                        end
                    end
                    phrases_cnt++;
                    if (phrases_cnt == 64) begin
                        phrases_cnt = 0;
                        groups_cnt++;
                        // $display("@%0t: %m groups_cnt is 8'b%b.", $time, groups_cnt);
                    end
                end
                if (i == 4096 * (sync_period / 2) - 1 || i ==  2 * 4096 * (sync_period / 2) - 1 || i == 3 * 4096 * (sync_period / 2) - 1 || i == 4 * 4096 * (sync_period / 2) - 1) begin
                    cycle_cnt++;
                end
                check_selftest(orb_w.data);
            end
        end
        $display("@%0t: %m Frame was collected.", $time);
    endtask: check_frame
    
    task orbit_checker::check_counter(input logic [9:0] data);
        automatic bit [7:0] map_data;
        map_scb.get(map_data);
        // $display("@%0t: %m Data Field is 10'h%h.", $time, data);
        case (map_data)
            /*8'hFE: begin
                if (data != 8'hFE) begin
                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, 8'hFE);
                    $finish;
                end
            end
            8'hFF: begin
                  $display("@%0t: %m PC2 Data Field is 10'h%h.", $time, data);
            end*/
            /*8'h0A: begin
                  $display("@%0t: %m SC Data Field is 10'h%h.", $time, data);
            end
            8'h0B: begin
                  $display("@%0t: %m UC (UART) Data Field is 10'h%h.", $time, data);
            end*/
            /* 8'h05: begin
                  // $display("@%0t: %m Data Field is 10'h%h.", $time, data);  
                  anp_chk[0].check(data);
            end */
            /*8'h06: begin
                  // $display("@%0t: %m Data Field is 10'h%h.", $time, data);  
                  anp_chk[1].check(data);
            end*/
            default: begin
                if (data != checking_cnt) begin
                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, checking_cnt);
                    $finish;
                end
                else begin
                    if (!mute) begin
                        $display("@%0t: %m Data Field is 10'h%h.", $time, data);
                    end
                end
                
            end
        endcase
        checking_cnt++;
    endtask: check_counter
    
    task orbit_checker::check_selftest(input logic [9:0] data);
        automatic bit [7:0] map_data;
        automatic logic [9:0] mem_data;
        map_scb.get(map_data);
        mem_queue.push_front(data);
        // $display("@%0t: %m Data Field is 10'h%h.", $time, data);
        if (map_data == 8'hA0 || map_data == 8'hA1) begin // MK0
            if (data != mil0_cnt) begin
                $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Mil0 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mil0_cnt);
                $finish;
            end
            mil0_cnt++;
        end
        else begin
            if (map_data == 8'hA2 || map_data == 8'hA3) begin // MK1
                if (data != mil1_cnt) begin
                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Mil1 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mil1_cnt);
                    $finish;
                end
                mil1_cnt++;
            end
            else begin
                if (map_data == 8'hA4 || map_data == 8'hA5) begin // MK1
                    if (data != mil2_cnt) begin
                        $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Mil2 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mil2_cnt);
                        $finish;
                    end
                    mil2_cnt++;
                end
                else begin
                    if (map_data == 8'hB0 || map_data == 8'hB2) begin // PC0
                        // $display("@%0t: %m Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, pc0_cnt);
                        if (data != pc0_cnt) begin
                            $display("@%0t: %m ERROR: Wrong Data Field in the Orbit PC0 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, pc0_cnt);
                            $finish;
                        end
                        pc0_cnt++;
                    end
                    else begin
                        if (map_data == 8'hB1 || map_data == 8'hB3) begin // PC1
                            if (data != pc1_cnt) begin
                                $display("@%0t: %m ERROR: Wrong Data Field in the Orbit PC1 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, pc1_cnt);
                                $finish;
                            end
                            pc1_cnt++;
                        end
                        else begin
                            if (map_data == 8'h10) begin // Calib0
                                if (cycle_cnt == 2'd0 || cycle_cnt == 2'd2) begin
                                    calib_ch0 = CALIB_CH0_LOW;
                                end
                                else begin
                                    calib_ch0 = CALIB_CH0_HIGH;
                                end
                                // $display("@%0t: %m Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data[9:1], calib_ch0);
                                if (data[9:1] != calib_ch0) begin
                                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Calib_ch0 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, calib_ch0);
                                    $display("@%0t: %m ERROR: 10'h%h", $time, cycle_cnt);
                                    $finish;
                                end
                            end
                            else begin
                                if (map_data == 8'hC0) begin // MEM
                                    if (mem_cnt < (sync_period * 2048 * 2)) begin
                                        if (data != '0) begin
                                            $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Mem word (Cycles 1, 2). Presented Data Field is 10'h%h, expected Data Field is 10'h%h. mem_cnt = %d", $time, data, '0, mem_cnt);
                                            $finish;
                                        end
                                    end
                                    else begin
                                        mem_data = mem_queue.pop_back();
                                        // $display("@%0t: %m Presented Data Field is 10'h%h, expected Data Field is 10'h%h. mem_cnt = %d", $time, data, mem_data, mem_cnt);
                                        if (data != mem_data) begin
                                            $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Mem word (Cycles 3, 4). Presented Data Field is 10'h%h, expected Data Field is 10'h%h. mem_cnt = %d", $time, data, mem_data, mem_cnt);
                                            $finish;
                                        end 
                                    end
                                end
                                else begin
                                    if (map_data >= 8'h80 && map_data <= 8'h8F) begin
                                        if (data != mem_map[map_data] + anpb_cnt) begin
                                            $display("@%0t: %m ERROR: Wrong Data Field in the Orbit AnpB word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mem_map[map_data] + anpb_cnt);
                                            $finish;
                                        end
                                    end
                                    else begin
                                        if (map_data >= 8'h11 && map_data <= 8'h2F) begin
                                            if (data != mem_map[map_data] + anp1_cnt) begin
                                                $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Anp1 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mem_map[map_data] + anp1_cnt);
                                                $finish;
                                            end
                                        end
                                        else begin
                                            if (map_data >= 8'h40 && map_data <= 8'h47) begin
                                                if (data != mem_map[map_data] + anp1b_cnt) begin
                                                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Anp1b word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mem_map[map_data] + anp1b_cnt);
                                                    $finish;
                                                end
                                            end
                                            else begin
                                                if (map_data == 8'h50) begin // Calib0
                                                    if (cycle_cnt == 2'd0 || cycle_cnt == 2'd2) begin
                                                        calib_ch1 = CALIB_CH1_ZERO;
                                                    end
                                                    else begin
                                                        if (cycle_cnt == 2'd1) begin
                                                            calib_ch1 = CALIB_CH1_LOW;
                                                        end
                                                        else begin
                                                            calib_ch1 = CALIB_CH1_HIGH;
                                                        end
                                                    end
                                                
                                                    if (data[9:1] != calib_ch1) begin
                                                        $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Calib_ch1 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, calib_ch1);
                                                        $finish;
                                                    end
                                                end
                                                else begin
                                                    if (map_data >= 8'h51 && map_data <= 8'h57) begin
                                                        if (data != mem_map[map_data] + anp2_cnt) begin
                                                            $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Anp2 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mem_map[map_data] + anp2_cnt);
                                                            $finish;
                                                        end
                                                    end                                        
                                                    else begin
                                                        if (map_data >= 8'h60 && map_data <= 8'h6F) begin
                                                            if (data != mem_map[map_data] + anp3_cnt) begin
                                                                $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Anp3 word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, mem_map[map_data] + anp3_cnt);
                                                                $finish;
                                                            end
                                                        end                                        
                                                        else begin
                                                            if (map_data == 8'hB8) begin // RS
                                                                if (data != rs_cnt) begin
                                                                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit RS word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, rs_cnt);
                                                                    $finish;
                                                                end
                                                                rs_cnt++;
                                                            end
                                                            else begin
                                                                if (map_data == 8'hB4) begin // PSK
                                                                    if (data != sc_cnt) begin
                                                                        $display("@%0t: %m ERROR: Wrong Data Field in the Orbit Serial word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, sc_cnt);
                                                                        $finish;
                                                                    end
                                                                    sc_cnt++;
                                                                end
                                                                else begin
                                                                    $display("@%0t: %m ERROR: Unknown! Map data = 8'h%h", $time, map_data);
                                                                    $finish;
                                                                end
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        mem_cnt++;
    endtask: check_selftest
`endif