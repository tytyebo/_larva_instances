/*
 * Класс rs232_trx - передатчик RS-232.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef RS232_TRX_INCLUDE
    `define RS232_TRX_INCLUDE
    `include "defines.svh"
    import classes_pkg::vRs232Test_t;
    class rs232_trx#(parameter int BAUD_RATE = 57600); 
        vRs232Test_t rs232;
        local int delay = (1_000_000 / BAUD_RATE) * 1_000; // с учетом timescale 1 ns

        // Конструктор класса rs232
        extern function new(input vRs232Test_t rs232);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод для отправки байта по RS-232
        extern task send_byte(bit [7:0] data);
    endclass: rs232_trx
    
    function rs232_trx::new(input vRs232Test_t rs232);
        this.rs232 = rs232;
    endfunction
    
    task rs232_trx::init;
        rs232.rx = 1'b1;
    endtask: init
    
    task rs232_trx::send_byte(bit [7:0] data);
        rs232.rx = 1'b0; // start bit
        #delay;
        for (int i = 0; i < $bits(data); i++) begin
            rs232.rx = data[i];
            #delay;    
        end
        rs232.rx = 1'b1; // stop bit
        #delay;
    endtask: send_byte
`endif