/*
 * Класс wb2qspi_cmdr для работы с DUT ft75_main.
 * Предоставляет доступ к ft75_main по шине Wishbone транзитом через qspi.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef WB2QSPI_CMDR_INCLUDE
    `define WB2QSPI_CMDR_INCLUDE
    `include "defines.svh"
    import classes_pkg::vQspiTestM_t;
    import classes_pkg::wbCmd_s;
    import classes_pkg::driver_t;
    import classes_pkg::qspiPacket_s;
    import classes_pkg::qspiAnswer_s;
    import classes_pkg::qspiControl_s;
    class wb2qspi_cmdr; 
        static int qspi_sck_period = 320;
        driver_t drv;
        qspiPacket_s qspi_p;
        // Конструктор класса, создающий внутри себя драйвер uart для доступа к ft75_main
        extern function new(input vQspiTestM_t intfc_in);   
        // Сброс в начальное состояние
        extern task init();  
        
        extern local function bit check_flags(qspiControl_s control);
        // Метод записи по шине Wb
        extern task wb_wr_cmd(input wbCmd_s cmd); 
        // Метод чтения по шине Wb        
        extern task wb_rd_cmd(input wbCmd_s cmd, output logic [31:0] rd_data);
        // Проверка правильности подключения по Qspi, чтение и запись тестового регистра на шине Wishbone
        extern task check_connection();
        // Проверка соединения по внешней шине 
        extern task check_extbus();
        // Проверка соединения по внешней шине цифрового модуля
        extern task check_extbus_digital();
        // Запуск модуля main
        extern task run_orbit(input bit [3:0] mode, bit record_ena, bit [7:0] record_time);
        // Чтение и вывод на экран CRC цикла Орбиты
        extern task read_cycle_crc();
        // Команда "Стоп"
        extern task stop();
        // Команда "Сброс"
        extern task reset();
        // Инициализация таймеров команд КОМ1, КОМ2, КОМ3
        extern task init_commands_timers();
        // Запуск самотестирования в выбранном режиме
        extern task selftest(input bit [3:0] mode);
    endclass: wb2qspi_cmdr
    
    // Конструктор класса, создающий внутри себя драйвер uart для доступа к larva_main
    function wb2qspi_cmdr::new(input vQspiTestM_t intfc_in);
        drv = new(intfc_in);
    endfunction
       
    // Сброс в начальное состояние
    task wb2qspi_cmdr::init();
        drv.period = qspi_sck_period;
        drv.init();
    endtask: init
    // Метод записи по шине Wb
    task wb2qspi_cmdr::wb_wr_cmd(input wbCmd_s cmd);
        qspi_p.adr = cmd.adr;
        qspi_p.data = cmd.data;
        qspi_p.cmd = drv.CMD_WR;
        drv.write(qspi_p);
    endtask: wb_wr_cmd
    // Метод чтения по шине Wb    
    task automatic wb2qspi_cmdr::wb_rd_cmd(input wbCmd_s cmd, output logic [31:0] rd_data);
        automatic qspiAnswer_s qspi_a;
        automatic logic [3:0] csum = 0;
        qspi_p.adr = cmd.adr;
        qspi_p.data = cmd.data;
        qspi_p.cmd = drv.CMD_RD;
        drv.read(qspi_p, qspi_a);
        if (check_flags(qspi_a.control)) begin
            $display("@%0t: %m There is trying to repeat reading", $time);
            qspi_p.cmd = drv.CMD_RD_REP;
            drv.read(qspi_p, qspi_a);
            if (check_flags(qspi_a.control)) begin
                $finish;
            end
        end
        for (int i = 0; i < 32; i = i + 4) begin
            csum = csum + {qspi_a.data[i + 3], qspi_a.data[i + 2], qspi_a.data[i + 1], qspi_a.data[i]};
        end
        // $display("@%0t: %m Calculated csum %h, receivedcsum %h.", $time, csum, qspi_a.control.csum);
        if (csum != qspi_a.control.csum) begin
            $display("@%0t: %m Control sum fields isn't same.", $time);
            $finish;
        end
        rd_data = qspi_a.data;
    endtask: wb_rd_cmd
    
    function bit wb2qspi_cmdr::check_flags(qspiControl_s control);
        check_flags = 1'b0;
        if (control.fl_wlost) begin
            $display("@%0t: %m \"fl_wlost\" is appeared.", $time);
            $finish;
        end
        if (control.fl_rlost) begin
            $display("@%0t: %m \"fl_rlost\" is appeared.", $time);
            $finish;
        end
        if (control.fl_nodata) begin
            $display("@%0t: %m \"fl_nodata\" is appeared", $time);
            check_flags = 1'b1;
        end
    endfunction: check_flags
    
    // Проверка правильности подключения по Qspi, чтение и запись тестового регистра на шине Wishbone
    task automatic wb2qspi_cmdr::check_connection;
        automatic wbCmd_s wb_cmd = '{adr:16'hF403, data:32'h789ABCDE};
        automatic logic [31:0] wb_rd_data;
        wb_wr_cmd(wb_cmd);
        wb_rd_cmd(wb_cmd, wb_rd_data);
        if (wb_rd_data == 32'h789ABCDE) begin
            $display("@%0t: %m Qspi connection is OK", $time);
        end
        else begin
            $display("@%0t: %m There is a problem in the Qspi connection", $time);
        end
    endtask: check_connection
    // Проверка соединения по внешней шине 
    task automatic wb2qspi_cmdr::check_extbus();
        automatic wbCmd_s cmd = '{adr:16'd0, data:32'd0};
        automatic logic [31:0] data = 0;
        wb_rd_cmd(cmd, data);
        if (data != 16'h55AA) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd1, data:32'd0};
        wb_rd_cmd(cmd, data);
        if (data != 16'hAA55) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd2, data:$random()};
        wb_wr_cmd(cmd);
        wb_rd_cmd(cmd, data);
        if (data != cmd.data[15:0]) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        
        cmd = '{adr:16'h60, data:32'd0};
        wb_rd_cmd(cmd, data);
        if (data != 16'h55AA) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'h61, data:32'd0};
        wb_rd_cmd(cmd, data);
        if (data != 16'hAA55) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'h62, data:$random()};
        wb_wr_cmd(cmd);
        wb_rd_cmd(cmd, data);
        if (data != cmd.data[15:0]) begin
            $display("@%0t: %m ERROR: The Extbus is wrong.", $time);
            $finish;
        end
        $display("@%0t: %m The Extbus is OK.", $time);
    endtask: check_extbus
    
    task automatic wb2qspi_cmdr::run_orbit(input bit [3:0] mode, bit record_ena, bit [7:0] record_time);
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        cmd = '{adr:16'h1000, data:{record_time, record_ena, mode}};
        wb_wr_cmd(cmd);
        cmd = '{adr:16'h1001, data:{2'd1}};
        wb_wr_cmd(cmd);
        $display("@%0t: %m The Main was started.", $time);
    endtask: run_orbit
    // Проверка соединения по внешней шине цифрового модуля
    task wb2qspi_cmdr::check_extbus_digital();
        automatic wbCmd_s cmd = '{adr:16'd96, data:32'd0};
        automatic logic [31:0] data = 0;
        automatic int len = 0;
        wb_rd_cmd(cmd, data);
        if (data != 16'h55AA) begin
            $display("@%0t: %m ERROR: The Digital Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd97, data:32'd0};
        wb_rd_cmd(cmd, data);
        if (data != 16'hAA55) begin
            $display("@%0t: %m ERROR: The Digital Extbus is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd98, data:$random()};
        wb_wr_cmd(cmd);
        wb_rd_cmd(cmd, data);
        if (data != cmd.data[15:0]) begin
            $display("@%0t: %m ERROR: The Digital Extbus is wrong.", $time);
            $finish;
        end
        $display("@%0t: %m The Digital Extbus is OK.", $time);
    endtask: check_extbus_digital

    // Инициализация таймеров КОМ1, КОМ2, КОМ3
    task wb2qspi_cmdr::init_commands_timers();
        automatic wbCmd_s cmd = '{adr:16'hB0, data:{15'd0, 1'd1, 15'd0, 1'd1}};
        automatic logic [31:0] data = 0;
        automatic int len = 0; 
        wb_wr_cmd(cmd);
        cmd.adr = 16'hB1;
        wb_wr_cmd(cmd);
        cmd.adr = 16'hB2;    
        wb_wr_cmd(cmd);
        cmd.adr = 16'hB3;    
        wb_wr_cmd(cmd);
        cmd.adr = 16'hB4;    
        wb_wr_cmd(cmd);
        cmd.adr = 16'hB5;    
        wb_wr_cmd(cmd);     
        $display("@%0t: %m The Relay Timers are initialized.", $time);
    endtask: init_commands_timers
    
    task automatic wb2qspi_cmdr::read_cycle_crc();
        automatic logic [31:0] data = 0, num = 0;
        automatic wbCmd_s cmd;
        fork
            forever begin
                #252_000_000
                cmd = '{adr:16'he004, data:'0};
                wb_rd_cmd(cmd, data);
                cmd = '{adr:16'he005, data:'0};
                wb_rd_cmd(cmd, num);
                $display("@%0t: %m The CRC is 16'h%h. The cycle number is %0d", $time, data, num);
            end
        join_none
    endtask
    
    task automatic wb2qspi_cmdr::stop();
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        cmd = '{adr:16'h1001, data:{2'd2}};
        wb_wr_cmd(cmd);
        $display("@%0t: %m The Main was stopped.", $time);
    endtask
    
    task automatic wb2qspi_cmdr::reset();
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        cmd = '{adr: 16'h1001, data: {2'd3}};
        wb_wr_cmd(cmd);
        $display("@%0t: %m The Main was resetted.", $time);
    endtask
    
    task automatic wb2qspi_cmdr::selftest(input bit [3:0] mode);
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        automatic bit [13:0] telem_config;
        automatic bit [7:0] mil_config;
        automatic bit [2:0] pc_sc_uart_config;
        automatic bit [19:0] service_config0;
        automatic bit [3:0] service_config1;
        automatic bit [2:0] telem_control;
        automatic bit [1:0] analog_config;
        telem_config = {10'd0, mode};
        mil_config = {2'd3, 6'd0};
        pc_sc_uart_config = {2'd3, 1'b0};
        service_config0 = {4'd1, 4'd2, 4'd3, 4'd4, 4'd5};
        analog_config = 2'd3;
        case (mode)
            4'd1: begin
                service_config1 = {2'd0, 2'd3};
            end
            4'd3: begin
                service_config1 = {2'd0, 2'd3};
            end
            default: begin
                service_config1 = '0;
            end
        endcase
        telem_control = 3'b101;
        cmd = '{adr: 16'h1000, data: telem_config};
        wb_wr_cmd(cmd);
        
        cmd = '{adr: 16'h2000, data: mil_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h3000, data: mil_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h4000, data: mil_config};
        wb_wr_cmd(cmd);
        
        cmd = '{adr: 16'h0071, data: {6'd2, 8'd15}};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h0070, data: {2'd0, 1'b1}};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h0078, data: pc_sc_uart_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h0080, data: pc_sc_uart_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h0090, data: pc_sc_uart_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'hA000, data: service_config0};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'hA001, data: service_config1};
        wb_wr_cmd(cmd);
        
        cmd = '{adr: `ANP0_BASE_ADR, data: analog_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: `ANP1_BASE_ADR, data: analog_config};
        wb_wr_cmd(cmd);
        cmd = '{adr: 16'h1001, data: telem_control};
        wb_wr_cmd(cmd);
        $display("@%0t: %m Selftest was started.", $time);
    endtask
`endif