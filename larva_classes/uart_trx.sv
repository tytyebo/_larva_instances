/*
 * Класс uart_trx для работы с DUT larva_main.
 * Предоставляет доступ к larva_main через канал uart. Подключается к testbench двумя интерфейсами Handshake Valid/Ready.
 * Содержит архитектуру пакетов симметричную реализованной в larva_uart_master.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef UART_TRX_INCLUDE
    `define UART_TRX_INCLUDE
    import classes_pkg::vHdshkTestM_t;
    import classes_pkg::vHdshkTestS_t;
    import classes_pkg::uartPacket_s;
    class uart_trx;
        localparam bit [7:0] CMD_WR_WB_1B = 8'h10;
        localparam bit [7:0] CMD_RD_WB_1B = 8'h11;
        localparam bit [7:0] CMD_WR_WB_2B = 8'h12;
        localparam bit [7:0] CMD_RD_WB_2B = 8'h13;
        localparam bit [7:0] CMD_WR_WB_4B = 8'h16;
        localparam bit [7:0] CMD_RD_WB_4B = 8'h17;
        parameter int TIMEOUT = 1000000000;
        vHdshkTestM_t hdshk_in;
        vHdshkTestS_t hdshk_out;
        uartPacket_s scb[$]; // scoreboard
        
        function new(input vHdshkTestM_t hdshk_in, vHdshkTestS_t hdshk_out);
            this.hdshk_in = hdshk_in;
            this.hdshk_out = hdshk_out;
        endfunction
        
        // Определение длины пакета для отправки
        local function int get_wr_len(input bit [7:0] cmd);
            case (cmd)
                CMD_WR_WB_1B: begin
                    return 4;
                end
                CMD_RD_WB_1B: begin
                    return 3;
                end
                CMD_WR_WB_2B: begin
                    return 5;
                end
                CMD_RD_WB_2B: begin
                    return 3;
                end
                CMD_WR_WB_4B: begin
                    return 7;
                end
                CMD_RD_WB_4B: begin
                    return 3;
                end
                default: begin
                    return 0;
                end
            endcase
        endfunction: get_wr_len
        
        // Определение кол-ва байт для чтения
        local function int get_rd_len(input bit [7:0] cmd);
            case (cmd)
                CMD_RD_WB_1B: begin
                    return 1;
                end
                CMD_RD_WB_2B: begin
                    return 2;
                end
                CMD_RD_WB_4B: begin
                    return 4;
                end
                default: begin
                    return 0;
                end
            endcase
        endfunction: get_rd_len
        
        task init;
            hdshk_in.cbm.data <= '0;
            hdshk_in.cbm.valid <= 1'b0;
        endtask: init
        
        // Отправка 1 байта 
        task send_byte(input bit [7:0] data);
            wait (!hdshk_in.ready);
            hdshk_in.cbm.data <= data;
            hdshk_in.cbm.valid <= 1'b1;
            wait (hdshk_in.ready);
            @hdshk_in.cbm;
            hdshk_in.cbm.valid <= 1'b0;
        endtask: send_byte
        
        // Прием 1 байта 
        task receive_byte(output logic [7:0] data);
            wait (hdshk_out.valid);
            data = hdshk_out.data;
            hdshk_out.cbs.ready <= 1'b1;
            @hdshk_out.cbs;
            hdshk_out.cbs.ready <= 1'b0;
        endtask: receive_byte
        
        // Последовательная отправка b2s байт массива data
        task automatic send(uartPacket_s data);
            automatic bit [6:0] [7:0] arr = data;
            automatic int b2s = get_wr_len(data.cmd);
            // Проверка, что пакет чтения. Только их добавляем в очередь
            if (b2s == 3) begin
                scb.push_front(data);
            end
            for (int i = 0; i < b2s; i++) begin
                // $display("arr[%d] = %d", i, arr[i]); // debug output
                send_byte(arr[i]);
            end
            $display("@%0t: %m %0d bytes were sent.", $time, b2s);
        endtask: send
        
        // Прием b2r байт в массив data.
        // Прерывание приема по TIMEOUT. TIMEOUT настраиваемое поле-параметр класса
        task automatic receive(output logic [31:0] data, int b2r); // p.229
            automatic uartPacket_s packet = scb.pop_back;
            automatic int btr = get_rd_len(packet.cmd);
            automatic logic [3:0] [7:0] arr = 0;
            b2r = 0;
            fork: timeout_block 
                begin
                    for (b2r = 0; b2r < btr; b2r++) begin
                        receive_byte(arr[b2r]);
                        // $display("arr[%d] = %d", b2r, arr[b2r]); // debug output
                    end
                    data = arr;
                    $display("@%0t: %m %0d bytes were received.", $time, b2r);
                end
                begin
                    #TIMEOUT;
                    $display("@%0t: %m ERROR: Timeout. %0d bytes were received.", $time, b2r);
                end
            join_any
            disable timeout_block;
        endtask: receive
        
        // Проверка подключения по 
        task automatic check_connection;
            automatic logic [7:0] data;
            automatic bit err = 0;
            fork
                begin
                    send_byte(8'h00);
                    send_byte(8'h01);
                end
                begin
                    receive_byte(data);
                    if (data != 8'hA5) begin
                        $display("@%0t: %m ERROR: There is connection error in the first byte.", $time);
                        err = 1'b1;
                    end
                    receive_byte(data);
                    if (data != 8'h5A) begin
                        $display("@%0t: %m ERROR: There is connection error in the second byte.", $time);
                        err = 1'b1;
                    end
                end
            join
            if (!err) begin
                $display("@%0t: %m Connection test is OK!", $time);
            end
            else begin
                $finish;
            end
        endtask: check_connection
    endclass: uart_trx
`endif