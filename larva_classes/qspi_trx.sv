/*
 * Класс qspi_trx для работы с DUT larva_main.
 * Предоставляет доступ к larva_main через канал qspi. Подключается к testbench двумя интерфейсом qspi_io.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef QSPI_INCLUDE
    `define QSPI_INCLUDE
    import classes_pkg::vQspiTestM_t;
    import classes_pkg::qspiPacket_s;
    import classes_pkg::qspiAnswer_s;
    class qspi_trx;
        static int dummy_length = 10;
        static int period = 320;
        parameter bit [7:0] CMD_RD = 8'h00,
                            CMD_RD_REP = 8'h10,
                            CMD_WR = 8'h20;
        qspiPacket_s scb[$]; // scoreboard for future use
        vQspiTestM_t qspi;
        // Конструктор класса qspi_trx, реализующий связь с проектом larva_main через qspi_io
        extern function new(input vQspiTestM_t qspi);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод записи по qspi
        extern task write(input qspiPacket_s data);
        // Метод чтения по qspi    
        extern task read(input qspiPacket_s data, output qspiAnswer_s answer);
    endclass: qspi_trx
    
    function qspi_trx::new(input vQspiTestM_t qspi);
        this.qspi = qspi;
    endfunction
    
    task qspi_trx::init;
        qspi.cbm.ncs <= 1'b1;
        qspi.cbm.sck <= 1'b0;
        qspi.cbm.dir <= 1'b0;
        qspi.cbm.io_mosi <= '0;
    endtask: init
    
    task qspi_trx::write(input qspiPacket_s data);
        wait (qspi.ncs);
        fork
            begin: clock_generate
                forever begin
                    qspi.cbm.sck <= 1'b0;
                    #(period / 2);
                    qspi.cbm.sck <= 1'b1;
                    #(period / 2);
                end
            end
            begin
                @(negedge qspi.cbm.sck);
                qspi.cbm.ncs <= 1'b0;
                qspi.cbm.dir <= 1'b1;
                for (int i = $bits(data) - 1; i > 0; i = i - 4) begin
                    qspi.cbm.io_mosi[3] <= data[i];
                    qspi.cbm.io_mosi[2] <= data[i - 1];
                    qspi.cbm.io_mosi[1] <= data[i - 2];
                    qspi.cbm.io_mosi[0] <= data[i - 3];
                    @(negedge qspi.cbm.sck);
                end
                qspi.cbm.ncs <= 1'b1;
                qspi.cbm.dir <= 1'b0;
            end
        join_any
        disable clock_generate;
    endtask: write
    
    task qspi_trx::read(input qspiPacket_s data, output qspiAnswer_s answer);
        wait (qspi.ncs);
        fork
            begin: clock_generate
                forever begin
                    qspi.cbm.sck <= 1'b0;
                    #(period / 2);
                    qspi.cbm.sck <= 1'b1;
                    #(period / 2);
                end
            end
            begin
                @(negedge qspi.cbm.sck);
                qspi.cbm.ncs <= 1'b0;
                qspi.cbm.dir <= 1'b1;
                for (int i = $bits(data) - 1; i > $bits(data) - $bits(data.adr) - $bits(data.cmd); i = i - 4) begin
                    qspi.cbm.io_mosi[3] <= data[i];
                    qspi.cbm.io_mosi[2] <= data[i - 1];
                    qspi.cbm.io_mosi[1] <= data[i - 2];
                    qspi.cbm.io_mosi[0] <= data[i - 3];
                    @(negedge qspi.cbm.sck);
                end
                qspi.cbm.dir <= 1'b0;
                for (int i = 0; i < dummy_length; i++) begin
                    qspi.cbm.io_mosi <= '0;
                    @(negedge qspi.cbm.sck);
                end
                for (int i = $bits(answer) - 1; i > 0; i = i - 4) begin
                    @(posedge qspi.cbm.sck);
                    answer[i] = qspi.io_miso[3];
                    answer[i - 1] = qspi.io_miso[2];
                    answer[i - 2] = qspi.io_miso[1];
                    answer[i - 3] = qspi.io_miso[0];
                end
                @(negedge qspi.cbm.sck);
                qspi.cbm.ncs <= 1'b1;
            end
        join_any
        disable clock_generate;
    endtask: read
`endif