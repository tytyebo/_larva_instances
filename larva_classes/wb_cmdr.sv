/*
 * Класс wb_cmdr для работы с DUT larva_main.
 * Предоставляет доступ к larva_main по шине Wishbone транзитом через uart.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef WB_CMDR_INCLUDE
    `define WB_CMDR_INCLUDE
    `include "defines.svh"
    import classes_pkg::vHdshkTestM_t;
    import classes_pkg::vHdshkTestS_t;
    import classes_pkg::wbCmd_s;
    import classes_pkg::driver_t;
    import classes_pkg::uartPacket_s;
    class wb_cmdr; 
        driver_t drv;
        uartPacket_s wr_data;
        int wr_len;
        
        // Конструктор класса, создающий внутри себя драйвер uart для доступа к larva_main
        extern function new(input vHdshkTestM_t intfc_in, vHdshkTestS_t intfc_out);
        // Преобразует структуру wbCmd_s пакета шины Wishbone в пакет для передачи по uart
        extern function void unpack(input wbCmd_s cmd);   
        // Сброс в начальное состояние
        extern task init();       
        // Метод записи по шине Wb
        extern task wb_wr_cmd(input wbCmd_s cmd); 
        // Метод чтения по шине Wb        
        extern task wb_rd_cmd(input wbCmd_s cmd, output logic [31:0] rd_data, int rd_len);
        // Проверка правильности подключения по uart, чтение и запись тестовых регистров uart
        extern task check_connection();
        // Проверка соединения по внешней шине 
        extern task check_extbus();
        // Инициализация эмитатора флэш памяти
        extern task init_map();
        // Запуск модуля main
        extern task run_orbit(input bit [3:0] mode);
    endclass: wb_cmdr
    
    // Конструктор класса, создающий внутри себя драйвер uart для доступа к larva_main
    function wb_cmdr::new(input vHdshkTestM_t intfc_in, vHdshkTestS_t intfc_out);
        drv = new(intfc_in, intfc_out);
        wr_len = 0;
    endfunction
    
    // Преобразует структуру wbCmd_s пакета шины Wishbone в пакет для передачи по uart
    function void wb_cmdr::unpack(input wbCmd_s cmd);
        automatic int i = 0;
        wr_data.adr = cmd.adr;
        wr_len = 3;
        case (cmd.sel) 
            4'h1: begin
                wr_data.cmd = drv.CMD_WR_WB_1B;
                wr_data.data = cmd.data[7:0];
                wr_len++;
            end
            4'h3: begin
                wr_data.cmd = drv.CMD_WR_WB_2B;
                wr_data.data = cmd.data[15:0];
                wr_len += 2;
            end
            4'hf: begin
                wr_data.cmd = drv.CMD_WR_WB_4B;
                wr_data.data = cmd.data[31:0];
                wr_len += 4;
            end
            default: begin
                wr_len = 0;
                $display("@%0t: %m ERROR: Wrong Wishbone command.", $time);
                $finish;
            end
        endcase
    endfunction: unpack
    
    // Сброс в начальное состояние
    task wb_cmdr::init();
        drv.init();
    endtask: init
    // Метод записи по шине Wb
    task wb_cmdr::wb_wr_cmd(input wbCmd_s cmd);
        unpack(cmd);
        drv.send(wr_data);
    endtask: wb_wr_cmd
    // Метод чтения по шине Wb    
    task wb_cmdr::wb_rd_cmd(input wbCmd_s cmd, output logic [31:0] rd_data, int rd_len);
        fork
            begin
                unpack(cmd);
                wr_data.cmd++;
                drv.send(wr_data);
            end
            drv.receive(rd_data, rd_len);
       join
    endtask: wb_rd_cmd
    // Проверка правильности подключения по uart, чтение и запись тестовых регистров uart
    task wb_cmdr::check_connection();
        drv.check_connection();
    endtask: check_connection
    // Проверка соединения по внешней шине 
    task wb_cmdr::check_extbus();
        automatic wbCmd_s cmd = '{adr:16'd0, data:32'd0 , sel:4'h3};
        automatic logic [31:0] data = 0;
        automatic int len = 0;
        wb_rd_cmd(cmd, data, len);
        if (data != 16'h55AA) begin
            $display("@%0t: %m ERROR: Extbus checking is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd1, data:32'd0 , sel:4'h3};
        wb_rd_cmd(cmd, data, len);
        if (data != 16'hAA55) begin
            $display("@%0t: %m ERROR: Extbus checking is wrong.", $time);
            $finish;
        end
        cmd = '{adr:16'd2, data:$random() , sel:4'hf};
        wb_wr_cmd(cmd);
        wb_rd_cmd(cmd, data, len);
        if (data != cmd.data[15:0]) begin
            $display("@%0t: %m ERROR: Extbus checking is wrong.", $time);
            $finish;
        end
        $display("@%0t: %m Extbus checking is OK.", $time);
    endtask: check_extbus
    
    task wb_cmdr::init_map();
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        automatic int len = 0;
        /*cmd = '{adr:16'h6000, data:32'hAA_000100 , sel:4'hf};
        wb_wr_cmd(cmd);
        cmd = '{adr:16'h6000, data:32'hBB_000101 , sel:4'hf};
        wb_wr_cmd(cmd);
        cmd = '{adr:16'h6001, data:32'h00_000100 , sel:4'hf};
        wb_wr_cmd(cmd);*/
        /*cmd = '{adr:16'h6000, data:32'h00_000000 , sel:4'h1};
        wb_rd_cmd(cmd, data, len);
        $display("data = %0d", data);
        if (data != 8'hFF) begin
            $display("@%0t: %m ERROR: Map checking is wrong.", $time);
            $finish;
        end
        wb_rd_cmd(cmd, data, len);
        $display("data = %0d", data);
        if (data != 8'hFF) begin
            $display("@%0t: %m ERROR: Map checking is wrong.", $time);
            $finish;
        end*/
        $display("@%0t: %m Map initialization is complete.", $time);
    endtask: init_map
    
    task wb_cmdr::run_orbit(input bit [3:0] mode);
        automatic wbCmd_s cmd;
        automatic logic [31:0] data = 0;
        automatic int len = 0;
        cmd = '{adr:16'h1000, data:{ 27'd0, mode, 1'b1 } , sel:4'hf};
        wb_wr_cmd(cmd);
        $display("@%0t: %m Main was started.", $time);
    endtask: run_orbit
`endif