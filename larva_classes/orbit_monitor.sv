/*
 * Класс orbit_monitor для работы с DUT larva_main.
 * Считывает и формирует в слова информацию, полученную по Орбите. Подключается к testbench интерфейсом orbit.test.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef ORBI_MONITOR_INCLUDE
    `define ORBI_MONITOR_INCLUDE
    import classes_pkg::orbitWord_s;
    import classes_pkg::vOrbitTest_t;
    import defines_pkg::bool_e;
    import defines_pkg::TRUE;
    class orbit_monitor;
        vOrbitTest_t orbit;
        bit synced;
        bit [3:0] mode;
        mailbox pipe_scb;
        logic [23:0] pipe;
        logic [383:0] m16_sync_pipe;
        bool_e mute;
        // Конструктор класса. Содержит указатель на интерфейс Орбиты orbit и mailbox для хранения удвоенных слов Орбиты pipe_scb.
        extern function new(input vOrbitTest_t orbit, mailbox mbx, bit [3:0] mode);     
        // Сброс в начальное состояние
        extern task init;
        // Запуск сбора данных от Орбиты
        extern task run;
        // Метод, осуществляющий синхронизацию по первому синхроимпульсу в потоке Орбиты.
        // Работает для информативностей М01, М02, М04, М08
        extern task sync;
        // Метод, осуществляющий формирования удвоенного слова Орбиты, сохраненине его в mailbox pipe_scb
        extern task collect_word;
        // Метод, осуществляющий формирования потока из удвоенных слов Орбиты, сохранение их в mailbox pipe_scb
        extern task collect;
    endclass: orbit_monitor
    
    // Конструктор класса. Содержит указатель на интерфейс Орбиты orbit и mailbox для хранения удвоенных слов Орбиты pipe_scb.
    function orbit_monitor::new(input vOrbitTest_t orbit, mailbox mbx, bit [3:0] mode);
        this.orbit = orbit;
        pipe_scb = mbx;
        this.mode = mode;
    endfunction
    
    // Сброс в начальное состояние
    task orbit_monitor::init;
        synced <= 1'b0;
        pipe <= '0;
        m16_sync_pipe <= '0;
        mute = TRUE;
    endtask: init
    
    // Запуск сбора данных от Орбиты
    task orbit_monitor::run;
        orbit.ready_run();
        sync();
        collect();
    endtask: run

    // Метод, осуществляющий синхронизацию по первому синхроимпульсу в потоке Орбиты.
    // Работает для информативностей М01, М02, М04, М08
    task orbit_monitor::sync;
        automatic bit ena = 0;
        fork: syncing
            while (!synced) begin
                @(negedge orbit.ready);
                if (mode < 4) begin
                    pipe <= {pipe[22:0], orbit.orbit};
                    if (pipe[23:22] == 2'b10) begin
                        synced <= 1'b1;
                        pipe_scb.put(pipe);
                    end
                end
                else begin
                    m16_sync_pipe <= {m16_sync_pipe[383:0], orbit.orbit};
                    if (orbit.orbit) begin
                        ena = 1'b1;
                    end
                     // 0111_1000_z100_1101 - 0x784D
                    // if (ena)
                    //    $display("@%0t: %m PIPE: 384'b%b .", $time, m16_sync_pipe);
                                            
                    if (!m16_sync_pipe[383] && m16_sync_pipe[359] && m16_sync_pipe[335] && m16_sync_pipe[311] &&
                        m16_sync_pipe[287] && !m16_sync_pipe[263] && !m16_sync_pipe[239] && !m16_sync_pipe[215] &&
                        m16_sync_pipe[167] && !m16_sync_pipe[143] && !m16_sync_pipe[119] &&
                        m16_sync_pipe[95] && m16_sync_pipe[71] && !m16_sync_pipe[47] && m16_sync_pipe[23]) begin
                        synced <= 1'b1;
                        $display("@%0t: %m M16 synced!.", $time);
                        /*for (int i = 32; i < 0; i--) begin
                            pipe_scb.put(m16_sync_pipe[i * 12 - 1:i * 12 - 12]);
                        end  */
                        // $display("@%0t: %m m16_sync_pipe %h", $time, m16_sync_pipe);
                        pipe_scb.put(m16_sync_pipe[383:372]);
                        pipe_scb.put(m16_sync_pipe[371:360]);
                        pipe_scb.put(m16_sync_pipe[359:348]);
                        pipe_scb.put(m16_sync_pipe[347:336]);
                        pipe_scb.put(m16_sync_pipe[335:324]);
                        pipe_scb.put(m16_sync_pipe[323:312]);
                        pipe_scb.put(m16_sync_pipe[311:300]);
                        pipe_scb.put(m16_sync_pipe[299:288]);
                        
                        pipe_scb.put(m16_sync_pipe[287:276]);
                        pipe_scb.put(m16_sync_pipe[275:264]);
                        pipe_scb.put(m16_sync_pipe[263:252]);
                        pipe_scb.put(m16_sync_pipe[251:240]);
                        pipe_scb.put(m16_sync_pipe[239:228]);
                        pipe_scb.put(m16_sync_pipe[227:216]);
                        pipe_scb.put(m16_sync_pipe[215:204]);
                        pipe_scb.put(m16_sync_pipe[203:192]);
                        
                        pipe_scb.put(m16_sync_pipe[191:180]);
                        pipe_scb.put(m16_sync_pipe[179:168]);
                        pipe_scb.put(m16_sync_pipe[167:156]);
                        pipe_scb.put(m16_sync_pipe[155:144]);
                        pipe_scb.put(m16_sync_pipe[143:132]);
                        pipe_scb.put(m16_sync_pipe[131:120]);
                        pipe_scb.put(m16_sync_pipe[119:108]);
                        pipe_scb.put(m16_sync_pipe[107:96]);
                        
                        pipe_scb.put(m16_sync_pipe[95:84]);
                        pipe_scb.put(m16_sync_pipe[83:72]);
                        pipe_scb.put(m16_sync_pipe[71:60]);
                        pipe_scb.put(m16_sync_pipe[59:48]);
                        pipe_scb.put(m16_sync_pipe[47:36]);
                        pipe_scb.put(m16_sync_pipe[35:24]);
                        pipe_scb.put(m16_sync_pipe[23:12]);
                        pipe_scb.put(m16_sync_pipe[11:0]);                        
                    end
                end
            end
        join_none
    endtask: sync
    
    // Метод, осуществляющий формирования удвоенного слова Орбиты, сохранение его в mailbox pipe_scb
    task orbit_monitor::collect_word;
        wait (synced);
        if (mode < 4) begin
            for (int i = 0; i < 24; i++) begin
                @(negedge orbit.ready);
                pipe <= {pipe[22:0], orbit.orbit};
            end
            if (!mute) begin
                $display("@%0t: %m Received PIPE: 24'b%b .", $time, pipe);
            end
        end
        else begin
            for (int i = 0; i < 12; i++) begin
                @(negedge orbit.ready);
                pipe <= {pipe[10:0], orbit.orbit};
            end
            if (!mute) begin
                $display("@%0t: %m Received PIPE: 12'h%h .", $time, pipe[11:0]);
            end
        end

        pipe_scb.put(pipe);
    endtask: collect_word
    
    // Метод, осуществляющий формирования потока из удвоенных слов Орбиты, сохранение их в mailbox pipe_scb
    task orbit_monitor::collect;
        fork: collecting
            forever begin
                collect_word();
            end
        join_none
    endtask: collect
`endif