/*
 * Класс anp_checker для работы с DUT larva_main.
 * Проверяет слова Орбиты полученные от класса orbit_checker. Сохраняет данные в файл.
 * Подключается к классу orbit_monitor с помощью mailbox для удвоенных слов Орбиты logic [23:0].
 * collect_cycle - осуществляет сортировку и преобразование удвоенных слов Орбиты в слова-структуры Орбиты orbitWord_s.
 * check_frame - проверяет кадры Орбиты на правильность служебной информации (разработка в процессе)
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef ANP_CHECKER_INCLUDE
    `define ANP_CHECKER_INCLUDE
    class anp_checker;
        mailbox orbit_scb, anp_scb;
        int acc_f120, acc_f15;
        int f120_cnt, f15_cnt;
        int error_allowed, error_cnt;
        int ptr_f120, ptr_f15;
        logic [32:0][11 + 5:0] data_f120;
        logic [8:0][24:0] data_f15;
        logic [11:0] scb_data;
        bit synced, first_synced, mode_f;
        string info;
        int ch_num;
        // Конструктор класса.
        extern function new(mailbox mbx_anp, bit mode_f, string info, int ch_num);
        
        // метод, преобразующий удвоенные слова в одиночные данные
        extern task check(input logic [9:0] orb_anp_data);
        // Сброс в начальное состояне
        extern task init;
    endclass: anp_checker

    // Конструктор класса.
    function anp_checker::new(mailbox mbx_anp, bit mode_f, string info, int ch_num);
        anp_scb = mbx_anp;
        this.mode_f = mode_f;
        this.info = info;
        this.ch_num = ch_num;
    endfunction
    
    // метод, преобразующий удвоенные слова в одиночные данные
    task anp_checker::check(input logic [9:0] orb_anp_data);
        ptr_f120 = ((f120_cnt + 1) < 33) ?  f120_cnt + 1 : 0;
        anp_scb.get(scb_data);
        // $display("@%0t: %m orb_anp_data %h f120_cnt %d orb_anp_data %h", $time, scb_data * 32, f120_cnt, orb_anp_data);
        acc_f120 = acc_f120 + scb_data * 32 - data_f120[ptr_f120];
        data_f120[f120_cnt] = scb_data * 32; 
        // $display("@%0t: %m acc_f120 %h", $time, acc_f120);
        f120_cnt++;
        if (f120_cnt == 33) begin
            f120_cnt = 0;
        end
        if (mode_f == 1'b1) begin
            ptr_f15 = ((f15_cnt + 1) < 9) ?  f15_cnt + 1 : 0;
            acc_f15 = acc_f15 + acc_f120 - data_f15[ptr_f15];  
            data_f15[f15_cnt] = acc_f120;
            f15_cnt++;
            if (f15_cnt == 9) begin
                f15_cnt = 0;
            end
        end
        // $display("@%0t: %m acc_f120 %h  acc_f120 >> 12 %h data_f120[ptr_f120] %h ptr_f120 %d scb_data %h scb_data * 32 %h", $time, acc_f120, acc_f120 >> 12, data_f120[ptr_f120], ptr_f120, scb_data, scb_data * 32);
        // $display("@%0t: %m acc_f15 %h  acc_f15 >> 15 %h data_f15[ptr_f15] %h ptr_f15 %d data_f120[ptr_f120] %h orb_anp_data %h", $time, acc_f15, acc_f15 >> 15, data_f15[ptr_f15], ptr_f15, data_f120[ptr_f120], orb_anp_data);
        synced = 1'b0;
        error_cnt = 0;
        if (orb_anp_data != 0) begin
            do begin
                if (((mode_f == 1'b0) ? acc_f120 >> 12 : acc_f15 >> 15) != orb_anp_data) begin
                    ptr_f120 = ((f120_cnt + 1) < 33) ?  f120_cnt + 1 : 0;
                    anp_scb.get(scb_data);
                    acc_f120 = acc_f120 + scb_data * 32 - data_f120[ptr_f120];
                    data_f120[f120_cnt] = scb_data * 32;        
                    f120_cnt++;
                    if (f120_cnt == 33) begin
                        f120_cnt = 0;
                    end
                    if (mode_f == 1'b1) begin
                        ptr_f15 = ((f15_cnt + 1) < 9) ?  f15_cnt + 1 : 0;
                        acc_f15 = acc_f15 + acc_f120 - data_f15[ptr_f15];  
                        data_f15[f15_cnt] = acc_f120;
                        f15_cnt++;
                        if (f15_cnt == 9) begin
                            f15_cnt = 0;
                        end
                    end
                    if (mode_f == 1'b0) begin
                        $display({"@%0t: %m SYNCING!!! ", info, "[%2d] acc_f120 %h  acc_f120 >> 12 %h data_f120[ptr_f120] %h ptr_f120 %d scb_data * 32 %h orb_anp_data %h"}, $time, ch_num, acc_f120, acc_f120 >> 12, data_f120[ptr_f120], ptr_f120, scb_data * 32, orb_anp_data);
                    end
                    else begin
                        $display({"@%0t: %m SYNCING!!! ", info, "[%2d] acc_f15 %h  acc_f15 >> 15 %h data_f15[ptr_f15] %h ptr_f15 %d data_f120[ptr_f120] %h orb_anp_data %h"}, $time, ch_num, acc_f15, acc_f15 >> 15, data_f15[ptr_f15], ptr_f15, data_f120[ptr_f120], orb_anp_data);
                    end
                end
                else begin
                    if (!first_synced) begin
                        first_synced = 1'b1;
                    end
                    else begin
                        error_cnt++;
                        if (error_cnt == error_allowed) begin
                            $finish;
                        end
                    end
                    synced <= 1'b1;
                    if (mode_f == 1'b0) begin
                        $display("@%0t: %m SYNCED!!! acc_f120 >> 12 %h orb_anp_data %h", $time, acc_f120 >> 12, orb_anp_data);
                    end
                    else begin
                        $display("@%0t: %m SYNCED!!! acc_f15 >> 15 %h orb_anp_data %h", $time, acc_f15 >> 15, orb_anp_data);
                    end
                    break;
                end
            end
            while (synced == 1'b0);
        end
    endtask: check
    
    // Сброс в начальное состояне
    task anp_checker::init;
        acc_f120 = 0;
        acc_f15 = 0;
        f120_cnt = 0;
        f15_cnt = 0;
        data_f120 = '0;
        data_f15 = '0;
        scb_data = '0;
        synced <= 1'b0;
        first_synced = 1'b0;
        error_allowed = 2;
        error_cnt = 0;
        if (mode_f == 1'b1) begin
            error_allowed = 2;
        end
    endtask: init
`endif