`ifndef ENVIRONMENT_INCLUDE
    `define ENVIRONMENT_INCLUDE
    import classes_pkg::vQspiTestM_t;
    import classes_pkg::vOrbitTest_t;
    import classes_pkg::vAnpTest_t;
    import classes_pkg::vSpiSlave_t;
    import classes_pkg::vRs232Test_t;
    import classes_pkg::vDisSigTest_t;
    import classes_pkg::wb2qspi_cmdr;
    import classes_pkg::orbit_monitor;
    import classes_pkg::orbit_checker;
    import classes_pkg::anp_gen;
    import classes_pkg::epcq16a;
    import classes_pkg::xmtr;
    import classes_pkg::discrete_signals_mng;
    import defines_pkg::FALSE;
    
    import classes_pkg::wbCmd_s;
    class environment;
        static int qspi_sck_period = 320;
        wb2qspi_cmdr cmdr;
        orbit_monitor orbit_mon;
        orbit_checker orbit_chk;
        mailbox orbit_mon2chk, map2chk, anp2chk[0:7];
        anp_gen anp_g;
        // epcq16a#(.FILE_NAME("epcq16a_memtest_mode5.bin")) flash;
        epcq16a#(.FILE_NAME("epcq16a.bin")) flash;
        xmtr xmtr_dev;
        discrete_signals_mng ds_mng;
        
        bit [3:0] mode;
        function new(input vQspiTestM_t qspi_master, vOrbitTest_t orbit, bit [3:0] mode, vAnpTest_t anp, vSpiSlave_t spi, vRs232Test_t rs232, vDisSigTest_t ds);
            this.mode = mode;
            cmdr = new(qspi_master);
            orbit_mon2chk = new();
            map2chk = new();
            for (int i = 0; i < 8; i++) begin
                anp2chk[i] = new();
            end 
            orbit_mon = new(orbit, orbit_mon2chk, mode);
            orbit_chk = new(orbit_mon2chk, mode, map2chk, anp2chk[0:7]);
            anp_g = new(anp, anp2chk[0:7]);
            flash = new(spi, map2chk);
            xmtr_dev = new(rs232);
            ds_mng = new(ds);
        endfunction

        task init;
            cmdr.qspi_sck_period = qspi_sck_period;
            anp_g.init();
            flash.init();
            cmdr.init();
            orbit_mon.init();
            orbit_chk.init();
            xmtr_dev.init();
            
            // blank
        endtask: init
        
        task run;
            automatic wbCmd_s wb_cmd = '{adr:16'h0008, data:32'h0};
            automatic logic [31:0] wb_rd_data;
            // anp_g.run();
            flash.run();
            cmdr.check_connection();
            cmdr.check_extbus();
            cmdr.init_commands_timers();
            // cmdr.init_map();
            // ------ cmdr.run_orbit(mode, 1'b1, 8'd0);
            cmdr.selftest(mode);
            orbit_mon.run();
            // orbit_mon.mute = FALSE;
            // orbit_chk.mute = FALSE;
            // orbit_chk.run();
            cmdr.read_cycle_crc();
            xmtr_dev.run();
            ds_mng.run_after(100);
            #2500000;
            // cmdr.wb_rd_cmd(wb_cmd, wb_rd_data);
            // $display("@%0t: %m wb_rd_data %h", $time, wb_rd_data);
            /*#250000
            cmdr.stop();
            #250000
            cmdr.reset();
            #250000
            cmdr.run_orbit(mode);*/
        endtask: run
    endclass: environment
`endif