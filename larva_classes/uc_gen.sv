/*
 * Класс uc_gen (aka uart chanel generation) для работы с DUT larva_digital.
 * Эмитирует передачу информации по uart каналу.
 *
 * Made by NotBlacksmithMax.
 * ЗИТЦ.
 * 2020
 */
`ifndef UART_CHAN_INCLUDE
    `define UART_CHAN_INCLUDE
    import classes_pkg::vUcTest_t;
    class uc_gen;
        static int pause_time = 32000000;
        vUcTest_t uc;
        mailbox mbx;
        bit [7:0] data;
        bit [9:0] data_shift;
        // Конструктор класса uс_gen, реализующий связь с проектом larva_dgital через uart канал.
        extern function new(input vUcTest_t uc, mailbox mbx);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод-сериализатор данных параллельному каналу
        extern task send_answer;
        // Метод-генератор данных
        extern task gen_data;
        extern task run;
    endclass: uc_gen
    
    function uc_gen::new(input vUcTest_t uc, mailbox mbx);
        this.uc = uc;
        this.mbx = mbx;
    endfunction
    
    task uc_gen::init;
        uc.cbm.tx <= 1'b1;
        data = 8'd0;
        data_shift = 10'd512;
    endtask: init
    
    task uc_gen::send_answer;
        $display("@%0t: %m UC Data gen is 10'h%h.", $time, data);
        for (int i = 0; i < 10; i++) begin
            uc.cbm.tx <= data_shift[i];
            #10;
        end
    endtask: send_answer
    
    task uc_gen::gen_data;
        data = $urandom_range(255);
        mbx.put(data);
        data_shift[9:1] = data;
    endtask: gen_data
    
    task uc_gen::run;
        fork
            forever begin
                gen_data();
                send_answer();
                #pause_time;
                gen_data();
                send_answer();
                #pause_time;
            end
        join_none
    endtask: run
`endif