/*
 * Класс pc_checker для работы с DUT larva_digital.
 * Запрашиваем данные, принятых по параллельному каналу. Сохраняет их в mailbox.
 *
 * Made by NotBlacksmithMax.
 * ЗИТЦ
 * 2020
 */
`ifndef PC_CHECKER_INCLUDE
    `define PC_CHECKER_INCLUDE

    import classes_pkg::wb2qspi_cmdr;
    
    import defines_pkg::bool_e;
    import defines_pkg::TRUE;
    import defines_pkg::FALSE;
    
    import classes_pkg::orbitWord_s;
    import classes_pkg::orbitType_e;
    import classes_pkg::SYCN_WORD;
    import classes_pkg::DATA_WORD;
    import classes_pkg::SERVICE_WORD;
    import classes_pkg::orbitService_s;
    
    class pc_checker;
        mailbox pipe_scb;
        mailbox cycle_scb;
        mailbox map_scb;
        bit [3:0] mode;
        int sync_period;
        local bit [/*9*/7:0] checking_cnt;
        bool_e mute;
        
        // Конструктор класса. Принимает mailbox mbx с удвоенными словами Орбиты и код mode режима работы.
        extern function new(mailbox mbx_pipe, bit [3:0] mode, mailbox mbx_map);
        // Private метод, преобразующий удвоенные слова в одиночные данные
        extern local function logic [11:0] undouble(logic [23:0] data);
        // Сброс в начальное состояне
        extern task init;
        // Запуск проверки кадров Орбиты
        extern task run;
        // Метод-конвертор удвоенных слов Орбиты в слова Орбиты структуры orbitWord_s
        extern task collect_cycle;
        // Метод, запускающий конвертацию и проверку слов Орбиты
        extern task collect;
        // Метод, проверяющий служебную информацию в кадре Орбиты (в процессе разработки)
        extern task check_frame;
        // Метод для проверки данных в словах Орбиты при использовании счётчика в качестве источника данных
        extern task check_counter(input logic [9:0] data);
    endclass: orbit_checker

    // Конструктор класса. Принимает mailbox mbx с удвоенными словами Орбиты и код mode режима работы.
    function orbit_checker::new(mailbox mbx_pipe, bit [3:0] mode, mailbox mbx_map);
        pipe_scb = mbx_pipe;
        map_scb = mbx_map;
        this.mode = mode;
    endfunction
    
    // Private метод, преобразующий удвоенные слова в одиночные данные
    function logic [11:0] orbit_checker::undouble(logic [23:0] data);
        for (int i = 0; i < 12; i++) begin
            if (data[2 * i] == data[2 * i + 1]) begin
                undouble[i] = data[2 * i];
            end
            else begin
                undouble[i] = 1'bx;
            end
        end
    endfunction: undouble
    
    // Сброс в начальное состояне
    task orbit_checker::init;
        cycle_scb = new();
        checking_cnt = '0;
        mute = TRUE;
        unique case(mode)
            4'd0: begin
                sync_period = 2; // sync_period - период следования синхроимпульса - 1 раз в 2 слова.
            end
            4'd1: begin
                sync_period = 4;
            end
            4'd2: begin
                sync_period = 8;
            end
            4'd3: begin
                sync_period = 16;
            end
            default: begin
                sync_period = 32;
            end
        endcase
    endtask: init
    
    // Запуск проверки кадров Орбиты
    task orbit_checker::run;
        collect();
    endtask: run

    // Метод-конвертор удвоенных слов Орбиты в слова Орбиты структуры orbitWord_s
    task orbit_checker::collect_cycle;
        automatic orbitWord_s orb_w;
        automatic logic [11:0] tmp_dat;
        automatic int words_cnt, m16_cnt;
        automatic bit part;
        automatic logic [23:0] dword;
        words_cnt = 0;
        m16_cnt = 0;
        part = 1'b0;
        for (int i = 0; i < 4096 * sync_period; i++) begin

            if (mode < 4) begin
                pipe_scb.get(dword);
                tmp_dat = undouble(dword);
            end
            else begin
                pipe_scb.get(tmp_dat);
            end
            
            orb_w.\type = DATA_WORD;
            orb_w.service = tmp_dat[11];
            orb_w.data = tmp_dat[10:1];
            if (mode < 4) begin         
                if (words_cnt == 0) begin
                    part = 1'b1;
                    orb_w.\type = SYCN_WORD;
                    if (dword[23:22] == 2'b10) begin
                        orb_w.valid = TRUE;
                    end 
                    else begin
                        orb_w.valid = FALSE;
                    end   
                end
            end
            else begin
                if (!words_cnt[0]) begin
                    part = 1'b1;
                    orb_w.\type = SYCN_WORD;
                    // 0111_1000_z100_1101 - 0x784D
                    if (words_cnt == 0 || words_cnt == 10 || words_cnt == 12 || words_cnt == 14 || words_cnt == 20 || words_cnt == 22 || words_cnt == 28) begin
                        if (!tmp_dat[11]) begin
                            orb_w.valid = TRUE;
                        end 
                        else begin
                            orb_w.valid = FALSE;
                        end  
                    end
                    if (words_cnt == 2 || words_cnt == 4 || words_cnt == 6 || words_cnt == 8 || words_cnt == 18 || words_cnt == 24 || words_cnt == 26 || words_cnt == 30) begin
                        if (tmp_dat[11]) begin
                            orb_w.valid = TRUE;
                        end 
                        else begin
                            orb_w.valid = FALSE;
                        end  
                    end
                end
            end

            if (words_cnt == (sync_period / 2)) begin
                part = 1'b0;    
                orb_w.\type = SERVICE_WORD;
                orb_w.service = tmp_dat[11];
                if (tmp_dat[11] == 1'bx) begin
                    orb_w.valid = FALSE;
                end
                else begin
                    orb_w.valid = TRUE;
                end
            end
            
            if (tmp_dat[0] == 1'bx) begin
                orb_w.parity = FALSE;
                orb_w.valid = FALSE;
            end
            else begin
                if ((tmp_dat[0] != ^tmp_dat[10:1])) begin
                    orb_w.parity = TRUE;
                end
                else begin
                    orb_w.parity = FALSE;
                end
            end
            cycle_scb.put(orb_w);
            words_cnt++;
            if (words_cnt == sync_period) begin
                words_cnt = 0;
            end
            // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid);
        end
    endtask: collect_cycle

    // Метод, запускающий конвертацию и проверку слов Орбиты
    task orbit_checker::collect;
        fork: collecting
            forever begin
                collect_cycle();
            end
            forever begin
                check_frame();
            end                

        join_none
    endtask: collect
    
    // Метод, проверяющий служебную информацию в кадре Орбиты (в процессе разработки)
    task orbit_checker::check_frame;
        automatic orbitWord_s orb_w;
        automatic orbitService_s service_w;
        automatic int phrases_cnt;
        automatic logic [6:0] group_number, groups_cnt;
        automatic logic [7:0] group_label;
        automatic logic [15:0] local_time_label;
        phrases_cnt = 0;
        groups_cnt = '0;
        for (int i = 0; i < 4 * 4096 * (sync_period / 2); i++) begin // 4096 кол-во фраз в цикле
            cycle_scb.get(orb_w);
            if (orb_w.valid == TRUE) begin
                if (orb_w.\type == SERVICE_WORD) begin
                    // $display("@%0t: %m Type: %s; Service: 1'b%b; Data: 10'h%h; Parity: %s; Valid: %s.", $time, orb_w.\type , orb_w.service, orb_w.data, orb_w.parity, orb_w.valid); // Uncomment for debug
                    if (phrases_cnt >= 0 && phrases_cnt < 7) begin // формируется маркер номера группы
                        group_number = { group_number[5:0], orb_w.service };
                        if (phrases_cnt == 6) begin
                            if (group_number != groups_cnt) begin  // проверяется маркер номера группы
                                $display("@%0t: %m ERROR: Wrong Group Number in the frame. Presented Group Number is 7'b%b, expected Group Number is 7'b%b.", $time, group_number, groups_cnt);
                                $finish;
                            end
                            else begin
                                // $display("@%0t: %m Presented Group Number is 7'b%b.", $time, group_number);
                            end
                        end
                    end
                    
                    if (phrases_cnt == 7) begin  // проверяется маркер кадра
                        if (groups_cnt == '0) begin
                            if (orb_w.service != 1'b1) begin
                                $display("@%0t: %m ERROR: Wrong Frame Label. Presented Frame Label is 1'b%b, expected Frame Label is 1'b1.", $time, orb_w.service);
                                $finish;
                            end
                            else begin
                                $display("@%0t: %m Presented  Frame Label is 1'b%b.", $time, orb_w.service);
                            end
                        end
                        else begin
                            if (orb_w.service != 1'b0) begin
                                $display("@%0t: %m ERROR: Wrong Frame Label. Presented Frame Label is 1'b%b, expected Frame Label is 1'b0.", $time, orb_w.service);
                                $finish;
                            end
                            else begin
                                // $display("@%0t: %m Presented  Frame Label is 1'b%b.", $time, orb_w.service);
                            end
                        end                          
                    end
                    
                    if (phrases_cnt >= 8 && phrases_cnt < 24) begin // формируется МТВ
                        local_time_label = { local_time_label[14:0], orb_w.service };
                        if (phrases_cnt == 23) begin // проверяется МТВ
                            if (groups_cnt[4:0] == '0) begin
                                 $display("@%0t: %m Presented Local Timer Label is %d%d%d%d.", $time, local_time_label[15:12], local_time_label[11:8], local_time_label[7:4], local_time_label[3:0]);
                            end
                        end
                    end
                    
                    if (phrases_cnt >= 56 && phrases_cnt < 64) begin // формируется маркер группы
                        group_label = { group_label[6:0], orb_w.service };
                        if (phrases_cnt == 63) begin // проверяется маркер группы
                            if (groups_cnt[4:0] != '1) begin
                                if (group_label != 8'b01110010) begin
                                    $display("@%0t: %m ERROR: Wrong Group Label in the frame. Presented Group Label is 8'b%b, expected Group Label is 8'b01110010.", $time, group_label);
                                    $finish;
                                end
                                else begin
                                    // $display("@%0t: %m Presented Group Label is 8'b%b.", $time, group_label);
                                end
                            end
                            else begin
                                if (group_label != 8'b10001101) begin
                                    $display("@%0t: %m ERROR: Wrong Group Label in the frame. Presented Group Label is 8'b%b, expected Group Label is 8'b10001101.", $time, group_label);
                                    $finish;
                                end
                                else begin
                                    $display("@%0t: %m Presented Group Label is 8'b%b.", $time, group_label);
                                end
                            end
                        end
                    end

                    phrases_cnt++;
                    if (phrases_cnt == 64) begin
                        phrases_cnt = 0;
                        groups_cnt++;
                        // $display("@%0t: %m groups_cnt is 8'b%b.", $time, groups_cnt);
                    end
                end
                check_counter(orb_w.data);
            end
        end
        $display("@%0t: %m Frame was collected.", $time);
    endtask: check_frame
    
    task orbit_checker::check_counter(input logic [9:0] data);
        automatic bit [7:0] map_data;
        map_scb.get(map_data);
        case (map_data)
            8'h05: begin
                  $display("@%0t: %m Data Field is 10'h%h.", $time, data);  
            
            end
            default: begin
                if (data != checking_cnt) begin

                    $display("@%0t: %m ERROR: Wrong Data Field in the Orbit word. Presented Data Field is 10'h%h, expected Data Field is 10'h%h.", $time, data, checking_cnt);
                    $finish;
                end
                else begin
                    if (!mute) begin
                        $display("@%0t: %m Data Field is 10'h%h.", $time, data);
                    end
                end
                
            end
        endcase
        checking_cnt++;
    endtask: check_counter
`endif