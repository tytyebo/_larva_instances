/*
 * Класс xmtr - имитатор RS-232 от передатчика.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */

`ifndef XMTR_INCLUDE
    `define XMTR_INCLUDE
    import classes_pkg::rs232_trx;
    import classes_pkg::vRs232Test_t;
    class xmtr; 
        rs232_trx rs232;
        // Конструктор класса xmtr
        extern function new(input vRs232Test_t rs232);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод run, имитирующий посылку по RS-232 от передатчика
        extern task run;
    endclass: xmtr
    
    function xmtr::new(input vRs232Test_t rs232);
        this.rs232 = new(rs232);
    endfunction
    
    task xmtr::init;
        rs232.init();
    endtask: init

    task xmtr::run;
        fork
            forever begin
                rs232.send_byte(8'hA1);
                rs232.send_byte(8'h0A);
                rs232.send_byte(8'h55);
                rs232.send_byte(8'hAA);
                rs232.send_byte(8'h95);
                rs232.send_byte(8'h33);
                rs232.send_byte(8'hA1);
                rs232.send_byte(8'hB1);
                #10000000;
            end
        join_none
    endtask: run
`endif