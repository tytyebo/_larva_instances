/*
 * Класс эмулятора микросхемы флэш-памяти EPCQ16A для проекта larva_main.
 * Требует на вход сигнал тактовой частоты в 2 раза выше тактовой частоты larva_main.
 *
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
 
// ‭The clk should be 25.165824‬ MHz
`ifndef EPCQ16A_INCLUDE
    `define EPCQ16A_INCLUDE
    `include "defines.svh"
    import classes_pkg::vSpiSlave_t;
    
    class epcq16a#(parameter string FILE_NAME = "epcq16a.bin"); 
        parameter bit [7:0] CMD_RD = 8'h03,
                            CMD_WR = 8'h20;
        static int fd = 0;
        bit [7:0] mem_data [0:462848];
        mailbox mbx;
        vSpiSlave_t spi;
        
        logic [2:0][2:0] mux;
        bit [5:0][15:0] data;
        // Конструктор класса anp_gen, реализующий связь с проектом larva_anp через интерфейсы Spi.
        extern function new(input vSpiSlave_t spi, mailbox mbx);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;       
        // Метод-сериализатор данных АЦП по Spi
        extern task read;
        // Метод-генератор данных АЦП
        extern task receive_byte(output logic [7:0] data);
        extern task send_byte(input bit [7:0] data);
        extern task run;
    endclass: epcq16a
    
    function epcq16a::new(input vSpiSlave_t spi, mailbox mbx);
        this.spi = spi;
        this.mbx = mbx;
    endfunction
    
    task epcq16a::init;
        fd = $fopen(FILE_NAME, "rb");
        $fread(mem_data, fd);
        $fclose(fd);
        spi.miso = '0;
        /*$display("@%0t: %m mem_data %h", $time, mem_data[40]);
        $display("@%0t: %m mem_data %h", $time, mem_data[41]);
        $display("@%0t: %m mem_data %h", $time, mem_data[42]);
        $display("@%0t: %m mem_data %h", $time, mem_data[42]);
        $finish;*/
    endtask: init
    
    task epcq16a::receive_byte(output logic [7:0] data);
        wait (!spi.ncs);
        for (int i = 7; i >= 0; i--) begin
            @(posedge spi.sck);
            data[i] = spi.mosi;
        end
    endtask: receive_byte
    
    task epcq16a::send_byte(input bit [7:0] data);
        for (int i = 7; i >= 0; i--) begin
            @(negedge spi.sck);
            if (!spi.ncs) begin
                spi.miso = data[i];
            end
        end
    endtask: send_byte
        
    task epcq16a::read;
        automatic logic [7:0] operation = '0;
        automatic logic [23:0] address = '0;
        receive_byte(operation);
        // $display("@%0t: %m operation %h", $time, operation);
        receive_byte(address[23:16]);
        receive_byte(address[15:8]);
        receive_byte(address[7:0]);
        // $display("@%0t: %m address %h", $time, address);
        if (operation == CMD_RD) begin: send_block
            fork
            while (!spi.ncs) begin
                send_byte(mem_data[address - `EPCQ_BASE_MODE_ADR]);
                mbx.put(mem_data[address - `EPCQ_BASE_MODE_ADR]);
                // $display("@%0t: %m mem_data[address - `EPCQ_BASE_MODE_ADR] %h", $time, mem_data[address - `EPCQ_BASE_MODE_ADR]);
                address++;
            end
            wait (spi.ncs);
            join_any
        end
        disable send_block;
    endtask: read
    
    task epcq16a::run;
        fork
            forever begin
                read();
            end
        join_none
    endtask: run
`endif