/*
 * Класс sc_gen (aka serial chanel generation) для работы с DUT larva_digital.
 * Эмитирует передачу информации по последовательному каналу.
 *
 * Made by NotBlacksmithMax.
 * ЗИТЦ.
 * 2020
 */
`ifndef SERIAL_CHAN_INCLUDE
    `define SERIAL_CHAN_INCLUDE
    import classes_pkg::vScTest_t;
    class sc_gen;
        vScTest_t sc;
        mailbox mbx;
        bit [9:0] data;
        bit [13:0] data_shift;
        // Конструктор класса sс_gen, реализующий связь с проектом larva_digital через последовательный канал.
        extern function new(input vScTest_t sc, mailbox mbx);
        // Метод инициализации параметров класса в начальное состояние
        extern task init;
        // Метод-сериализатор данных параллельному каналу
        extern task send_answer;
        // Метод-генератор данных
        extern task gen_data;
        extern task run;
    endclass: sc_gen
    
    function sc_gen::new(input vScTest_t sc, mailbox mbx);
        this.sc = sc;
        this.mbx = mbx;
        data = 10'd0;
        data_shift = 14'd0;
    endfunction
    
    task sc_gen::init;
        sc.psk = '0;
    endtask: init
    
    task sc_gen::send_answer;
        @(posedge sc.req);
        //$display("@%0t: %m SC Data gen is 10'h%h.", $time, data);
        for (int i = 0; i < 14; i++) begin
            @(posedge sc.si)
            sc.psk = data_shift[i];
        end
    endtask: send_answer
    
    task sc_gen::gen_data;
        data = $urandom_range(1023);
        mbx.put(data);
        for (int i = 0; i < 10; i++) begin
            data_shift[i+3] = data[i];
        end
    endtask: gen_data
    
    task sc_gen::run;
        fork
            forever begin
                gen_data();
                send_answer();
            end
        join_none
    endtask: run
`endif