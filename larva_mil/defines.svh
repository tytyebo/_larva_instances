`ifndef DEFINES_INCLUDE
    `define DEFINES_INCLUDE
    
    `timescale 1ns/1ns
    // `define SIMULATION 1

    `define WB_ADR_EXT_WIDTH 16
    `define WB_ADR_INT_WIDTH 12
`endif