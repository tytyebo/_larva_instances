`timescale 1 ns/ 1 ns
`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::milTxWord_s;
import defines_pkg::MIL_DATA;
import defines_pkg::MIL_CMD;
import defines_pkg::FALSE;
import defines_pkg::TRUE;
    
module tb_mil_tx_phy();
	bit nrst, clk, mil_clk; 
    milStd_io mil();
    logic data_ready;
    milTxWord_s data;
    
	initial begin
        nrst <= 1'b0; 
        data <= '0;
        data_ready <= 1'b0;
        @(posedge mil_clk);
        @(posedge mil_clk);
        @(posedge mil_clk);
        nrst <= 1'b1;
        
        mil_send_word('{\type :MIL_DATA, data:16'h1234});
    end
    
	always #39.736 clk = !clk; // 12.582912 MHz
	// always #21.150 mil_clk = !mil_clk;
    always #20.833 mil_clk = !mil_clk;

    mil_tx_phy milTxPhy(
        .nrst(nrst),
        .clk(mil_clk),
        .mil(mil),
        .mil_data(data),
        .data_ready(data_ready),
        .busy());
                   
    task mil_send_word(input milTxWord_s mil_data); // needed 24 MHz clock frequency
        data_ready <= 1'b1;
        data <= mil_data;
        @(posedge mil_clk);
        data_ready <= 1'b0;
    endtask: mil_send_word
endmodule


