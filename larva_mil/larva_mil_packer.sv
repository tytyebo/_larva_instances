/*
 * Модуль "larva_mil_packer" для проекта larva_main.
 * Осуществляет переформатированние слов mil в слова orbit.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
import defines_pkg::milRxWord_s;
import defines_pkg::orbitMilWord_s;
import defines_pkg::MIL_DATA;
import defines_pkg::MIL_CMD;
import defines_pkg::FALSE;
import defines_pkg::TRUE;
import defines_pkg::HIGH;
import defines_pkg::LOW;

module larva_mil_packer(
        input bit nrst, clk, // The mil_clk should be 24 MHz (the clock for "mil_rx_phy" module).  
        input logic mil_wren,
        input milRxWord_s mil_data,
        output orbitMilWord_s orbit_data,
        output logic orbit_wren);
        
    logic part;
    always_ff @(posedge clk or negedge nrst) begin
        if(!nrst) begin
            orbit_wren <= 1'b0;
            part <= 1'b0;
            orbit_data <= '0;
        end
        else begin
            orbit_wren <= 1'b0;
            if (mil_wren && !part) begin
                part <= 1'b1;
                orbit_data <= '{data:mil_data.data[15:8], word_rank:HIGH, \type :mil_data.\type };
                orbit_wren <= 1'b1;
            end
                
            if (part) begin
                part <= 1'b0;
                orbit_data <= '{data:mil_data.data[7:0], word_rank:LOW, \type :mil_data.\type };
                orbit_wren <= 1'b1;
            end
        end
    end
endmodule: larva_mil_packer