import defines_pkg::milRxWord_s;
import defines_pkg::MIL_DATA;
import defines_pkg::MIL_CMD;
import defines_pkg::FALSE;
import defines_pkg::TRUE;

module mil_rx_phy(
        input bit nrst, clk, // The input clk should be 24 MHz
        input logic ena,
        milStd_io.rx mil,
        output milRxWord_s mil_data,
        output logic mil_wren);
    
    localparam THRESHOLD_HIGH = 45, // Верхний порог обнаружения синхроимпульса
               THRESHOLD_LOW = 3; // Нижний пород обнаружения синхоимпульса
    localparam DELAY1 = 19,
               DELAY = 23;
           
    logic rxp, rxn;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(2))
    metaChainMil(.clk(clk), .nrst(nrst),
                 .din({ mil.rxp, mil.rxn }), .dout({ rxp, rxn }));

    logic rx_ena;
    metastable_chain#(.CHAIN_DEPTH(5), .BUS_WIDTH(1)) 
    delayRxEna(.clk(clk), .nrst(nrst),
               .din(1'b1), .dout(rx_ena));
    
    assign mil.rx_ena = rx_ena & ena;
    logic f_rxp, f_rxn;
    input_filter inputFilterRxp(.clk(clk), .nrst(nrst), .signal(rxp), .out(f_rxp));
    input_filter inputFilterRxn(.clk(clk), .nrst(nrst), .signal(rxn), .out(f_rxn));

    logic [1:0] cnt_in;
    logic [23:0] matched_p, matched_n;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            cnt_in <= '0;
            matched_p <= '0;
            matched_n <= '0;
        end
        else begin
            cnt_in <= cnt_in + 1'b1;
            if (cnt_in == 2'd2) begin
                matched_p <= {matched_p[($bits(matched_p) - 2):0], f_rxp};
                matched_n <= {matched_n[($bits(matched_n) - 2):0], f_rxn};
                cnt_in <= 2'd0;
            end
        end
    end
      
    logic [23:0] sync_p_r, sync_n_r;
    logic [4:0] sync_sum_p_r, sync_sum_n_r, sync_sum_p, sync_sum_n;
    logic [5:0] sync_sum[0:1];
    logic is_sync, is_exceed_high, is_exceed_low, rx_error, rx_stop;

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            sync_p_r <= '0;
            sync_sum_p_r <= '0;
            
            sync_n_r <= '0;
            sync_sum_n_r <= '0;
            
            sync_sum[0] <= '0;
            sync_sum[1] <= '0;
            is_sync <= '0;
            is_exceed_high <= 1'b0;
            is_exceed_low <= 1'b0;
        end
        else begin
            sync_p_r <= ~{matched_p[23:12] ^ 12'hFFF, (matched_p[11:0] ^ 12'h000)};
            sync_sum_p_r <= sync_sum_p;
            
            sync_n_r <= ~{matched_n[23:12] ^ 12'h000, matched_n[11:0] ^ 12'hFFF};
            sync_sum_n_r <= sync_sum_n;
            
            sync_sum[0] <= sync_sum_p_r + sync_sum_n_r;
            sync_sum[1] <= sync_sum[0];
            
            if (sync_sum[0] > THRESHOLD_HIGH && rx_ena) begin
                is_exceed_high <= 1'b1;
            end
            
            if (sync_sum[0] < THRESHOLD_LOW && rx_ena) begin
                is_exceed_low <= 1'b1;
            end

            if ((is_exceed_high & sync_sum[0] < sync_sum[1]) || (is_exceed_low & sync_sum[0] > sync_sum[1])) begin
                is_sync <= 1'b1;
            end
                
            if (rx_error | rx_stop) begin
                is_sync <= 1'b0;
                is_exceed_high <= 1'b0;
                is_exceed_low <= 1'b0;
            end
        end
    end

    always_comb begin
        sync_sum_p = '0;
        sync_sum_n = '0;
        for (int i = 0; i < 24; i++) begin
            if (sync_p_r[i]) begin
                sync_sum_p = sync_sum_p + 1'b1;
            end
            if (sync_n_r[i]) begin
                sync_sum_n = sync_sum_n + 1'b1;
            end
        end
    end    

    logic [3:0] data_p_r, data_n_r;
    logic [2:0] data_sum_p, data_sum_n;
    logic [3:0] data_sum[0:1];
    logic [4:0] data_sync_cnt, data_cnt;
    logic is_next, is_data;
    logic [16:0] rx_data;
    always_ff @(posedge clk or negedge nrst) begin
    if (!nrst) begin
        data_p_r <= '0;        
        data_n_r <= '0;        
        data_sum[0] <= '0;
        data_sum[1] <= '0;
        data_sync_cnt <= '0;
        is_next <= 1'b0;
        is_data <= 1'b0;
        rx_data <= '0;
        data_cnt <= '0;
        rx_stop <= 1'b0;
        rx_error <= 1'b0;
    end else
        begin
            data_p_r <= ~{matched_p[6:5] ^ 2'b11, (matched_p[2:1] ^ 2'b00)};                    
            data_n_r <= ~{matched_n[6:5] ^ 2'b00, (matched_n[2:1] ^ 2'b11)};
            data_sum[0] <= data_sum_p + data_sum_n;
            data_sum[1] <= data_sum[0];
            
            if (is_sync) begin
                data_sync_cnt <= data_sync_cnt + 1'b1;
            end
            else begin
                data_sync_cnt <= '0;
                is_next <= 1'b0;
            end
            
            rx_error <= 1'b0;
            is_data <= 1'b0;
            if ((is_next && data_sync_cnt == DELAY) || (~is_next && data_sync_cnt == DELAY1)) begin
                is_data <= 1'b1;
                data_sync_cnt <= '0;
            end
                
            if (is_data) begin
                is_next <= 1'b1;
                if (data_sum[1] > 4) begin
                    rx_data <= { rx_data[($bits(rx_data) - 2):0], 1'b1 };
                    data_cnt <= data_cnt + 1'b1;
                end
                if (data_sum[1] < 4) begin
                    rx_data <= { rx_data[($bits(rx_data) - 2):0], 1'b0 };
                    data_cnt <= data_cnt + 1'b1;
                end
                if (data_sum[1] == 4) begin
                    rx_error <= 1'b1;
                end   

            end
            
            rx_stop <= 1'b0;
            if (data_cnt == 17) begin
                rx_stop <= 1'b1;
                data_cnt <= '0;
            end
        end
    end
        
    always_comb begin
        data_sum_p = '0;
        data_sum_n = '0;
        for (int i = 0; i < 4; i++) begin
            if (data_p_r[i]) begin
                data_sum_p = data_sum_p + 1'b1;
            end
            if (data_n_r[i]) begin
                data_sum_n = data_sum_n + 1'b1;
            end
        end
    end

    logic parity;
    assign parity = (^mil_data.data == ~rx_data[0]);
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            mil_data.\type <= MIL_CMD;
            mil_data.valid <= FALSE;
            mil_wren <= 1'b0;
        end
        else begin
            if (rx_stop) begin
                mil_data.valid <= (parity) ? TRUE : FALSE; 
                if (is_exceed_high) begin
                   mil_data.\type <= MIL_CMD;
                end
                if (is_exceed_low) begin
                   mil_data.\type <= MIL_DATA;
                end
            end
            mil_wren <= rx_stop;
        end
    end
    assign mil_data.data = rx_data[16:1]; 
endmodule: mil_rx_phy

module input_filter(
        input bit nrst, clk, signal,
        output bit out
        );
    logic [4:0] buffer;

    always_ff @ (posedge clk or negedge nrst) begin
        if(!nrst) begin
            buffer <= '0;
        end
        else begin	
            buffer <= {buffer[3:0], signal};
        end
    end

    logic [2:0] amount;
    always_comb begin
        amount = '0;
        for (int i = 0; i < 5; i++) begin
            if (buffer[i] == 1'b1) begin
                amount = amount + 1'b1;
            end
        end
            
        if (amount > 5'd2) begin
            out = 1'b1;
        end
        else begin
            out = 1'b0;
        end
    end
endmodule: input_filter