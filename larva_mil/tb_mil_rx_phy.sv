`timescale 1 ns/ 1 ns
`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::milRxWord_s;
import defines_pkg::milTxWord_s;
import defines_pkg::MIL_DATA;
import defines_pkg::MIL_CMD;
import defines_pkg::FALSE;
import defines_pkg::TRUE;
import defines_pkg::RECEIVE;
import defines_pkg::milCmdWord_s;

module tb_mil_rx_phy();
	bit nrst, clk, mil_clk; 
    logic p, n;
    milStd_io mil_rx(), mil_tx();
    wishbone_io wbs();
    logic data_ready, busy;
    milTxWord_s data;
    
	initial begin
        automatic milCmdWord_s mil_cmd_word;
        nrst <= 1'b0; 
        data_ready <= 1'b0;
        data <= '0;
        p = 1'b0;
        n = 1'b0;
        
        wbs.wbm_stb <= 1'b0; 
        @(posedge mil_clk);
        @(posedge mil_clk);
        @(posedge mil_clk);
        nrst <= 1'b1;
        
        repeat(10) begin
            @(posedge mil_clk);
        end
        
        mil_cmd_word = '{addr:5'd0, dir:RECEIVE, sub_addr:5'd0,words_cnt:5'd0};
        mil_send_word('{\type :MIL_CMD, data:mil_cmd_word});
        for (int i = 0; i < 8; i++) begin
            mil_send_word('{\type :MIL_DATA, data:16'h1234});
            mil_send_word('{\type :MIL_DATA, data:16'h5678});
            mil_send_word('{\type :MIL_DATA, data:16'h9ABC});
            mil_send_word('{\type :MIL_DATA, data:16'hDEF0});
        end
    end
    
	always #39.736 clk = ~clk; // 12.582912 MHz
    always #20.833 mil_clk = ~mil_clk;
    
    assign mil_rx.rxn = mil_tx.txn;
    assign mil_rx.rxp = mil_tx.txp;
    larva_mil_fifo2wb larvaMilFifo2wb(.nrst(nrst), .clk(clk), .mil_clk(mil_clk), .mil(mil_rx), .wbs(wbs));
    
    mil_tx_phy milTxPhy(
        .nrst(nrst),
        .clk(mil_clk),
        .mil(mil_tx),
        .mil_data(data),
        .data_ready(data_ready),
        .busy(busy));
                   
    task mil_send_word(input milTxWord_s mil_data); // needed 24 MHz clock frequency
        if (busy) begin
            @(negedge busy);
        end
        data_ready <= 1'b1;
        data <= mil_data;
        @(posedge mil_clk);
        data_ready <= 1'b0;
        @(posedge mil_clk);
    endtask: mil_send_word
endmodule


