/*
 * Модуль "larva_mil_responder" для проекта larva_main.
 * Осуществляет приёмом сообщений в Формате 1 со своим адресом ОУ.
 * Формирует ответное слово в Формате 1 и отправляет его.
 * При отправке ответного слова игнорирует возможные проблемы на линии.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
import defines_pkg::milRxWord_s;
import defines_pkg::milTxWord_s;
import defines_pkg::MIL_CMD;
import defines_pkg::MIL_DATA;
import defines_pkg::RECEIVE;
import defines_pkg::TRUE;
import defines_pkg::milCmdWord_s;

module larva_mil_responder#(parameter PAUSE_CNT = 120,
                            parameter LOSS_CNT = 720)
                           (input bit nrst, clk, // The mil_clk should be 24 MHz (the clock for "mil_rx_phy" module).
                            input logic [4:0] dev_addr,
                            milStd_io.tx mil,
                            input milRxWord_s mil_data,
                            input logic mil_wren,
                            output logic [15:0] wrong_msgs,
                            output logic [15:0] interrupted_msgs);
        
    enum {ST0, ST1, ST2} resp_state;
    milCmdWord_s mil_cmd;
    milTxWord_s mil_answer;
    logic [5:0] words_cnt;
    logic [$clog2(PAUSE_CNT):0] pause_cnt;
    logic [$clog2(LOSS_CNT):0] loss_cnt;
    logic dr;
    assign mil_cmd = mil_data.data;
    assign mil_answer = '{\type :MIL_CMD, data:{dev_addr, 11'd0}};
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            words_cnt <= '0;
            pause_cnt <= '0;
            loss_cnt <= '0;
            resp_state <= ST0;
            dr <= 1'b0;
            wrong_msgs <= '0;
            interrupted_msgs <= '0;
        end
        else begin
            dr <= 1'b0;
            case (resp_state)
                ST0: begin
                    if (mil_wren) begin
                        if (mil_data.valid == TRUE) begin
                            if (mil_cmd.addr == dev_addr && mil_cmd.dir == RECEIVE && mil_data.\type == MIL_CMD) begin
                                words_cnt <= (mil_cmd.words_cnt != '0) ? mil_cmd.words_cnt : 6'd32;
                                resp_state <= ST1;
                            end
                        end
                        else begin
                            wrong_msgs <= wrong_msgs + 1'b1;
                        end
                    end
                end
                ST1: begin
                    loss_cnt <= loss_cnt + 1'b1;
                    if (mil_wren) begin
                        if (mil_data.valid == TRUE) begin
                            if (mil_data.\type == MIL_DATA) begin
                                words_cnt <= words_cnt - 1'b1;
                                if (words_cnt == 5'd1) begin
                                    resp_state <= ST2;
                                end
                            end
                            else begin                     
                                words_cnt <= '0;
                                interrupted_msgs <= interrupted_msgs + 1'b1;
                                resp_state <= ST0;
                            end
                        end
                        else begin
                            wrong_msgs <= wrong_msgs + 1'b1;
                            interrupted_msgs <= interrupted_msgs + 1'b1;
                            words_cnt <= '0;
                            resp_state <= ST0;
                        end
                        loss_cnt <= '0;
                    end
                    if (loss_cnt == LOSS_CNT - 1'b1) begin
                        loss_cnt <= '0;
                        interrupted_msgs <= interrupted_msgs + 1'b1;
                        resp_state <= ST0;
                    end 
                end
                ST2: begin
                    pause_cnt <= pause_cnt + 1'b1;
                    if (pause_cnt == PAUSE_CNT - 2'd2) begin
                        dr <= 1'b1;
                        pause_cnt <= '0;
                        resp_state <= ST0;
                    end
                end                        
            endcase           
        end
    end
    mil_tx_phy milTxPhy(.nrst(nrst), .clk(clk), .mil(mil), .mil_data(mil_answer), .data_ready(dr), .busy());
endmodule: larva_mil_responder