/*
 * Модуль "mil_tx_phy" передатчика MIL-STD-1553B
 * для микросхемы HI-1573.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
import defines_pkg::milTxWord_s;
import defines_pkg::MIL_CMD;

// CLK_VALUE_ENA is the devider of the input "clk" frequency.
// It wants to make the 2 MHz "clk_ena" signal.
module mil_tx_phy#(parameter CLK_VALUE_ENA=12) 
                  (input bit nrst, clk, // The input clk should be 24 MHz
                   milStd_io.tx mil,
                   input milTxWord_s mil_data,
                   input logic data_ready,
                   output logic busy);
        
    enum {ST0, ST1, ST2, ST3, ST4} tx_state;
    
    logic tx_odd, tx_ena, ban_odd, tx_out, parity, last_bit, tx_end;
    logic tx_ena_d, tx_out_d; 
    logic [15:0] shift_reg, tx_cnt;
    
    logic clk_ena;
    logic [$clog2(CLK_VALUE_ENA):0] cnt_ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            clk_ena <= 1'b0;
            cnt_ena <= '0;
        end
        else begin
            cnt_ena <= cnt_ena + 1'b1;
            clk_ena <= 1'b0;
            if (cnt_ena == CLK_VALUE_ENA - 1'b1) begin
                clk_ena <= 1'b1;
                cnt_ena <= '0;
            end
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            tx_odd <= 1'b0;
            tx_ena <= 1'b0;
            tx_ena_d <= 1'b0;
            tx_state <= ST0;
            ban_odd <= 1'b0;
            tx_out <= 1'b0;
            tx_out_d <= 1'b0;
            shift_reg <= 1'b0;
            parity <= 1'b0;
            tx_cnt <= '0;
            last_bit <= 1'b0;
            tx_end <= 1'b0;
            busy <= 1'b0;
            mil.txp <= 1'b0;  
            mil.txn <=  1'b0;
            mil.tx_ena <= 1'b0;
        end
        else begin
            if (data_ready) begin
                busy <= 1'b1;
            end

            if (clk_ena) begin
                if (tx_end) begin
                    tx_ena <= 1'b0;
                    tx_end <= 1'b0;
                end
                
                if (data_ready || busy) begin
                    ban_odd <= 1'b0; // Флаг разрешения инверсии, снят по умолчанию
                    if (tx_odd) begin
                        // Инверсная фаза выставления последовательных данных
                        if (!ban_odd) begin
                            tx_out <= ~tx_out;
                        end
                        if (last_bit) begin
                            last_bit <= 1'b0;
                            busy <= 1'b0;
                            tx_end <= 1'b1;
                        end
                    end
                    else begin
                        // Прямая фаза выставления последовательных данных (сдвиг)
                        unique case (tx_state)
                            ST0: begin
                                tx_out <= (mil_data.\type == MIL_CMD) ? 1'b1 : 1'b0;
                                tx_ena <= 1'b1;
                                shift_reg <= mil_data.data;
                                ban_odd <= 1'b1;
                                parity <= 1'b1;
                                tx_state <= ST1;
                            end
                            ST1: begin
                                tx_state <= ST2;
                            end
                            ST2: begin
                                ban_odd <= 1'b1;
                                tx_cnt <= 4'hf;
                                tx_state <= ST3;
                            end
                            ST3: begin
                                tx_out <= shift_reg[15]; // Загружаем старшими разрядами вперед
                                parity <= parity ^ shift_reg[15];
                                shift_reg[15:1] <= shift_reg[14:0];
                                if (tx_cnt != '0) begin
                                    tx_cnt <= tx_cnt - 1'b1;
                                end
                                else begin
                                    tx_state <= ST4;
                                end
                            end
                            ST4: begin
                                tx_out <= parity;
                                tx_state <= ST0;
                                last_bit <= 1'b1;
                            end
                        endcase
                    end
                    tx_odd <= ~tx_odd; // Смена фазы
                end

            end
            tx_ena_d <= tx_ena;
            tx_out_d <= tx_ena & tx_out; // Формирование выхода Cдвигового Регистра Сериализатора Передатчика, задержанного на 1 такт
            
            mil.txp <= ((tx_ena & tx_out) | tx_out_d);   
            mil.txn <=  ~((tx_ena & tx_out) & tx_out_d);
            mil.tx_ena <= ~(tx_ena | tx_ena_d);
        end
    end 

endmodule: mil_tx_phy