`include "defines.svh"
import defines_pkg::milRxWord_s;
import defines_pkg::orbitMilWord_s;

    // The following inputs should be:
    // "clk" is the system clock, "mil_clk" is the 24 MHz clock for "mil_rx_phy" module.
module larva_mil_fifo2wb#(parameter bit [9:0] DEBUG_CONST = '0)
                         (input bit nrst, clk, mil_clk,
                          input logic sync,
                          milStd_io mil,
                          wishbone_io.slave wbs,
                          output logic [5:0] terminal_address,
                          output logic [1:0] mil_mode);
    // Mil Phy Module
    logic mil_wren, rx_ena;
    milRxWord_s mil_data;
    mil_rx_phy milRxPhy(.nrst(nrst), .clk(mil_clk), .ena(rx_ena), .mil(mil), .mil_data(mil_data), .mil_wren(mil_wren));
        
    // Mil Packer To Orbit Module
    orbitMilWord_s fifo_wdata;
    logic fifo_wrq;
    logic [15:0] wrong_msgs, interrupted_msgs;
    larva_mil_packer larvaMilPacker(.nrst(nrst), .clk(mil_clk), .mil_data(mil_data), .mil_wren(mil_wren),
                                    .orbit_wren(fifo_wrq), .orbit_data(fifo_wdata));
    
    logic fifo_rrq, fifo_empty;
    orbitMilWord_s fifo_rdata;
    logic [4:0] resp_addr;
    logic resp_ena, orb_word_odd, orb_word_null;
    logic [9:0] debug_cnt;
    logic [1:0] ctrl_mode;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
            fifo_rrq <= 1'b0;
            resp_addr <= '0;
            resp_ena <= 1'b0;
            debug_cnt <= DEBUG_CONST;
            orb_word_odd <= 1'b0;
            orb_word_null <= 1'b0;
            ctrl_mode <= '0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            fifo_rrq <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                if (wbs.wbm_we) begin
                    if (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == `WB_ADR_INT_WIDTH'd0) begin
                        {ctrl_mode, resp_ena, resp_addr} <= wbs.wbm_dat[7:0];
                        end
                end 
                else begin
                    if (wbs.wbm_sel[1:0] == 2'b11) begin
                        case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                            `WB_ADR_INT_WIDTH'd1: begin   
                                unique case (ctrl_mode)
                                    2'd0: begin
                                        orb_word_odd <= ~orb_word_odd;
                                        orb_word_null <= 1'b0;
                                        if (!fifo_empty) begin
                                            if (!orb_word_null) begin
                                                fifo_rrq <= 1'b1;
                                                wbs.wbs_dat <= {22'd0, fifo_rdata};
                                            end
                                            else begin
                                                wbs.wbs_dat <= '0;
                                            end
                                        end
                                        else begin
                                            if (!orb_word_odd) begin
                                                orb_word_null <= 1'b1;
                                            end
                                            wbs.wbs_dat <= '0;
                                        end
                                    end
                                    2'd1: begin
                                        wbs.wbs_dat <= '0;
                                    end
                                    2'd2: begin
                                        wbs.wbs_dat  <= '1;
                                    end
                                    2'd3: begin
                                        wbs.wbs_dat  <= {22'd0, debug_cnt};
                                        debug_cnt <= debug_cnt + 1'b1;
                                    end
                                endcase
                            end
                            `WB_ADR_INT_WIDTH'd2: begin
                                wbs.wbs_dat <= {16'd0, wrong_msgs};
                            end
                            `WB_ADR_INT_WIDTH'd3: begin
                                wbs.wbs_dat <= {16'd0, interrupted_msgs};
                            end
                            default: begin
                                wbs.wbs_dat <= '0;
                            end
                        endcase
                    end
                end
                wbs.wbs_ack <= 1'b1;
            end
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            rx_ena <= 1'b0;
        end
        else begin
            if (sync) begin
                rx_ena <= 1'b1;
            end
        end
    end
        
    larva_mil_fifo larvaMilFifo( // 10 разрядное фифо!!
        .aclr(!nrst),
        .data(fifo_wdata),
        .rdclk(clk),
        .rdreq(fifo_rrq),
        .wrclk(mil_clk),
        .wrreq(fifo_wrq),
        .q(fifo_rdata),
        .wrfull(),
        .rdempty(fifo_empty));

    logic wren;
    assign wren = mil_wren & resp_ena;
    larva_mil_responder larvaMilResponder(.nrst(nrst), .clk(mil_clk), .dev_addr(resp_addr), .mil(mil), .mil_data(mil_data), .mil_wren(wren),
                                          .wrong_msgs(wrong_msgs), .interrupted_msgs(interrupted_msgs));
    assign terminal_address = {resp_addr, ~(^resp_addr)};
    assign mil_mode = (resp_ena) ? 2'b11 : 2'b01;
endmodule: larva_mil_fifo2wb