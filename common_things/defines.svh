`ifndef DEFINES_INCLUDE
    `define DEFINES_INCLUDE
    `timescale 1ns/1ns
//    `define SIMULATION 1
//    `define SMALL_TB 1
    
    `define WB_ADR_EXT_WIDTH 16
    `define WB_ADR_INT_WIDTH 12
	`define EPCQ_BASE_MODE_ADR 24'h010000 // Адрес в флэш-памяти EPCQ, где расположено начало кодов режимов опросов
    
    `define WB_MAP_READER_BASE 16'h6000
    
    `define ANP0_CHA_F 8'hFF
    `define ANP0_CHB_F 16'hFF
    `define ANP0_CHC_F 16'hFF
    `define ANP0_BASE_ADR 8'h08
    `define ANP0_TREG_BASE 8'h00
    `define ANP0_TEMP_BASE 8'h04
    
    `define ANP1_CHA_F 8'h00
    `define ANP1_CHB_F 16'hFF
    `define ANP1_CHC_F 16'h00
    `define ANP1_BASE_ADR 8'h30
    `define ANP1_TREG_BASE 8'h58
    `define ANP1_TEMP_BASE 8'h5D // Внимание! Должно было быть 8'h5C, но по ошибке теперь 8'h5D
    `define DIG_DS_BASE 8'hA0    
    `define S1_FLOW_BASE `ANP0_BASE_ADR
    `define S2_FLOW_BASE 8'h48
    
    `define DIG_TREG_BASE 8'h60
    `define DIG_PC0_BASE 8'h70
    `define DIG_PC1_BASE 8'h78
    `define DIG_SC_BASE 8'h80
    `define DIG_RS_BASE 8'h90
    
    `define M01_SIZE 17'd4096
    `define M02_SIZE 17'd8192
    `define M04_SIZE 17'd16384
    `define M08_SIZE 17'd32768
    `define M16_SIZE 17'd65536
    
    `define M01_SHIFT_LN 10'd767 // Длительность сериализации слова в тактах 
    `define M02_SHIFT_LN 10'd383
    `define M04_SHIFT_LN 10'd191
    `define M08_SHIFT_LN 10'd95
    `define M16_SHIFT_LN 10'd47
         
    `define MIL0_DEBUG_CONTS 10'h10
    `define MIL1_DEBUG_CONTS 10'h20
    `define MIL2_DEBUG_CONTS 10'h30
`endif