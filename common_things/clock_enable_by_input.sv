/*
 * Модуль формирователь сигнала clk_ena при достижении счетчика значения, поданного на вход ena_value
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
module clock_enable_by_input#(parameter int CNT_WIDTH = 10)
                    (input bit clk, nrst, ena,
                     input logic [CNT_WIDTH - 1:0] ena_value,
                     output logic clk_ena);
    
    logic [CNT_WIDTH:0] cnt;    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            clk_ena <= 1'b0;
            cnt <= '0;
        end
        else begin
            clk_ena <= 1'b0;
            if (ena) begin
                cnt <= cnt + 1'b1;
                if (cnt == ena_value) begin
                    cnt <= '0;
                    clk_ena <= 1'b1;
                end
            end
            else begin
                cnt <= '0;
            end
        end
    end
endmodule: clock_enable_by_input


