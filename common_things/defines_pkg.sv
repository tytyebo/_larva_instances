`include "defines.svh"
package defines_pkg;
	typedef enum bit {
        MIL_DATA = 1'b0,
		MIL_CMD = 1'b1
	} milWordType_e;

	typedef enum bit {
		FALSE = 1'b0,
		TRUE = 1'b1
	} bool_e;
    
    typedef enum bit {
        RECEIVE = 1'b0,
        TRANSMIT = 1'b1
    } milRxTxBit_e;
    
	typedef struct packed {
		milWordType_e \type ;
		logic [15:0] data;
        bool_e valid;
	} milRxWord_s;
    
    typedef struct packed {
		milWordType_e \type ;
		logic [15:0] data;
	} milTxWord_s;
    
    typedef struct packed {
		logic [4:0] addr;
        milRxTxBit_e dir;
        logic [4:0] sub_addr;
        logic [4:0] words_cnt;
	} milCmdWord_s;
    
    typedef enum bit {
        LOW = 1'b0,
        HIGH = 1'b1
    } rank_e;
    
    typedef struct packed {
        logic [7:0] data;
        rank_e word_rank;
		milWordType_e \type ;
	} orbitMilWord_s;
	
	typedef enum bit [7:0] {
		EPCQ_WRITE = 8'h02,
        EPCQ_READ = 8'h03
    } epcqOp_e;

    typedef struct packed {
        bit [`WB_ADR_EXT_WIDTH - 1:0] adr;
        bit [31:0] data;
        bit [3:0] sel;
    } wbCmd_s;
    
    typedef enum bit [7:0] {
        RS_START = 8'hA1,
        RS_XMTR_MARK = 8'h0A,
        RS_STOP = 8'hB1
    } rs232XmtrShell_e;
    
    typedef struct packed {
        logic [7:0] xmtr_u;
        logic [3:0] xmtr_p;
        logic [1:0] xmtr_break;
        logic [7:0] xmtr_t;
    } xmtrStatus_s;
    
    typedef struct packed {
        logic [9:0] word_clocks_size;
        logic [5:0] bit_clocks_size;
        logic [4:0] phrase_size;
    } orbitTelemSettings_s;
    
    typedef struct packed { // 128 bits
        logic [15:0] local_time_label; // Метка текущего времени (далее - МТВ)
        logic [15:0] start_command_label; // Метка прохождения команды (далее - МПК)
        logic [1:0] online; // Команда Исправность
        logic [1:0] com1; // Команда КОМ1
        logic [1:0] com2; // Команда КОМ2
        logic [1:0] com3; // Команда КОМ3
        logic [1:0] pc1_mode; // Режим ЦФП1
        logic [1:0] pc2_mode; // Режим ЦФП2
        logic [19:0] system_number; // Номер системы
        logic [7:0] mem_time_rec_label; // Метка времени записи в ЗУ (далее - МВЗ)
        logic [1:0] mem_mode; // Режим ЗУ
        logic [5:0] terminal_address; // Адрес ОУ
        logic [1:0] mil1_mode; // Режим МК1
        logic [1:0] lost_gen_mode; // Режим СГ
        logic [5:0] speed_code; // Код Информативности (И)
        logic [1:0] blank_field; // Пустое поле
        logic [3:0] transmitter_power; // Мощность передатчика (М ПР)
        logic [7:0] self_temp; // Температура прибора ФТ75 (Т ФТ)
        logic [7:0] trans_temp; // Температура передатчика (Т ПР)
        logic [7:0] danger_nets; // Напряжение опасных цепей (U оц)
        logic [7:0] transmitter_voltage; // Напряжение передатчика (U им)
    } orbitService_s;
    
    typedef enum bit [8:0] {
        CALIB_CH0_LOW = 9'd50,
        CALIB_CH0_HIGH = 9'd365
    } calib_ch0_e;
    typedef enum bit [8:0] {
        CALIB_CH1_ZERO = 9'd246,
        CALIB_CH1_LOW = 9'd20,
        CALIB_CH1_HIGH = 9'd472
    } calib_ch1_e;
endpackage: defines_pkg
