/*
 * Файл "intefaces.svh" для проектов larva_main, larva_analog.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef INTERFACES_INCLUDE
    `define INTERFACES_INCLUDE
    `include "defines.svh"
    interface wishbone_io;
        logic [`WB_ADR_EXT_WIDTH - 1:0] wbm_adr;
        logic [31:0] wbm_dat;
        logic [3:0] wbm_sel;
        logic wbm_cyc;
        logic wbm_stb;
        logic wbm_we;
        logic [31:0] wbs_dat;
        logic wbs_ack;

        modport master(output wbm_adr, wbm_dat, wbm_sel, wbm_cyc, wbm_stb, wbm_we,
                       input wbs_dat, wbs_ack);
        modport slave(input wbm_adr, wbm_dat, wbm_sel, wbm_cyc, wbm_stb, wbm_we,
                      output wbs_dat, wbs_ack);
    endinterface: wishbone_io
    
    interface milStd_io;
        logic txp;
        logic txn;
        logic tx_ena;
        logic rxp;
        logic rxn;
        logic rx_ena;
        
        modport line(input rxp, rxn, output txp, txn, tx_ena, rx_ena);
        modport rx(input rxp, rxn, output rx_ena);
        modport tx(output txp, txn, tx_ena);
    endinterface: milStd_io
    
    interface sdram_io#(parameter ADR_WIDTH = 13, DATA_WIDTH = 16);
        logic cke;
        logic cs_n;
        logic cas_n;
        logic ras_n;
        logic we_n;
        logic [1:0] dqm; // dqm[0] is dqml, dqm[1] is dqmh
        logic [1:0] ba;
        logic [ADR_WIDTH - 1:0] a;
        logic [DATA_WIDTH - 1:0] dq_in;
        logic [DATA_WIDTH - 1:0] dq_out;
                 
        task reset_ports();
            cke <= 1'b0;
            cs_n <= 1'b1;
            ras_n <= 1'b1;
            cas_n <= 1'b1;
            we_n <= 1'b1;
            dqm <= '0;
            ba <= '0;
            a <= '0;
            dq_out <= '0;
        endtask: reset_ports
        
        task nop_cmd();
            cs_n <= 1'b0;
            ras_n <= 1'b1;
            cas_n <= 1'b1;
            we_n <= 1'b1;
        endtask: nop_cmd
        
        task precharge_all_cmd();
            cs_n <= 1'b0;
            ras_n <= 1'b0;
            cas_n <= 1'b1;
            we_n <= 1'b0;
            a <= {{(ADR_WIDTH - 11){1'b0}}, 1'b1, 10'd0};
        endtask: precharge_all_cmd

        task auto_refresh_cmd();
            cs_n <= 1'b0;
            ras_n <= 1'b0;
            cas_n <= 1'b0;
            we_n <= 1'b1;
        endtask: auto_refresh_cmd

        task lmr_cmd(input logic [2:0] cas_latency);
            cs_n <= 1'b0;
            ras_n <= 1'b0;
            cas_n <= 1'b0;
            we_n <= 1'b0;
            a <= {{(ADR_WIDTH - 7){1'b0}}, cas_latency, 4'd0};
        endtask: lmr_cmd
        
        task active_cmd(input logic [ADR_WIDTH - 1:0] row_addr,
                        input logic [1:0] bank_addr);
            cs_n <= 1'b0;
            ras_n <= 1'b0;
            cas_n <= 1'b1;
            we_n <= 1'b1;
            a <= row_addr;
            ba <= bank_addr;
        endtask: active_cmd
        
        task write_cmd(input logic [9:0] column_addr,
                       input logic [1:0] bank_addr,
                       input logic [DATA_WIDTH - 1:0] data);
            cs_n <= 1'b0;
            ras_n <= 1'b1;
            cas_n <= 1'b0;
            we_n <= 1'b0;
            dqm <= 2'd0;
            a <= {{(ADR_WIDTH - 11){1'b0}}, 1'b1, column_addr};
            ba <= bank_addr;
            dq_out <= data;
        endtask: write_cmd

        task read_cmd(input logic [9:0] column_addr,
                       input logic [1:0] bank_addr);
            cs_n <= 1'b0;
            ras_n <= 1'b1;
            cas_n <= 1'b0;
            we_n <= 1'b1;
            dqm <= 2'd0;
            a <= {{(ADR_WIDTH - 11){1'b0}}, 1'b1, column_addr};
            ba <= bank_addr;
        endtask: read_cmd
    endinterface: sdram_io
    
    interface generic_bus_io#(parameter ADR_WIDTH = 16, MOSI_WIDTH = 16, MISO_WIDTH = 16);
        logic re;
        logic we;
        logic valid;
        logic ready;
        logic [MOSI_WIDTH - 1:0] dat_mosi;
        logic [MISO_WIDTH - 1:0] dat_miso;
        logic [ADR_WIDTH - 1:0] adr;
        logic busy;
        
        modport master(input dat_miso, busy, output re, we, dat_mosi, adr);
        modport slave(input re, we, dat_mosi, adr, output dat_miso, busy);       
        modport master_hndshk(input dat_miso, ready, output adr, valid, we, dat_mosi);
        modport slave_hndshk(input valid, we, dat_mosi, adr, output dat_miso, ready);
        modport slave_wo_adr(input re, we, dat_mosi, output dat_miso, busy);
    endinterface: generic_bus_io
    
    interface spi_io;
        logic ncs;
        logic sck;
        logic mosi;
        logic miso;

        modport master(input miso, output ncs, sck, mosi);
        modport slave(input ncs, sck, mosi, output miso);
    endinterface: spi_io
    
    interface i2c_io;
        logic scl_mosi;
        logic scl_miso;
        logic scl_noe;        
        logic sda_mosi;
        logic sda_miso;
        logic sda_noe;

        modport master(input sda_miso, scl_miso, output scl_mosi, sda_mosi, sda_noe, scl_noe);
    endinterface: i2c_io
    
    // Simple Valid-Ready handshake interface
    interface handshake_io#(parameter DATA_WIDTH = 8, DATA_WIDTH_OUT = 8)
                           (input bit clk);
        logic valid;
        logic ready;
        // wire ready;
        logic [DATA_WIDTH - 1:0] data;
        logic [DATA_WIDTH_OUT - 1:0] data_out;
    `ifdef SIMULATION
        clocking cbm @(posedge clk);
            output valid, data;
        endclocking: cbm
        
        clocking cbs @(posedge clk);
            output ready;
        endclocking: cbs
    `endif        
        modport master(input ready, output valid, data);
        modport slave(input valid, data, output ready);
        modport slave_out(input valid, data, output ready, data_out);
    `ifdef SIMULATION
        modport test_master(clocking cbm, input ready); // ready вне cbm, потому что это синхронный с clk сигнал
        modport test_slave(clocking cbs, input valid, data);
    `endif
    endinterface: handshake_io
    

    interface orbit_io;
        logic orbit;
        logic clock_orb;
        logic strobe_w;
        logic strobe_c;
    `ifdef SIMULATION
        bit [3:0] mode;
        logic ready;
        bit clk;
    `endif
        modport out(output orbit, clock_orb, strobe_w, strobe_c);
        modport in(input orbit, clock_orb, strobe_w, strobe_c);
        `ifdef SIMULATION
        modport test(input clk, mode, orbit, output ready,
                     import task ready_run());
                    
        task automatic ready_run;
            automatic bit [5:0] cnt = '0, stop = '0;
            unique case (mode)
                4'd0: begin
                    stop = 6'd32;
                end
                4'd1: begin
                    stop = 6'd16;
                end
                4'd2: begin
                    stop = 6'd8;
                end
                default: begin
                    stop = 6'd4;
                end
            endcase
            fork: gen_ready
                forever begin
                    ready <= 1'b0;
                    if (cnt == stop) begin
                        cnt = '0;
                        ready <= 1'b1;
                    end
                    cnt++;
                    @(posedge clk);
                end
            join_none
        endtask: ready_run
        `endif
    endinterface: orbit_io


    interface fifo_io#(parameter int DATA_WIDTH = 32);
        logic wrclk;
        logic wrreq;
        logic wrfull;
        logic [DATA_WIDTH - 1:0] data;
        
        logic rdclk;
        logic rdreq;
        logic rdfull;
        logic [DATA_WIDTH - 1:0] q;
        
        modport for_read(input rdfull, q, output rdreq);
        modport for_write(input wrfull, output wrreq, data);
    endinterface: fifo_io
    
    interface qspi_io(input bit clk);
        logic ncs;
        logic sck;
        logic dir;
        logic [3:0] io_mosi, io_miso;
    `ifdef SIMULATION
        clocking cbm @(posedge clk);
            output ncs, sck, dir, io_mosi;
        endclocking: cbm
        
        modport test_master(clocking cbm, input io_miso, output ncs);
    `endif
        modport slave(input ncs, sck, io_mosi, output dir, io_miso);
    endinterface: qspi_io
    
    interface anp_io;
        logic [2:0] mux;
        logic [2:0] ncs, sck;
        logic [5:0] miso;
        logic [5:0] ndisch;
        modport test(input ncs, sck, mux, output miso);
        modport master(input miso, output ncs, sck, mux, ndisch);
    endinterface: anp_io
    
    interface pc_io;
        logic req, si, sync;
        logic [9:0] prk;
        modport test(input req, si, sync, output prk);
    endinterface: pc_io
    
    interface sc_io;
        logic req, si, sync, psk;
        modport test(input req, si, sync, output psk);
    endinterface: sc_io
    
    interface uc_io(input bit clk);
        logic tx;
    `ifdef SIMULATION
        clocking cbm @(posedge clk);
            output tx;
        endclocking
        
        modport test(clocking cbm);
    `endif
    endinterface: uc_io
    
    interface rs232_io;
        logic rx;
        logic tx;

        modport rx_only(input rx);
        modport test(output rx);
    endinterface: rs232_io
    
    interface extbus_io;
        logic nwr_mosi;
        logic nwr_miso;
        logic nale;
        logic [15:0] adata_mosi;
        logic [15:0] adata_miso;
        logic oe_master;
        logic oe_slave;
        
        modport master(input nwr_miso, adata_miso, output nwr_mosi, nale, adata_mosi, oe_master);
        modport slave(input nwr_mosi, adata_mosi, nale, output nwr_miso, adata_miso, oe_slave);
    endinterface: extbus_io
    
    interface discrete_signals_io(input bit clk);
        logic nrecord;
        
        modport device_main(input nrecord);
    `ifdef SIMULATION
        clocking cb @(posedge clk);
            output nrecord;
        endclocking
        modport test(output nrecord);
    `endif
    endinterface: discrete_signals_io
    interface parallel_channel_io;
        logic [9:0] data;
        logic clk;
        logic sync;
        logic request;
        modport rx(input data, output clk, sync, request);
    endinterface: parallel_channel_io
    interface serial_channel_io;
        logic data;
        logic clk;
        logic sync;
        logic request;
        modport rx(input data, output clk, sync, request);
    endinterface: serial_channel_io
    
`endif