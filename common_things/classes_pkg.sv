`include "defines.svh"
`include "interfaces.svh"
package classes_pkg;
    typedef struct packed{
        bit [31:0] data;
        bit [15:0] adr;
        bit [7:0] cmd;
    } uartPacket_s; // Не синтезируемое, только для testbench

    typedef struct packed{
        bit [15:0] adr;
        bit [7:0] cmd;
        bit [31:0] data;
    } qspiPacket_s; // Не синтезируемое, только для testbench
    
    typedef struct packed{
        logic blank;
        logic fl_wlost;
        logic fl_rlost;
        logic fl_nodata;
        logic [3:0] csum;
    } qspiControl_s; // Не синтезируемое, только для testbench
    
    typedef struct packed{
        logic [31:0] data;
        qspiControl_s control;
    } qspiAnswer_s; // Не синтезируемое, только для testbench

    typedef virtual handshake_io.test_master vHdshkTestM_t;
    typedef virtual handshake_io.test_slave vHdshkTestS_t;
    typedef virtual qspi_io.test_master vQspiTestM_t;
    typedef virtual orbit_io.test vOrbitTest_t;
    typedef virtual anp_io.test vAnpTest_t;
    typedef virtual pc_io.test vPcTest_t;
    typedef virtual sc_io.test vScTest_t;
    typedef virtual uc_io.test vUcTest_t;
    typedef virtual spi_io.slave vSpiSlave_t;
    typedef virtual rs232_io.test vRs232Test_t;
    typedef virtual discrete_signals_io.test vDisSigTest_t;
    typedef struct packed {
        bit [`WB_ADR_EXT_WIDTH - 1:0] adr;
        bit [31:0] data;
        // bit [3:0] sel;
    } wbCmd_s;
    
    typedef enum bit [1:0] {
        SYCN_WORD = 2'd0,
        DATA_WORD = 2'd1,
        SERVICE_WORD = 2'd2
    } orbitType_e;
    
    import defines_pkg::bool_e;
    import defines_pkg::TRUE;
    import defines_pkg::FALSE;
    typedef struct packed {
        orbitType_e \type ;
        logic service;
        logic [9:0] data;
        bool_e parity;
        bool_e valid;
    } orbitWord_s;
    
    typedef struct packed {
        logic [15:0] local_time_label; // Метка текущего времени (далее - МТВ)
        logic [13:0] start_command_label; // Метка прохождения команды (далее - МПК)
        logic [1:0] online; // Команда Исправность
        logic [1:0] com1; // Команда КОМ1
        logic [1:0] com2; // Команда КОМ2
        logic [1:0] com3; // Команда КОМ3
        logic [1:0] par_ch1_mode; // Режим ЦФП1
        logic [1:0] par_ch2_mode; // Режим ЦФП2
        logic [19:0] system_number; // Номер системы
        logic [7:0] mem_time_rec_label; // Метка времени записи в ЗУ (далее - МВЗ)
        logic [1:0] mem_mode; // Режим ЗУ
        logic [5:0] terminal_address; // Адрес ОУ
        logic [1:0] MK1_mode; // Режим МК1
        logic [1:0] lost_gen_mode; // Режим СГ
        logic [5:0] speed_code; // Код Информативности (И)
        logic [3:0] transmitter_power; // Мощность передатчика (М ПР)
        logic [7:0] self_temp; // Температура прибора ФТ75 (Т ФТ)
        logic [7:0] trans_temp; // Температура передатчика (Т ПР)
        logic [7:0] danger_nets; // Напряжение опасных цепей (U оц)
        logic [7:0] transmitter_voltage; // Напряжение передатчика (U им)
    } orbitService_s;
    
    // `include "uart_trx.sv"
    `include "qspi_trx.sv"
    typedef qspi_trx driver_t;
    `include "wb2qspi_cmdr.sv"
    `include "anp_checker.sv"
    `include "anp_gen.sv"
//    `include "pc_gen.sv"
//    `include "sc_gen.sv"
//    `include "uc_gen.sv"
    `include "epcq16a.sv"
    `include "orbit_monitor.sv"
    `include "orbit_checker.sv"
    `include "rs232_trx.sv"
    `include "xmtr.sv"
    `include "discrete_signals_mng.sv"
    `include "environment.sv"

endpackage: classes_pkg