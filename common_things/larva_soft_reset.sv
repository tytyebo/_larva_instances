module larva_soft_reset(input bit clk, nrst,
                  input logic reset_req,
                  output logic soft_nrst);

    logic [7:0] soft_nrst_cnt;
    logic start;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            soft_nrst <= 1'b0;
            start <= 1'b0;
            soft_nrst_cnt <= '0;
        end
        else begin
            if (!start) begin
                soft_nrst <= 1'b1;
                start <= 1'b1;
            end
            if ((reset_req || !soft_nrst) && start) begin
                soft_nrst <= 1'b0;
                soft_nrst_cnt <= soft_nrst_cnt + 1'b1;
                if (soft_nrst_cnt == '1) begin
                    soft_nrst <= 1'b1;
                end
            end
        end
    end
endmodule: larva_soft_reset