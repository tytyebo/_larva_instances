/*
 * Модуль "sync_async_reset" для проекта ft75_main.
 * Синхронизирует асинхронный сигнал сброса.
 * Создан из изходников Altera 12. Recommended Design Practices QII51006-13.1.0
 * Страница 12-33
 * 
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
module sync_async_reset(input clk, input nrst, output synced_nrst);
    reg reg1, reg2;
    assign  synced_nrst = reg2;
    always @(posedge clk, negedge nrst) begin
        if (!nrst) begin
            reg1 <= 1'b0;
            reg2 <= 1'b0;
        end
        else begin
            reg1 <= 1'b1;
            reg2 <= reg1;
        end
    end
endmodule  // sync_async_reset