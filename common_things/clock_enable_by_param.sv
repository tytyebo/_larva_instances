/*
 * Модуль формирователь сигнала clk_ena при достижении счетчика значения CNT_ENA
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
module clock_enable_by_param#(parameter int CNT_ENA = 10)
                    (input bit clk, nrst, ena,
                     output logic clk_ena);
    
    logic [$clog2(CNT_ENA) - 1:0] cnt;    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            clk_ena <= 1'b0;
            cnt <= '0;
        end
        else begin
            clk_ena <= 1'b0;
            if (ena) begin
                cnt <= cnt + 1'b1;
                if (cnt == CNT_ENA) begin
                    cnt <= '0;
                    clk_ena <= 1'b1;
                end
            end
            else begin
                cnt <= '0;
            end
        end
    end
endmodule: clock_enable_by_param


