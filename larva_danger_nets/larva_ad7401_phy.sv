/*
 * Модуль "larva_ad7401_phy" для проекта ft75_main.
 * Контроллер физического уровня микросхемы АЦП AD7401.
 * Сделан на основе verilog-описания в datasheet AD7401. 
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
module larva_ad7401_phy(input bit clk, nrst, 
                        output logic mclk,
                        input logic mdata,
                        output logic [15:0] data);
                        
    logic [1:0] clk_cnt;
    logic valid;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            mclk <= 1'b0;
            clk_cnt <= '0;
            valid <= 1'b0;
        end
        else begin
            mclk <= ~mclk;
            valid <= 1'b0;
            clk_cnt <= clk_cnt + 1'b1;
            if (clk_cnt == 2'd1) begin
                valid <= 1'b1;
                clk_cnt <= '0;
            end
        end
    end
    
    logic ip_data1, valid_d;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            ip_data1 <= 1'b0;
            valid_d <= 1'b0;
        end
        else begin
            valid_d <= valid;
            if (valid) begin
                ip_data1 <= mdata;
            end
        end
    end

    logic [23:0] acc1, acc2, acc3;
    always_ff @ (posedge clk or negedge nrst) begin
        if (!nrst) begin 
            acc1 <= '0;
            acc2 <= '0;
            acc3 <= '0;
        end
        else begin
            if (valid_d) begin
                acc1 <= acc1 + ip_data1;
                acc2 <= acc2 + acc1;
                acc3 <= acc3 + acc2;
            end
        end
    end
    
    logic [8:0] decim_cnt;
    logic decim_valid;
    always_ff @ (posedge clk or negedge nrst) begin
        if (!nrst) begin
            decim_cnt <= '0;
            decim_valid <= 1'b0;
        end
        else begin
            decim_valid <= 1'b0;
            decim_cnt <= decim_cnt + 1'b1;
            if (decim_cnt == 9'd255) begin
                decim_valid <= 1'b1;
            end
        end
    end

    logic [23:0] acc3_d2, diff1, diff2, diff3, diff1_d, diff2_d;
    always_ff @ (posedge clk or negedge nrst) begin
        if (!nrst) begin
            acc3_d2 <= '0;
            diff1_d <= '0;
            diff2_d <= '0;
            diff1 <= '0;
            diff2 <= '0;
            diff3 <= '0;
            data <= '0;
        end
        else begin
            if (decim_valid) begin
                diff1 <= acc3 - acc3_d2;
                diff2 <= diff1 - diff1_d;
                diff3 <= diff2 - diff2_d;
                acc3_d2 <= acc3;
                diff1_d <= diff1;
                diff2_d <= diff2;
                data <= diff3[23:8];
            end
        end
    end
endmodule: larva_ad7401_phy
