`timescale 1ns/1ns
// 12.582912
`include "defines.svh" 
`include "interfaces.svh"
module tb_adc(); 
    bit clk;
    bit reset;
    always #40 clk = ~clk; // rounded to 12.5 MHz // #39.736 12.582912 MHz
		
    initial begin
        reset <= 1'b1;
        #200
        reset <= 1'b0;
    end
    logic mclk1, mdata1;
    logic [7:0] cnt;
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            mclk1 <= 1'b0;
            mdata1 <= 1'b0;
            cnt <= '0;
        end
        else begin
            mclk1 <= ~mclk1;

            if (!mclk1) begin
                mdata1 <= 1'b1;
                cnt <= cnt + 1'b1;
                if (cnt == 8'd3) begin
                    mdata1 <= 1'b0;
                    // mdata1 <= ~mdata1;
                    cnt <= '0;
                end
            end
        end
    end

    DEC256SINC24B rrrDEC256SINC24B
    (
        .mclk1(mclk1),
        .mdata1(mdata1),
               
        .reset(reset),
        .DATA()
    );
    wishbone_io wbs();
    larva_danger_nets_ctrl larvaDangerNetsCtrl(.clk(clk), .nrst(~reset), 
                        .mclk(),
                        .mdata(mdata1),
                        .wbs(wbs),
                        .danger_nets());
endmodule: tb_adc