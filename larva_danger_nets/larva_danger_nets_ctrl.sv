/*
 * Модуль "larva_danger_nets_ctrl" для проекта ft75_main.
 * Cхема взвешивания аналоговых данных схемы "опасных цепей".
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh" 
`include "interfaces.svh"
module larva_danger_nets_ctrl(input bit clk, nrst,
                              output logic mclk,
                              input logic mdata,
                              wishbone_io.slave wbs,
                              output logic [7:0] danger_nets);

    logic [31:0] mult;
    logic signed [16:0] pre_result;
    logic [15:0] pre_result_q;
    
    logic [15:0] b_factor;
    logic [2:0][15:0] k_factor;
    logic [15:0] adc_data, rounded;
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            pre_result <= '0;
            pre_result_q <= '0;
            danger_nets <= '0;
        end
        else begin
            pre_result <= rounded - b_factor;
            if (pre_result < $signed(17'd0)) begin
                pre_result_q <= '0;
            end
            else begin
                pre_result_q <= pre_result[15:0];
            end
            if (mult[31:30] > 2'd0) begin
                danger_nets <= '1;
            end
            else begin
                danger_nets <= mult[29:22];
            end
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            b_factor <= 16'd20774;
            k_factor[2] <= 16'd44587;
            k_factor[1] <= 16'd44587;
            k_factor[0] <= 16'd44587;
        end
        else begin
            k_factor[2] <= k_factor[1];
            k_factor[1] <= k_factor[0];
            wbs.wbs_ack = 1'b0;
            if (wbs.wbm_cyc && wbs.wbm_stb && !wbs.wbs_ack) begin
                if (wbs.wbm_we) begin
                    if (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == 12'h000  && wbs.wbm_sel == 4'hf) begin 
                        {k_factor[0], b_factor} <= wbs.wbm_dat;
                    end
                end
                else begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                wbs.wbs_dat <= {k_factor[0], b_factor};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                wbs.wbs_dat <= {rounded, 8'd0, danger_nets};
                            end
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                        end
                    endcase
                end
                wbs.wbs_ack = 1'b1;
            end
        end
    end
    larva_weigher_mult larvaWeigherMult(.dataa(pre_result_q), .datab(k_factor[2]), .result(mult)); // larvaWeigherMult is unsigned mult
    
    larva_ad7401_phy larvaAd7401Phy(.clk(clk), .nrst(nrst), 
                        .mclk(mclk),
                        .mdata(mdata),
                        .data(adc_data));
                        
    logic [8:0] cnt_512;
    logic [3:0][31:0] acc;
    logic [3:0] acc_ena;
    logic cnt_ena;
	logic [11:0] cnt_4096;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            cnt_512 <= '0;
            acc_ena <= '0;
			cnt_4096 <= '0;
            cnt_ena <= 1'b0;
        end
        else begin
            cnt_512 <= cnt_512 + 1'b1;
            acc_ena <= '0;
            cnt_ena <= 1'b0;
            if (cnt_512 == '1) begin
                cnt_ena <= 1'b1;
            end
			if (cnt_ena) begin
				cnt_4096 <= cnt_4096 + 1'b1;
				if (cnt_4096 == 12'd1023) begin
					acc_ena[1] <= 1'b1;
				end
				if (cnt_4096 == 12'd2047) begin
					acc_ena[2] <= 1'b1;
				end
				if (cnt_4096 == 12'd3071) begin
					acc_ena[3] <= 1'b1;
				end
                if (cnt_4096 == 12'd4095) begin
					acc_ena[0] <= 1'b1;
				end
			end
        end
    end
    
	logic [31:0] acc_data;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            acc <= '0;
			acc_data <= '0;
        end
        else begin
            if (cnt_ena) begin
                acc[0] <= acc[0] + adc_data;
                acc[1] <= acc[1] + adc_data;
                acc[2] <= acc[2] + adc_data;
                acc[3] <= acc[3] + adc_data;
            end
            if (acc_ena[0]) begin
                acc_data <= acc[0];
                acc[0] <= '0;
            end

            if (acc_ena[1]) begin
                acc_data <= acc[1];
                acc[1] <= '0;
            end

            if (acc_ena[2]) begin
                acc_data <= acc[2];
                acc[2] <= '0;
            end

            if (acc_ena[3]) begin
                acc_data <= acc[3];
                acc[3] <= '0;
            end
        end
    end
    
    logic round_ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            round_ena <= 1'b0;
            rounded <= '0;
        end
        else begin
            round_ena <= acc_ena[0];
            if (round_ena) begin
                if (acc_data[11:0] < 12'h800) begin
                    rounded <= acc_data[27:12];
                end
                else begin
                    if (acc_data[11:0] > 12'h800) begin
                        rounded <= acc_data[27:12] + 1'b1;
                    end
                    else begin
                        if (acc_data[12] == 1'b0) begin
                            rounded <= acc_data[27:12];
                        end
                        else begin
                            rounded <= acc_data[27:12] + 1'b1;
                        end
                    end
                end
            end
        end
    end
    
endmodule: larva_danger_nets_ctrl
