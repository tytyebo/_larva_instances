/**
  * Версия 2.0. 01.10.2020
  * Код откорректирован по итогам проверки в "железе". Убрана параметризация.
  * Версия 1.1. 12.07.2019
  * Добавлена вторая последовательная линия данных sdata.
  * Версия 1. 10.06.2019
  * Контроллер АЦП AD7476 для модуля Analog проекта Личинка (Орбита).
  * Разработан для ПЛИС Actel A3P600-PQ208I.
  * Частота sclk вырабатывается в отдельном блоке always.
  * Параметр CLK_CNT определяет число, до котого будет считать счётчик, прежде чем переключить sclk в другое состояние
  */
  
module larva_ad7476_phy(input bit clk, nrst,
                        input logic read,
                        output logic [11:0] data0, data1,
                        input sdata0, sdata1,
                        output logic sclk, ncs, busy);

    typedef enum bit [1:0] {ST_IDLE, ST_READ_ZEROS, ST_READ_DATA, ST_END} state_e;
    state_e state;
    logic [3:0] read_cnt;
    logic cs, sck;
    assign ncs = ~cs;
    assign sclk = sck;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            busy <= 1'b0;
            state <= ST_IDLE;
            cs <= 1'b0;
            sck <= 1'b0;
            read_cnt <= 4'd0;
            data0 <= 12'd0;
            data1 <= 12'd0;
        end
        else begin
            sck <= 1'b1;
            case (state)
                ST_IDLE: begin
                    if (read) begin
                        busy <= 1'b1;
                        cs <= 1'b1;
                        sck <= 1'b0;
                        state <= ST_READ_ZEROS;
                    end
                end
                ST_READ_ZEROS: begin
                    if (sck) begin
                        sck <= 1'b0;
                        read_cnt <= read_cnt + 1'b1;
                    end
                    if (read_cnt == 4'd2) begin
                        state <= ST_READ_DATA;
                    end
                end
                ST_READ_DATA: begin
                    if (sck) begin
                        sck <= 1'b0;
                        read_cnt <= read_cnt + 1'b1;
                        data0 <= {data0[10:0], sdata0};
                        data1 <= {data1[10:0], sdata1};
                    end
                    if (read_cnt == 4'hf) begin
                        sck <= 1'b1;
                        read_cnt <= 4'd0;
                        state <= ST_END;
                    end
                end
                ST_END: begin
                    cs <= 1'b0;
                    busy <= 1'b0;
                    data0 <= {data0[10:0], sdata0};
                    data1 <= {data1[10:0], sdata1};
                    state <= ST_IDLE;
                end
            endcase
        end
    end
endmodule: larva_ad7476_phy