/**
  * Версия 1.
  */
  
module larva_ad7476_ctrl
    #(
        parameter [7:0] BASE_ADDR = 0       
    )
    (
        input clk,
        input nrst,
               
        // ad7476 phy side
        input busy,
        output reg read,
        input [11:0] data,

		// bus side
		input [7:0] address,
		output reg [11:0] data_out,
        output reg wrq,		
		input rrq,
		input bus_busy 
    );

    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    wrq <= 1'b0;
                end
            else
                begin
                    wrq <= 1'b0;
                    if (rrq && address == BASE_ADDR && ~bus_busy && ~wrq)
                        begin
                            wrq <= 1'b1;
                        end
                    
                end
        end
    reg busy_d;
    wire busy_down = ~busy & busy_d;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    busy_d <= 1'b0;
                    data_out <= 12'd0;
                end
            else
                begin
                    busy_d <= busy;
                    read <= 1'b0;
                    if (~busy)
                        begin
                            read <= 1'b1;
                        end
                    if (busy_down)
                        begin
                            data_out <= data;
                        end
                end
        end
endmodule