module larva_ad7476_top
    #(
        parameter [7:0] BASE_ADDRESS = 0
    )
    (
        input clk,
        input nrst,
        
        input [7:0] address,
        input [15:0] data_in,
        output [11:0] data_out,
        input rrq,
        output wrq,
        input data_ready,
        input busy,
        
        output sclk,
        input sdata,
        output ncs
    );
    
    wire phy_busy, read;
    wire [11:0] data;
    larva_ad7476_phy
    #(
        .CLK_CNT(1)
    )
    ad7476_phy0
    (
        .clk(clk),
        .nrst(nrst),
               
        .read(read),
        .busy(phy_busy),
        .data0(data),
        
        .sclk(sclk),
        .sdata0(sdata),
        .ncs(ncs)
    );
    
    larva_ad7476_ctrl
    #(
        .BASE_ADDR(BASE_ADDRESS)
    )
    larva_ad7476_ctrl0
    (
        .clk(clk),
        .nrst(nrst),
               
        // ad7476 phy side
        .busy(phy_busy),
        .read(read),
        .data(data),

		// bus side
		.address(address),
		.data_out(data_out),
        .wrq(wrq),		
		.rrq(rrq),
		.bus_busy(busy) 
    );    
endmodule