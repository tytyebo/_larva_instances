/*
 * Модуль "larva_trx_data_responder" для проекта ft75_main.
 * Приемник служебных данных по RS-232 от передатчика.
 * 
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"
import defines_pkg::RS_START;
import defines_pkg::RS_XMTR_MARK;
import defines_pkg::RS_STOP;
import defines_pkg::xmtrStatus_s;
module larva_xmtr_data_responder(input bit clk, nrst,
                                rs232_io.rx_only rs232,
                                input logic sync,
                                output xmtrStatus_s status);
    
    typedef enum bit [2:0] {ST_START1, ST_START2, ST_U, ST_PWR, ST_TEMP, ST_STOP1, ST_STOP2} state_e;
    state_e state;
    xmtrStatus_s data;
    handshake_io#(.DATA_WIDTH(8)) hndshk(clk); 
    logic rx_error;
    logic [3:0] data_lock;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            state <= ST_START1;
            data <= '0;
            status <= '0;
            hndshk.ready <= 1'b0;
            data_lock <= '0;
        end
        else begin
            hndshk.ready <= 1'b0;
            if (hndshk.valid && !hndshk.ready) begin 
                unique case (state)
                    ST_START1: begin
                        if (hndshk.data == RS_START) begin
                            state <= ST_START2;
                        end
                        else begin
                            state <= ST_START1;
                        end
                        hndshk.ready <= 1'b1;
                    end
                    ST_START2: begin
                        if (hndshk.data == RS_XMTR_MARK) begin
                            state <= ST_U;
                        end
                        else begin
                            state <= ST_START1;
                        end
                    end
                    ST_U: begin
                        data.xmtr_u <= hndshk.data;
                        state <= ST_PWR;
                    end
                    ST_PWR: begin
                        data.xmtr_p <= hndshk.data[3:0];
                        data.xmtr_break[1:0] <= {1'b0, hndshk.data[7]};
                        state <= ST_TEMP;
                    end
                    ST_TEMP: begin
                        data.xmtr_t <= hndshk.data;
                        state <= ST_STOP1;
                    end
                    ST_STOP1: begin
                        if (hndshk.data == RS_START) begin
                            state <= ST_STOP2;
                        end
                        else begin
                            state <= ST_START1;
                        end
                    end
                    ST_STOP2: begin
                        if (hndshk.data == RS_STOP) begin
                            state <= ST_START1;
                            status <= data;
                            data_lock[0] <= 1'b1;
                        end
                        else begin
                            state <= ST_START1;
                        end
                    end
                endcase
                hndshk.ready <= 1'b1;
            end
            
            if (sync) begin
                data_lock <= {data_lock[2:0], 1'b0};
                if (!(|data_lock)) begin
                    status <= '0;
                end
            end
            if (rx_error) begin
                state <= ST_START1;
            end
        end
    end
    
    larva_rs232_rx_phy larvaRs232RxPhy(.clk(clk), .nrst(nrst), .ena(1'b1), .speed_num(4'd8), .rs232(rs232), .hndshk(hndshk), .rx_error(rx_error));
endmodule: larva_xmtr_data_responder
