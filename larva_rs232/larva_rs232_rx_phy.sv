/*
 * Модуль "larva_rs232_rx_phy" для проекта ft75_main и ft75_analog.
 * Контроллер приемник физического уровня RS-232.
 * 
 * Богданов В.А, Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"
module larva_rs232_rx_phy(input bit clk, nrst,
                    input logic ena,
                    input logic [3:0] speed_num,
                    rs232_io.rx_only rs232,
                    handshake_io.master hndshk,                    
                    output logic rx_error);
    
    typedef enum bit [1:0] {ST_IDLE, ST_QRTR_BOD, ST_RECEIVE, ST_SAVE} state_e;
    state_e state;

    logic rx;
    // Регистры мажоритарной схемы
    logic maj_state;
    logic [1:0] maj_count;
    logic [3:0] maj_mass;
    // Регистр принятых данных
    logic [8:0] shift_data;
    // Регистр счетчика принятых данных
    logic [3:0] bit_cnt;
    // Регистр счетчика тактов, применяемы для соблюдения установленной скорости UART
    logic [31:0] bdrate_cnt;
    // Регистры для настройки нужной скорости UART
    logic [11:0][12:0] cnt_pulses;
    logic [12:0] wait_max_time;
    
    // Инициализация констант для работы на частоте 12.582912 МГц
    assign cnt_pulses[0] = 13'd5243; //   2400 bod
    assign cnt_pulses[1] = 13'd2621; //   4800 bod
    assign cnt_pulses[2] = 13'd1748; //   7200 bod
    assign cnt_pulses[3] = 13'd1311; //   9600 bod
    assign cnt_pulses[4] = 13'd874;  //  14400 bod
    assign cnt_pulses[5] = 13'd655;  //  19200 bod
    assign cnt_pulses[6] = 13'd437;  //  28800 bod
    assign cnt_pulses[7] = 13'd328;  //  38400 bod
    assign cnt_pulses[8] = 13'd218;  //  57600 bod
    assign cnt_pulses[9] = 13'd109;  // 115200 bod
    assign cnt_pulses[10] = 13'd55;  // 230400 bod
    assign cnt_pulses[11] = 13'd27;  // 460800 bod
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            state <= ST_IDLE;
            shift_data <= '0;
            bit_cnt <= '0;
            bdrate_cnt <= '0;
            maj_count <= '0;
            hndshk.valid <= 1'b0;
            hndshk.data <= '0;
            maj_state <= 1'b0;
            maj_mass <= '0;
            rx_error <= 1'b0;
        end
        else begin
            if (hndshk.ready) begin // 
                hndshk.valid <= 1'b0;
            end
            rx_error <= 1'b0;
            
            wait_max_time <= cnt_pulses[speed_num][12:2] - 1'b1;
            if (maj_count == '1) begin
                wait_max_time <= cnt_pulses[speed_num][12:2] - 1'b1 + cnt_pulses[speed_num][1:0];
            end
            maj_state <= (maj_mass[0] && maj_mass[1]) || (maj_mass[0] && maj_mass[2]) || (maj_mass[1] && maj_mass[2]);    
            unique case (state)
                ST_IDLE: begin
                    if (rx == 1'b0) begin // возможно тут нужна проверка на !hndshk.valid
                        bit_cnt <= '0;
                        bdrate_cnt <= '0;
                        maj_count <= '0;
                        // Условие на разрешение работы приемника
                        if (ena) begin
                            state <= ST_QRTR_BOD;
                        end
                    end
                end
                ST_QRTR_BOD: begin
                    bdrate_cnt <= bdrate_cnt + 1'b1;
                    if (bdrate_cnt == wait_max_time) begin
                        bdrate_cnt <= '0;
                        maj_mass[maj_count] <= rx;
                        maj_count <= maj_count + 1'b1;
                        if (maj_count == 2'd2) begin
                            if (bit_cnt == 4'd9) begin
                                state <= ST_SAVE;
                            end
                            else begin
                                state <= ST_RECEIVE;
                            end
                        end
                    end
                end
                ST_RECEIVE: begin
                    bdrate_cnt <= bdrate_cnt + 1'b1;
                    bit_cnt <= bit_cnt + 1'b1;
                    shift_data <= {maj_state, shift_data[8:1]};
                    state <= ST_QRTR_BOD;
                    // Проверяется стартовый бит
                    if (bit_cnt == '0) begin
                        if(maj_state) begin
                            state <= ST_IDLE;
                        end
                    end
                end
                ST_SAVE: begin
                    state <= ST_IDLE;
                    // Проверяется стоповый бит
                    if (maj_state) begin
                        hndshk.valid <= 1'b1;
                        hndshk.data <= shift_data[8:1];
                    end
                    else begin
                        rx_error <= 1'b1;
                    end
                end
            endcase
        end
    end
    
    metastable_chain #(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainRs232(.clk(clk), .nrst(nrst), .din(rs232.rx), .dout(rx));
endmodule: larva_rs232_rx_phy
