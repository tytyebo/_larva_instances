/*
 * Модуль "larva_rs232_ctrl" для проекта ft75_digital.
 * Приемник телеметрических данных по RS-232.
 * На вход clk подавать 12,582912 МГц.
 * 
 * Богданов В.А., Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
module larva_rs232_ctrl#(parameter bit [7:0] BASE_ADR = '0)
                        (input bit clk, nrst,
                         rs232_io.rx_only rs232,
                         input logic sync,
                         generic_bus_io.slave_hndshk data_bus);
                         
    handshake_io#(.DATA_WIDTH(8)) hndshk(clk);
    logic fifo_clear, fifo_rrq, enable;
    logic [1:0] ctrl_mode;
    logic [3:0] uart_speed;
    logic [9:0] debug_cnt;
    logic [9:0] fifo_data_out;
    logic fifo_full, fifo_empty, miss_data, rx_error;
    logic [15:0] missing_data_counter, data_error_counter;
    
    // Обработка сообщений от КШВ
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.ready <= 1'b0;
            fifo_rrq <= 1'b0;
            missing_data_counter <= 16'd0;
            fifo_clear <= 1'b1;
            data_bus.dat_miso <= 16'd0;
            enable <= 1'b0;
            ctrl_mode <= 2'd0;
            debug_cnt <= BASE_ADR;
        end
        else begin
            data_bus.ready <= 1'b0;
            fifo_rrq <= 1'b0;
            fifo_clear <= 1'b0;
            
            if (data_bus.valid && !data_bus.ready) begin
                data_bus.ready <= 1'b1;
                if (!data_bus.we) begin                
                    unique case (data_bus.adr)
                        BASE_ADR: begin
                            data_bus.dat_miso <= {13'd0, ctrl_mode, enable};
                        end
                        BASE_ADR + 8'd1: begin
                            data_bus.dat_miso <= {12'd0, uart_speed};
                        end
                        BASE_ADR + 8'd2: begin
                            unique case (ctrl_mode)
                                2'd0: begin
                                    if (fifo_empty) begin
                                        data_bus.dat_miso <= '0;
                                    end
                                    else begin
                                        fifo_rrq <= 1'b1;
                                        data_bus.dat_miso <= {6'd0, fifo_data_out};
                                    end
                                end
                                2'd1: begin
                                    data_bus.dat_miso <= '0;
                                end
                                2'd2: begin
                                    data_bus.dat_miso <= '1;
                                end
                                2'd3: begin
                                    data_bus.dat_miso <= debug_cnt;
                                    debug_cnt <= debug_cnt + 1'b1;
                                end
                            endcase
                        end
                        BASE_ADR + 8'd3: begin
                            data_bus.dat_miso <= missing_data_counter;
                        end
                        BASE_ADR + 8'd4: begin
                            data_bus.dat_miso <= data_error_counter;
                        end
                        default: begin
                            data_bus.dat_miso <= '0;
                            data_bus.ready <= 1'b0;
                        end
                    endcase
                end
                else begin
                    unique case (data_bus.adr)
                        BASE_ADR: begin
                            {ctrl_mode, enable} <= data_bus.dat_mosi[2:0]; // Настройка параметров работы
                        end
                        BASE_ADR + 8'd1: begin
                            uart_speed <= data_bus.dat_mosi[3:0]; // Настройка скорости RS232
                        end
                        BASE_ADR + 8'd5: begin
                            fifo_clear <= data_bus.dat_mosi[0];
                        end
                        default: begin
                            data_bus.ready <= 1'b0;
                        end
                    endcase
                end
            end
            
            if (miss_data) begin
                missing_data_counter <= missing_data_counter + 1'b1;
            end
            if (rx_error) begin
                data_error_counter <= data_error_counter + 1'b1;
            end
        end
    end
        
    logic sync_ena;
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            sync_ena <= 1'b0;
        end
        else begin
            if (sync) begin
                sync_ena <= 1'b1;
            end
        end
    end
    
    larva_rs232_rx_phy larvaRs232RxPhy(.clk(clk), .nrst(nrst), .ena(enable & sync_ena), .speed_num(uart_speed), .rs232(rs232), .hndshk(hndshk), .rx_error(rx_error));
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            hndshk.ready <= 1'b0;
            miss_data <= 1'b0;
        end
        else begin
            hndshk.ready <= 1'b0;
            if (hndshk.valid && !hndshk.ready) begin
                if (fifo_full) begin
                    miss_data <= 1'b1;
                end
                else begin
                    hndshk.ready <= 1'b1;
                end
            end
        end
    end

    larva_generic_fifo larvaFifoRs232(.aclr(~nrst | fifo_clear),
                                      .clock(clk),
                                      .data({hndshk.data, 2'd0}),
                                      .rdreq(fifo_rrq),
                                      .wrreq(hndshk.ready),
                                      .empty(fifo_empty),
                                      .full(fifo_full),
                                      .q(fifo_data_out));
endmodule: larva_rs232_ctrl
