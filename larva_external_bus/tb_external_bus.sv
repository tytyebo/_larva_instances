`timescale 1ps/1ps
module tb_external_bus(); 

reg clk;
reg nrst;
reg wr = 0;
reg rd = 0;
reg [7:0] address;
reg [7:0] burst_len;
reg [7:0] saddress;
reg [7:0] sburst_len;
reg [15:0] mdata;
reg [8:0] rd_cnt;
reg [15:0] sdata_in;
reg swrq;
reg [1:0] state;
localparam  ST_IDLE = 0,
            ST_ANSWER = 1,
            ST_WAIT = 2;

wire nwrm;
wire nwrs;
wire nale;
wire mbusy;
wire sbusy;
wire mdready;
wire sdready;
wire rd_req;
wire [15:0] adata;
wire srrq;
    
/**
  * @brief  Формирование сигнала тактовой частоты "clk".
  */ 
    initial 
        begin
            clk <= 0;
            forever 
                begin
                    #10 clk <= ~clk;
                end
		end
		
    initial 
        begin
            nrst <= 0;
            #100
            nrst <= 1;
        end
		
	task write_data
        (
            input [7:0] wrd_address,
            input [15:0] wrd_data
        );
        begin
            address = wrd_address;
			burst_len = 8'd0;
            mdata = wrd_data;
            @(posedge clk) wr = 1'b1;                           
            @(posedge clk) wr = 1'b0;
        end
    endtask
	
    task read_data
        (
            input [7:0] rdd_address,
            input [7:0] rdd_burst_len
        );
        begin
            address = rdd_address;
			burst_len = rdd_burst_len;
            @(posedge clk) rd = 1'b1;                           
            @(posedge clk) rd = 1'b0;
        end
    endtask
    
	initial 
		begin
            #110
			write_data(8'h55, 16'h2280);
            #300
            write_data(8'h55, 16'h2281);
            #300
            read_data(8'h33, 8'hF);
		end 
		
	always @(posedge clk, negedge nrst)
		begin
            if (~nrst)
                begin
                    rd_cnt <= 9'd0;
                    sdata_in <= 16'd0;
                    swrq <= 1'b0;
                    state <= ST_IDLE;
                end
            else
                begin
                    swrq <= 1'b0;
                    case (state)
                        ST_IDLE:
                            begin
                                if (srrq)
                                    begin 
                                        rd_cnt <= burst_len + 1'b1;
                                        if (~sbusy)
                                            begin
                                                swrq <= 1'b1;
                                                sdata_in <= 16'd0;
                                                state <= ST_WAIT;
                                            end
                                    end
                            end
                        ST_WAIT:
                            begin
                                if (sbusy)
                                    begin
                                        state <= ST_ANSWER;
                                        sdata_in <= sdata_in + 1'b1;
                                        rd_cnt <= rd_cnt - 1'b1;
                                    end
                            end
                        ST_ANSWER:
                            begin
                                if (~sbusy)
                                    begin   
                                        state <= ST_WAIT;
                                    end
                                if (rd_cnt == 8'd0)
                                    state <= ST_IDLE;
                                else
                                    swrq <= 1'b1;
                            end                
                    endcase

                        
                end
		end
    
    larva_extbus_master ebm0
	(
		.clock(clk),
		.nreset(nrst),
		
		// external bus side
		.nwrs(nwrs),	
		.adata(adata),	
		.nwrm(nwrm),
		.nale(nale),
		
		// internal side
		
		.address(address),	// address, burst_len and data_in must be set until busy is'n down
		.burst_len(burst_len), 	// len = burst_len + 1, i.e. if burst_len = 0,then will be read 1 cycle
								// in WRITE transactions burst_len is ignore
		.data_in(mdata),
		.wrq(wr),				// 1 clock period length
		.rrq(rd),				// 1 clock period length
		.data_out(),	// when data_ready is active (up) data_out is valid
		.busy(mbusy),			// operating with controller allow only when busy is non active (down)	
		.data_ready(mdready)		// 1 clock period length
	);
    
    larva_extbus_slave ebs0
	(
		.clock(clk),
		.nreset(nrst),
		
		// external bus side
		.nwrm(nwrm),
		.nale(nale),		
		
		.adata(adata),
		
		.nwrs(nwrs),		
		
		// internal side
		
		.address(),
		.burst_len(),

		.data_in(sdata_in),
		.wrq(swrq),				// 1 clock period length
		
		.rrq(srrq),				// sets until wr not be is active
		
		.data_out(),	// when data_ready is active (up) data_out is valid
		.busy(sbusy),			// operating with controller allow only when busy is non active (down)	
		.data_ready(sdready)		// 1 clock period length
	);
endmodule