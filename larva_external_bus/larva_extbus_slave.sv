`include "defines.svh"
`include "interfaces.svh"
module larva_extbus_slave#(parameter int CYCLE_OPEN = 1,    // 1 is min and default value
                                         CYCLE_ACTIVE = 2, // 2 is min and default value
                                         CYCLE_CLOSE = 1) 	// 1 is min and default value   
                                                            // total max capacity must be less that 16
                           (input bit clk, nrst,
                            // external bus side
                            extbus_io.slave extbus,                           
                            // internal side
                            generic_bus_io.master_hndshk data_bus);
	
	typedef enum bit [1:0] {ST_IDLE, ST_OPEN, ST_ACTIVE, ST_CLOSE} state_e;
	state_e state;
	
	logic [3:0] cyc_cnt;
	logic [15:0] adata_meta;
    
    always_ff @(posedge clk or negedge nrst) begin
		if (!nrst) begin				
            state <= ST_IDLE;
            extbus.oe_slave <= 1'b0;                
            cyc_cnt <= '0;            
            extbus.nwr_miso <= 1'b1;
            extbus.adata_miso <= '0;
		end
		else begin
            unique case (state)
                ST_IDLE: begin
                    if (data_bus.ready && !data_bus.we) begin
                        extbus.adata_miso <= data_bus.dat_miso;   
                        cyc_cnt <= cyc_cnt + 1'b1;
                        extbus.oe_slave <= 1'b1;
                        state <= ST_OPEN;
                    end
                end
                ST_OPEN: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == CYCLE_OPEN) begin
                        extbus.nwr_miso <= 1'b0;
                        state <= ST_ACTIVE;										
                    end
                end
                ST_ACTIVE: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == (CYCLE_OPEN + CYCLE_ACTIVE)) begin
                        extbus.nwr_miso <= 1'b1;
                        state <= ST_CLOSE;
                    end
                end
                ST_CLOSE: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == (CYCLE_OPEN + CYCLE_ACTIVE + CYCLE_CLOSE)) begin
                        extbus.oe_slave <= 1'b0;
                        cyc_cnt <= 4'd0;
                        state <= ST_IDLE;								
                    end
                end		
            endcase
        end
	end
	
    logic nwrm_meta_d, nale_meta_d;
	logic is_wrm, is_ale, nale_meta, nwrm_meta, dir_d;	 
	assign is_ale = ~nale_meta & nale_meta_d;
	assign is_wrm = ~nwrm_meta & nwrm_meta_d;
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.we <= 1'b0;   
            data_bus.dat_mosi <= '0;
            nwrm_meta_d <= 1'b0;
            nale_meta_d <= 1'b0;
            data_bus.valid <= 1'b0;
            data_bus.adr <= '0;
        end
        else begin
            nwrm_meta_d <= nwrm_meta;
            nale_meta_d <= nale_meta;	
            if (is_ale) begin
                data_bus.adr <= adata_meta[7:0];
                if (adata_meta[15:8] != '0) begin
                    data_bus.valid <= 1'b1;
                    data_bus.we <= 1'b0;
                end
            end
            if (is_wrm && !dir_d) begin
                data_bus.valid <= 1'b1;
                data_bus.we <= 1'b1;    
                data_bus.dat_mosi <= adata_meta;                       
            end
            
            if (data_bus.ready) begin
                data_bus.valid <= 1'b0;
                data_bus.we <= 1'b0;
            end
        end
    end

    metastable_chain#(.CHAIN_DEPTH(3), .BUS_WIDTH(3 + 16)) 
    metaChainExtBus(.clk(clk), .nrst(1'b1), .din({ extbus.nwr_mosi, extbus.nale, extbus.oe_slave, extbus.adata_mosi }), .dout({nwrm_meta, nale_meta, dir_d, adata_meta}));
endmodule
