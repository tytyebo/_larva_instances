`include "defines.svh"
`include "interfaces.svh"
module larva_extbus2wb_bridge(input bit clk, nrst,
                              // external bus side
                              extbus_io.master extbus,                                     
                              // slave wishbone side
                              wishbone_io.slave ext_wbs,
                              wishbone_io.slave mem_wbs,
                                
                              input logic [1:0] s_flow,
                              output logic [1:0] s_flow_mstb,
                              input logic selftest);
                                
    typedef enum bit [2:0] {ST_IDLE, ST_WAIT_BUSY, ST_WAIT_READY, ST_WAIT_NOT_BUSY, ST_WAIT_MULT} state_e;
    state_e state;
    logic is_read, weigher_ena, busy_d;
    logic [9:0] data_weighed;
    generic_bus_io#(.ADR_WIDTH(8), .MOSI_WIDTH(16), .MISO_WIDTH(16)) extbus_bus();
    
    generic_bus_io#(.ADR_WIDTH(8), .MOSI_WIDTH(16), .MISO_WIDTH(16)) weigher_bus();
    generic_bus_io#(.ADR_WIDTH(8), .MOSI_WIDTH(32), .MISO_WIDTH(32)) mem_bus();
    
    assign weigher_bus.valid = busy_d & ~extbus_bus.busy;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            ext_wbs.wbs_ack <= 1'b0;
            state <= ST_IDLE;
            extbus_bus.we <= 1'b0;
            extbus_bus.re <= 1'b0;
            extbus_bus.adr <= '0;
            extbus_bus.dat_mosi <= '0;
            is_read <= 1'b0;
            ext_wbs.wbs_dat <= '0;
        end
        else begin
            extbus_bus.we <= 1'b0;
            extbus_bus.re <= 1'b0;
            ext_wbs.wbs_ack <= 1'b0;
            busy_d <= extbus_bus.busy;
            unique case (state)
                ST_IDLE: begin
                    if (ext_wbs.wbm_cyc && ext_wbs.wbm_stb && !ext_wbs.wbs_ack) begin
                        extbus_bus.adr <= ext_wbs.wbm_adr[7:0];
                        case (ext_wbs.wbm_sel[3:0])
                            4'd1: begin
                                extbus_bus.dat_mosi <= { 8'd0, ext_wbs.wbm_dat[7:0] };
                            end
                            4'd2: begin
                                extbus_bus.dat_mosi <= { ext_wbs.wbm_dat[7:0], 8'd0 };
                            end
                            default: begin
                                extbus_bus.dat_mosi <= ext_wbs.wbm_dat[15:0];
                            end
                        endcase
                        
                        if (ext_wbs.wbm_we) begin
                            extbus_bus.we <= 1'b1;
                        end
                        else begin
                            extbus_bus.re <= 1'b1;
                            is_read <= 1'b1;
                        end
                        state <= ST_WAIT_BUSY;
                    end
                end
                ST_WAIT_BUSY: begin
                    if (extbus_bus.busy) begin                                   
                        if (is_read) begin
                            state <= ST_WAIT_READY;
                            is_read <= 1'b0;
                        end
                        else begin
                            state <= ST_WAIT_NOT_BUSY;
                        end
                    end
                end
                ST_WAIT_READY: begin
                    if (!extbus_bus.busy) begin
                        unique case (ext_wbs.wbm_sel[3:0])
                            4'd1: begin
                                ext_wbs.wbs_dat <= { 24'd0, extbus_bus.dat_miso[7:0] };
                            end
                            4'd2: begin
                                ext_wbs.wbs_dat <= { 16'd0, extbus_bus.dat_miso[15:8], 8'd0 };
                            end
                            default: begin
                                ext_wbs.wbs_dat <= { 16'd0, extbus_bus.dat_miso[15:0] };
                            end
                        endcase
                        if (weigher_ena) begin
                            state <= ST_WAIT_MULT;
                        end
                        else begin
                            ext_wbs.wbs_ack <= 1'b1;
                            state <= ST_IDLE;
                        end
                    end
                end
                ST_WAIT_MULT: begin
                    if (weigher_bus.ready) begin
                        if (!selftest) begin
                            ext_wbs.wbs_dat <= { ext_wbs.wbs_dat[15:0], 6'd0, data_weighed};
                        end
                        else begin
                            ext_wbs.wbs_dat <= { ext_wbs.wbs_dat[15:0], 7'd0, extbus_bus.dat_miso[8:0]};
                        end
                        ext_wbs.wbs_ack <= 1'b1;
                        state <= ST_IDLE;
                    end
                end
                ST_WAIT_NOT_BUSY: begin
                    if (!extbus_bus.busy) begin
                        ext_wbs.wbs_ack <= 1'b1;
                        state <= ST_IDLE;
                    end
                end
            endcase
        end
    end
    
    typedef enum bit {ST_MEM_WR, ST_MEM_RD} mem_state_e;
    mem_state_e mem_state;
    logic [7:0] direct_adr;
    logic weigher_mode;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            mem_wbs.wbs_ack <= 1'b0;
            mem_wbs.wbs_dat <= '0;
            mem_state <= ST_MEM_WR;
            direct_adr <= '0;
            mem_bus.we <= 1'b0;
            weigher_mode <= 1'b0;
        end
        else begin
            mem_wbs.wbs_ack <= 1'b0;
            mem_bus.we <= 1'b0;
            unique case (mem_state)
                ST_MEM_WR: begin
                    if (mem_wbs.wbm_cyc && mem_wbs.wbm_stb && !mem_wbs.wbs_ack) begin
                        direct_adr <= mem_wbs.wbm_adr[7:0];
                        if (mem_wbs.wbm_we) begin
                            if (mem_wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == 12'h100) begin // weigher memory switcher extbus_bus.adr
                                weigher_mode <= mem_wbs.wbm_dat[0];
                            end
                            else begin
                                mem_bus.we <= 1'b1;
                            end
                            mem_wbs.wbs_ack <= 1'b1;
                        end
                        else begin
                            if (mem_wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == 12'h100) begin // weigher memory switcher extbus_bus.adr
                                mem_wbs.wbs_dat <= {31'd0, weigher_mode};
                                mem_wbs.wbs_ack <= 1'b1;
                            end
                            else begin
                                mem_state <= ST_MEM_RD;
                            end
                        end
                    end
                end
                ST_MEM_RD: begin
                    if (mem_bus.ready) begin
                        mem_wbs.wbs_ack <= 1'b1;
                        mem_wbs.wbs_dat <= mem_bus.dat_miso;
                        mem_state <= ST_MEM_WR;
                    end
                end
            endcase
        end
    end
        
    always_comb begin
        if (extbus_bus.adr >= `ANP0_BASE_ADR && extbus_bus.adr <= (`ANP1_TREG_BASE - 1'b1)) begin // 0x8 - 0x5D
            weigher_ena <= 1'b1;
        end
        else begin
            weigher_ena <= 1'b0;
        end
    end

    assign weigher_bus.adr = { weigher_mode, extbus_bus.adr[6:0] - 7'd8 };
    assign mem_bus.adr = direct_adr;
    assign weigher_bus.dat_mosi = extbus_bus.dat_miso;
    assign mem_bus.dat_mosi =  mem_wbs.wbm_dat;
    larva_weigher larvaWeigher(.clk(clk), .nrst(nrst), .ena(weigher_ena), .weigher_bus(weigher_bus), .mem_bus(mem_bus));

    
    larva_signal_flow_adder larvaSignalFlowAdder(.clk(clk), .nrst(nrst), .s_flow(s_flow), .s_flow_mstb(s_flow_mstb), .adr(extbus_bus.adr[6:0]), .data_in(weigher_bus.dat_miso[8:0]), .data_out(data_weighed));
    
    larva_extbus_master larvaExtbusMaster(.clk(clk), .nrst(nrst), .extbus(extbus), .data_bus(extbus_bus)); 
endmodule: larva_extbus2wb_bridge
