module larva_extbus_slave_mux(input bit [7:0] sel0, sel1, sel2, sel_adr,
                              input logic [15:0] mux_in0, mux_in1, mux_in2,
                              input logic ready_in0, ready_in1, ready_in2,
                              output logic [15:0] mux_out,
                              output logic ready_out);
    always_comb begin
        if (sel_adr >= sel0 && sel_adr < sel1) begin
            mux_out <= mux_in0;
            ready_out <= ready_in0;
        end
        else begin
            if (sel_adr >= sel1 && sel_adr < sel2) begin
                mux_out <= mux_in1;
                ready_out <= ready_in1;
            end
            else begin
                if (sel_adr >= sel2) begin
                    mux_out <= mux_in2;
                    ready_out <= ready_in2;
                end
                else begin
                    mux_out <= '0;
                    ready_out <= 1'b0;
                end
            end
        end
    end
endmodule: larva_extbus_slave_mux