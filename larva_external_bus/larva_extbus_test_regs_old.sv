/**
  * ������ 2. 29.09.2020
  * ������ ��������� BASE_ADDRESS �� ���� base_adr ��� ���������� �������� ������� ��� � ���-01.
  * ������ 1. 17.06.2019
  * ������ � ������� �������� ��������� ��� �������� ������������ ����� � ������� ������� (������) (larva_external_bus).
  * ���������� ��� ���� Actel A3P600-PQ208I.
  * �������� BASE_ADDRESS ������ ������� ����� ������ � �������� ������������ ������������ ���� larva_external_bus.
  * ������ �������� ��� �������������������� ��������:
  * - ������� reg0 �������� ��������� 16'h55AA, �������� ������ ��� ������, �������� 0;
  * - ������� reg1 �������� ��������� 16'hAA55, �������� ������ ��� ������, �������� 1;
  * - ������� reg2 ����� ������ �������� 0, �������� ������ ��� ������ � ������, �������� 2.
  * ��� ������� ������ ��������������� ������ ����� �������� 0.
  */
module larva_extbus_test_regs(input bit clk, nrst,
                              input logic [7:0] base_adr, adr,
                              input logic [15:0] data_in,
                              output logic [15:0] data_out,
                              input logic rrq, data_ready, busy,
                              output logic wrq);
        
    logic [15:0] reg0;
    logic [15:0] reg1;
    logic [15:0] reg2;
    logic is_rrq;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            reg0 <= 16'd0;
            reg1 <= 16'd0;
            reg2 <= 16'd0;
            data_out <= 16'd0;
            is_rrq <= 1'b0;
            wrq <= 1'b0;
        end
        else begin
            wrq <= 1'b0;
            reg0 <= 16'h55AA;
            reg1 <= 16'hAA55;
            if (data_ready && adr == (base_adr + 8'd2)) begin
                reg2 <= data_in;
            end                
            if (rrq) begin
                unique case (adr)
                    (base_adr + 8'd0): begin
                        data_out <= reg0;
                        if (!busy) begin
                            wrq <= 1'b1;
                        end
                        else begin
                            is_rrq <= 1'b1;
                        end
                    end
                    (base_adr + 8'd1): begin
                        data_out <= reg1;
                        if (!busy) begin
                            wrq <= 1'b1;
                        end
                        else begin
                            is_rrq <= 1'b1;
                        end
                    end
                    (base_adr + 8'd2): begin
                        data_out <= reg2;
                        if (!busy) begin
                            wrq <= 1'b1;
                        end
                        else begin
                            is_rrq <= 1'b1;
                        end
                    end
                    default: begin
                        wrq <= 1'b0;
                        is_rrq <= 1'b0;
                    end
                endcase                        
            end
                    
            if (is_rrq && !busy) begin
                wrq <= 1'b1;
                is_rrq <= 1'b0;
            end                 
        end
    end
endmodule: larva_extbus_test_regs