/*
 * Модуль "larva_weigher" для проекта ft75_main.
 * Cхема взвешивания аналоговых данные от модулей ft75_analog
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"
module larva_weigher(input bit clk, nrst,
                     input logic ena,
                     generic_bus_io.slave_hndshk weigher_bus,
                     generic_bus_io.slave_hndshk mem_bus);

    typedef enum bit [1:0] {ST_MULT, ST_SUM, ST_CUT} state_e;
    state_e weigher_state;
    typedef enum bit [1:0] {ST_IDLE, ST_RD_WAIT, ST_RD} mem_state_e;
    mem_state_e mem_state;
    logic [31:0] mult, mem_qa, mem_qb;
    logic signed [17:0] pre_result;
    logic [15:0] k_factor, b_factor, data_in_l, k_factor_l;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            weigher_bus.dat_miso <= '0;
            weigher_bus.ready <= 1'b0;
            pre_result <= '0;
            weigher_state <= ST_MULT;
        end
        else begin
            weigher_bus.ready <= 1'b0;
            case (weigher_state)
                ST_MULT: begin
                    if (weigher_bus.valid && ena) begin
                        if (weigher_bus.dat_mosi == '0) begin
                            weigher_bus.dat_miso <= 9'd511;
                            weigher_bus.ready <= 1'b1;
                        end
                        else begin
                            if (weigher_bus.dat_mosi == '1) begin
                                weigher_bus.dat_miso <= 9'd0;
                                weigher_bus.ready <= 1'b1;
                            end
                            else begin
                                weigher_state <= ST_SUM;
                            end
                        end
                    end
                end
                ST_SUM: begin
                    pre_result <= b_factor - mult[31:16];
                    weigher_state <= ST_CUT;
                end
                ST_CUT: begin
                    if (pre_result < $signed(18'd0)) begin
                        weigher_bus.dat_miso = '0;
                    end
                    else begin
                        if (pre_result > $signed({3'd0, 9'd511, 6'b000000})) begin
                            weigher_bus.dat_miso <= 9'd511;
                        end
                        else begin
                            weigher_bus.dat_miso <= pre_result[14:6];
                        end
                    end
                    weigher_bus.ready <= 1'b1;
                    weigher_state <= ST_MULT;
                end
            endcase
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            mem_bus.dat_miso <= '0;
            mem_bus.ready <= 1'b0;
            mem_state <= ST_IDLE;
        end
        else begin
            mem_bus.ready <= 1'b0;
            case (mem_state)
                ST_IDLE: begin
                    if (mem_bus.we) begin
                        mem_state <= ST_RD_WAIT;
                    end
                end
                ST_RD_WAIT: begin
                    mem_state <= ST_RD;
                end
                ST_RD: begin
                    mem_bus.dat_miso <= mem_qb;
                    mem_bus.ready <= 1'b1;
                    mem_state <= ST_IDLE;
                end
            endcase
        end
    end
                
    assign data_in_l = (weigher_bus.valid) ? weigher_bus.dat_mosi : data_in_l;
    assign k_factor_l = (weigher_bus.valid) ? k_factor : k_factor_l;
    assign k_factor = mem_qa[31:16];
    assign b_factor = mem_qa[15:0];
    larva_weigher_mult larvaWeigherMult(.dataa(data_in_l), .datab(k_factor_l), .result(mult)); // larvaWeigherMult is unsigned mult
    larva_weigher_mem larvaWeigherMem(.clock(clk), .address_a(weigher_bus.adr), .data_a('0), .wren_a('0), .q_a(mem_qa),
                                      .address_b(mem_bus.adr), .data_b(mem_bus.dat_mosi), .wren_b(mem_bus.we), .q_b(mem_qb)); 
endmodule: larva_weigher
