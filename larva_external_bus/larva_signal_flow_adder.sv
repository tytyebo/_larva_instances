/*
 * Модуль "larva_signal_flow_adder" для проекта ft75_main.
 * Cхема добавления данных сигнальных датчиков СГП в слова потоков АНП.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
module larva_signal_flow_adder(input bit clk, nrst,
                               input logic [1:0] s_flow,
                               input logic [6:0] adr,
                               input logic [8:0] data_in,                                   
                               output logic [9:0] data_out,
                               output logic [1:0] s_flow_mstb);
                               
    metastable_chain#(.CHAIN_DEPTH(3), .BUS_WIDTH(2)) 
    sFlowMeta(.clk(clk), .nrst(nrst),
              .din(s_flow), .dout(s_flow_mstb));
    
    always_comb begin
        if (adr >= `S1_FLOW_BASE && adr < `S2_FLOW_BASE) begin
            data_out <= {data_in, s_flow_mstb[0]};
        end
        else begin
            data_out <= {data_in, s_flow_mstb[1]};
        end
    end
endmodule: larva_signal_flow_adder
