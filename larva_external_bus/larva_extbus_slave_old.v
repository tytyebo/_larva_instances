module larva_extbus_slave_old
    #(
	parameter cycle_open = 1,	// 1 is min and default value
	parameter cycle_active = 2, // 2 is min and default value
	parameter cycle_close = 1 	// 1 is min and default value   
								// total max capacity must be less that 16
    )
	(
		input clock,
		input nreset,
		
		// external bus side
		input nale,		
		
		inout[15:0] adata,
		
		inout nwrs,		
		
		// internal side
		
		output reg [7:0] address,

		input [15:0] data_in,
        input wrq,				// 1 clock period length
		
		output reg rrq,				// sets until wr not be is active
		
		output reg [15:0] data_out,	// when data_ready is active (up) data_out is valid
		output busy,			// operating with controller allow only when busy is non active (down)	
		output reg data_ready		// 1 clock period length
	);
	
	localparam 	ST_IDLE = 0,
				ST_OPEN = 1,
				ST_ACTIVE = 2,
				ST_CLOSE = 3;
				
	reg [1:0] state;
	reg dir;
    reg nwrs_reg;
	reg [3:0] cyc_cnt;
	reg [15:0] adata_out;
    
	wire [15:0] adata_meta;
	assign adata = (dir) ? adata_out : 16'dz;
    assign nwrs = (dir) ? nwrs_reg : 1'bz;
    assign busy = dir;
always @(posedge clock or negedge nreset)
	begin
		if (~nreset)
			begin				
				state <= ST_IDLE;
				dir <= 1'b0;                
				cyc_cnt <= 4'd0;
                adata_out <= 16'd0;               
                nwrs_reg <= 1'b1;
			end
		else
			begin
                case (state)
                    ST_IDLE:
                        begin
                            if (wrq)
                                begin
                                    adata_out <= data_in;
                                        
                                    cyc_cnt <= cyc_cnt + 1'b1;
                                    dir <= 1'b1;
                                    state <= ST_OPEN;
                                end
                        end
                    ST_OPEN:
                        begin
                            cyc_cnt <= cyc_cnt + 1'b1;
                            if (cyc_cnt == cycle_open)
                                begin
                                    nwrs_reg <= 1'b0;
                                    state <= ST_ACTIVE;										
                                end
                        end
                    ST_ACTIVE:
                        begin
                            cyc_cnt <= cyc_cnt + 1'b1;
                            if (cyc_cnt == (cycle_open + cycle_active))
                                begin
                                    nwrs_reg <= 1'b1;
                                    state <= ST_CLOSE;
                                end
                        end
                    ST_CLOSE:
                        begin
                            cyc_cnt <= cyc_cnt + 1'b1;
                            if (cyc_cnt == (cycle_open + cycle_active + cycle_close))
                                begin
                                    dir <= 1'b0;
                                    cyc_cnt <= 4'd0;
                                    state <= ST_IDLE;								
                                end
                        end		
                endcase
            end
	end
	
    reg nwrm_meta_d;
	reg	nale_meta_d;	
     
    wire is_wrm;
	wire is_ale;
    wire nale_meta;
    wire nwrm_meta;
    wire dir_d;
    
	assign is_ale = ~nale_meta & nale_meta_d;
	assign is_wrm = ~nwrm_meta & nwrm_meta_d;
    always @(posedge clock or negedge nreset)
        begin
            if (~nreset)
                begin
                    data_ready <= 1'b0;   
                    data_out <= 16'd0;
                    nwrm_meta_d <= 1'b0;
                    nale_meta_d <= 1'b0;
                    rrq <= 1'b0;
                    address <= 8'd0;
                end
            else
                begin
                    nwrm_meta_d <= nwrm_meta;
                    nale_meta_d <= nale_meta;	
                    data_ready <= 1'b0;
                    rrq <= 1'b0;
                    if (is_ale)
                        begin
                            address <= adata_meta[7:0];
                            if (adata_meta[15:8] != 8'd0)
                                rrq <= 1'b1;
                        end
                    if (is_wrm && !dir_d)
                        begin
                            data_ready <= 1'b1;    
                            data_out <= adata_meta;                       
                        end
                end
        end
        
        
    metastable_chain
        #(
            .CHAIN_DEPTH(3),
            .BUS_WIDTH(3 + 16)
        ) 
    metastable_chain_record
        (
            .clk(clock),
            .nrst(1'b1),
            .din({ nwrs, nale, dir, adata }),
            .dout( {nwrm_meta, nale_meta, dir_d, adata_meta })
        ); 
endmodule
