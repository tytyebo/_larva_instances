/**
  * ������ 4. 26.11.2021
  * �������� ������� � ������� ������ ��������.
  * ������ 3. 30.04.2021
  * ������ ��������� �� ��������� generic_bus_io.slave_hndshk.
  * ������ 2. 29.09.2020
  * ������ ��������� BASE_ADDRESS �� ���� base_adr ��� ���������� �������� ������� ��� � ���-01.
  * ������ 1. 17.06.2019
  * ������ � ������� �������� ��������� ��� �������� ������������ ����� � ������� ������� (������) (larva_external_bus).
  * ���������� ��� ���� Actel A3P600-PQ208I.
  * �������� BASE_ADDRESS ������ ������� ����� ������ � �������� ������������ ������������ ���� larva_external_bus.
  * ������ �������� ��� �������������������� ��������:
  * - ������� reg0 �������� ��������� 16'h55AA, �������� ������ ��� ������, �������� 0;
  * - ������� reg1 �������� ��������� 16'hAA55, �������� ������ ��� ������, �������� 1;
  * - ������� reg2 ����� ������ �������� 0, �������� ������ ��� ������ � ������, �������� 2.
  * ��� ������� ������ ��������������� ������ ����� �������� 0.
  */
`include "interfaces.svh"
`include "defines.svh"
module larva_extbus_test_regs(input bit clk, nrst,
                                 input logic [7:0] base_adr, 
                                 generic_bus_io.slave_hndshk data_bus);

    logic [15:0] reg0, reg1, reg2;
    logic is_rrq;
    logic [31:0] version;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            reg0 <= 16'd0;
            reg1 <= 16'd0;
            reg2 <= 16'd0;
            data_bus.dat_miso <= 16'd0;
            data_bus.ready <= 1'b0;
        end
        else begin
            reg0 <= 16'h55AA;
            reg1 <= 16'hAA55;
            data_bus.ready <= 1'b0;
            if (data_bus.valid && !data_bus.ready) begin
                data_bus.ready <= 1'b1;
                if (!data_bus.we) begin
                    unique case (data_bus.adr)
                        (base_adr + 8'd0): begin
                            data_bus.dat_miso <= reg0;                        
                        end
                        (base_adr + 8'd1): begin
                            data_bus.dat_miso <= reg1;
                        end
                        (base_adr + 8'd2): begin
                            data_bus.dat_miso <= reg2;
                        end
                        // ����� base_adr + 8'd4 ����� ������������� ��������
                        (base_adr + 8'd6): begin
                            data_bus.dat_miso <= version[15:0];
                        end
                        (base_adr + 8'd7): begin
                            data_bus.dat_miso <= version[31:16];
                        end
                        default: begin
                            data_bus.dat_miso <= '0;
                            data_bus.ready <= 1'b0;
                        end
                    endcase   
                end
                else begin
                    if (data_bus.we && data_bus.adr == (base_adr + 8'd2)) begin
                        reg2 <= data_bus.dat_mosi;
                    end
                    else begin
                        data_bus.ready <= 1'b0;
                    end 
                end
            end           
        end
    end
    
    version_reg versionReg(version);
endmodule: larva_extbus_test_regs