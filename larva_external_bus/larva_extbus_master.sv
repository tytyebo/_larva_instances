`include "defines.svh"
`include "interfaces.svh"
module larva_extbus_master#(parameter int CYCLE_OPEN = 1,	// 1 is min and default value
                                          CYCLE_ACTIVE = 2, // 2 is min and default value
                                          CYCLE_CLOSE = 1) 	// 1 is min and default value   
                                                            // total max capacity must be less that 16
                           (input bit clk, nrst,
                            // external bus side
                            extbus_io.master extbus,                           
                            // internal side
                            generic_bus_io.slave data_bus);
    
    typedef enum bit [1:0] {ST_IDLE, ST_OPEN, ST_ACTIVE, ST_CLOSE} state_e;
    state_e state;
				
	logic is_wr;
	logic is_rd;
	
	logic [3:0] cyc_cnt;
    logic nwrs_meta_d;
	wire nwrs_meta;
	wire [15:0] adata_meta;
    wire is_wrs;
    assign is_wrs = ~nwrs_meta & nwrs_meta_d;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            state <= ST_IDLE;        
            extbus.oe_master <= 1'b0;
            data_bus.busy <= 1'b0;
            
            extbus.nwr_mosi <= 1'b1;
            extbus.nale <= 1'b1;

            is_wr <= 1'b0;
            is_rd <= 1'b0;
            
            cyc_cnt <= 4'd0;
            data_bus.dat_miso <= '0;
            nwrs_meta_d <= 1'b0;
            extbus.adata_mosi <= '0;
        end
        else begin
            unique case (state)
                ST_IDLE: begin
                    cyc_cnt <= '0;
                    if (data_bus.we) begin
                        is_rd <= 1'b0;
                        extbus.adata_mosi <= { 8'd0, data_bus.adr};
                            
                        cyc_cnt <= cyc_cnt + 1'b1;
                        extbus.oe_master <= 1'b1;
                        data_bus.busy <= 1'b1;
                        state <= ST_OPEN;
                    end
                            
                    if (data_bus.re) begin
                        is_rd <= 1'b1;
                        
                        cyc_cnt <= cyc_cnt + 1'b1;
                        extbus.adata_mosi <= { 8'h80, data_bus.adr};
                        extbus.oe_master <= 1'b1;
                        data_bus.busy <= 1'b1;
                        state <= ST_OPEN;                                   
                    end
                end
                ST_OPEN: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == CYCLE_OPEN)
                        begin
                            if (is_wr)
                                extbus.nwr_mosi <= 1'b0;
                            else
                                extbus.nale <= 1'b0;
                            state <= ST_ACTIVE;										
                        end
                end
                ST_ACTIVE: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == (CYCLE_OPEN + CYCLE_ACTIVE))
                        begin
                            extbus.nale <= 1'b1;
                            extbus.nwr_mosi <= 1'b1;
                            state <= ST_CLOSE;
                        end
                end
                ST_CLOSE: begin
                    cyc_cnt <= cyc_cnt + 1'b1;
                    if (cyc_cnt == (CYCLE_OPEN + CYCLE_ACTIVE + CYCLE_CLOSE)) begin
                        if (is_rd) begin
                            extbus.oe_master <= 1'b0;
                            state <= ST_IDLE;
                        end
                        else begin
                            if (is_wr) begin
                                extbus.oe_master <= 1'b0;
                                data_bus.busy <= 1'b0;
                                cyc_cnt <= 4'd0;
                                is_wr <= 1'b0;
                                state <= ST_IDLE;
                            end
                            else begin
                                extbus.adata_mosi <= data_bus.dat_mosi;
                                is_wr <= 1'b1;	
                                cyc_cnt <= 4'd1;		
                                state <= ST_OPEN;													
                            end											
                        end
                    end
                end		
            endcase
            
            nwrs_meta_d <= nwrs_meta;
            if (is_wrs && !extbus.oe_master) begin
                data_bus.busy <= 1'b0;                
                data_bus.dat_miso <= adata_meta;
            end
        end
    end
    
        
    metastable_chain#(.CHAIN_DEPTH(3), .BUS_WIDTH(1 + 16)) 
    metastableChainExtBus(.clk(clk), .nrst(1'b1), .din({extbus.nwr_miso, extbus.adata_miso}), .dout({nwrs_meta, adata_meta})); 
endmodule: larva_extbus_master
