/**
  * Версия 1.1 30.07.2018
  * Добавил в упрощенном виде выход на шину Wishbone.
  * Версия 1. 23.07.2018
  * Контроллер температурного датчика LM77 для модуля Main проекта Личинка (Орбита).
  * Разработан для ПЛИС Actel A3P600-PQ208I. Сделан на основе контроллера I2C + Wishbone с opencores.org.
  */    
module larva_wb_lm77_ctrl
    (
        input clk,
        input nrst,
                
        // Wishbone slave interface
        // input [7:0] wb_adr_i,
        // input [31:0] wb_dat_i,	
        input wb_stb_i,
        input wb_cyc_i,
        // input [3:0] wb_sel_i,
        // input wb_we_i,	
        output [31:0] wb_dat_o,
        output wb_ack_o,
        
        output reg [15:0] tdata,
        
        // Hardware IO        
        inout scl_pad_io,
        inout sda_pad_io
    );
    
    wire wb_stb = wb_stb_i;
    wire wb_cyc = wb_cyc_i;
    reg wb_ack;
    assign wb_ack_o = wb_ack;
    assign wb_dat_o = {16'd0, tdata};
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    wb_ack <= 1'b0;
                end
            else
                begin
                    wb_ack <= 1'b0;
                    if (wb_stb && wb_cyc && ~wb_ack)
                        wb_ack <= 1'b1;
                end
        end   
        
    localparam ST_IDLE = 0,
           ST_ADR_POINTREG = 1,
           ST_DATA_POINTREG = 2,
           ST_ADR_TEMP = 3,
           ST_DATA_MSDB = 4,
           ST_DATA_LSDB = 5,
           ST_SAVE_LSDB = 6,
           ST_ERROR = 7;
               
    reg [3:0] state;
    assign debug_state = state; 
    reg sta;
    reg sto;
    reg wr; 
    reg rd;
    reg ack;
    reg [7:0] txr;
    //reg [15:0] tdata;
    wire [7:0] rxr;
    wire done;
    wire i2c_busy;
    wire i2c_al;
    wire irxack;
    wire ena;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    state <= ST_IDLE;
                    sta <= 1'b0;
                    sto <= 1'b0;
                    wr <= 1'b0;
                    rd <= 1'b0;
                    ack <= 1'b0;
                    txr <= 8'd0;
                    tdata <= 16'd0;          
                end
            else
                begin
                    if (done)
                        begin
                            sta <= 1'b0;
                            sto <= 1'b0;
                        end
                    wr <= 1'b0;
                    rd <= 1'b0;
                    case (state)
                        ST_IDLE:
                            begin
                                if (ena)
                                    state <= ST_ADR_POINTREG;
                            end
                        ST_ADR_POINTREG:
                            begin
                                if (~i2c_busy)
                                    begin
                                        sta <= 1'b1;
                                        wr <= 1'b1;
                                        txr <= 8'b10010_00_0;
                                        state <= ST_DATA_POINTREG;
                                    end
                            end
                        ST_DATA_POINTREG:
                            begin
                                if (done)     
                                    begin
                                        if (~irxack)
                                            begin
                                                wr <= 1'b1;
                                                sto <= 1'b1;
                                                txr <= 8'd0;
                                                state <= ST_ADR_TEMP;
                                            end
                                        else
                                            state <= ST_ERROR;
                                    end
                            
                            end
                        ST_ADR_TEMP:
                            begin
                                if (~i2c_busy)
                                    begin
                                        sta <= 1'b1;
                                        wr <= 1'b1;
                                        txr <= 8'b10010_00_1;
                                        state <= ST_DATA_MSDB;
                                    end
                            end
                        ST_DATA_MSDB:
                            begin
                                if (done)     
                                    begin
                                        if (~irxack)
                                            begin
                                                rd <= 1'b1;
                                                ack <= 1'b0;
                                                state <= ST_DATA_LSDB;
                                            end
                                        else
                                            state <= ST_ERROR;
                                    end
                            end
                        ST_DATA_LSDB:
                            begin
                                if (done)     
                                    begin
                                        tdata[15:8] <= rxr;
                                        rd <= 1'b1;
                                        ack <= 1'b1;
                                        sto <= 1'b1;
                                        state <= ST_SAVE_LSDB;
                                    end
                            end
                        ST_SAVE_LSDB:
                            begin
                                if (done)
                                    begin
                                        tdata[7:0] <= rxr;
                                        state <= ST_IDLE;
                                    end
                            end
                        ST_ERROR:
                            begin
                                state <= ST_IDLE;
                            end
                    endcase
                end
        
        end
    
    wire scl_pad_i;
    wire scl_pad_o;
    wire scl_padoen_o;
    wire sda_pad_i;
    wire sda_pad_o;
    wire sda_padoen_o;
        
    i2c_master_byte_ctrl byte_controller
    (
		.clk(clk),
		.rst(1'b0),
		.nReset(nrst),
		.ena(1'b1),
		.clk_cnt(16'd23),
		.start(sta),
		.stop(sto),
		.read(rd),
		.write(wr),
		.ack_in(ack),
		.din(txr),
		.cmd_ack(done),
		.ack_out(irxack),
		.dout(rxr),
		.i2c_busy(i2c_busy),
		.i2c_al(i2c_al),
		.scl_i(scl_pad_i),
		.scl_o(scl_pad_o),
		.scl_oen(scl_padoen_o),
		.sda_i(sda_pad_i),
		.sda_o(sda_pad_o),
		.sda_oen(sda_padoen_o)
	);
    
    assign scl_pad_i = scl_pad_io;
    assign scl_pad_io = scl_padoen_o ? 1'bZ : scl_pad_o;
    
    assign sda_pad_i = sda_pad_io;
    assign sda_pad_io = sda_padoen_o ? 1'bZ : sda_pad_o;
    
    
    clock_enable
    #(
        .FREQ_MHZ(24), 
        .TIME_MS(150) 
	)
    clk_ena_inst
    (
        .clk(clk),
        .nrst(nrst),
        .ena(1'b1),
        .out(ena)
    );
endmodule