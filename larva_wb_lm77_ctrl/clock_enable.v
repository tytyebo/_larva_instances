module clock_enable 
    #(
        parameter FREQ_MHZ = 10, 
        parameter TIME_MS = 10
	)
    (
        input clk,
        input nrst,
        input ena,
        output reg out
    );
    localparam TIME = 1000 * TIME_MS * FREQ_MHZ;
    localparam WIDTH = log2(TIME);
    
    reg [WIDTH - 1:0] cnt;
    
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    out <= 1'b0;
                    cnt <= 0;
                end
            else
                begin
                    out <= 1'b0;
                    if (ena)
                        begin
                            cnt <= cnt + 1'b1;
                            if (cnt == TIME)
                                begin
                                    cnt <= 0;
                                    out <= 1'b1;
                                end
                        end
                    else
                        cnt <= 0;
                end
        end
    
    function [31:0]	log2;
        input  [31:0] data;
        begin
            for ( log2 = 0; data > 0; log2 = log2 + 1 )
                data = data >> 1;
        end
    endfunction
endmodule


