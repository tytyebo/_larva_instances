`include "defines.svh"
`include "interfaces.svh"
// 12,582912 MHz

import defines_pkg::*;

module tb_larva_top#(parameter bit [3:0] MODE = 4'd4, 
                     parameter int QSPI_DUMMY = 10);

    bit clk_ext, clk24, clk_x2, clk_orb, clk_mem, clk_uart;
    logic nrst;
    wire sync;
    always #20 clk_ext = ~clk_ext; // rounded to 25 MHz // #20.345 clk_ext is 24.576 i.e. there is 40.69 ns period 
    always #20 clk24 = ~clk24; // rounded to 25 MHz // #20.833 24 MHz 
	always #20 clk_x2 = ~clk_x2; // rounded to 25 MHz // ‭#19.868 25.165824‬ MHz 
    always #40 clk_orb = ~clk_orb; // rounded to 12.5 MHz // #39.736 12.582912 MHz
    always #436 clk_uart = ~clk_uart; // to 12.5 MHz, to 12.582912 MHz is #434

    initial begin
        clk_mem = 1'b0;
        @(posedge clk_orb);
        forever begin
            #40 clk_mem = ~clk_mem;
        end
    end

    qspi_io qspi(clk24);
    wire [3:0] qspi_w;
    assign qspi.io_miso = qspi_w;
    assign qspi_w = qspi.dir ? qspi.io_mosi : 'z;

    orbit_io orbit();
    logic orbit_a;
    assign orbit.clk = clk_orb; 
    assign orbit.mode = MODE;
    assign orbit.orbit = orbit_a;
    
    spi_io spi();
    anp_io anp();
    rs232_io rs232();
    discrete_signals_io ds();
    parallel_channel_io pc1(), pc2();
    serial_channel_io sc();
    uc_io uc(clk_uart);

    // larva_test#(MODE, QSPI_DUMMY) larvaTest(nrst, sync, orbit, qspi, uc, anp, spi, pc, pc2, sc);
    larva_test#(MODE, QSPI_DUMMY) larvaTest(nrst, sync, orbit, qspi, anp, spi, rs232, ds);
    /**
    * Окружение для тестируемых модулей
    *
    * larvaSdramModel - модель sdram памяти
    */
    logic [12:0] sdram_a;
    wire [15:0] sdram_dq;
    logic [1:0] sdram_ba;
    logic sdram_clk, sdram_cke, sdram_cs_n, sdram_ras_n, sdram_cas_n, sdram_we_n, sdram_dqmh, sdram_dqml;
    sdr larvaSdramModel(
        .Dq(sdram_dq),
        .Addr(sdram_a),
        .Ba(sdram_ba),
        .Clk(sdram_clk),
        .Cke(sdram_cke),
        .Cs_n(sdram_cs_n),
        .Ras_n(sdram_ras_n),
        .Cas_n(sdram_cas_n),
        .We_n(sdram_we_n),
        .Dqm({sdram_dqmh, sdram_dqml}));


    // WB slave #6
    // larva_epcq16a_dummy larvaEpcq16aDummy(.clk(clk_x2), .nrst(nrst), .spi(spi));
    wire [1:0] UPR_MISC;
    /**
    * Тестируемые модули
    *
    * larvaMain - плата формирователя
    * larvaAnalog - плата аналогового модуля
    * larvaDigital - плата цифрового модуля
    */
    milStd_io tb_mil_tx();
    wire [15:0] UPR_DATA;
    wire UPR_RD, UPR_WR, UPR_FLAG, UPR_CLK;
    assign nrst = UPR_CLK;
    pullup (pull1) pull_record_cmd(ds.nrecord);
    ft75_main#(.QSPI_DUMMY(QSPI_DUMMY))
    larvaMain(
        // RAM pins
        .RAM_A(sdram_a),
        .RAM_DQ(sdram_dq),
        .RAM_BA(sdram_ba),
        .RAM_nCS(sdram_cs_n),
        .RAM_CLK(sdram_clk),
        .RAM_CKE(sdram_cke),
        .RAM_nWE(sdram_we_n),
        .RAM_nRAS(sdram_ras_n),
        .RAM_nCAS(sdram_cas_n),
        .RAM_DQMH(sdram_dqmh),
        .RAM_DQML(sdram_dqml),

        // BUS pins
        .UPR_DATA(UPR_DATA),
        .UPR_CLK(UPR_CLK),
        .UPR_RD(UPR_RD),
        .UPR_WR(UPR_WR),
        .UPR_SGP1(sync),
        .UPR_FLAG(UPR_FLAG),
        .UPR_MISC(UPR_MISC),

        .CLK_24M576(clk_ext),
        .CLK_24M(clk24),

        // RK pins
        .NOV(1'bz),
        .KOM1(ds.nrecord),

        // MKO buf pins
        .MPK0_RX_P(tb_mil_tx.txp),
        .MPK0_RX_N(tb_mil_tx.txn),
        .MPK1_RX_P(1'bz),
        .MPK1_RX_N(1'bz),
        .MPK2_RX_P(1'bz),
        .MPK2_RX_N(1'bz),
        
        // VIDEO pins
        .VIDEO_MOD_A(),
        .VIDEO_MOD_B(orbit_a),

        .MCU_IO1(1'b0),
        .EE_CS(spi.ncs),
        .EE_CLK(spi.sck),
        .EE_DI(spi.mosi),
        .EE_DO(spi.miso),
        
        .QSPI_NCS(qspi.ncs),
        .QSPI_SCK(qspi.sck),
        .QSPI_IO(qspi_w),
        .MCU_CLK(),
        
        .MCLK(),
        .MDAT(1'b0),
        
        .clk(clk_orb),
        .clk_mem(clk_mem), 
        .pll0_lock(1'b1),
        .pll1_lock(1'b1));
    
    ft75_digital larvaDigital(
        .CLK_24M576(clk_ext),
        // BUS pins
        .UPR_DATA(UPR_DATA), // adata
        .UPR_RD(UPR_RD),     // nwrs
        .UPR_WR(UPR_WR),     // nwrm
        .UPR_FLAG(UPR_FLAG), // nale
        //service signals pins
        .UPR_SGP1(sync),     //синхро импульс
        .UPR_CLK(UPR_CLK),      //инверсный сброс
        //parallel interfaces pins
        .si_1_int(pc1.clk),
        .prk1_int(pc1.data),
        .prk2_int(pc2.data),
        .m4_1_int(pc1.sync),
        .m4_2_int(pc2.sync),
        .zapr1_1_int(pc1.request),
        .zapr1_2_int(pc2.request),
        //serial interfaces pins
        .si_tmp_int(sc.clk),
        .psk_int(sc.data),
        .m4_tmp_int(sc.sync),
        .zapr_tmp_int(sc.request),
        //UART pins
        .RxD_232_LVA(uc.tx),
        .RxD_232_LVB(rs232.rx),
        //RK pins
        .ACK_x(),
        .ON_x(),
        .OFF_x(),
        
        .clk(clk_orb),
        .pll0_lock(1'b1),
        .pll1_lock(1'b1));
  
`ifndef SMALL_TB
    logic act_adca_sdata_reg;
    logic act_adca_sclk;
    logic act_adca_ncs;
    pulldown (pull0) p1(UPR_MISC[0]);
    pulldown (pull0) p2(UPR_MISC[1]);
    wire TEMP_SCA, TEMP_SCL;
    pullup (pull1) p3(TEMP_SCA);
    pullup (pull1) p4(TEMP_SCL);
    pullup (pull1) p5(UPR_RD);
    ft75_analog larvaAnalog0(.UPR_DATA(UPR_DATA), // adata
                             .UPR_CLK(UPR_CLK),
                             .UPR_RD(UPR_RD),     // nwrs
                             .UPR_SGP1(1'bz),
                             .UPR_FLAG(UPR_FLAG), // nale
                             .UPR_MISC(UPR_MISC),
                             .CLK_24M576(clk_ext),
                             .BOARD_ID(1'b0),
                             
                             .ACT_MUXA_A(anp.mux),
                             .ACT_ADCA_SDATA(anp.miso[0]),
                             .ACT_ADCA_SCLK(anp.sck[0]),
                             .ACT_ADCA_nCS(anp.ncs[0]),
                             .ACT_nDISCH_A(),
                             .ACT_MUXB_A(),
                             .ACT_ADCB_SDATA(anp.miso[3:2]),
                             .ACT_ADCB_SCLK(anp.sck[1]),
                             .ACT_ADCB_nCS(anp.ncs[1]),
                             .ACT_nDISCH_B(),
                             .SGP1_ACT(1'b1),
                             .SGP2_ACT(1'b1),
                             .ACT_MUXC_A(),
                             .ACT_ADCC_SDATA(anp.miso[5:4]),
                             .ACT_ADCC_SCLK(anp.sck[2]),
                             .ACT_ADCC_nCS(anp.ncs[2]),
                             .ACT_nDISCH_C(),
                             .TEMP_SCA(TEMP_SCA),
                             .TEMP_SCL(TEMP_SCL),
                             .clk(clk_orb),
                             .pll0_lock(1'b1),
                             .pll1_lock(1'b1));
                             
    ft75_analog larvaAnalog1(.UPR_DATA(UPR_DATA), // adata
                             .UPR_CLK(UPR_CLK),
                             .UPR_RD(UPR_RD),     // nwrs
                             .UPR_SGP1(1'bz),
                             .UPR_FLAG(UPR_FLAG), // nale
                             .UPR_MISC(UPR_MISC),
                             .CLK_24M576(clk_ext),
                             .BOARD_ID(1'b1),
                             
                             .ACT_MUXA_A(anp.mux),
                             .ACT_ADCA_SDATA(anp.miso[0]),
                             .ACT_ADCA_SCLK(anp.sck[0]),
                             .ACT_ADCA_nCS(anp.ncs[0]),
                             .ACT_nDISCH_A(),
                             .ACT_MUXB_A(),
                             .ACT_ADCB_SDATA(anp.miso[3:2]),
                             .ACT_ADCB_SCLK(anp.sck[1]),
                             .ACT_ADCB_nCS(anp.ncs[1]),
                             .ACT_nDISCH_B(),
                             .SGP1_ACT(1'b1),
                             .SGP2_ACT(1'b1),
                             .ACT_MUXC_A(),
                             .ACT_ADCC_SDATA(anp.miso[5:4]),
                             .ACT_ADCC_SCLK(anp.sck[2]),
                             .ACT_ADCC_nCS(anp.ncs[2]),
                             .ACT_nDISCH_C(),
                             .TEMP_SCA(TEMP_SCA),
                             .TEMP_SCL(TEMP_SCL),
                             .clk(clk_orb),
                             .pll0_lock(1'b1),
                             .pll1_lock(1'b1));    
`endif
endmodule

