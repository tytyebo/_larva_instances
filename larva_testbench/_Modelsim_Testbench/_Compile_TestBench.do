# ���������� ������ ��������

# �����
vlog -sv -work work {../../common_things/clock_enable_by_input.sv}
# ������
vlog -sv -work work +incdir+../../common_things {../../larva_classes/uart_trx.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/environment.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/wb2qspi_cmdr.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/orbit_monitor.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/orbit_checker.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/qspi_trx.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/anp_checker.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/anp_gen.sv}
# vlog -sv -work work +incdir+../../common_things {../../larva_classes/uc_gen.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/epcq16a.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/rs232_trx.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_classes/discrete_signals_mng.sv}
# ������ ������
vlog -vlog01compat -work work  +incdir+../../larva_sdram/ram_model +define+den512Mb +define+sg6a +define+x16 {../../larva_sdram/ram_model/sdr.v}
# ��������� ������
vlog -sv -work work +incdir+../../common_things {../../larva_testbench/larva_orbit_control.sv}
# ���������
vlog -sv -work work +incdir+../../common_things +incdir+.. {../tb_larva_top.sv}
vlog -sv -work work +incdir+../../common_things {../larva_test.sv}




