# VHDL & Verilog Simulation
vsim  -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L cycloneiii_ver -L rtl_work -L work -voptargs="+acc" tb_larva_top

encoding system cp1251

do wave.do

view structure
view signals

run 0 us
view wave -undock
wm state .main_pane.wave zoomed
view wave



run 5000 ms
wave zoom full

