vlib rtl_work
vmap work rtl_work
# file copy -force ../../../_proj_Altera/ft75_main/_instances/generated_mem/larva_orbit_mem.mif ./
file copy -force ../../../_proj_Altera/ft75_main/_instances/generated_mem/larva_weigher_mem.mif ./
file copy -force ../epcq16a.bin ./
# ���������� ������ �������

# ����� ����� �������
vlog -sv -work work +incdir+../../common_things {../../common_things/defines_pkg.sv}
vlog -sv -work work +incdir+../../larva_classes+../../common_things {../../common_things/classes_pkg.sv}
vlog -vlog01compat -work work {../../common_things/metastable_chain.v}
vlog -vlog01compat -work work {../../common_things/PONR.v}
vlog -vlog01compat -work work {../../common_things/sync_async_reset.v}
vlog -sv -work work {../../common_things/larva_soft_reset.sv}

# FT75_main

# ��������������� � Wizard ����� ��� ������� ft75_main
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_pll/inPll0.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_pll/inPll1.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_fifo/larva_mil_fifo.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_fifo/larva_orbit_fifo.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_fifo/larva_uart_fifo.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_fifo/larva_map_fifo.v}
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_main/_instances/generated_mem/larva_orbit_mem.v}
vlog -vlog01compat -work work +incdir+../../../_proj_Altera/ft75_main/_instances/generated_mem/ {../../../_proj_Altera/ft75_main/_instances/generated_mem/larva_weigher_mem.v}
vlog -vlog01compat -work work  {../../../_proj_Altera/ft75_main/_instances/generated_mult/larva_weigher_mult.v}
# ������������ ���� Master
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_extbus2wb_bridge.sv} 
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_extbus_master.sv} 
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_weigher.sv} 
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_signal_flow_adder.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_calib_chnls/larva_calibrating_channels.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_overcurr/larva_overcurr.sv}

# ������������ ���� Slave\ 
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_extbus_slave.sv}
vlog -vlog01compat -work work {../../larva_external_bus/larva_extbus_slave_old.v}
vlog -sv -work work {../../larva_external_bus/larva_extbus_slave_mux.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_external_bus/larva_extbus_test_regs.sv}

# Wishbone Connection Matrix, ������ � OpenCores 
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_arb.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_master_if.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_msel.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_pri_dec.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_pri_enc.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_rf.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_slave_if.v}
vlog -vlog01compat -work work +incdir+../../wb_conmax/ {../../wb_conmax/wb_conmax_top.v}

# ������������ ������
vlog -sv -work work +incdir+../../common_things {../../larva_orbit/larva_orbit_sequencer.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_orbit/larva_orbit_inserter.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_orbit/larva_orbit_serviceman.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_orbit/larva_orbit_telemeter.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_orbit/larva_orbit_crc.sv} 

# ������� ���������� ������� � ������� ������ �������
vlog -sv -work work {../../larva_local_timer/larva_local_timer.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_local_timer/larva_relay_timers.sv}

# ���������� �������� ������� ������
vlog -sv -work work {../../larva_local_timer/larva_discrete_signals_ctrl.sv}

# ������� � �������� MIL-STD-1553
vlog -sv -work work {../../larva_mil/larva_mil_packer.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_mil/larva_mil_fifo2wb.sv}
vlog -sv -work work {../../larva_mil/mil_rx_phy.sv}
vlog -sv -work work {../../larva_mil/mil_tx_phy.sv}
vlog -sv -work work {../../larva_mil/larva_mil_responder.sv}

# ���������� � PHY ������ SDRAM
vlog -sv -work work +incdir+../../common_things {../../larva_sdram/larva_sdram2wb.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_sdram/larva_sdram_phy.sv}

# ����������� map ��� ����
vlog -sv -work work +incdir+../../common_things {../../larva_flash/larva_map_reader.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_flash/larva_epcq16a_phy.sv}

# �������� �������� ���� WB
vlog -sv -work work +incdir+../../common_things {../../larva_debug/larva_debug_regs.sv}

# ���������� QSPI
vlog -sv -work work +incdir+../../common_things {../../larva_qspi/larva_qspi_phy.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_qspi/larva_qspi2wb.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_qspi/larva_fifo1.sv}

# RS �� �����������
vlog -sv -work work +incdir+../../common_things {../../larva_rs232/larva_rs232_rx_phy.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_rs232/larva_xmtr_data_responder.sv}

# ������� ���� 
vlog -sv -work work +incdir+../../common_things {../../larva_danger_nets/larva_danger_nets_ctrl.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_danger_nets/larva_ad7401_phy.sv}

# ��������� ����������
vlog -sv -work work +incdir+../../common_things {../../larva_service/larva_service.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_lm77_ctrl/larva_lm77_wb_ctrl.sv}


# ������� ������� ������� ft75_main
vlog -sv -work work +incdir+../../common_things/ {../../larva_top_level/ft75_main.sv}
###########################################################################################################################

# FT75_analog

# ������� ������� ������� ft75_analog
vlog -sv -work work +incdir+../../common_things {../../larva_top_level/ft75_analog.sv}

# ����������� ��� � ���������������
vlog -vlog01compat -work work {../../larva_ad7476_ctrl/larva_ad7476_ctrl.v}
vlog -sv -work work {../../larva_ad7476_ctrl/larva_ad7476_phy.sv}
vlog -vlog01compat -work work {../../larva_ad7476_ctrl/larva_ad7476_top.v}
vlog -sv -work work  +incdir+../../common_things/ {../../larva_anp_ctrl/larva_anp_ctrl.sv}
vlog -sv -work work  +incdir+../../common_things/ {../../larva_anp_ctrl/larva_anp_accum.sv}
vlog -sv -work work  +incdir+../../common_things/  {../../larva_lm77_ctrl/larva_lm77_ctrl.sv}

vlog -vlog01compat -work work  +incdir+../../common_things/  {../../larva_lm77_ctrl/i2c_master_bit_ctrl.v}
vlog -vlog01compat -work work  +incdir+../../common_things/  {../../larva_lm77_ctrl/i2c_master_byte_ctrl.v}
###########################################################################################################################

# FT75_digital

# ������� ������� ������� ft75_digital
vlog -sv -work work +incdir+../../common_things {../../larva_top_level/ft75_digital.sv}

# Generic FIFO ��� ft75_digital
vlog -vlog01compat -work work {../../../_proj_Altera/ft75_digital/_instances/generated_fifo/larva_generic_fifo.v}

# ����������� ������������� ������
vlog -vlog01compat -work work {../../larva_digital/ll_phi.v}
vlog -vlog01compat -work work {../../larva_digital/ll_pk_2_orb.v}

vlog -sv -work work +incdir+../../common_things {../../larva_digital/larva_pc_ctrl.sv}

vlog -vlog01compat -work work {../../../_proj_Altera/ft75_digital/_instances/version_reg/version_reg.v}

# ����������� ����������������� ������
vlog -vlog01compat -work work {../../larva_digital/si_phi.v}
vlog -sv -work work +incdir+../../common_things  {../../larva_digital/larva_serial_ctrl.sv}

# UART �� ���� ���� ����������
vlog -sv -work work +incdir+../../common_things {../../larva_rs232/larva_rs232_rx_phy.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_rs232/larva_rs232_ctrl.sv}

# ������ ������������ ���������������, mux ��� ������������ ���� � ���������� ��
vlog -sv -work work +incdir+../../common_things  {../../larva_digital/larva_digital_sync.sv}
vlog -sv -work work +incdir+../../common_things {../../larva_digital/larva_ds_ctrl.sv}