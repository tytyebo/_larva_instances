`include "defines.svh"
`include "interfaces.svh"
// 12.582912 MHz

import defines_pkg::*;
import classes_pkg::environment;
import classes_pkg::qspi_trx;
import classes_pkg::qspiPacket_s;

program automatic larva_test#(parameter bit [3:0] MODE = 4'd0,
                              parameter int QSPI_DUMMY = 10,
                              parameter int QSPI_SCK_PERIOD = 320)
                             /*(input bit nrst, sync, orbit_io.test orbit, qspi_io.test_master qspi_master, uc_io.test uc,
                              anp_io anp, spi_io spi, pc_io pc, pc_io pc2, sc_io sc)*/
                              (input bit nrst, sync, orbit_io.test orbit, qspi_io.test_master qspi_master,
                              anp_io anp, spi_io spi, rs232_io.test rs232, discrete_signals_io.test ds);
    environment env;
    qspi_trx qspi;
    initial begin
        qspi_trx::dummy_length = QSPI_DUMMY;
        // env = new(qspi_master, orbit, uc, MODE, anp, spi, pc, pc2, sc);
        env = new(qspi_master, orbit, MODE, anp, spi, rs232, ds);
        env.qspi_sck_period = QSPI_SCK_PERIOD;
        env.init();
        wait (nrst);       
        env.run();
        #15000.000;


        
        
        for (int i = 0; i < 15; i++) begin
            @(posedge sync);
            @(negedge sync);
        end
        $finish;
    end  
endprogram: larva_test

