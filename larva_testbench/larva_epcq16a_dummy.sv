/*
 * Модуль эмулятора микросхемы флэш-памяти EPCQ16A для проекта larva_main.
 * Требует на вход сигнал тактовой частоты в 2 раза выше тактовой частоты larva_main.
 *
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
 
// ‭The clk should be 25.165824‬ MHz
module larva_epcq16a_dummy(input bit clk, nrst,
                           spi_io.slave spi);
    //-------------------------------------------------------------------------------------//
    //--------------------------------- Global things -------------------------------------//
    //-------------------------------------------------------------------------------------//
    
    typedef enum {ST_OPERATION, ST_ADR, ST_GET_DATA, ST_SET_DATA, ST_UNKNOWN} state_e;
    state_e state;
    typedef enum bit [7:0] {WRITE_OP = 8'h02, READ_OP = 8'h03} operation_e;
    operation_e op;

    logic [4:0] spi_cnt;
    logic spi_sck_d, spi_ncs_d;
    logic sck_rise, sck_down, ncs_down;
    logic [7:0] spi_operation, mem_data, mem_q;
    logic [23:0] spi_adr;
	logic [15:0] mem_adr;
	logic mem_wr;

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            spi_cnt <= '0;
            spi_operation <= '0;
            spi_adr <= '0;
            mem_data <= '0;
            state <= ST_OPERATION;
            spi_sck_d <= 1'b0;
            spi_ncs_d <= 1'b0;
			mem_adr <= '0;
			mem_wr <= 1'b0;
            spi.miso <= 1'b0;
        end
        else begin
			mem_wr <= 1'b0;
            spi_sck_d <= spi.sck;
            spi_ncs_d <= spi.ncs;

            if (!spi.ncs) begin
                unique case (state)
                    ST_OPERATION: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            spi_operation[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
                                state <= ST_ADR;
                                spi_cnt <= 5'd23;
                            end
                        end
                    end
                    ST_ADR: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            spi_adr[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
								spi_cnt <= 5'd7;                                
                                unique case (spi_operation)
                                    WRITE_OP: begin
                                        state <= ST_GET_DATA;
                                    end
                                    READ_OP: begin
                                        state <= ST_SET_DATA;
                                    end
                                    default: begin
                                        state <= ST_UNKNOWN;
                                    end
                                endcase    
                            end
                        end
                    end
                    ST_GET_DATA: begin
                        if (sck_rise) begin
                            spi_cnt <= spi_cnt - 1'b1;
                            mem_data[spi_cnt] <= spi.mosi;
                            if (spi_cnt == 5'd0) begin
                                state <= ST_GET_DATA;
                                spi_cnt <= 5'd7;
								mem_wr <= 1'b1;
                            end
                        end
						if (mem_wr) begin
							spi_adr <= spi_adr + 1'b1;
						end
                    end
					ST_SET_DATA: begin
						if (sck_down) begin // 
							spi_cnt <= spi_cnt - 1'b1;
							spi.miso <= mem_q[spi_cnt];
							if (spi_cnt == 5'd0) begin
                                state <= ST_SET_DATA;
                                spi_cnt <= 5'd7;
								spi_adr <= spi_adr + 1'b1;
                            end
						end
					end
					ST_UNKNOWN: begin
						state <= ST_UNKNOWN;
					end
                endcase
            end
            else begin
                state <= ST_OPERATION;
				spi_cnt <= 7'd7;
            end
        end
    end
    assign sck_rise = (spi.sck && ~spi_sck_d);
    assign sck_down = (~spi.sck && spi_sck_d);
    assign ncs_down = ~spi.ncs && spi_ncs_d;
	
	logic [1023:0] [7:0] map_mem;
    always @(posedge clk or negedge nrst) begin
		if (!nrst) begin
			mem_q <= '0;
		end
		else begin
			mem_q <= map_mem[spi_adr[9:0]];
			if (mem_wr) begin
				map_mem[spi_adr[9:0]] <= mem_data;
				mem_q <= mem_data;
			end
		end
	end
    
    initial begin
        for (int i = 0; i < $size(map_mem); i++) begin
            map_mem[i] = i[7:0];
        end
    end
endmodule: larva_epcq16a_dummy