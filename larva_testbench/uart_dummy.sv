`include "defines.svh"
`include "interfaces.svh"
module uart_dummy(input bit nrst, clk,
                  handshake_io.slave hdshk);
                          
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            hdshk.ready <= 1'b0;
        end
        else begin
            hdshk.ready <= 1'b0;
            if (hdshk.valid && !hdshk.ready) begin
                hdshk.ready <= 1'b1;
            end
        end
    end
endmodule: uart_dummy