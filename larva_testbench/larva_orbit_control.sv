/*
 * Модуль, контроллирующий сигнал Орбиты проекта larva_main.
 * Идея в том, чтобы на вход подавать сгенерированную в testbench частоту 12.582912 МГц,
 * формировать clk_ena в зависимости от mode.
 * Т.к. сгенерированную в testbench частота 12.582912 МГц будет синхронна полученной в larva_main на PLL,
 * то вход orbit будет успешно захватываться.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
 
module larva_orbit_control(input bit clk, nrst,
                           input logic orbit, logic [3:0] mode);
    //-------------------------------------------------------------------------------------//
    //--------------------------------- Global things -------------------------------------//
    //-------------------------------------------------------------------------------------//
    
    typedef enum {ST_IDLE, ST_WORK} state_e;
    state_e state;

    logic [23:0] orbit_word, word;
    logic [5:0] cnt;
    logic [9:0] data1, data2;
    logic [31:0][63:0] service_info;
    logic clk_ena;
    clock_enable_by_input#(5) clockEnabler(.clk(clk), .nrst(nrst), .ena(1'b1), .ena_value('1), .clk_ena(clk_ena));
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            orbit_word <= '0;
            word <= '0;
            cnt <= '0;
            state <= ST_IDLE;
        end
        else begin
            if (clk_ena) begin
                orbit_word <= {orbit_word[22:0], orbit};
                unique case (state)
                    ST_IDLE: begin
                        if (orbit_word[23:22] == 2'b10) begin
                            state <= ST_WORK;
                            word <= orbit_word;
                        end    
                    end
                    ST_WORK: begin
                        cnt <= cnt + 1'b1;
                        if (cnt == 6'd23) begin
                            word <= orbit_word;
                            cnt <= '0;
                        end
                    end
                endcase
            end
        end
    end
    assign data1 = {word[21], word[19], word[17], word[15], word[13], word[11], word[9], word[7], word[5], word[3]};
    assign data2 = {word[20], word[18], word[16], word[14], word[12], word[10], word[8], word[6], word[4], word[2]};
endmodule: larva_orbit_control