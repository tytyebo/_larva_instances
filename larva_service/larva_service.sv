/*
 * Модуль "larva_service" для проекта larva_main.
 * Содержит регистры с информацией для потока служебной информации.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
module larva_service(input bit nrst, clk,
                     wishbone_io.slave wbs,
                     output logic [1:0] pc1_mode, pc2_mode,
                     output logic [19:0] system_number,
                     output logic square_out_mode);
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
            pc1_mode <= '0;
            pc2_mode <= '0;
            system_number <= '0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                wbs.wbs_ack <= 1'b1;
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel[2:0] == '1) begin
                                system_number <= wbs.wbm_dat[19:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel[0] == 1'b1) begin
                                {square_out_mode, pc2_mode, pc1_mode} <= wbs.wbm_dat[4:0];
                            end
                        end
                    endcase
                end 
                else begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel[2:0] == '1) begin
                                wbs.wbs_dat <= {12'd0, system_number};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel[0] == 1'b1) begin
                                wbs.wbs_dat <= {28'd0, square_out_mode, pc2_mode, pc1_mode};
                            end
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                        end
                    endcase
                end
            end
        end
    end    
endmodule: larva_service