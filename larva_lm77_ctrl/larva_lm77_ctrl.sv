/*
 * Модуль "larva_lm77_ctrl" для проекта ft75_analog.
 * В качестве контроллера физического уровня использован контроллер I2C Master, взятый с OpenCores.org.
 * Upd: Доработан для использования интерфейса generic_bus_io.slave_hndshk. 2021.04.30
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */   
`include "defines.svh"
`include "interfaces.svh"
module larva_lm77_ctrl(input bit clk, nrst,
                       input logic [7:0] base_adr,
                       generic_bus_io.slave_hndshk data_bus,
                       i2c_io.master i2c);
    
    generic_bus_io#(.MOSI_WIDTH(8), .MISO_WIDTH(8)) lowlvl_bus();
    
    typedef enum bit [2:0] {ST_ADR_POINTREG, ST_DATA_POINTREG, ST_ADR_TEMP, ST_DATA_MSDB, ST_DATA_LSDB, ST_END, ST_PAUSE} state_e;
    state_e state;

    logic [15:0] data;
    logic [7:0] data_part;
    logic start, stop;
    logic cmd_done, ack;
    logic [21:0] pause_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            state <= ST_ADR_POINTREG;
            lowlvl_bus.re <= 1'b0;
            lowlvl_bus.we <= 1'b0;
            lowlvl_bus.dat_mosi <= '0;
            start <= 1'b0;
            stop <= 1'b0;
            ack <= 1'b0;
            data_part <= '0;
            data <= '0;
            pause_cnt <= '0;
        end
        else begin
            lowlvl_bus.re <= 1'b0;
            lowlvl_bus.we <= 1'b0;
            if (cmd_done) begin
                start <= 1'b0;
                stop <= 1'b0;
                ack <= 1'b0;
            end
            unique case (state)
                ST_ADR_POINTREG: begin
                    if (!lowlvl_bus.busy) begin
                        start <= 1'b1;
                        lowlvl_bus.we <= 1'b1;
                        lowlvl_bus.dat_mosi <= 8'b10010_00_0;
                        state <= ST_DATA_POINTREG;
                    end
                end
                ST_DATA_POINTREG: begin
                    if (cmd_done) begin
                        stop <= 1'b1;
                        lowlvl_bus.we <= 1'b1;
                        lowlvl_bus.dat_mosi <= '0;
                        state <= ST_ADR_TEMP;
                    end
                end
                ST_ADR_TEMP: begin
                    if (!lowlvl_bus.busy) begin
                        start <= 1'b1;
                        lowlvl_bus.we <= 1'b1;
                        lowlvl_bus.dat_mosi <= 8'b10010_00_1;
                        state <= ST_DATA_MSDB;
                    end
                end                
                ST_DATA_MSDB: begin
                    if (cmd_done) begin
                        lowlvl_bus.re <= 1'b1;
                        state <= ST_DATA_LSDB;
                    end
                end
                ST_DATA_LSDB: begin
                    if (cmd_done) begin
                        data_part <= lowlvl_bus.dat_miso;
                        stop <= 1'b1;
                        lowlvl_bus.re <= 1'b1;
                        ack <= 1'b1;
                        state <= ST_END;
                    end
                end
                ST_END: begin
                    if (cmd_done) begin
                        data[15:8] <= data_part;
                        data[7:0] <= lowlvl_bus.dat_miso;
                        state <= ST_PAUSE;
                    end
                end
                ST_PAUSE: begin
                    pause_cnt <= pause_cnt + 1'b1;
                    if (pause_cnt == '1) begin
                        state <= ST_ADR_POINTREG;
                    end
                end
            endcase      
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.ready <= 1'b0;
            data_bus.dat_miso <= '0;
        end
        else begin
            data_bus.ready <= 1'b0;
            if (data_bus.valid && !data_bus.ready) begin
                if (!data_bus.we) begin
                    if (data_bus.adr == base_adr) begin
                        data_bus.dat_miso <= { data[15], data[15], data[15], data[15:3] };
                        data_bus.ready <= 1'b1;
                    end
                    else begin
                        data_bus.dat_miso <= '0;
                    end
                end
            end   
        end
    end
    
    logic scl_pad_i, scl_pad_o, scl_padoen_o, sda_pad_i, sda_pad_o, sda_padoen_o;
    i2c_master_byte_ctrl larvaLm77Ctrl
    (
		.clk(clk),
		.rst(1'b0),
		.nReset(nrst),
		.ena(1'b1),
		.clk_cnt(16'd23),
		.start(start),
		.stop(stop),
		.read(lowlvl_bus.re),
		.write(lowlvl_bus.we),
		.ack_in(ack),
		.din(lowlvl_bus.dat_mosi),
		.cmd_ack(cmd_done),
		.ack_out(),
		.dout(lowlvl_bus.dat_miso),
		.i2c_busy(lowlvl_bus.busy),
        
        
		.i2c_al(),
		.scl_i(scl_pad_i),
		.scl_o(scl_pad_o),
		.scl_oen(scl_padoen_o),
		.sda_i(sda_pad_i),
		.sda_o(sda_pad_o),
		.sda_oen(sda_padoen_o)
	);
    
    assign scl_pad_i = i2c.scl_miso;
    assign sda_pad_i = i2c.sda_miso;
    assign i2c.sda_mosi = sda_pad_o;
    assign i2c.sda_noe = sda_padoen_o;
    assign i2c.scl_mosi = scl_pad_o;
    assign i2c.scl_noe = scl_padoen_o;
endmodule