/*
 * Модуль "larva_lm77_wb_ctrl" для проекта ft75_main.
 * В качестве контроллера физического уровня использован контроллер I2C Master, взятый с OpenCores.org.
 * 
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */  
`include "interfaces.svh"
`include "defines.svh" 
module larva_lm77_wb_ctrl#(parameter bit SEL = 1'b0)
                         (input bit clk, nrst,
                          wishbone_io.slave wbs,
                          output logic [7:0] self_temp,
                          i2c_io.master i2c);
    
    generic_bus_io#(.MOSI_WIDTH(8), .MISO_WIDTH(8)) data_bus();
    
    typedef enum bit [2:0] {ST_ADR_POINTREG, ST_DATA_POINTREG, ST_ADR_TEMP, ST_DATA_MSDB, ST_DATA_LSDB, ST_END, ST_PAUSE} state_e;
    state_e state;

    logic [15:0] data;
    logic [7:0] data_part, self_temp_mcu, self_temp_fpga;
    logic start, stop;
    logic cmd_done, ack;
    logic [21:0] pause_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            state <= ST_ADR_POINTREG;
            data_bus.re <= 1'b0;
            data_bus.we <= 1'b0;
            data_bus.dat_mosi <= '0;
            start <= 1'b0;
            stop <= 1'b0;
            ack <= 1'b0;
            data_part <= '0;
            data <= '0;
            pause_cnt <= '0;
        end
        else begin
            data_bus.re <= 1'b0;
            data_bus.we <= 1'b0;
            if (cmd_done) begin
                start <= 1'b0;
                stop <= 1'b0;
                ack <= 1'b0;
            end
            unique case (state)
                ST_ADR_POINTREG: begin
                    if (!data_bus.busy) begin
                        start <= 1'b1;
                        data_bus.we <= 1'b1;
                        data_bus.dat_mosi <= 8'b10010_00_0;
                        state <= ST_DATA_POINTREG;
                    end
                end
                ST_DATA_POINTREG: begin
                    if (cmd_done) begin
                        stop <= 1'b1;
                        data_bus.we <= 1'b1;
                        data_bus.dat_mosi <= '0;
                        state <= ST_ADR_TEMP;
                    end
                end
                ST_ADR_TEMP: begin
                    if (!data_bus.busy) begin
                        start <= 1'b1;
                        data_bus.we <= 1'b1;
                        data_bus.dat_mosi <= 8'b10010_00_1;
                        state <= ST_DATA_MSDB;
                    end
                end                
                ST_DATA_MSDB: begin
                    if (cmd_done) begin
                        data_bus.re <= 1'b1;
                        state <= ST_DATA_LSDB;
                    end
                end
                ST_DATA_LSDB: begin
                    if (cmd_done) begin
                        data_part <= data_bus.dat_miso;
                        stop <= 1'b1;
                        data_bus.re <= 1'b1;
                        ack <= 1'b1;
                        state <= ST_END;
                    end
                end
                ST_END: begin
                    if (cmd_done) begin
                        data[15:8] <= data_part;
                        data[7:0] <= data_bus.dat_miso;
                        state <= ST_PAUSE;
                    end
                end
                ST_PAUSE: begin
                    pause_cnt <= pause_cnt + 1'b1;
                    if (pause_cnt == '1) begin
                        state <= ST_ADR_POINTREG;
                    end
                end
            endcase      
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0;
            self_temp_mcu <= '0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                wbs.wbs_ack <= 1'b1;
                if (wbs.wbm_we) begin
                    if (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0] == `WB_ADR_INT_WIDTH'd2) begin // 0xC002
                        if (wbs.wbm_sel[0]) begin
                            self_temp_mcu <= wbs.wbm_dat[7:0];
                        end
                    end
                end
                else begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel[2:0] == '1) begin
                                wbs.wbs_dat <= {{19{data[15]}}, data[15:3]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel[0]) begin
                                wbs.wbs_dat <= {{25{data[12]}}, data[10:4]};
                            end
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                        end
                    endcase
                end
            end
        end
    end
    assign self_temp_fpga = {data[12], data[10:4]};
    assign self_temp = (SEL) ? self_temp_mcu : self_temp_fpga;
    
    logic scl_pad_i, scl_pad_o, scl_padoen_o, sda_pad_i, sda_pad_o, sda_padoen_o;
    i2c_master_byte_ctrl larvaLm77Ctrl
    (
		.clk(clk),
		.rst(1'b0),
		.nReset(nrst),
		.ena(1'b1),
		.clk_cnt(16'd23),
		.start(start),
		.stop(stop),
		.read(data_bus.re),
		.write(data_bus.we),
		.ack_in(ack),
		.din(data_bus.dat_mosi),
		.cmd_ack(cmd_done),
		.ack_out(),
		.dout(data_bus.dat_miso),
		.i2c_busy(data_bus.busy),
        
        
		.i2c_al(),
		.scl_i(scl_pad_i),
		.scl_o(scl_pad_o),
		.scl_oen(scl_padoen_o),
		.sda_i(sda_pad_i),
		.sda_o(sda_pad_o),
		.sda_oen(sda_padoen_o)
	);
    
    assign scl_pad_i = i2c.scl_miso;
    assign sda_pad_i = i2c.sda_miso;
    assign i2c.sda_mosi = sda_pad_o;
    assign i2c.sda_noe = sda_padoen_o;
    assign i2c.scl_mosi = scl_pad_o;
    assign i2c.scl_noe = scl_padoen_o;
endmodule