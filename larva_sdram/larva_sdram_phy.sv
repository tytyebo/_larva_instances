/*
 * Модуль "larva_sdram_phy" для проекта larva_main.
 * Осуществляет инициализацию, обновление, запись и чтение SDRAM-памяти (Микросхема AS4C32M16SB-6TIN).
 * Реализует запись/чтение по случайному адресу. Запись "взрывом" (burst) не поддерживается.
 * Для записи/чтения использует 4х тактный цикл: 
 * 1. Активация строки (Row) банка (Bank) памяти;
 * 2. Запись столбца (Column) в активированном банке с автоматической предварительной зарядкой (Auto Precharge);
 * 3. Такт Nop;
 * 4. Автоматическое обновление памяти (Auto Refresh). 
 * Таким образом запись одного 16ти разрядного слова занимает 4 такта.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
 // RAM_CLK parameter is the clock providing to RAM in MHz.
module larva_sdram_phy#(parameter int RAM_ADR_WIDTH = 12, RAM_DATA_WIDTH = 16,
                                      RAM_COLUMN_WIDTH = 9,
                        parameter bit [2:0] RAM_CL = 3,
                        parameter real RAM_CLK = 10) // 12.582912 
                       (input bit clk, nrst,
                        sdram_io sdram,
                        generic_bus_io.slave data_bus);
   
    localparam int WAIT_TIME = 100 * RAM_CLK;
    typedef enum {ST_RESET, ST_INIT_NOP_0, ST_INIT_PRECHARGE, ST_INIT_AUTO_REFRESH_0, ST_INIT_NOP_1, 
                  ST_INIT_AUTO_REFRESH_1, ST_INIT_NOP_2, ST_INIT_LMR, ST_INIT_NOP_3,
                  ST_AUTO_REFRESH, ST_ACTIVE, ST_RW, ST_NOP} state_e;
    state_e state;
    logic [$clog2(WAIT_TIME):0] init_wait_cnt;
    logic ram_ready;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            init_wait_cnt <= '0;
            ram_ready <= 1'b0;
        end
        else begin          
            if (init_wait_cnt != WAIT_TIME)
                init_wait_cnt++;
            else
                ram_ready <= 1'b1;
        end
    end

    logic we_reg, re_reg;
    logic [2:0] reader_cnt;
    logic [RAM_DATA_WIDTH - 1:0] dat_mosi_reg;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            sdram.reset_ports();
            data_bus.busy <= 1'b0;
            data_bus.dat_miso <= '0;
            sdram.cke <= 1'b0;
            we_reg <= 1'b0;
            re_reg <= 1'b0;
            reader_cnt <= '0;
            dat_mosi_reg <= '0;
            state <= ST_RESET;
        end
        else begin
            unique case (state)
                ST_RESET: begin
                    sdram.reset_ports();
                    sdram.cke <= 1'b1;
                    if (ram_ready)
                        state <= ST_INIT_NOP_0;
                end
                ST_INIT_NOP_0: begin
                    sdram.nop_cmd();
                    state <= ST_INIT_PRECHARGE;
                end
                ST_INIT_PRECHARGE: begin
                    sdram.precharge_all_cmd();
                    state <= ST_INIT_AUTO_REFRESH_0;
                end
                ST_INIT_AUTO_REFRESH_0: begin
                    sdram.auto_refresh_cmd();
                    state <= ST_INIT_NOP_1;
                end
                ST_INIT_NOP_1: begin
                    sdram.nop_cmd();
                    state <= ST_INIT_AUTO_REFRESH_1;
                end
                ST_INIT_AUTO_REFRESH_1: begin
                    sdram.auto_refresh_cmd();
                    state <= ST_INIT_NOP_2;
                end
                ST_INIT_NOP_2: begin
                    sdram.nop_cmd();
                    state <= ST_INIT_LMR;
                end
                ST_INIT_LMR: begin
                    sdram.lmr_cmd(RAM_CL);
                    state <= ST_INIT_NOP_3;
                end
                ST_INIT_NOP_3: begin
                    sdram.nop_cmd();
                    state <= ST_AUTO_REFRESH;
                end
                ST_AUTO_REFRESH: begin
                    sdram.auto_refresh_cmd();
                    dat_mosi_reg <= data_bus.dat_mosi;
                    if (!data_bus.busy) begin
                        if (data_bus.we || data_bus.re) begin
                            state <= ST_ACTIVE;
                            data_bus.busy <= 1'b1;
                        end
                        else begin
                            state <= ST_AUTO_REFRESH;
                        end
                        
                        if (data_bus.we)
                            we_reg <= 1'b1;
                        if (data_bus.re)
                            re_reg <= 1'b1;
                    end
                end
                ST_ACTIVE: begin
                    sdram.active_cmd(data_bus.adr[RAM_ADR_WIDTH + RAM_COLUMN_WIDTH + 1:RAM_COLUMN_WIDTH + 2], data_bus.adr[1:0]);
                    state <= ST_RW;
                end
                ST_RW: begin
                    if (we_reg) begin
                        sdram.write_cmd({{(10 - RAM_COLUMN_WIDTH){1'b0}}, data_bus.adr[RAM_COLUMN_WIDTH + 1:2]}, data_bus.adr[1:0], dat_mosi_reg);     
                    end
                    if (re_reg) begin
                        sdram.read_cmd({{(10 - RAM_COLUMN_WIDTH){1'b0}}, data_bus.adr[RAM_COLUMN_WIDTH + 1:2]}, data_bus.adr[1:0]);
                    end
                    state <= ST_NOP;
                end
                ST_NOP: begin
                    sdram.nop_cmd();
                    if (we_reg) begin
                        data_bus.busy <= 1'b0; 
                        we_reg <= 1'b0;
                    end
                    state <= ST_AUTO_REFRESH;
                end
            endcase
            
            if (re_reg)
                reader_cnt <= reader_cnt + 1'b1;
            else
                reader_cnt <= '0;    
                
            if (reader_cnt == 3'd4) begin
                data_bus.dat_miso <= sdram.dq_in;
                data_bus.busy <= 1'b0;
                re_reg <= 1'b0;
            end
        end
    end
endmodule: larva_sdram_phy
    
