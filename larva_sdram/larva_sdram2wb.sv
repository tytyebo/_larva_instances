/*
 * Модуль "larva_sdram2wb" для проекта larva_main.
 * Мост с шины Wishbone на SDRAM память в проекте larva.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2019
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
module larva_sdram2wb#(parameter real RAM_CLK = 10,
                       parameter int SDRAM_ADR_WIDTH = 25)
                      (input bit nrst, clk,
                       sdram_io sdram,
                       wishbone_io.slave wbs,
                       input logic [SDRAM_ADR_WIDTH - 1:0] stop_adr);
    
    generic_bus_io#(.ADR_WIDTH(SDRAM_ADR_WIDTH), .MOSI_WIDTH(16), .MISO_WIDTH(16)) data_bus();
    larva_sdram_phy#(.RAM_ADR_WIDTH(13), .RAM_DATA_WIDTH(16), .RAM_COLUMN_WIDTH(10), .RAM_CLK(RAM_CLK)) 
    sdramPhy(.clk(clk), .nrst(nrst), .sdram(sdram), .data_bus(data_bus.slave));

    logic [SDRAM_ADR_WIDTH - 1:0] debug_adr, rd_ptr_adr, wr_ptr_adr;
    logic was_busy, busy_reg, is_cmd, rd_ena, is_debug;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
            debug_adr <= '0;
            rd_ptr_adr <= '0;
            wr_ptr_adr <= '0;
            data_bus.re <= 1'b0;
            data_bus.we <= 1'b0;
            data_bus.dat_mosi <= '0;
            data_bus.adr <= '0;
            busy_reg <= 1'b0;
            was_busy <= 1'b0;
            is_cmd <= 1'b0;
            rd_ena <= 1'b0;
            is_debug <= 1'b0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            busy_reg <= data_bus.busy;
            
            // Keep following "if" here
            if (!data_bus.busy && busy_reg && is_cmd) begin
                is_cmd <= 1'b0;
                is_debug <= 1'b0;
                if (rd_ena || is_debug) begin
                    wbs.wbs_dat <= {16'd0, data_bus.dat_miso};
                end
                else begin
                    wbs.wbs_dat <= {16'd0, 16'd0};
                end
                wbs.wbs_ack <= 1'b1;
            end
            
            if (data_bus.we && data_bus.busy) begin
                if (!was_busy) begin
                    data_bus.we <= 1'b0;
                end
                else begin
                    if (!data_bus.busy && busy_reg) begin
                        was_busy <= 1'b0;
                    end
                end
            end

            if (data_bus.re && data_bus.busy) begin
                if (!was_busy) begin
                    data_bus.re <= 1'b0;
                end
                else begin
                    if (!data_bus.busy && busy_reg) begin
                        was_busy <= 1'b0;
                    end
                end
            end
            
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack && !is_cmd) begin
                // wbs.wbs_ack <= 1'b1;
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        /*`WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                stop_adr <= wbs.wbm_dat[SDRAM_ADR_WIDTH - 1:0];
                                wbs.wbs_ack <= 1'b1;
                            end
                        end*/
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                debug_adr <= wbs.wbm_dat[SDRAM_ADR_WIDTH - 1:0];
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        `WB_ADR_INT_WIDTH'd2: begin
                            if (wbs.wbm_sel[1:0] == 2'h3) begin
                                data_bus.we <= 1'b1;
                                data_bus.re <= 1'b0;
                                data_bus.dat_mosi <= wbs.wbm_dat[15:0];
                                data_bus.adr <= debug_adr;
                                was_busy <= data_bus.busy;
                                is_cmd <= 1'b1;
                                is_debug <= 1'b1;
                                // wbs.wbs_ack <= 1'b0;
                            end
                        end
                        `WB_ADR_INT_WIDTH'd3: begin
                            if (wbs.wbm_sel[1:0] == 2'h3) begin
                                data_bus.we <= 1'b1;
                                data_bus.re <= 1'b0;
                                data_bus.dat_mosi <= wbs.wbm_dat[15:0];
                                data_bus.adr <= wr_ptr_adr;
                                was_busy <= data_bus.busy;
                                is_cmd <= 1'b1;
                                // wbs.wbs_ack <= 1'b0;
                                if (wr_ptr_adr != stop_adr) begin
                                    wr_ptr_adr <= wr_ptr_adr + 1'b1;
                                end
                                else begin
                                    rd_ena <= 1'b1;
                                end
                            end
                        end
                        default: begin
                            wbs.wbs_ack <= 1'b1;
                        end
                    endcase
                end 
                else begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                wbs.wbs_dat <= {{(32 - SDRAM_ADR_WIDTH){1'b0}}, stop_adr};
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel == 4'hf) begin
                                wbs.wbs_dat <= {{(32 - SDRAM_ADR_WIDTH){1'b0}}, debug_adr};
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        `WB_ADR_INT_WIDTH'd2: begin
                            if (wbs.wbm_sel[1:0] == 2'h3) begin
                                data_bus.re <= 1'b1;
                                data_bus.we <= 1'b0;
                                data_bus.adr <= debug_adr;
                                was_busy <= data_bus.busy;
                                is_cmd <= 1'b1;
                                is_debug <= 1'b1;
                                // wbs.wbs_ack <= 1'b0;
                            end
                        end
                        `WB_ADR_INT_WIDTH'd3: begin
							if (rd_ena) begin
								if (wbs.wbm_sel[1:0] == 2'h3) begin
									data_bus.re <= 1'b1;
									data_bus.we <= 1'b0;
									data_bus.adr <= rd_ptr_adr;
									was_busy <= data_bus.busy;
									is_cmd <= 1'b1;
									// wbs.wbs_ack <= 1'b0;
									if (rd_ptr_adr != stop_adr)
										rd_ptr_adr <= rd_ptr_adr + 1'b1;
									else
										rd_ptr_adr <= '0;
								end
							end
							else begin
								wbs.wbs_dat <= {16'd0, 16'd0};
								wbs.wbs_ack <= 1'b1;
							end
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                            wbs.wbs_ack <= 1'b1;
                        end
                    endcase
                end
            end
        end
        
        // Keep following "if" here
        /* if (!data_bus.busy && busy_reg && is_cmd) begin
            // wbs.wbs_ack <= 1'b1;
            wbs.wbs_dat <= {16'd0, data_bus.dat_miso};
        end*/
    end    
endmodule: larva_sdram2wb