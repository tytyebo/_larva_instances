`timescale 1 ns/ 1 ns
`include "defines.svh"
`include "interfaces.svh"
    
module tb_larva_sdram_phy();
	bit nrst, clk, clk_sdram;
    sdram_io #(.ADR_WIDTH(13), .DATA_WIDTH(16)) sdram();
    generic_bus_io#(.ADR_WIDTH(25), .DATA_WIDTH(16)) data_bus();
    wishbone_io wbs();
    logic [31:0] wb_data;
    /* task bus_write(input bit [24:0] adr, bit [15:0] data);
        if (data_bus.busy) begin
            @(negedge data_bus.busy);
        end
        data_bus.we <= 1'b1;
        data_bus.dat_mosi <= data;
        data_bus.adr <= adr;
        @(posedge clk);
        @(posedge clk);
        if (data_bus.busy) begin
            data_bus.we <= 1'b0;
        end
    endtask: bus_write

    task bus_read(input bit [24:0] adr);
        if (data_bus.busy) begin
            @(negedge data_bus.busy);
        end
        data_bus.re <= 1'b1;
        data_bus.adr <= adr;
        @(posedge clk);
        @(posedge clk);
        if (data_bus.busy) begin
            data_bus.re <= 1'b0;
        end
    endtask: bus_read*/

    task bus_write(input bit [15:0] adr, bit [31:0] data, bit [3:0] sel);
        while (wbs.wbs_ack) begin
            @(posedge clk);
        end
        wbs.wbm_we <= 1'b1;
        wbs.wbm_stb <= 1'b1;
        wbs.wbm_cyc <= 1'b1;
        wbs.wbm_sel <= sel;
        wbs.wbm_adr <= adr;
        wbs.wbm_dat <= data;
        while (!wbs.wbs_ack) begin
            @(posedge clk);
        end
        wbs.wbm_stb <= 1'b0;
        wbs.wbm_cyc <= 1'b0;
    endtask: bus_write

    task bus_read(input bit [15:0] adr, bit [3:0] sel);
        while (wbs.wbs_ack) begin
            @(posedge clk);
        end
        wbs.wbm_we <= 1'b0;
        wbs.wbm_stb <= 1'b1;
        wbs.wbm_cyc <= 1'b1;
        wbs.wbm_sel <= sel;
        wbs.wbm_adr <= adr;
        while (!wbs.wbs_ack) begin
            @(posedge clk);
        end
        wb_data <= wbs.wbs_dat;
        wbs.wbm_stb <= 1'b0;
        wbs.wbm_cyc <= 1'b0;
    endtask: bus_read
    
	initial begin
        wb_data <= '0;
        nrst <= 1'b0;
        wbs.reset_master();
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        #10
        nrst <= 1'b1;
        repeat (2000) begin
            @(posedge clk);
        end
        bus_write(16'd0, 32'd3, 4'd15);
        
        bus_write(16'd1, 32'd963, 4'd15);
        bus_write(16'd2, 32'd963, 4'd3);
        bus_read(16'd2, 4'd3);
        
        bus_write(16'd3, 32'h0123, 4'd3);
        bus_write(16'd3, 32'h4567, 4'd3);
        bus_write(16'd3, 32'h89AB, 4'd3);
        bus_write(16'd3, 32'hCDEF, 4'd3);
        
        bus_read(16'd3, 4'd3);
        bus_read(16'd3, 4'd3);
        bus_read(16'd3, 4'd3);
        bus_read(16'd3, 4'd3);
        
        bus_read(16'd3, 4'd3);
        bus_read(16'd3, 4'd3);
    end
    
	always #39.736 clk = !clk; // 12.582912 MHz
    
    initial begin
        while (1) begin
            @(posedge clk)
            clk_sdram <= #39.736 clk;
            @(negedge clk)
            clk_sdram <= #39.736 clk;
        end
    end
            
    wire [15:0] dq;
    assign dq = (!sdram.we_n) ? sdram.dq_out : 'z;
    sdr mem(
        .Dq(dq),
        .Addr(sdram.a),
        .Ba(sdram.ba),
        .Clk(clk_sdram),
        .Cke(sdram.cke),
        .Cs_n(sdram.cs_n),
        .Ras_n(sdram.ras_n),
        .Cas_n(sdram.cas_n),
        .We_n(sdram.we_n),
        .Dqm(sdram.dqm));  
    
    assign sdram.dq_in = dq;
    // larva_sdram_phy#(.RAM_ADR_WIDTH(13), .RAM_DATA_WIDTH(16), .RAM_COLUMN_WIDTH(10), .RAM_CLK(12.582912)) 
    // phy(.clk(clk), .nrst(nrst), .sdram(sdram), .data_bus(data_bus.slave));
    
    larva_sdram2wb sdram2wb(.clk(clk), .nrst(nrst), .sdram(sdram), .wbs(wbs));
endmodule: tb_larva_sdram_phy


