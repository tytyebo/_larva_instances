`include "interfaces.svh"
module larva_fifo1(input wr_nrst, rd_nrst, fifo_io fifo);
    logic [3:0] rd_loop_wrclk;
    always_ff @(posedge fifo.wrclk or negedge wr_nrst) begin
        if (!wr_nrst) begin
            fifo.wrfull <= 1'b0;
        end
        else begin
            if (!fifo.wrfull && fifo.wrreq) begin
                fifo.wrfull <= 1'b1;
            end
            else begin
                if (fifo.wrfull && rd_loop_wrclk[2] && !rd_loop_wrclk[3])
                    fifo.wrfull <= 1'b0;
            end
        end
    end
    
    always_ff @(posedge fifo.wrclk or negedge wr_nrst) begin
        if (!wr_nrst) begin
            fifo.q <= '0;
        end
        else begin
            if (!fifo.wrfull && fifo.wrreq) begin
                fifo.q <= fifo.data;
            end
        end
    end

    logic [2:0] rd_loopback;
    logic wr_loop;
    always_ff @(posedge fifo.wrclk or negedge wr_nrst) begin
        if (!wr_nrst) begin
            wr_loop <= 1'b0;
        end
        else begin
            if (!wr_loop && fifo.wrreq && !fifo.wrfull) begin
                wr_loop <= 1'b1;
            end
            else begin
                if (wr_loop && rd_loopback[2]) begin 
                    wr_loop <= 1'b0;
                end
            end

        end
    end

    logic [3:0] wr_loop_rdclk;
    always_ff @(posedge fifo.rdclk or negedge rd_nrst) begin                             
        if (!rd_nrst) begin
            wr_loop_rdclk <= '0;
        end
        else begin
            wr_loop_rdclk[0] <= wr_loop;
            for (int i = 0; i < $bits(wr_loop_rdclk) - 1; i++) begin
                wr_loop_rdclk[i + 1] <= wr_loop_rdclk[i];
            end
        end
    end

    always_ff @(posedge fifo.rdclk or negedge rd_nrst) begin
        if (!rd_nrst) begin
            fifo.rdfull <= 1'b0;
        end
        else begin
            if (!fifo.rdfull && wr_loop_rdclk[2] && !wr_loop_rdclk[3]) begin
                fifo.rdfull <= 1'b1;
            end
            else begin 
                if(fifo.rdfull && fifo.rdreq) begin
                    fifo.rdfull <= 1'b0;
                end
            end
        end
    end

    always_ff @(posedge fifo.wrclk or negedge wr_nrst) begin
        if (!wr_nrst) begin
            rd_loopback <= '0;
        end
        else begin
            rd_loopback[0] <= wr_loop_rdclk[2];
            for (int i = 0; i < $bits(rd_loopback) - 1; i++) begin
                rd_loopback[i + 1] <= rd_loopback[i];
            end
        end
    end

    logic rd_loop;
    logic [2:0] wr_loopback;
    always_ff @ (posedge fifo.rdclk or negedge rd_nrst) begin
        if (!rd_nrst) begin
            rd_loop <= 1'b0;
        end
        else begin
            if (!rd_loop && fifo.rdfull && fifo.rdreq) begin
                rd_loop <= 1'b1;
            end
            else begin
                if (rd_loop && wr_loopback[2]) begin
                    rd_loop <= 1'b0;
                end
            end
        end
    end

    always_ff @(posedge fifo.wrclk or negedge wr_nrst) begin
        if (!wr_nrst) begin 
            rd_loop_wrclk <= '0;
        end
        else begin
            rd_loop_wrclk[0] <= rd_loop;
            for (int i = 0; i < $bits(rd_loop_wrclk) - 1; i++) begin
                rd_loop_wrclk[i + 1] <= rd_loop_wrclk[i];
            end
        end
    end

    always_ff @(posedge fifo.rdclk or negedge rd_nrst) begin
        if (!rd_nrst) begin
            wr_loopback <= '0;
        end
        else begin
            wr_loopback[0] <= rd_loop_wrclk[2];
            for (int i = 0; i < $bits(wr_loopback) - 1; i++) begin
                wr_loopback[i + 1] <= wr_loopback[i];
            end
        end
    end
endmodule: larva_fifo1
