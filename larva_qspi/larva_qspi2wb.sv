`include "interfaces.svh"
`include "defines.svh"
module larva_qspi2wb#(parameter int QSPI_DUMMY = 10)
                     (input bit clk, clk24, nrst, 
                      qspi_io.slave qspi,
                      wishbone_io.master wbm);

    typedef enum bit {ST_IDLE, ST_ACK} state_e;
    state_e state;
	
    logic nrst24;
    sync_async_reset SyncAsyncReset(.clk(clk24), .nrst(nrst), .synced_nrst(nrst24));
    
    fifo_io#(.DATA_WIDTH(49)) rx_fifo();
    assign rx_fifo.wrclk = clk24;
    assign rx_fifo.rdclk = clk;
    
    fifo_io#(.DATA_WIDTH(32)) tx_fifo();
    assign tx_fifo.wrclk = clk;
    assign tx_fifo.rdclk = clk24;
    
    larva_fifo1 larvaRxFifo1(.wr_nrst(nrst24), .rd_nrst(nrst),.fifo(rx_fifo));
    larva_fifo1 larvaTxFifo1(.wr_nrst(nrst), .rd_nrst(nrst24), .fifo(tx_fifo));
    
    always_ff @ (posedge clk or negedge nrst) begin
        if (!nrst) begin 
            wbm.wbm_adr <= '0;
            wbm.wbm_dat <= '0;
            wbm.wbm_sel <= '0;
            wbm.wbm_cyc <= 1'b0;
            wbm.wbm_stb <=  1'b0;
            wbm.wbm_we <= 1'b0;
            state <= ST_IDLE;
            rx_fifo.rdreq <= 1'b0;
            tx_fifo.data <= '0;
            tx_fifo.wrreq <= 1'b0;
        end
        else begin
            wbm.wbm_sel <= '1;
            rx_fifo.rdreq <= 1'b0;
            tx_fifo.wrreq <= 1'b0;
            unique case (state)
                ST_IDLE: begin
                    if (rx_fifo.rdfull) begin
                        wbm.wbm_dat <= rx_fifo.q[31:0];
                        wbm.wbm_adr <= rx_fifo.q[47:32];
                        wbm.wbm_we <= rx_fifo.q[48];
                        wbm.wbm_stb <= 1'b1;
                        wbm.wbm_cyc <= 1'b1;
                        rx_fifo.rdreq <= 1'b1;
                        state <= ST_ACK;
                    end
                end
                ST_ACK: begin
                    if (wbm.wbs_ack) begin
                        tx_fifo.data <= wbm.wbs_dat;
                        if (!wbm.wbm_we) begin
                            tx_fifo.wrreq <= 1'b1;
                        end
                        wbm.wbm_stb <= 1'b0;
                        wbm.wbm_cyc <= 1'b0;
                        state <= ST_IDLE;
                    end
                end
            endcase
        end
    end 
    larva_qspi_phy#(.DUMMY_LENGTH(QSPI_DUMMY)) larvaQspiPhy(.clk(clk24), .nrst(nrst24), .qspi(qspi), .rx_fifo(rx_fifo), .tx_fifo(tx_fifo));
 endmodule: larva_qspi2wb
