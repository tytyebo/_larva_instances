/*
 * ������ "larva_qspi_phy" ��� ������� ft75_main.
 * ���������� ����������� ������ ��� ����� � ����������������� �� ���� Quad SPI.
 * ���������� ������������ ��� ������ � �������� Slave.
 * ���� ������������ ������ nrst ������ ���� ��������������� � ������� �������� clk.
 * ����������� tx_fifo.q ������ ���� 32.
 * ���������� ��������� �� ��, ��� sck_clk � 4 ���� ��������� clk.
 * 
 * �������� �.�.
 * ����, �������� ����
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"

module larva_qspi_phy#(parameter int DUMMY_LENGTH = 10)
                      (input  bit clk, nrst, 
                       qspi_io.slave qspi,
                       fifo_io.for_write rx_fifo,
                       fifo_io.for_read tx_fifo); // ����������� tx_fifo.q ������ ���� 32

    logic ncs, sck;
    logic [3:0] io_mosi;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(6))
    metaChainQspi(.clk(clk), .nrst(nrst),
                 .din({ qspi.ncs, qspi.sck, qspi.io_mosi}), .dout({ncs, sck, io_mosi}));
                 

    logic sck_q, sck_rise;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            sck_q <= 1'b0;
        end
        else begin
            sck_q <= sck;
        end
    end
    assign sck_rise = sck & ~sck_q;

    typedef enum bit [2:0] {ST_ADR, ST_CMD, ST_WR, ST_DUMMY, ST_RD, ST_INF, ST_CSUM, ST_END_RD} state_e;
    state_e state;

    logic [4:0] sck_cnt;
    logic [15:0] adr; // ����� � ��������� ��������
    logic [31:0] data; // ���� � ��������� ��������
    logic [1:0] cmd;// ������� ������\������
    
    logic fl_rlost;// ����, ��� �� ������� ������� ������� ������, �������� �����������:fifo_tx � ������ wrreq_tx ����� full (�� ���� �� ������ � ������)
    logic fl_wlost;// ����, ��� �� ������� ������� ������� ������, �������� �����������:fifo_tx � ������ wrreq_tx ����� full (�� ���� �� ������ � ������)
    logic fl_nodata;// ����, ��� � ������ ����� ����� ���������� ����� dummy rdfull==0
    
    logic [3:0] csum;//����������� �����

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            rx_fifo.wrreq <= 1'b0;
            tx_fifo.rdreq <= 1'b0;
            sck_cnt <= '0;
            adr <= '0;
            data <= '0;
            cmd <= '0;
            state <= ST_ADR;
            fl_rlost <= 1'b0;
            fl_wlost <= 1'b0;
            fl_nodata <= 1'b0;
            csum <= '0;
            qspi.io_miso <= '0;

            qspi.dir <= 1'b0; //���������� ������  � IO
        end
        else begin
            rx_fifo.wrreq <= 1'b0;
            tx_fifo.rdreq <= 1'b0;
            if (!ncs) begin
                if (sck_rise) begin
                    unique case (state)
                        ST_ADR: begin
                            sck_cnt <= sck_cnt + 1'b1;
                            adr[3:0] <= io_mosi;
                            for (int i = $bits(adr) - 1; i > 4; i = i - 4) begin
                                adr[i] <= adr[i - 4];
                                adr[i - 1] <= adr[i - 5];
                                adr[i - 2] <= adr[i - 6];
                                adr[i - 3] <= adr[i - 7];
                            end
                            if (sck_cnt == 5'd3) begin
                                state <= ST_CMD;
                            end
                        end
                        ST_CMD: begin
                            sck_cnt <= sck_cnt + 1'b1;
                            if (sck_cnt == 5'd4) begin
                                cmd <= io_mosi[1:0];
                            end
                            else begin
                                sck_cnt <= '0;
                                if (cmd[1]) begin
                                    state <= ST_WR;
                                end
                                else begin
                                    if (!rx_fifo.wrfull) begin
                                        if (!cmd[0]) begin
                                            rx_fifo.wrreq <= 1'b1;
                                            if (tx_fifo.rdfull) begin
                                                tx_fifo.rdreq <= 1'b1;
                                            end
                                        end
                                    end
                                    else begin    
                                        fl_rlost <= 1'b1;                                        
                                    end
                                    state <= ST_DUMMY;
                                end
                            end
                        end
                        ST_WR: begin
                            sck_cnt <= sck_cnt + 1'b1;
                            data[3:0] <= io_mosi;
                            for (int i = $bits(data) - 1; i > 4; i = i - 4) begin
                                data[i] <= data[i - 4];
                                data[i - 1] <= data[i - 5];
                                data[i - 2] <= data[i - 6];
                                data[i - 3] <= data[i - 7];
                            end
                            
                            if (sck_cnt == 5'd7) begin
                                sck_cnt <= '0;
                                if (!rx_fifo.wrfull) begin
                                    rx_fifo.wrreq <= 1'b1;
                                end
                                else begin
                                    fl_wlost <= 1'b1;
                                end
                                state <= ST_ADR;
                            end
                        end
                        ST_DUMMY: begin
                            sck_cnt <= sck_cnt + 1'b1;
                            if (sck_cnt == DUMMY_LENGTH - 2) begin
                                sck_cnt <= '0;
                                if (!tx_fifo.rdfull) begin
                                    fl_nodata <= 1'b1;
                                end
                                else begin
                                    fl_nodata <= 1'b0;
                                end
                                state <= ST_RD;
                            end
                        end
                        ST_RD: begin
                            qspi.dir <= 1'b1;
                            sck_cnt <= sck_cnt + 1'b1;
                            if (!fl_nodata) begin
                                unique case (sck_cnt)
                                    5'd0: begin
                                        qspi.io_miso <= tx_fifo.q[31:28];
                                        csum <= csum + tx_fifo.q[31:28];
                                    end
                                    5'd1: begin
                                        qspi.io_miso <= tx_fifo.q[27:24];
                                        csum <= csum + tx_fifo.q[27:24];
                                    end
                                    5'd2: begin
                                        qspi.io_miso <= tx_fifo.q[23:20];
                                        csum <= csum + tx_fifo.q[23:20];
                                    end
                                    5'd3: begin
                                        qspi.io_miso <= tx_fifo.q[19:16];
                                        csum <= csum + tx_fifo.q[19:16];
                                    end
                                    5'd4: begin
                                        qspi.io_miso <= tx_fifo.q[15:12];
                                        csum <= csum + tx_fifo.q[15:12];
                                    end
                                    5'd5: begin
                                        qspi.io_miso <= tx_fifo.q[11:8];
                                        csum <= csum + tx_fifo.q[11:8];
                                    end
                                    5'd6: begin
                                        qspi.io_miso <= tx_fifo.q[7:4];
                                        csum <= csum + tx_fifo.q[7:4];
                                    end
                                    5'd7: begin
                                        qspi.io_miso <= tx_fifo.q[3:0];
                                        csum <= csum + tx_fifo.q[3:0];
                                    end
                                endcase
                            end
                            else begin
                                qspi.io_miso <= '0;
                                csum <= '0;
                            end
                            if (sck_cnt  == 5'd7) begin
                                if (!fl_nodata) begin
                                    tx_fifo.rdreq <= 1'b1;
                                end
                                sck_cnt <= '0;
                                state <= ST_INF;
                            end
                        end                        
                        ST_INF: begin
                            qspi.io_miso <= {1'b0, fl_wlost, fl_rlost, fl_nodata};
                            csum <= csum + {1'b0, fl_wlost, fl_rlost, fl_nodata};
                            state <= ST_CSUM;
                        end
                        ST_CSUM: begin
                            qspi.io_miso <= csum;
                            state <= ST_END_RD;
                        end
                        ST_END_RD: begin
                            fl_wlost <= 1'b0;
                            fl_rlost <= 1'b0;
                            qspi.dir <= 1'b0;
                            state <= ST_ADR;
                        end
                    endcase
                end
            end
            else begin
                csum <= '0;
                qspi.dir <= 1'b0;
                state <= ST_ADR;
            end
		end
    end
    assign rx_fifo.data = {cmd[1], adr, data};
endmodule
