/*
 * ������ "larva_master_qspi_phy" ��� testbecnh ������� ft75_main.
 * ���������� ����������� ������ ��� �������� ���� Quad SPI ����������������.
 * ���������� ������������ ��� ������ � �������� Master.
 * ��������� �� ������ � ������� �������� clk 24 ���.
 * ���������� ��������� �� ��, ��� sck_clk � 4 ���� ��������� clk.
 * 
 * �������� �.�.
 * ����, �������� ����
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"
import classes_pkg::qspiPacket_s;
module larva_master_qspi_phy#(parameter int DUMMY_LENGTH = 3)
                      (input  bit clk, nrst, 
                       qspi_io.master qspi,
                       generic_bus_io.slave data_bus);
    
    localparam bit[1:0] NEG_VAL = 2'd1, ENA_VAL = 2'd3;  
    logic clk_ena;   
    logic [1:0] cnt_ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            clk_ena <= 1'b0;
            qspi.sck <= 1'b0;
            cnt_ena <= '0;
        end
        else begin
            clk_ena <= 1'b0;
            cnt_ena <= cnt_ena + 1'b1;
            if (cnt == NEG_VAL) begin
                qspi.sck <= ~qspi.sck;
            end
            if (cnt == ENA_VAL) begin
                cnt <= '0;
                clk_ena <= 1'b1;
            end
        end
    end

    clock_enable_by_param#(.CNT_ENA(3)) clkEna(.clk(clk), .nrst(nrst), .ena(ena), .clk_ena(clk_ena));
    clock_enable_by_param#(.CNT_ENA(1)) clkEnaSck(.clk(clk), .nrst(nrst), .ena(ena), .clk_ena(clk_ena));
    
    typedef enum {ST_ADR, ST_CMD, ST_WR, ST_DUMMY, ST_RD, ST_INF, ST_CSUM, ST_END_RD} state_e;
    state_e state;

    logic we;
    qspiPacket_s packet;
    logic [:0] sck_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            ena <= 1'b0;
            data_bus.reset_slave();
            we <= 1'b0;
            qspi.io_mosi <= '0;
            qspi.ncs <= 1'b0;
            sck_cnt <= '0;

        end
        else begin
            unique case (state)   
                ST_IDLE: begin
                    qspi.ncs <= 1'b1;
                    we <= 1'b0;
                    if (data_bus.re || data_bus.we) begin
                        packet <= data_bus.mosi;
                        state <= ST_ADR;
                        data_bus.busy <= 1'b1;
                    end
                    if (data_bus.we) begin
                        we <= 1'b1;
                    end
                end
                ST_ADR: begin
                    if (clk_ena) begin
                        sck_cnt <= sck_cnt + 1'b1;
                        qspi.ncs <= 1'b0;
                        for (int i = $bits(packet.adr) - 1; i > 4; i = i - 4) begin
                            qspi.io_mosi[3] <= packet.adr[i];
                            qspi.io_mosi[2] <= packet.adr[i - 1];
                            qspi.io_mosi[1] <= packet.adr[i - 2];
                            qspi.io_mosi[0] <= packet.adr[i - 3];
                        end
                        if (sck_cnt == 5'd3) begin
                            state <= ST_CMD;
                        end
                    end
                ST_CMD: begin
                    if (clk_ena) begin
                        sck_cnt <= sck_cnt + 1'b1;
                        for (int i = $bits(packet.cmd) - 1; i > 4; i = i - 4) begin
                            qspi.io_mosi[3] <= packet.cmd[i];
                            qspi.io_mosi[2] <= packet.cmd[i - 1];
                            qspi.io_mosi[1] <= packet.cmd[i - 2];
                            qspi.io_mosi[0] <= packet.cmd[i - 3];
                        end
                        if (sck_cnt == 5'd5) begin
                            state <= ST_DATA;
                        end
                    end
                end
                ST_DATA: begin
                    if (clk_ena) begin
                        sck_cnt <= sck_cnt + 1'b1;
                        for (int i = $bits(packet.data) - 1; i > 4; i = i - 4) begin
                            qspi.io_mosi[3] <= packet.data[i];
                            qspi.io_mosi[2] <= packet.data[i - 1];
                            qspi.io_mosi[1] <= packet.data[i - 2];
                            qspi.io_mosi[0] <= packet.data[i - 3];
                        end
                        if (sck_cnt == 5'd13) begin
                            if (we) begin
                                state <= ST_END;
                            end
                            else begin
                                state <= ST_DUMMY;
                        end
                    end
                end
                ST_DUMMY: begin
                    if (clk_ena) begin
                        sck_cnt <= sck_cnt + 1'b1;
                        qspi.io_mosi <= '0;
                        if (sck_cnt == 5'd13 + DUMMY_LENGTH) begin
                            state <= ST_RD
                        end
                    end
                end
                ST_WR: begin
                    if (clk_ena) begin   
                        sck_cnt <= sck_cnt + 1'b1;                       
                        data_bus.miso[3:0] <= qspi.miso;
                        for (int i = $bits(data_bus.miso) - 1; i > 4; i = i - 4) begin
                            data_bus.miso[i] <= data_bus.miso[i - 4];
                            data_bus.miso[i-1] <= data_bus.miso[i - 5];
                            data_bus.miso[i-2] <= data_bus.miso[i - 6];
                            data_bus.miso[i-3] <= data_bus.miso[i - 7];
                        end
                        if (sck_cnt == 5'd23 + DUMMY_LENGTH) begin
                            state <= ST_IDLE;
                            qspi.ncs <= 1'b1;
                            data_bus.busy <= 1'b0;
                            sck_cnt <= '0;
                        end
                    end
                end
endmodule: larva_master_qspi_phy
