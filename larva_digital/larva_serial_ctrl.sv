/*
 * Модуль "larva_serial_ctrl" для проекта ft75_digital.
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2021
 * email: kuznetsov_ms@org.miet.ru
 */   
module larva_serial_ctrl#(parameter bit [7:0] BASE_ADR = '0)
                         (input bit clk, nrst,
                          generic_bus_io.slave_hndshk data_bus,
                          input logic pclk_fall,
                          input logic pclk,
                          input logic sync,  
                          serial_channel_io.rx sc,
                          output logic sync_ena);
    
    logic fifo_clear, fifo_rdreq;
    logic [1:0] ctrl_mode;
    logic [9:0] debug_cnt;
    logic fifo_full, fifo_wrreq, miss_data, we_phi, fifo_empty;
    logic [9:0] fifo_data_in, fifo_data_out, data_phi;
    logic [15:0] missing_data_counter;
    
    always_ff @(posedge clk or negedge nrst) begin // Обработка сообщений от КШВ
        if (!nrst) begin
            data_bus.ready <= 1'b0;
            fifo_rdreq <= 1'b0;
            missing_data_counter <= 16'd0;
            sync_ena <= 1'b0;
            fifo_clear <= 1'b1;
            data_bus.dat_miso <= 16'd0;
            ctrl_mode <= 2'd0;
            debug_cnt <= BASE_ADR;
        end
        else begin
            data_bus.ready <= 1'b0;
            fifo_rdreq <= 1'b0;
            fifo_clear <= 1'b0;
            if (data_bus.valid && !data_bus.ready) begin
                if (!data_bus.we) begin
                    if (data_bus.adr == BASE_ADR + 8'd1) begin
                        data_bus.ready <= 1'b1;
                        if (ctrl_mode == 2'd0)
                            if (fifo_empty)
                                data_bus.dat_miso <= 16'd0;
                            else begin
                                fifo_rdreq <= 1'b1;
                                data_bus.dat_miso <= {6'd0, {fifo_data_out[0], fifo_data_out[1], fifo_data_out[2], fifo_data_out[3], fifo_data_out[4], 
                                                             fifo_data_out[5], fifo_data_out[6], fifo_data_out[7], fifo_data_out[8], fifo_data_out[9]}};
                            end
                        if (ctrl_mode == 2'd1)
                            data_bus.dat_miso <= 16'd0;
                        if (ctrl_mode == 2'd2)
                            data_bus.dat_miso <= 16'hffff;
                        if (ctrl_mode == 2'd3) begin
                            data_bus.dat_miso <= debug_cnt;
                            debug_cnt <= debug_cnt + 1'b1;
                        end
                    end
                    else begin
                        if (data_bus.adr == BASE_ADR + 8'd2) begin
                            data_bus.ready <= 1'b1;
                            data_bus.dat_miso <= missing_data_counter;
                        end
                        else begin
                            data_bus.ready <= 1'b0;
                            data_bus.dat_miso <= '0;
                        end
                    end
                end
                else begin                   
                    if (data_bus.adr == BASE_ADR) begin // Настройка параметров работы
                        sync_ena <= data_bus.dat_mosi[0];
                        ctrl_mode <= data_bus.dat_mosi[2:1];
                        data_bus.ready <= 1'b1;
                    end
                    if (data_bus.adr == BASE_ADR + 8'd3) begin // Очистка FIFO
                        fifo_clear <= data_bus.dat_mosi[0];
                        data_bus.ready <= 1'b1;
                    end
                end
            end
            if (miss_data) begin
                missing_data_counter <= missing_data_counter + 1'b1;
            end
        end
    end
    
    si_phi phi0 (.clk(clk),
                 .nrst(nrst),
                 .enable(sync_ena),
                 .sinh_puls_in(pclk),
                 .pha_puls_in(sync),
                 .f(pclk_fall),
                 .data_line(sc.data),
                 .sinh_puls_out(sc.clk),
                 .pha_puls_out(sc.sync),
                 .request(sc.request),
                 .data_out(data_phi),
                 .we(we_phi));
    
    ll_pk_2_orb converter(.clk(clk),
                          .nrst(nrst),
                          .mode(8'hFF),
                          .we(we_phi),
                          .data(data_phi),
                          .fifo_full(fifo_full),
                          .re(fifo_wrreq),
                          .data_miss(miss_data),
                          .q(fifo_data_in));
    
    larva_generic_fifo fifo0(.aclr(fifo_clear),
                            .clock(clk),
                            .data(fifo_data_in),
                            .rdreq(fifo_rdreq),
                            .wrreq(fifo_wrreq),
                            .empty(fifo_empty),
                            .full(fifo_full),
                            .q(fifo_data_out));
endmodule: larva_serial_ctrl
