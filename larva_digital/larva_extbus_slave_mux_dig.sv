module larva_extbus_slave_mux(input bit [7:0] sel0, sel1, sel2, sel3, sel4, sel5, sel6, sel_adr,

                              input logic [15:0] mux_in0, mux_in1, mux_in2, mux_in3, mux_in4, mux_in5, mux_in6,
                              input logic ready_in0, ready_in1, ready_in2, ready_in3, ready_in4, ready_in5, ready_in6,  
                              output logic [15:0] mux_out,
                              output logic ready_out);
    always_comb begin
        if (sel_adr >= sel0 && sel_adr < sel1) begin
            mux_out <= mux_in0;
            ready_out <= ready_in0;
        end
        else begin
            if (sel_adr >= sel1 && sel_adr < sel2) begin
                mux_out <= mux_in1;
                ready_out <= ready_in1;
            end
            else begin
                if (sel_adr >= sel2 && sel_adr < sel3) begin
                    mux_out <= mux_in2;
                    ready_out <= ready_in2;
                end
                else begin
                    if (sel_adr >= sel3 && sel_adr < sel4) begin
                        mux_out <= mux_in3;
                        ready_out <= ready_in3;
                    end
                    else begin
                        if (sel_adr >= sel4 && sel_adr < sel5) begin
                            mux_out <= mux_in4;
                            ready_out <= ready_in4;
                        end
                        else begin
                            if (sel_adr >= sel5 && sel_adr < sel6) begin
                                mux_out <= mux_in5;
                                ready_out <= ready_in5;
                            end
                            else begin
                                if (sel_adr >= sel6) begin
                                    mux_out <= mux_in6;
                                    ready_out <= ready_in6;
                                end
                                else begin
                                    mux_out <= '0;
                                    ready_out <= 1'b0;
                                end
                            end
                        end
                    end
                end
            end
        end
    end
endmodule: larva_extbus_slave_mux_dig
