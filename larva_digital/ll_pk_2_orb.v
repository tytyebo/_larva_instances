/***********************************************************************
 * Copiright 2019 ZITC
 **********************************************************************/
/*
 * Module'PC_to_Orbita_converter' for parallel interface.
 * Made by NotBlacksmithMax.
 */

module ll_pk_2_orb (
    input clk,
    input nrst,
    input [7:0] mode,
    input we,
    input [9:0] data,
    input fifo_full,
    output reg re,
    output reg data_miss,
    output reg [9:0] q);

    reg [2:0] channel_counter;

    always @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            re <= 1'b0;
            channel_counter <= 3'd0;
        end
        else begin
            re <= 1'b0;
            data_miss <= 1'b0;
            if (we) begin
                if (fifo_full)
                    data_miss <= 1'b1;
                else begin
                    channel_counter <= channel_counter + 1'b1;
                    if (mode[channel_counter]) begin
                        q <= ~data;
                        re <= 1'b1;
                    end
                end
            end
        end
    end
endmodule
