/***********************************************************************
 * Copiright 2020 ZITC
 **********************************************************************/
/*
 * Module'Serial interface "ТМП"'.
 * Made by NotBlacksmithMax.
 * На вход(clk) приходят 12,582912 МГц.
 * Сигналы СИ и М4_ТМП приходят с блока общего тактирования и перезащелктваются с сигнадом разрешения.
 * Импульсы сигнала F (fall) используются для формирования запросов ТМП и считывания данных.
 * Длительность запроса ТМП равна одному периоду СИ.
 */
 module si_phi (
        input clk,
        input nrst,
        input enable,
        input sinh_puls_in,
        input pha_puls_in,
        input f,
        input data_line,
        output reg sinh_puls_out,
        output reg pha_puls_out,
        output reg request,
        output reg [9:0] data_out,
        output reg we
    );
    
    reg fall;
    reg was_start;
    reg [8:0] fall_counter;
    
    always @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            sinh_puls_out <= 1'b0;
            pha_puls_out <= 1'b0;
        end
        else begin
            if (enable) begin
                sinh_puls_out <= sinh_puls_in;
                pha_puls_out <= pha_puls_in;
            end
            else begin
                sinh_puls_out <= 1'b0;
                pha_puls_out <= 1'b0;
            end
        end
    end
    
    always @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            was_start <= 1'b0;
            fall_counter <= 9'd0;
            request <= 1'b0;
            we <= 1'b0;
            data_out <= 10'd0;
        end
        else begin
            we <= 1'b0;
            fall <= f;
            if (sinh_puls_out && fall && (~was_start)) begin
                was_start <= 1'b1;
                request <= 1'b1;
                fall_counter <= 9'd1;
            end
            if (fall && was_start) begin
                case (fall_counter)
                    9'd0: request <= 1'b1;
                    9'd1: request <= 1'b0;
                    9'd4: data_out[0] <= data_line;
                    9'd5: data_out[1] <= data_line;
                    9'd6: data_out[2] <= data_line;
                    9'd7: data_out[3] <= data_line;
                    9'd8: data_out[4] <= data_line;
                    9'd9: data_out[5] <= data_line;
                    9'd10: data_out[6] <= data_line;
                    9'd11: data_out[7] <= data_line;
                    9'd12: data_out[8] <= data_line;
                    9'd13: begin
                        data_out[9] <= data_line;
                        we <= 1'b1;
                    end
                endcase
                fall_counter <= fall_counter + 1'b1;
            end
            if (~enable) begin
                was_start <= 1'b0;
                we <= 1'b0;
                request <= 1'b0;
                data_out <= 10'd0;
            end
        end
    end
endmodule
