/*
 * Модуль "larva_pc_ctrl" для проекта ft75_digital.
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2021
 * email: kuznetsov_ms@org.miet.ru
 */   
`include "defines.svh"
`include "interfaces.svh"
module larva_pc_ctrl#(parameter bit [7:0] BASE_ADR = '0)
                     (input bit clk, nrst,
                      generic_bus_io.slave_hndshk data_bus,
                      input logic pclk_fall,
                      input logic pclk_rise,
                      input logic pclk,
                      input logic sync,
                      parallel_channel_io.rx pc,
                      output logic sync_ena);
    
    logic fifo_clear, fifo_rdreq;
    logic [1:0] ctrl_mode;
    logic [7:0] mode;
    logic [5:0] speed;
    logic [9:0] debug_cnt;
    logic [9:0] data_phi, fifo_data_in, fifo_data_out;
    logic we_phi, fifo_full, fifo_empty, fifo_wrreq, miss_data;
    logic [15:0] missing_data_counter;
    
    // Обработка сообщений от КШВ
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.ready <= 1'b0;
            fifo_rdreq <= 1'b0;
            missing_data_counter <= '0;
            sync_ena <= 1'b0;
            fifo_clear <= 1'b1;
            data_bus.dat_miso  <= '0;
            ctrl_mode <= '0;
            debug_cnt <= BASE_ADR;
        end
        else begin
            data_bus.ready <= 1'b0;
            fifo_rdreq <= 1'b0;
            fifo_clear <= 1'b0;
            if (data_bus.valid && !data_bus.ready) begin
                data_bus.ready <= 1'b1;
                if (!data_bus.we) begin
                    unique case (data_bus.adr)
                        (BASE_ADR + 8'd2): begin
                            unique case (ctrl_mode)
                                2'd0: begin
                                    if (fifo_empty) begin
                                        data_bus.dat_miso  <= '0;
                                    end
                                    else begin
                                        fifo_rdreq <= 1'b1;
                                        data_bus.dat_miso  <= {6'd0, fifo_data_out};
                                    end
                                end
                                2'd1: begin
                                    data_bus.dat_miso  <= '0;
                                end
                                2'd2: begin
                                    data_bus.dat_miso  <= '1;
                                end
                                2'd3: begin
                                    data_bus.dat_miso  <= debug_cnt;
                                    debug_cnt <= debug_cnt + 1'b1;
                                end
                            endcase
                        end
                        (BASE_ADR + 8'd3): begin
                            data_bus.dat_miso  <= missing_data_counter;
                        end
                        default: begin
                            data_bus.dat_miso <= '0;
                            data_bus.ready <= 1'b0;
                        end
                    endcase 
                end
                else begin
                    unique case (data_bus.adr)         
                        BASE_ADR: begin // Настройка параметров работы
                            sync_ena <= data_bus.dat_mosi[0];
                            ctrl_mode <= data_bus.dat_mosi[2:1];
                        end
                        (BASE_ADR + 8'd1): begin // Настройка парамметров интерфейса
                            mode <= data_bus.dat_mosi[7:0];
                            speed <= data_bus.dat_mosi[13:8];
                        end
                        (BASE_ADR + 8'd4): begin // Очистка FIFO
                            fifo_clear <= data_bus.dat_mosi[0];
                        end                        
                        default: begin
                            data_bus.ready <= 1'b0;
                        end
                    endcase
                end
            end
            if (miss_data) begin
                missing_data_counter <= missing_data_counter + 1'b1;
            end
        end
    end
    
    ll_phi phi0 (.clk(clk),
                 .nrst(nrst),
                 .enable(sync_ena),
                 .sinh_puls_in(pclk),
                 .pha_puls_in(sync),
                 .f(pclk_fall),
                 .r(pclk_rise),
                 .data_in(pc.data),
                 .speed(speed),
                 .sinh_puls_out(pc.clk),
                 .pha_puls_out(pc.sync),
                 .request(pc.request),
                 .data_out(data_phi),
                 .we(we_phi));
    
    ll_pk_2_orb converter(
        .clk(clk),
        .nrst(nrst),
        .mode(mode),
        .we(we_phi),
        .data(data_phi),
        .fifo_full(fifo_full),
        .re(fifo_wrreq),
        .data_miss(miss_data),
        .q(fifo_data_in));
    
    larva_generic_fifo fifo1(
        .aclr(fifo_clear),
        .clock(clk),
        .data(fifo_data_in),
        .rdreq(fifo_rdreq),
        .wrreq(fifo_wrreq),
        .empty(fifo_empty),
        .full(fifo_full),
        .q(fifo_data_out));
endmodule: larva_pc_ctrl
