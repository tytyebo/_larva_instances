/***********************************************************************
 * Copiright 2020 ZITC
 **********************************************************************/
/*
 * Module'Parallel interface'
 * Made by NotBlacksmithMax.
 * На вход(clk) приходят 12,582912 МГц.
 * Сигналы СИ и М4 приходят с блока общего тактирования и перезащелктваются с сигнадом разрешения.
 * Импульсы сигнала F (fall) используются для формирования запросов ЦФП.
 * Импульсы сигнала R (rise) используются для считывания информации.
 * Длительность импульса ЦФП составляет 89 тактов сигнала clk (~7,07 мкс).
 * Соответствие информативности слов в минуту и параметра speed "Слов/с(код, hex)":
 * 65536(0x2), 32768(0x4), 16384(0x8), 8192(0x10), 4096(0x20).
 */
module ll_phi (
        input clk,
        input nrst,
        input enable,
        input sinh_puls_in,
        input pha_puls_in,
        input f,
        input r,
        input [9:0] data_in,
        input [5:0] speed,
        output reg sinh_puls_out,
        output reg pha_puls_out,
        output reg request,
        output reg [9:0] data_out,
        output reg we
    );
    
    reg was_start, fall;
    reg [5:0] fall_counter;
    reg [6:0] count_request;
    //Защелкивание СИ и М4 по сигналу разрешения генерации
    always @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            sinh_puls_out <= 1'b0;
            pha_puls_out <= 1'b0;
        end
        else begin
            if (enable) begin
                sinh_puls_out <= sinh_puls_in;
                pha_puls_out <= pha_puls_in;
            end
            else begin
                sinh_puls_out <= 1'b0;
                pha_puls_out <= 1'b0;
            end
        end
    end
    
    always @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            was_start <= 1'b0;
            count_request <= 7'd0;
            fall_counter <= 5'd0;
            request <= 1'b0;
            we <= 1'b0;
            data_out <= 10'd0;
        end
        else begin
            fall <= f;
            count_request <= count_request + 1'b1;
            we <= 1'b0;
            // Ждём прихода первого сигнала "М4"
            if (sinh_puls_out && fall && (~was_start)) begin
                was_start <= 1'b1;
                request <= 1'b1;
                fall_counter <= 5'd1;
                count_request <= 7'd0;
            end
            if (fall && (fall_counter == 5'd0) && was_start) begin
                count_request <= 7'd0;
                request <= 1'b1;
                fall_counter <= fall_counter + 1'b1;
            end
            if (fall && fall_counter && was_start) begin
                fall_counter <= fall_counter + 1'b1;
            end
            if (count_request == 7'd88)
                request <= 1'b0;
            if (fall_counter == speed)
                fall_counter <= 5'd0;
            if (r && request) begin
                data_out <= data_in;
                we <= 1'b1;
            end
            if (~enable) begin
                was_start <= 1'b0;
                we <= 1'b0;
                request <= 1'b0;
                data_out <= 10'd0;
            end
        end
    end
endmodule
