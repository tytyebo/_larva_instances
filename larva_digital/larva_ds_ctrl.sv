/*
 * Модуль "larva_ds_ctrl" для проекта ft75_digital.
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2021
 * email: kuznetsov_ms@org.miet.ru
 */   
`include "defines.svh"
`include "interfaces.svh"
module larva_ds_ctrl#(parameter bit [7:0] BASE_ADR = '0)
               (input bit clk, nrst,
                generic_bus_io.slave_hndshk data_bus,
                input logic [3:0] ack_x, // сделать внутреннюю подтяжку // инверсный
                output logic [3:0] on_x, off_x,
                output logic ready_led);
    
    logic ex_comand, start;
    logic [3:0] ds_state;
    logic [21:0] pulse_cnt;
    
    //Обработка сообщений от КШВ
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.ready <= 1'b0;
            data_bus.dat_miso <= '0;
            ex_comand <= 1'b0;
            start <= 1'b0;
            pulse_cnt <= '0;
            ds_state <= '0;
            
            off_x <= '0;
            on_x <= '0;
        end
        else begin
            data_bus.ready <= 1'b0;
            off_x <= '0;
            on_x <= '0;
            if (data_bus.valid && !data_bus.ready) begin
                if (!data_bus.we) begin
                    if (data_bus.adr == BASE_ADR) begin
                        data_bus.dat_miso <= {12'd0, ds_state};
                        data_bus.ready <= 1'b1; 
                    end
                    else begin
                        if (data_bus.adr == BASE_ADR + 8'd1) begin
                            data_bus.dat_miso <= {12'd0, ack_x};
                            data_bus.ready <= 1'b1;
                        end
                        else begin
                            data_bus.ready <= 1'b0;
                            data_bus.dat_miso <= '0;
                        end
                    end
                end
                else begin
                    if (data_bus.adr == BASE_ADR) begin
                        ex_comand <= 1'b1;
                        ds_state[2:0] <= data_bus.dat_mosi[2:0]; // KOM1, KOM2, KOM3
                        data_bus.ready <= 1'b1;
                    end
                    if (data_bus.adr == BASE_ADR + 1'b1) begin
                        ex_comand <= 1'b1;
                        ds_state[3] <= data_bus.dat_mosi[0]; // ИСПР
                        data_bus.ready <= 1'b1;
                    end
                end
            end
            
            if (ex_comand || !start) begin
                off_x <= ~ds_state;
                on_x <= ds_state;
                pulse_cnt <= pulse_cnt + 1'b1;
                if (pulse_cnt == 22'd251658) begin
                    ex_comand <= 1'b0;
                    start <= 1'b1;
                    pulse_cnt <= '0;
                end
            end
        end
    end
    
    logic nready_led;
    always_ff @(posedge clk or negedge nrst) begin  // включение лампочки Исправности
        if (!nrst) begin
            nready_led <= 1'b0;
        end
        else begin
            if (on_x[3]) begin
                nready_led <= 1'b1;
            end
            if (off_x[3]) begin
                nready_led <= 1'b0;
            end
        end
    end
    assign ready_led = ~nready_led;
endmodule: larva_ds_ctrl
