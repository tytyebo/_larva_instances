/*
 * Update 1.
 * sync - M4
 * pclk - СИ
 * Модуль "larva_digital_sync" для проекта ft75_digital.
 * На вход(clk) приходят 12,582912 МГц
 * Частота сигнала "СИ" 131072 Гц (96 тактов clk)
 * Частота сигнала "М4" 4 Гц (32768 тактов "СИ", 3145728 тактов clk)
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2021
 * email: kuznetsov_ms@org.miet.ru
 */
module larva_digital_sync(input bit clk, nrst,
                          input logic sync_ena,
                          input logic ext_sync,
                          output logic pclk_fall, pclk_rise, pclk, sync);
    
    logic sync_mtst, self_ena, ext_sync_q;
    logic [6:0] pclk_longitude;
    logic [15:0] pclk_cycle_cnt;
    logic adj_start, sync_pulse, corr_sign;
    logic [1:0] delta;
    logic [15:0] corr_cnt, adj_cnt, pow_status;
    logic [3:0] div_tbl, div_chs;
    logic [14:0] div_shift;
    logic [14:0][14:0] div_table;

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            ext_sync_q <= '0;
        end
        else begin
            ext_sync_q <= ext_sync;
        end
    end
    
    metastable_chain #(.CHAIN_DEPTH(3), .BUS_WIDTH(1))
    mtStSync(.clk(clk), .nrst(nrst), .din(sync_ena & ext_sync & ~ext_sync_q), .dout(sync_mtst));
    //Защелкивание СИ и М4 по сигналу разрешения генерации

    assign div_table[0] = 15'd16384;
    assign div_table[1] = 15'd8192;
    assign div_table[2] = 15'd4096;
    assign div_table[3] = 15'd2048;
    assign div_table[4] = 15'd1024;
    assign div_table[5] = 15'd512;
    assign div_table[6] = 15'd256;
    assign div_table[7] = 15'd128;
    assign div_table[8] = 15'd64;
    assign div_table[9] = 15'd32;
    assign div_table[10] = 15'd16;
    assign div_table[11] = 15'd8;
    assign div_table[12] = 15'd4;
    assign div_table[13] = 15'd2;
    assign div_table[14] = 15'd1;

    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            div_shift <= '0;
            self_ena <= 1'b0;
            pclk <= 1'b0;
            pclk_fall <= 1'b0;
            pclk_rise <= 1'b0;
            pclk_longitude <= '0;
            pclk_cycle_cnt <= '0;
            adj_start <= 1'b0;
            delta <= 2'b1;
            corr_cnt <= '0;
            adj_cnt <= '0;
            pow_status <= '0;
            div_tbl <= '0;
            div_chs <= '0;
            corr_sign <= 1'b0;
            sync <= 1'b0;
        end
        else begin
            pclk_longitude <= pclk_longitude + 1'b1;
            pclk_rise <= 1'b0;
            pclk_fall <= 1'b0;
            sync_pulse <= 1'b0;
            if (sync_mtst && !self_ena) begin
                sync_pulse <= 1'b1;
                self_ena <= 1'b1;
                pclk_longitude <= 7'd0;
                sync <= 1'd1;
            end
            if (self_ena && (pclk_longitude == (7'd45 + delta))) begin
                pclk_rise <= 1'b1;
            end
            if (self_ena && (pclk_longitude == (7'd46 + delta))) begin
                pclk <= ~pclk;
            end
            if (self_ena && (pclk_longitude == (7'd93 + delta))) begin
                pclk_fall <= 1'b1;
                pclk_cycle_cnt <= pclk_cycle_cnt + 1'b1;
            end
            if (self_ena && (pclk_longitude == (7'd94 + delta))) begin
                pclk <= ~pclk;
                pclk_longitude <= 7'd0;
                sync <= 1'b0;
                delta <= 2'b1;
            end
            if (pclk_cycle_cnt == 16'd32768) begin
                pclk_cycle_cnt <= 16'd0;
                sync <= 1'd1;
                sync_pulse <= 1'b1;
            end
            if ((!adj_start) && (sync_mtst || sync_pulse)) begin
                if (sync_pulse && sync_mtst) begin
                    corr_cnt <= 15'd0;
                    adj_start <= 1'b0;
                end
                else begin
                    adj_start <= 1'b1;
                    pow_status <= 16'd1;
                    div_tbl <= '0;
                    adj_cnt <= 16'd1;
                end
            end
            if (adj_start) begin
                if (!sync_pulse && !sync_mtst) begin
                    if (adj_cnt == 16'd32767) begin
                        adj_start <= 1'b0;
                    end
                    adj_cnt <= adj_cnt + 1'b1;
                    if (adj_cnt > pow_status) begin
                        pow_status <= {pow_status[14:0], 1'b0};
                        div_tbl <= div_tbl + 1'b1;
                    end
                end
                else begin
                    adj_start <= 1'b0;
                    div_chs <= div_tbl;
                    corr_cnt <= adj_cnt;
                    div_shift <= div_table[div_tbl];
                    if (sync_pulse)
                        corr_sign <= 1'b0;
                    else
                        corr_sign <= 1'b1;
                end
            end
            // исполнение подстройки на необходимое количество тактов clk
            if ((pclk_cycle_cnt == div_shift) && (corr_cnt > 16'd0)) begin
                corr_cnt <= corr_cnt - 1'b1;
                div_shift <= div_shift + div_table[div_chs];
                if (corr_sign) begin
                    delta <= 2'd2;
                end
                else begin
                    delta <= 2'b0;
                end
            end
        end
    end
endmodule: larva_digital_sync
