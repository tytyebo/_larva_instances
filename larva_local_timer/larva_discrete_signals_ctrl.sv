module larva_discrete_signals_ctrl(
        input bit clk, nrst, ena, // clk is 12.582912 MHz
        // Discrete signals inputs
        input logic new_in, record_in, 
        // Second parts info
        input logic [13:0] second_parts, // ~61.035 us
        // Latched discrete signals outputs. "new_out" is one pulse signals. "record_out" is just latched.
        output logic new_out, record_out, 
        // Start Command Label output
        output logic [13:0] start_command_label);
    
    logic new_cmd , new_locked;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainNew(.clk(clk), .nrst(nrst),
                 .din(new_in), .dout(new_cmd));
                 
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            start_command_label <= '0;
            new_out <= 1'b0;
            new_locked <= 1'b0;
        end
        else begin
            new_out <= 1'b0;
            if (ena) begin
                if (!new_cmd && !new_locked) begin
                    new_out <= 1'b1;
                    new_locked <= 1'b1;
                    start_command_label <= second_parts;
                end
            end   
        end
    end
    
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainRecord(.clk(clk), .nrst(nrst),
                 .din(record_in), .dout(record_out));
                 
endmodule: larva_discrete_signals_ctrl