/*
 * Модуль "larva_relay_timers" для проекта larva_main.
 * Содержит регистры для установки времени действия команд КОМ1, КОМ2, КОМ3.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
module larva_relay_timers(input bit nrst, clk,
                          wishbone_io.slave wbs,
                          wishbone_io.master wbm,
                          
                          input logic ena, \new ,
                          output logic [2:0][1:0] command_service);
    
    logic [2:0][3:0][8:0] com_cycles;
    logic [2:0][3:0] timers_ena;
    
    genvar k, l;
    generate 
        begin: generate_timers_ena
            for (k = 0; k < 3; k++) begin: gen_k
                for (l = 0; l < 4; l++) begin: gen_l
                    assign timers_ena[k][l] = (com_cycles[k][l] != '0) ? 1'b1 : 1'b0;
                end
            end
        end
    endgenerate
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
            com_cycles <= '0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                wbs.wbs_ack <= 1'b1;
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[0][1] <= wbs.wbm_dat[24:16];
                                com_cycles[0][0] <= wbs.wbm_dat[8:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[0][3] <= wbs.wbm_dat[24:16];
                                com_cycles[0][2] <= wbs.wbm_dat[8:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd2: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[1][1] <= wbs.wbm_dat[24:16];
                                com_cycles[1][0] <= wbs.wbm_dat[8:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd3: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[1][3] <= wbs.wbm_dat[24:16];
                                com_cycles[1][2] <= wbs.wbm_dat[8:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd4: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[2][1] <= wbs.wbm_dat[24:16];
                                com_cycles[2][0] <= wbs.wbm_dat[8:0];
                            end
                        end
                        `WB_ADR_INT_WIDTH'd5: begin
                            if (wbs.wbm_sel == '1) begin
                                com_cycles[2][3] <= wbs.wbm_dat[24:16];
                                com_cycles[2][2] <= wbs.wbm_dat[8:0];
                            end
                        end
                    endcase
                end 
                else begin
                    unique case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[0][1], 7'd0, com_cycles[0][0]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[0][3], 7'd0, com_cycles[0][2]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd2: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[1][1], 7'd0, com_cycles[1][0]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd3: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[1][3], 7'd0, com_cycles[1][2]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd4: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[2][1], 7'd0, com_cycles[2][0]};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd5: begin
                            if (wbs.wbm_sel == '1) begin
                                wbs.wbs_dat <= {7'd0, com_cycles[2][3], 7'd0, com_cycles[2][2]};
                            end
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                        end
                    endcase
                end
            end
        end
    end
    
    logic [23:0] us_cnt;
    logic [2:0][8:0] s_cnt;
    logic new_locked;
    logic [2:0][3:0] com_ready, com_done;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            new_locked <= 1'b0;
            com_ready <= '0;
            us_cnt <= '0;
            s_cnt <= '0;
        end
        else begin
            if (ena) begin
                if (\new || new_locked) begin
                    new_locked <= 1'b1;
                    us_cnt <= us_cnt + 1'b1;
                    if (us_cnt == 24'd12582911) begin // 1s
                        for (int i = 0; i < 3; i++) begin
                            s_cnt[i] <= s_cnt[i] + 1'b1;
                        end
                        us_cnt <= '0;
                    end
                end
                for (int i = 0; i < 3; i++) begin
                    if (s_cnt[i] == com_cycles[i][0] && !com_ready[i][0] && timers_ena[i][0]) begin
                        com_ready[i][0] <= 1'b1;
                        s_cnt[i] <= '0;
                    end
                    if (s_cnt[i] == com_cycles[i][1] && com_ready[i][0] && !com_ready[i][1]  && timers_ena[i][1]) begin
                        com_ready[i][1] <= 1'b1;
                        s_cnt[i] <= '0;  
                    end
                    if (s_cnt[i] == com_cycles[i][2] && com_ready[i][1] && !com_ready[i][2]  && timers_ena[i][2]) begin
                        com_ready[i][2] <= 1'b1;
                        s_cnt[i] <= '0;  
                    end
                    if (s_cnt[i] == com_cycles[i][3] && com_ready[i][2] && !com_ready[i][3]  && timers_ena[i][3]) begin
                        com_ready[i][3] <= 1'b1;
                        s_cnt[i] <= '0; 
                    end
                end
            end
        end
    end
    
    logic [2:0][1:0] command_service_comb;
    logic [2:0] ds_state;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbm.wbm_adr <= '0;
            wbm.wbm_dat <= '0;
            wbm.wbm_sel <= '0;
            wbm.wbm_cyc <= 1'b0;
            wbm.wbm_stb <=  1'b0;
            wbm.wbm_we <= 1'b0;
            command_service <= '0;
            com_done <= '0;
        end
        else begin
            wbm.wbm_adr <= `DIG_DS_BASE;
            wbm.wbm_we <= 1'b1;
            wbm.wbm_sel <= 2'd3;
            if (ena) begin
                if (!wbm.wbm_stb) begin
                    for (int i = 0; i < 3; i++) begin
                        if (com_ready[i][0] && !com_done[i][0]) begin
                            wbm.wbm_dat <= {29'd0, ds_state};
                            wbm.wbm_stb <= 1'b1;
                            wbm.wbm_cyc <= 1'b1;
                            com_done[i][0] <= 1'b1;
                            command_service[i] <= command_service_comb[i];
                        end
                        if (com_ready[i][1] && !com_done[i][1]) begin
                            wbm.wbm_dat <= {29'd0, ds_state};
                            wbm.wbm_stb <= 1'b1;
                            wbm.wbm_cyc <= 1'b1;
                            com_done[i][1:0] <= '1;
                            command_service[i] <= command_service_comb[i];
                        end
                        if (com_ready[i][2] && !com_done[i][2]) begin
                            wbm.wbm_dat <= {29'd0, ds_state};
                            wbm.wbm_stb <= 1'b1;
                            wbm.wbm_cyc <= 1'b1;
                            com_done[i][2:0] <= '1;
                            command_service[i] <= command_service_comb[i];
                        end
                        if (com_ready[i][3] && !com_done[i][3]) begin
                            wbm.wbm_dat <= {29'd0, ds_state};
                            wbm.wbm_stb <= 1'b1;
                            wbm.wbm_cyc <= 1'b1;
                            com_done[i][3:0] <= '1;
                            command_service[i] <= command_service_comb[i];
                        end
                    end
                end
                if (wbm.wbs_ack) begin
                    wbm.wbm_stb <= 1'b0;
                    wbm.wbm_cyc <= 1'b0;
                end
            end
        end
    end

    always_comb begin
        for (int i = 0; i < 3; i++) begin
            if (com_ready[i][0] && !com_done[i][0]) begin
                ds_state[i] <= 1'b1;
                command_service_comb[i] <= 2'b01;  
            end
            else begin
                if (com_ready[i][1] && !com_done[i][1]) begin
                    ds_state[i] <= 1'b0;
                    command_service_comb[i] <= '0; 
                end
                else begin                
                    if (com_ready[i][2] && !com_done[i][2]) begin
                        ds_state[i] <= 1'b1;
                        command_service_comb[i] <= 2'b10; 
                    end
                    else begin
                        if (com_ready[i][3] && !com_done[i][3]) begin
                            ds_state[i] <= 1'b0;
                            command_service_comb[i] <= '0; 
                        end
                        else begin
                            ds_state[i] <= 1'b0;
                            command_service_comb[i] <= '0; 
                        end
                    end
                end
            end
        end
    end
endmodule: larva_relay_timers