module larva_local_timer(input bit clk, nrst,
                         input logic ena, \new , // clk is 12.582912 MHz
                         output logic [13:0] second_parts, // ~61.035 us
                         output logic [15:0] local_time,
                         output logic sync_out);
    logic [9:0] low_cnt;
    logic low_pulse;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            low_cnt <= 10'd0;
            low_pulse <= 1'b0;
        end
        else begin
            if (ena) begin
                low_cnt <= low_cnt + 1'b1;
                low_pulse <= 1'b0;
                
                if (low_cnt == 10'd766) begin // 12582912 = 768 * 16384
                    low_pulse <= 1'b1;    
                end
                if (low_pulse) begin
                    low_cnt <= 10'd0;
                end
            end
            
            if (\new ) begin
                low_cnt <= 10'd0;
                low_pulse <= 1'b0;
            end
        end
    end

    logic [13:0] high_cnt;
    logic high_pulse;    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            high_cnt <= 14'd0;
            high_pulse <= 1'b0;
        end
        else begin
            if (ena) begin
                if (low_pulse) begin
                    high_cnt <= high_cnt + 1'b1;
                    if (high_cnt == 14'h3ffe) // 12582912 = 768 * 16384
                        high_pulse <= 1'b1;
                    else
                        high_pulse <= 1'b0;
                end
            end
            
            if (\new ) begin
                high_cnt <= 14'd0;
                high_pulse <= 1'b0;
            end
        end
    end                

    logic [3:0] ones;
    logic [3:0] tens;
    logic [3:0] hundreds;
    logic [3:0] thousands;                              
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            ones <= 4'd0;
            tens <= 4'd0;
            hundreds <= 4'd0;
            thousands <= 4'd0;
        end
        else begin
            if (ena) begin
                if (high_pulse && low_pulse) begin
                    ones <= ones + 1'b1;
                    if (ones == 4'd9) begin
                        ones <= 4'd0;
                        tens <= tens + 1'b1;
                        if (tens == 4'd9) begin
                            tens <= 4'd0;
                            hundreds <= hundreds + 1'b1;
                            if (hundreds == 4'd9) begin
                                hundreds <= 4'd0;
                                thousands <= thousands + 1'b1;
                                if (thousands == 4'd9) begin
                                    thousands <= 4'd0;
                                end
                            end
                        end
                    end
                end
            end
            
            if (\new ) begin
                ones <= 4'd1; // The "new" command should reset local timer to 1 second value.
                tens <= 4'd0;
                hundreds <= 4'd0;
                thousands <= 4'd0;
            end
        end
    end
    
    logic [21:0] sync_cnt;
    logic sync;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            sync_cnt <= 22'd0;
            sync <= 1'b0;
        end
        else begin
            sync <= 1'b0;
            if (ena) begin
                sync_cnt <= sync_cnt + 1'b1;
                if (sync_cnt == 22'd0) begin
                    sync <= 1'b1;
                end
                if (sync_cnt == 22'd3145727) begin // 4 Hz
                    sync_cnt <= 22'd0;
                end
            end
        end
    end
    assign second_parts = high_cnt;
    assign local_time = {thousands, hundreds, tens, ones};
    assign sync_out = sync;
endmodule