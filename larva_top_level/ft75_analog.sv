`include "interfaces.svh"
`include "defines.svh"
module ft75_analog(  
        // BUS pins
        inout [15:0] UPR_DATA,              // adata
        input UPR_CLK,            // soft nrst
        inout UPR_RD,                      // nwrs
        input UPR_SGP1,           // sync
        input UPR_FLAG,                     // nale
        output logic [1:0] UPR_MISC,              // SGP outputs
        
        input CLK_24M576,

        input BOARD_ID,

        // ACT pins

        output [2:0] ACT_MUXA_A,
        input ACT_ADCA_SDATA,
        output ACT_ADCA_SCLK,
        output ACT_ADCA_nCS,
        output ACT_nDISCH_A,

        output [2:0] ACT_MUXB_A,
        input [1:0] ACT_ADCB_SDATA,
        output ACT_ADCB_SCLK,
        output ACT_ADCB_nCS,
        output [1:0] ACT_nDISCH_B,

        input SGP1_ACT,
        input SGP2_ACT,

        output [2:0] ACT_MUXC_A,
        input [1:0] ACT_ADCC_SDATA,
        output ACT_ADCC_SCLK,
        output ACT_ADCC_nCS,
        output [1:0] ACT_nDISCH_C,
        
        inout logic TEMP_SCA,
        inout logic TEMP_SCL
    
`ifdef SIMULATION
        ,
        input clk,
        input pll0_lock,
        input pll1_lock
`endif
        );      
// PLL Connection and environment 
    logic poweron_nrst; 
`ifndef SIMULATION
    logic clk, clk_15M72864;
    logic pll0_lock, pll1_lock;
    inPll0 pll0(
       .inclk0(CLK_24M576),
       .c0(clk_15M72864),
       .locked(pll0_lock));

    inPll1 pll1(
       .inclk0(clk_15M72864),
       .c0(clk),
       .locked(pll1_lock));
`endif
// PONR
    PONR PONR0(.nARESET_IN(1'b1),
               .CLOCK_IN(clk),
               .PLL_LOCK_IN(pll0_lock & pll1_lock),
               .nRESET_OUT(poweron_nrst),
               .RESET_OUT());
    
    logic synced_nrst;
    sync_async_reset syncAsyncReset(.clk(clk), .nrst(UPR_CLK), .synced_nrst(synced_nrst));
    logic nrst;
    assign nrst = poweron_nrst & synced_nrst;
//---------------------------------- EXTERNAL BUS -------------------------------------//
//-------------------------------------------------------------------------------------//
    // External Bus Connection and environment 
    extbus_io extbus();
    assign UPR_RD = (extbus.oe_slave) ? extbus.nwr_miso : 1'bz;
    assign UPR_DATA = (extbus.oe_slave) ? extbus.adata_miso : 16'hz;
    assign extbus.nale = UPR_FLAG;
    assign extbus.adata_mosi = UPR_DATA;
    assign extbus.nwr_mosi = UPR_RD;
    
    generic_bus_io#(.ADR_WIDTH(8)) data_extbus(), data_anp(), data_tst(), data_lm77();
    assign data_anp.valid = data_extbus.valid;
    assign data_anp.we = data_extbus.we;
    assign data_anp.dat_mosi = data_extbus.dat_mosi;
    assign data_anp.adr = data_extbus.adr;
    assign data_tst.valid = data_extbus.valid;
    assign data_tst.we = data_extbus.we;
    assign data_tst.dat_mosi = data_extbus.dat_mosi;
    assign data_tst.adr = data_extbus.adr;
    assign data_lm77.valid = data_extbus.valid;
    assign data_lm77.we = data_extbus.we;
    assign data_lm77.dat_mosi = data_extbus.dat_mosi;
    assign data_lm77.adr = data_extbus.adr;
    genvar i;
    generate 
        begin
            for (i = 0; i < $bits(data_extbus.dat_miso); i++) begin: generate_data_extbus
                assign data_extbus.dat_miso[i] = |{data_anp.dat_miso[i], data_tst.dat_miso[i], data_lm77.dat_miso[i]};
            end
        end
    endgenerate
    assign data_extbus.ready = |{data_anp.ready, data_tst.ready, data_lm77.ready};
    larva_extbus_slave larvaExtbusSlave(.clk(clk), .nrst(nrst), .extbus(extbus), .data_bus(data_extbus));
//-------------------------------------------------------------------------------------//
//---------------------------------- TEST REGISTERS------------------------------------//
//-------------------------------------------------------------------------------------//
    logic brd_id;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainBoardId(.clk(clk), .nrst(nrst),
                 .din(BOARD_ID), .dout(brd_id));
                 
    // Analog and ADC AD7476 controlling
    logic [2:0] extbus_mux_wrq;
    logic [2:0][15:0] extbus_mux_data;     
    logic [7:0] anp_ch_a, anp_adr, tr_adr, temp_adr;
    logic [15:0] anp_ch_b, anp_ch_c;      
    always_comb begin
        if (!brd_id) begin // ANP
            anp_adr <= `ANP0_BASE_ADR;
            anp_ch_a <= `ANP0_CHA_F;
            anp_ch_b <= `ANP0_CHB_F;
            anp_ch_c <= `ANP0_CHC_F;
            tr_adr <= `ANP0_TREG_BASE;
            temp_adr <= `ANP0_TEMP_BASE;
        end
        else begin // ANP_01
            anp_adr <= `ANP1_BASE_ADR;
            anp_ch_a <= `ANP1_CHA_F;
            anp_ch_b <= `ANP1_CHB_F;
            anp_ch_c <= `ANP1_CHC_F;
            tr_adr <= `ANP1_TREG_BASE;
            temp_adr <= `ANP1_TEMP_BASE;
        end
    end

    larva_extbus_test_regs larvaExtbusTestRegs(.clk(clk), .nrst(nrst), .base_adr(tr_adr), .data_bus(data_tst));

    anp_io anp();
    assign ACT_nDISCH_A = anp.ndisch[0];
    assign ACT_nDISCH_B = anp.ndisch[2:1];
    assign ACT_nDISCH_C = anp.ndisch[4:3];

    assign ACT_MUXA_A = anp.mux;
    assign ACT_MUXB_A = anp.mux;
    assign ACT_MUXC_A = anp.mux;
    assign ACT_ADCA_SCLK = anp.sck[0];
    assign ACT_ADCB_SCLK = anp.sck[1];
    assign ACT_ADCC_SCLK = anp.sck[2];
    
    assign ACT_ADCA_nCS = anp.ncs[0];
    assign ACT_ADCB_nCS = anp.ncs[1];
    assign ACT_ADCC_nCS = anp.ncs[2];
    // Компенсация перепутки SDATA0 и SDATA1 в схеме АНП
    assign anp.miso[0] = ACT_ADCA_SDATA; 
    assign anp.miso[1] = ACT_ADCB_SDATA[1]; // B0 - B7
    assign anp.miso[2] = ACT_ADCB_SDATA[0]; // B8 - B15
    assign anp.miso[3] = ACT_ADCC_SDATA[1]; // С0 - С7
    assign anp.miso[4] = ACT_ADCC_SDATA[0]; // С8 - С15  

    larva_anp_ctrl larvaAnpCtrl(.clk(clk), .nrst(nrst),
                                .base_adr(anp_adr),
                                .speed_ch_a(anp_ch_a),
                                .speed_ch_b(anp_ch_b),
                                .speed_ch_c(anp_ch_c),
                                .data_bus(data_anp),
                                .anp(anp));
    
    // Signal sensors (SGP) controlling
    always_comb begin
        if (!brd_id) begin
            UPR_MISC <= 'z;
        end
        else begin
            UPR_MISC <= {SGP2_ACT, SGP1_ACT};
        end
    end

    i2c_io i2c();
    assign TEMP_SCA = (i2c.sda_noe) ? 'z : i2c.sda_mosi;
    assign TEMP_SCL = (i2c.scl_noe) ? 'z : i2c.scl_mosi;
    assign i2c.sda_miso = TEMP_SCA;
    assign i2c.scl_miso = TEMP_SCL;
    larva_lm77_ctrl larvaLm77Ctrl(.clk(clk), .nrst(nrst), .base_adr(temp_adr), .data_bus(data_lm77), .i2c(i2c));
endmodule: ft75_analog

