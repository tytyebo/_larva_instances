 /***********************************************************************
 * Copiright 2020 ZITC
 **********************************************************************/
/*
 * Module'ft75_figital'
 * Made by NotBlacksmithMax
 */
`include "defines.svh"
`include "interfaces.svh"
module ft75_digital(
    input logic CLK_24M576,
    // external bus
    inout logic[15:0] UPR_DATA,
    inout logic UPR_RD,
    output logic UPR_WR,
    input logic UPR_FLAG,
    input logic UPR_SGP1,     //Синхро импульс
    input logic UPR_CLK,        //Инверсный сброс
    //parallel interfaces pins
    input logic [9:0] prk1_int,
    input logic [9:0] prk2_int,
    output logic si_1_int,
    output logic m4_1_int,
    output logic m4_2_int,
    output logic zapr1_1_int,
    output logic zapr1_2_int,
    //serial interfaces pins
    output logic m4_tmp_int,
    output logic si_tmp_int,
    output logic zapr_tmp_int,
    input logic psk_int,
    //UART pins
    input logic RxD_232_LVA,
    input logic RxD_232_LVB,
    //RK pins
    input logic [3:0] ACK_x,
    output logic [3:0] ON_x,
    output logic [3:0] OFF_x,
    output logic ISPR_LED_INT
        
`ifdef SIMULATION
    ,
    input logic clk,
    input logic pll0_lock,
    input logic pll1_lock
`endif
    );
`ifndef SIMULATION
    logic clk, clk_15M72864;
    logic pll0_lock, pll1_lock;
    inPll0 pll0(.inclk0(CLK_24M576), .c0(clk_15M72864), .locked(pll0_lock));
    inPll1 pll1(.inclk0(clk_15M72864), .c0(clk), .c1(), .locked(pll1_lock));
`endif
    logic nrst, ponr_nrst, synced_nrst; 
    PONR PONR0(.nARESET_IN(1'b1), .CLOCK_IN(clk), .PLL_LOCK_IN(pll0_lock & pll1_lock), .nRESET_OUT(ponr_nrst), .RESET_OUT());
    sync_async_reset syncAsyncReset(.clk(clk), .nrst(UPR_CLK), .synced_nrst(synced_nrst));
    assign nrst = ponr_nrst & synced_nrst;

    extbus_io extbus();
    assign UPR_RD = (extbus.oe_slave) ? extbus.nwr_miso : 1'bz;
    assign UPR_DATA = (extbus.oe_slave) ? extbus.adata_miso : 16'hz;
    assign extbus.nale = UPR_FLAG;
    assign extbus.adata_mosi = UPR_DATA;
    assign extbus.nwr_mosi = UPR_RD;
    
    generic_bus_io#(.ADR_WIDTH(8)) data_extbus(), data_pc0(), data_pc1(), data_tst(), data_sc(), data_rs(), data_ds();
    assign data_pc0.valid = data_extbus.valid;
    assign data_pc0.we = data_extbus.we;
    assign data_pc0.dat_mosi = data_extbus.dat_mosi;
    assign data_pc0.adr = data_extbus.adr;
    assign data_pc1.valid = data_extbus.valid;
    assign data_pc1.we = data_extbus.we;
    assign data_pc1.dat_mosi = data_extbus.dat_mosi;
    assign data_pc1.adr = data_extbus.adr;
    assign data_tst.valid = data_extbus.valid;
    assign data_tst.we = data_extbus.we;
    assign data_tst.dat_mosi = data_extbus.dat_mosi;
    assign data_tst.adr = data_extbus.adr;
    assign data_sc.valid = data_extbus.valid;
    assign data_sc.we = data_extbus.we;
    assign data_sc.dat_mosi = data_extbus.dat_mosi;
    assign data_sc.adr = data_extbus.adr;
    assign data_rs.valid = data_extbus.valid;
    assign data_rs.we = data_extbus.we;
    assign data_rs.dat_mosi = data_extbus.dat_mosi;
    assign data_rs.adr = data_extbus.adr;
    assign data_ds.valid = data_extbus.valid;
    assign data_ds.we = data_extbus.we;
    assign data_ds.dat_mosi = data_extbus.dat_mosi;
    assign data_ds.adr = data_extbus.adr;
    genvar i;
    generate 
        begin
            for (i = 0; i < $bits(data_extbus.dat_miso); i++) begin: generate_data_extbus
                assign data_extbus.dat_miso[i] = |{data_pc0.dat_miso[i], data_pc1.dat_miso[i], data_tst.dat_miso[i], data_sc.dat_miso[i], data_rs.dat_miso[i], data_ds.dat_miso[i]};
            end
        end
    endgenerate
    assign data_extbus.ready = |{data_pc0.ready, data_pc1.ready, data_tst.ready, data_sc.ready, data_rs.ready, data_ds.ready}; 
    logic pclk_fall, pclk_rise, pclk, sync, extbus_wrq_tst, extbus_wrq_pc0, pc0_ena, pc1_ena, extbus_ready, extbus_rrq,extbus_wrq,
         extbus_busy, extbus_wrq_pc1, extbus_wrq_sc, extbus_wrq_rs, extbus_wrq_ds, sc_ena;
    
    larva_extbus_slave larvaExtbusSlave(.clk(clk), .nrst(nrst), .extbus(extbus), .data_bus(data_extbus));
    larva_extbus_test_regs larvaExtbusTestRegs(.clk(clk), .nrst(nrst), .base_adr(`DIG_TREG_BASE), .data_bus(data_tst));
    
    parallel_channel_io pc0(), pc1();
    assign pc0.data = prk1_int;
    assign si_1_int = pc0.clk;
    assign m4_1_int = pc0.sync;
    assign zapr1_1_int = pc0.request;
    assign pc1.data = prk2_int;
    assign m4_2_int = pc1.sync;
    assign zapr1_2_int = pc1.request;
    larva_pc_ctrl#(.BASE_ADR(`DIG_PC0_BASE)) larvaPcCtrl0 (.clk(clk), .nrst(nrst),
                                                           .data_bus(data_pc0),
                                                           .pclk_fall(pclk_fall),
                                                           .pclk_rise(pclk_rise),
                                                           .pclk(pclk),
                                                           .sync(sync),
                                                           .sync_ena(pc0_ena),
                                                           .pc(pc0));
                                                           
    larva_pc_ctrl#(.BASE_ADR(`DIG_PC1_BASE)) larvaPcCtrl1 (.clk(clk), .nrst(nrst),
                                                           .data_bus(data_pc1),
                                                           .pclk_fall(pclk_fall),
                                                           .pclk_rise(pclk_rise),
                                                           .pclk(pclk),
                                                           .sync(sync),
                                                           .sync_ena(pc1_ena),
                                                           .pc(pc1));
    
    serial_channel_io sc();
    assign sc.data = psk_int;
    assign si_tmp_int = sc.clk;
    assign m4_tmp_int = sc.sync;
    assign zapr_tmp_int = sc.request;
        
    larva_serial_ctrl#(.BASE_ADR(`DIG_SC_BASE)) larvaSerialCtrl(.clk(clk), .nrst(nrst),
                                                                .data_bus(data_sc),
                                                                .pclk_fall(pclk_fall),
                                                                .pclk(pclk),
                                                                .sync(sync),  
                                                                .sc(sc),
                                                                .sync_ena(sc_ena));
                          
    larva_digital_sync larvaDigitalSync(.clk(clk), .nrst(nrst),
                                        .sync_ena(pc0_ena | pc1_ena | sc_ena),
                                        .ext_sync(UPR_SGP1),
                                        .pclk_fall(pclk_fall),
                                        .pclk_rise(pclk_rise),
                                        .pclk(pclk),
                                        .sync(sync));
                                        
    rs232_io rs232();
    assign rs232.rx = RxD_232_LVA;
    larva_rs232_ctrl#(.BASE_ADR(`DIG_RS_BASE)) larvaRs232Ctrl(.clk(clk), .nrst(nrst), .sync(sync), .data_bus(data_rs), .rs232(rs232));
    
    larva_ds_ctrl#(.BASE_ADR(`DIG_DS_BASE)) dsCtrl(.clk(clk), .nrst(nrst),
                                                       .data_bus(data_ds),
                                                       .ack_x(ACK_x),
                                                       .on_x(ON_x),
                                                       .off_x(OFF_x),
                                                       .ready_led(ISPR_LED_INT));
    assign UPR_WR = RxD_232_LVB;
endmodule
