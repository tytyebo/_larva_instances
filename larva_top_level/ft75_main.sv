`include "defines.svh"
`include "interfaces.svh"
// `define SIMULATION 1
import defines_pkg::xmtrStatus_s;
module ft75_main#(parameter int QSPI_DUMMY = 10)
                  // RAM pins
                 (output logic [12:0] RAM_A,
                  inout logic [15:0] RAM_DQ,
                  output logic [1:0] RAM_BA,
                  output logic RAM_nCS,
                  output logic RAM_CLK,
                  output logic RAM_CKE,
                  output logic RAM_nWE,
                  output logic RAM_nRAS,
                  output logic RAM_nCAS,
                  output logic RAM_DQMH,
                  output logic RAM_DQML,
                  
                  // BUS pins
                  inout logic[15:0] UPR_DATA,
                  output logic UPR_CLK,
                  inout logic UPR_RD,
                  input logic UPR_WR,
                  output logic UPR_SGP1,
                  output logic UPR_FLAG,
                  input logic [1:0] UPR_MISC,           
                  
                  input logic CLK_24M576,
                  input logic CLK_24M,
                  
                  // RK pins
                  input logic NOV,
                  input logic KOM1,
                  
                  // MKO buf pins
                  input logic MPK0_RX_P,
                  input logic MPK0_RX_N,
                  output logic MPK0_TX_P,
                  output logic MPK0_TX_N,
                  output logic MPK0_TX_ENA,
                  input logic MPK1_RX_P,
                  input logic MPK1_RX_N,
                  input logic MPK2_RX_P,
                  input logic MPK2_RX_N,
                  
                  // VIDEO pins
                  output logic VIDEO_MOD_A,
                  output logic VIDEO_MOD_B,
		          
                  input logic MCU_IO1,
                  
		          // Temporary pins
		          output logic EE_CS,
		          output logic EE_CLK,
		          output logic EE_DI,
		          input logic EE_DO,
                  
                  
                  input logic QSPI_NCS,
                  input logic QSPI_SCK,
                  inout logic [3:0] QSPI_IO,
                  
                  output logic MCU_CLK,
                  
                  input logic OVRCURR,
                  output logic VST_SHDN,
                  
                  output logic TBIT_D,
                  output logic TS_D,
                  output logic F4_D,
                  output logic SINXR_D,
                  
                  output logic MCLK,
                  input logic MDAT
                  
`ifdef SIMULATION
                  ,
                  input logic clk,
                  input logic clk_mem, 
                  input logic pll0_lock,
                  input logic pll1_lock
`endif
		          );
    localparam int SDRAM_ADR_WIDTH = 25;
// PLL Connection and environment 
    logic hard_nrst, soft_nrst; 
`ifndef SIMULATION
    logic clk, clk_15M72864, clk_mem;
    logic pll0_lock, pll1_lock;
    inPll0 pll0(
       .inclk0(CLK_24M576),
       .c0(clk_15M72864),
       .locked(pll0_lock));

    inPll1 pll1(
       .inclk0(clk_15M72864),
       .c0(clk),
       .c1(clk_mem),
       .locked(pll1_lock));
`endif
    logic mil_clk;   
    assign mil_clk = CLK_24M;
    assign MCU_CLK = CLK_24M;
    
// PONR Connection and environment 
    PONR ponr0(
        .nARESET_IN(1'b1),
        .CLOCK_IN(clk),
        .PLL_LOCK_IN(pll0_lock & pll1_lock),
        .nRESET_OUT(hard_nrst),
        .RESET_OUT());
    logic nrst;
	assign nrst = soft_nrst & hard_nrst;
    assign UPR_CLK = nrst;
// Timer Connection and environment 
	logic global_sync, timer_enable, new_pulse, record, cb_ch_sync;
    logic [15:0] local_time;
    logic [13:0] second_parts;
    larva_local_timer larvaLocalTimer(.clk(clk), .nrst(nrst), .ena(timer_enable), .\new (new_pulse), .second_parts(second_parts), .local_time(local_time), .sync_out(global_sync));

    logic [2:0] ext_sync;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            ext_sync <= '0;
        end
        else begin
            ext_sync <= { ext_sync[1:0], global_sync };
        end
    end 
    assign UPR_SGP1 = |ext_sync;
	logic [13:0] start_command_label;
// Discrete signal controller Connection and environment 
	larva_discrete_signals_ctrl larvaDiscreteSignalsCtrl(.clk(clk), .nrst(nrst), .ena(timer_enable), 
													     .new_in(NOV), .record_in(KOM1), 
														 .second_parts(second_parts), 
														 .new_out(new_pulse), .record_out(record),
														 .start_command_label(start_command_label));

// Orbit Connection and environment 
	// Map signals
	logic [7:0] map_data;
	logic [3:0] map_mode;
 	logic map_nempty, map_rrq, map_enable;
	
	// Wb signals
    wire [15:0] tlm_wbs_adr_i;	
    wire [31:0] tlm_wbs_dat_i;
    wire [3:0] tlm_wbs_sel_i;
    wire tlm_wbs_we_i;
    wire tlm_wbs_stb_i;
    wire tlm_wbs_cyc_i;
    wire [31:0] tlm_wbs_dat_o;
    wire tlm_wbs_ack_o;   
    
    wire [15:0] tlm_wbm_adr_o;	
    wire [31:0] tlm_wbm_dat_o;
    wire [3:0] tlm_wbm_sel_o;
    wire tlm_wbm_we_o;
    wire tlm_wbm_stb_o;
    wire tlm_wbm_cyc_o;
    wire [31:0] tlm_wbm_dat_i;
    wire tlm_wbm_ack_i;  

	logic [SDRAM_ADR_WIDTH - 1:0] sdram_stop_adr;
	logic [1:0] pc1_mode, pc2_mode;
    logic [19:0] system_number;
    logic [7:0] self_temp, danger_nets;
    logic [5:0] terminal_address;
    logic [1:0] mil_mode;
    logic [2:0][1:0] command_service;
    logic [5:0] orbit_repaired;
    orbit_io orbit();
    
    logic reset_req, selftest;
    // logic [8 + 4 + 2 + 8 - 1:0] xmtr_status;
    xmtrStatus_s xmtr_status;
    wire [7:0] xmtr_u;
    wire [3:0] xmtr_p;
    wire [1:0] xmtr_break;
    wire [7:0] xmtr_t;/*
    assign xmtr_u = xmtr_status[21:14];
    assign xmtr_p = xmtr_status[13:10];
    assign xmtr_break = xmtr_status[9:8];
    assign xmtr_t = xmtr_status[7:0];*/
      
    larva_orbit_telemeter#(.MEMORY_ADDR(16'h5003))
    larvaOrbitTelemeter(
        .clk(clk),
        .nrst(nrst),
        // Wishbone master interface
        .wbm_adr_o(tlm_wbm_adr_o),
        .wbm_dat_o(tlm_wbm_dat_o),	
        .wbm_stb_o(tlm_wbm_stb_o),
        .wbm_cyc_o(tlm_wbm_cyc_o),
        .wbm_sel_o(tlm_wbm_sel_o),
        .wbm_we_o(tlm_wbm_we_o),	
        .wbm_dat_i(tlm_wbm_dat_i),
        .wbm_ack_i(tlm_wbm_ack_i),
        
        // Wishbone slave interface
        .wbs_adr_i(tlm_wbs_adr_i),
        .wbs_dat_i(tlm_wbs_dat_i),	
        .wbs_stb_i(tlm_wbs_stb_i),
        .wbs_cyc_i(tlm_wbs_cyc_i),
        .wbs_sel_i(tlm_wbs_sel_i),
        .wbs_we_i(tlm_wbs_we_i),	
        .wbs_dat_o(tlm_wbs_dat_o),
        .wbs_ack_o(tlm_wbs_ack_o),
        
		// Fifo map-interface
		.map_data(map_data), // код адреса опроса - вход адреса памяти larva_orbit_mem
		.map_nempty(map_nempty),
		.map_rrq(map_rrq),
		.map_mode(map_mode),
		.map_enable(map_enable),

		// Local timer enable
		.timer_enable(timer_enable),
		// Discrete signal one pulse signal input
		.record(record), 
		// Global Sync input. There is 4 Hz period. 
		.sync(global_sync),
		// sdram stop address
		.sdram_stop_adr(sdram_stop_adr),
							  
        // Service information inputs
        .local_time_label(local_time), // Метка текущего времени (далее - МТВ)
        .start_command_label(start_command_label), // Метка прохождения команды (далее - МПК)
        // .online('0), // Команда Исправность
        .com1(command_service[0]), // Команда КОМ1
        .com2(command_service[1]), // Команда КОМ2
        .com3(command_service[2]), // Команда КОМ3
        .pc1_mode(pc1_mode), // Режим ЦФП1
        .pc2_mode(pc2_mode), // Режим ЦФП2
        .system_number(system_number), // Номер системы
        .terminal_address(terminal_address), // Адрес ОУ
        .mil1_mode(mil_mode), // Режим МК1
        .lost_gen_mode(xmtr_status.xmtr_break), // Режим СГ
        // .speed_code('0), // Код Информативности (И)
        .transmitter_power(xmtr_status.xmtr_p), // Мощность передатчика (М ПР)
        .self_temp(self_temp), // Температура прибора ФТ75 (Т ФТ)
        .trans_temp(xmtr_status.xmtr_t), // Температура передатчика (Т ПР)
        .danger_nets(danger_nets), // Напряжение опасных цепей (U оц)
        .transmitter_voltage(xmtr_status.xmtr_u), // Напряжение передатчика (U им)
        
        .reset_req(reset_req),
        .cb_ch_sync(cb_ch_sync),
        .selftest(selftest),
        .orbit(orbit));
        
    larva_soft_reset larvaSoftReset(.clk(clk), .nrst(hard_nrst), .reset_req(reset_req), .soft_nrst(soft_nrst));
`ifndef SMALL_TB
    // Temperature Sensor Connection and environment 
/*    wire [31:0] lm77_wbs_dat_o;
    wire [15:0] lm77_wbs_adr_i;	
    wire [31:0] lm77_wbs_dat_i;
    wire [3:0] lm77_wb_sel_i;
    wire lm77_wb_we_i;
    wire lm77_wb_stb_i;
    wire lm77_wb_cyc_i;
    wire lm77_wb_ack_o;

    larva_wb_lm77_ctrl lm77_ctrl_inst
    (
        .clk(clk),
        .nrst(nrst),
        
        .wb_stb_i(lm77_wb_stb_i),
        .wb_cyc_i(lm77_wb_cyc_i),
        .wb_dat_o(lm77_wbs_dat_o),
        .wb_ack_o(lm77_wb_ack_o),
        
        .scl_pad_io(TEMP_SCL),
        .sda_pad_io(TEMP_SDA)
    ); 
*/    
// External Bus Connection and environment 
    extbus_io extbus();
    assign UPR_RD = (extbus.oe_master) ? extbus.nwr_mosi : 1'bz;
    assign UPR_DATA = (extbus.oe_master) ? extbus.adata_mosi : 16'hz;
    assign UPR_FLAG = extbus.nale;
    assign extbus.nwr_miso = UPR_RD;
    assign extbus.adata_miso = UPR_DATA;
// External Bus To Wishbone Connection and environment 
    wishbone_io ext_wbs(), mem_wbs();
    logic [1:0] s_flow_mstb;
    larva_extbus2wb_bridge larvaExtbus2wbBridge(.clk(clk), .nrst(nrst),        
                                                .extbus(extbus),
                                                
                                                // slave wishbone side
                                                .ext_wbs(ext_wbs),
                                                .mem_wbs(mem_wbs),
                                                
                                                .s_flow(UPR_MISC),
                                                .s_flow_mstb(s_flow_mstb),
                                                .selftest(selftest));  
		
    // MIL CH0 RX+TX Connection and environment 
    wishbone_io mil_wbs0();
    logic [31:0] mil0_wb_dat_o;
    logic [15:0] mil0_wb_adr_i;	
    logic [31:0] mil0_wb_dat_i;
    logic [3:0] mil0_wb_sel_i;
    logic mil0_wb_we_i;
    logic mil0_wb_stb_i;
    logic mil0_wb_cyc_i;
    logic mil0_wb_ack_o;
    assign mil_wbs0.wbm_adr = mil0_wb_adr_i;
    assign mil_wbs0.wbm_dat = mil0_wb_dat_i;
    assign mil_wbs0.wbm_sel = mil0_wb_sel_i;
    assign mil_wbs0.wbm_cyc = mil0_wb_cyc_i;
    assign mil_wbs0.wbm_stb = mil0_wb_stb_i;
    assign mil_wbs0.wbm_we = mil0_wb_we_i;
    assign mil0_wb_dat_o = mil_wbs0.wbs_dat; 
    assign mil0_wb_ack_o = mil_wbs0.wbs_ack;

    milStd_io mil0();
    assign mil0.rxp = MPK0_RX_P;
    assign mil0.rxn = MPK0_RX_N;
    assign MPK0_TX_P = mil0.txp;
    assign MPK0_TX_N = mil0.txn;
    assign MPK0_TX_ENA = mil0.tx_ena;
    larva_mil_fifo2wb#(.DEBUG_CONST(`MIL0_DEBUG_CONTS)) larvaMilFifo2wb0(.nrst(nrst), .clk(clk), .sync(global_sync), .mil_clk(mil_clk), .mil(mil0), .wbs(mil_wbs0), .terminal_address(terminal_address), .mil_mode(mil_mode));
    
    // MIL CH1 RX Only Connection and environment 
    wishbone_io mil_wbs1();
    logic [31:0] mil1_wb_dat_o;
    logic [15:0] mil1_wb_adr_i;	
    logic [31:0] mil1_wb_dat_i;
    logic [3:0] mil1_wb_sel_i;
    logic mil1_wb_we_i;
    logic mil1_wb_stb_i;
    logic mil1_wb_cyc_i;
    logic mil1_wb_ack_o;
    assign mil_wbs1.wbm_adr = mil1_wb_adr_i;
    assign mil_wbs1.wbm_dat = mil1_wb_dat_i;
    assign mil_wbs1.wbm_sel = mil1_wb_sel_i;
    assign mil_wbs1.wbm_cyc = mil1_wb_cyc_i;
    assign mil_wbs1.wbm_stb = mil1_wb_stb_i;
    assign mil_wbs1.wbm_we = mil1_wb_we_i;
    assign mil1_wb_dat_o = mil_wbs1.wbs_dat; 
    assign mil1_wb_ack_o = mil_wbs1.wbs_ack;

    milStd_io mil1();
    assign mil1.rxp = MPK1_RX_P;
    assign mil1.rxn = MPK1_RX_N;

    larva_mil_fifo2wb#(.DEBUG_CONST(`MIL1_DEBUG_CONTS)) larvaMilFifo2wb1(.nrst(nrst), .clk(clk), .sync(global_sync), .mil_clk(mil_clk), .mil(mil1), .wbs(mil_wbs1), .terminal_address(), .mil_mode());

    // MIL CH2 RX Only Connection and environment 
    wishbone_io mil_wbs2();
    logic [31:0] mil2_wb_dat_o;
    logic [15:0] mil2_wb_adr_i;	
    logic [31:0] mil2_wb_dat_i;
    logic [3:0] mil2_wb_sel_i;
    logic mil2_wb_we_i;
    logic mil2_wb_stb_i;
    logic mil2_wb_cyc_i;
    logic mil2_wb_ack_o;
    assign mil_wbs2.wbm_adr = mil2_wb_adr_i;
    assign mil_wbs2.wbm_dat = mil2_wb_dat_i;
    assign mil_wbs2.wbm_sel = mil2_wb_sel_i;
    assign mil_wbs2.wbm_cyc = mil2_wb_cyc_i;
    assign mil_wbs2.wbm_stb = mil2_wb_stb_i;
    assign mil_wbs2.wbm_we = mil2_wb_we_i;
    assign mil2_wb_dat_o = mil_wbs2.wbs_dat; 
    assign mil2_wb_ack_o = mil_wbs2.wbs_ack;

    milStd_io mil2();
    assign mil2.rxp = MPK2_RX_P;
    assign mil2.rxn = MPK2_RX_N;

    larva_mil_fifo2wb#(.DEBUG_CONST(`MIL2_DEBUG_CONTS)) larvaMilFifo2wb2(.nrst(nrst), .clk(clk), .sync(global_sync), .mil_clk(mil_clk), .mil(mil2), .wbs(mil_wbs2), .terminal_address(), .mil_mode());
    
// SDRAM Connection    
    wishbone_io sdram_wbs();
    logic [31:0] sdram_wb_dat_o;
    logic [15:0] sdram_wb_adr_i;	
    logic [31:0] sdram_wb_dat_i;
    logic [3:0] sdram_wb_sel_i;
    logic sdram_wb_we_i;
    logic sdram_wb_stb_i;
    logic sdram_wb_cyc_i;
    logic sdram_wb_ack_o;
    assign sdram_wbs.wbm_adr = sdram_wb_adr_i;
    assign sdram_wbs.wbm_dat = sdram_wb_dat_i;
    assign sdram_wbs.wbm_sel = sdram_wb_sel_i;
    assign sdram_wbs.wbm_cyc = sdram_wb_cyc_i;
    assign sdram_wbs.wbm_stb = sdram_wb_stb_i;
    assign sdram_wbs.wbm_we = sdram_wb_we_i;
    assign sdram_wb_dat_o = sdram_wbs.wbs_dat; 
    assign sdram_wb_ack_o = sdram_wbs.wbs_ack;
    sdram_io #(.ADR_WIDTH(13), .DATA_WIDTH(16)) sdram();
    assign RAM_A = sdram.a;
    assign RAM_DQ = (!sdram.we_n) ? sdram.dq_out : 'z;
    assign RAM_BA = sdram.ba;
    assign RAM_nCS = sdram.cs_n;
    assign RAM_CLK = clk_mem;
    assign RAM_CKE = sdram.cke;
    assign RAM_nWE = sdram.we_n;
    assign RAM_nRAS = sdram.ras_n;
    assign RAM_nCAS = sdram.cas_n;
    assign RAM_DQMH = sdram.dqm[1];
    assign RAM_DQML = sdram.dqm[0];
    assign sdram.dq_in = RAM_DQ;
    
    larva_sdram2wb#(.RAM_CLK(12.5), .SDRAM_ADR_WIDTH(SDRAM_ADR_WIDTH))
    larvaSdram2Wb(.clk(clk), .nrst(nrst), .sdram(sdram), .wbs(sdram_wbs), .stop_adr(sdram_stop_adr));
`endif

    // QSPI/SPI Mux
    logic spi_sel;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainSpiSel(.clk(clk), .nrst(nrst),
                 .din(MCU_IO1), .dout(spi_sel));
    
    // Map and EPCA controllers connections
	wishbone_io map_wbs();
	logic [31:0] map_wb_dat_o;
    logic [15:0] map_wb_adr_i;	
    logic [31:0] map_wb_dat_i;
    logic [3:0] map_wb_sel_i;
    logic map_wb_we_i;
    logic map_wb_stb_i;
    logic map_wb_cyc_i;
    logic map_wb_ack_o;
    assign map_wbs.wbm_adr = map_wb_adr_i;
    assign map_wbs.wbm_dat = map_wb_dat_i;
    assign map_wbs.wbm_sel = map_wb_sel_i;
    assign map_wbs.wbm_cyc = map_wb_cyc_i;
    assign map_wbs.wbm_stb = map_wb_stb_i;
    assign map_wbs.wbm_we = map_wb_we_i;
    assign map_wb_dat_o = map_wbs.wbs_dat; 
    assign map_wb_ack_o = map_wbs.wbs_ack;
	
	spi_io epcq_spi();
	assign EE_CS = (spi_sel) ? QSPI_IO[1] : epcq_spi.ncs; 
	assign EE_CLK = (spi_sel) ? QSPI_SCK : epcq_spi.sck;
	assign EE_DI = (spi_sel) ? QSPI_IO[2] : epcq_spi.mosi; 
	assign epcq_spi.miso = EE_DO;

	larva_map_reader larvaMapReader(.clk(clk), .nrst(nrst),
									.map_data(map_data), 
									.map_nempty(map_nempty),
									.map_rrq(map_rrq),
									.map_enable(map_enable),
									.map_mode(map_mode),
									.map_wbs(map_wbs),
									.spi(epcq_spi));

    rs232_io rs232();
    assign rs232.rx = UPR_WR;
    larva_xmtr_data_responder larvaXmtrDataResponder(.clk(clk), .nrst(nrst), .rs232(rs232), .sync(global_sync), .status(xmtr_status));
                    
    // Debug regs
	wishbone_io debug_regs_wbs();
    logic [15:0] debug_wb_adr_i;	
    logic [31:0] debug_wb_dat_i;
    logic [3:0] debug_wb_sel_i;
    logic debug_wb_we_i;
    logic debug_wb_stb_i;
    logic debug_wb_cyc_i;
    assign debug_regs_wbs.wbm_adr = debug_wb_adr_i;
    assign debug_regs_wbs.wbm_dat = debug_wb_dat_i;
    assign debug_regs_wbs.wbm_sel = debug_wb_sel_i;
    assign debug_regs_wbs.wbm_cyc = debug_wb_cyc_i;
    assign debug_regs_wbs.wbm_stb = debug_wb_stb_i;
    assign debug_regs_wbs.wbm_we = debug_wb_we_i;


	larva_debug_regs larvaDebugRegs(.clk(clk), .nrst(nrst),
									.wbs(debug_regs_wbs));                        
    // QSPI                                    
    wishbone_io qspi_wbm();
    qspi_io qspi(clk);
    assign qspi.sck = QSPI_SCK;
    assign qspi.ncs = (!spi_sel) ? QSPI_NCS : 1'bz;
    assign qspi.io_mosi = QSPI_IO;
    assign QSPI_IO = (spi_sel) ? {EE_DO, 1'bz, 1'bz, 1'bz} : ((qspi.dir) ? qspi.io_miso : 'z);
    larva_qspi2wb#(.QSPI_DUMMY(QSPI_DUMMY)) larvaQspi2Wb(.clk(clk), .clk24(CLK_24M), .nrst(nrst), .qspi(qspi), .wbm(qspi_wbm));

    // Video Crc32 Control
    wishbone_io crc_wbs();
    larva_orbit_crc larvaOrbitCrc(.clk(clk), .nrst(nrst), .orbit(orbit), .wbs(crc_wbs), .mode(map_mode));

    assign VIDEO_MOD_A = orbit_repaired[0];
    assign VIDEO_MOD_B = orbit_repaired[1];
    assign TBIT_D = orbit_repaired[2];
    assign TS_D = orbit_repaired[3];
    assign F4_D = orbit_repaired[4];
    assign SINXR_D = orbit_repaired[5];

    // Calibrating Channels For Analog
    wishbone_io cbch_wbs();
    larva_calibrating_channels larvaCalibratingChannels(.clk(clk), .nrst(nrst), .sync(global_sync), .cbch_wbs(cbch_wbs), .s_flow_mstb(s_flow_mstb), .selftest(selftest));
               
    wishbone_io dn_wbs();
    larva_danger_nets_ctrl larvaDangerNetsCtrl(.clk(clk), .nrst(nrst), 
                        .mclk(MCLK),
                        .mdata(MDAT),
                        .wbs(dn_wbs),
                        .danger_nets(danger_nets));
    
    wishbone_io service_wbs();    
    larva_service larvaService(.nrst(nrst), .clk(clk), .wbs(service_wbs), .pc1_mode(pc1_mode), .pc2_mode(pc2_mode), .system_number(system_number), .square_out_mode(sqout_mode));
    
    wishbone_io lm77_wbs();
    i2c_io i2c();
    // Только в макете выполнено подключение lm77 к ПЛИС
    // assign TEMP_SCA = (i2c.sda_noe) ? 'z : i2c.sda_mosi;
    // assign TEMP_SCL = (i2c.scl_noe) ? 'z : i2c.scl_mosi;
    // assign i2c.sda_miso = TEMP_SCA;
    // assign i2c.scl_miso = TEMP_SCL;
    larva_lm77_wb_ctrl#(.SEL(1'b1))
    larvaLm77WbCtrl(.clk(clk), .nrst(nrst), .wbs(lm77_wbs), .self_temp(self_temp), .i2c(i2c));
        
    wishbone_io relay_wbs();
    wishbone_io relay_wbm();
    larva_relay_timers larvaRelayTimers(.nrst(nrst), .clk(clk), .wbs(relay_wbs), .wbm(relay_wbm), .ena(timer_enable), .\new (new_pulse), .command_service(command_service));
    //-------------------------------------------------------------------------------------//
    //-------------------------- WISHBONE CONNECTION MATRIX -------------------------------//
    //-------------------------------------------------------------------------------------//
    wb_conmax_top
    #(
        .dw(32), .aw(16), .rf_addr(4'hf),
        .pri_sel0(2'd2), .pri_sel1(2'd2), .pri_sel2(2'd2), .pri_sel3(2'd2), .pri_sel4(2'd2), .pri_sel5(2'd2), .pri_sel6(2'd2), .pri_sel7(2'd2),
        .pri_sel8(2'd2), .pri_sel9(2'd2), .pri_sel10(2'd2), .pri_sel11(2'd2), .pri_sel12(2'd2), .pri_sel13(2'd2), .pri_sel14(2'd2), .pri_sel15(2'd1)
    ) 
    wb_conmax_top0
    (
        .clk_i(clk), .rst_i(~nrst),
        // Master 0 Interface
        .m0_data_i(qspi_wbm.wbm_dat), .m0_data_o(qspi_wbm.wbs_dat), .m0_addr_i(qspi_wbm.wbm_adr),
        .m0_sel_i(qspi_wbm.wbm_sel), .m0_we_i(qspi_wbm.wbm_we), .m0_cyc_i(qspi_wbm.wbm_cyc),
        .m0_stb_i(qspi_wbm.wbm_stb), .m0_ack_o(qspi_wbm.wbs_ack), .m0_err_o(), .m0_rty_o(),
        // Master 1 Interface 
        .m1_data_i(tlm_wbm_dat_o), .m1_data_o(tlm_wbm_dat_i), .m1_addr_i(tlm_wbm_adr_o), 
        .m1_sel_i(tlm_wbm_sel_o), .m1_we_i(tlm_wbm_we_o), .m1_cyc_i(tlm_wbm_cyc_o),
        .m1_stb_i(tlm_wbm_stb_o), .m1_ack_o(tlm_wbm_ack_i), .m1_err_o(), .m1_rty_o(),
        // Master 2 Interface
        .m2_data_i(relay_wbm.wbm_dat), .m2_data_o(relay_wbm.wbs_dat), .m2_addr_i(relay_wbm.wbm_adr),
        .m2_sel_i(relay_wbm.wbm_sel), .m2_we_i(relay_wbm.wbm_we), .m2_cyc_i(relay_wbm.wbm_cyc),
        .m2_stb_i(relay_wbm.wbm_stb), .m2_ack_o(relay_wbm.wbs_ack), .m2_err_o(), .m2_rty_o(),
        // Master 3 Interface
        .m3_data_i('0), .m3_data_o(), .m3_addr_i('0), .m3_sel_i('0), .m3_we_i('0), .m3_cyc_i('0),
        .m3_stb_i('0), .m3_ack_o(), .m3_err_o(), .m3_rty_o(),
        // Master 4 Interface
        .m4_data_i('0), .m4_data_o(), .m4_addr_i('0), .m4_sel_i('0), .m4_we_i('0), .m4_cyc_i('0),
        .m4_stb_i('0), .m4_ack_o(), .m4_err_o(), .m4_rty_o(),
        // Master 5 Interface
        .m5_data_i('0), .m5_data_o(), .m5_addr_i('0), .m5_sel_i('0), .m5_we_i('0), .m5_cyc_i('0),
        .m5_stb_i('0), .m5_ack_o(), .m5_err_o(), .m5_rty_o(),
        // Master 6 Interface
        .m6_data_i('0), .m6_data_o(), .m6_addr_i('0), .m6_sel_i('0), .m6_we_i('0), .m6_cyc_i('0),
        .m6_stb_i('0), .m6_ack_o(), .m6_err_o(), .m6_rty_o(),
        // Master 7 Interface
        .m7_data_i('0), .m7_data_o(), .m7_addr_i('0), .m7_sel_i('0), .m7_we_i('0), .m7_cyc_i('0),
        .m7_stb_i('0), .m7_ack_o(), .m7_err_o(), .m7_rty_o(),
        // Slave 0 Interface
        .s0_data_i(ext_wbs.wbs_dat), .s0_data_o(ext_wbs.wbm_dat), .s0_addr_o(ext_wbs.wbm_adr), 
        .s0_sel_o(ext_wbs.wbm_sel), .s0_we_o(ext_wbs.wbm_we), .s0_cyc_o(ext_wbs.wbm_cyc),
        .s0_stb_o(ext_wbs.wbm_stb), .s0_ack_i(ext_wbs.wbs_ack), .s0_err_i('0), .s0_rty_i('0),
        // Slave 1 Interface
        .s1_data_i(tlm_wbs_dat_o), .s1_data_o(tlm_wbs_dat_i), .s1_addr_o(tlm_wbs_adr_i),
        .s1_sel_o(tlm_wbs_sel_i), .s1_we_o(tlm_wbs_we_i), .s1_cyc_o(tlm_wbs_cyc_i),
        .s1_stb_o (tlm_wbs_stb_i), .s1_ack_i (tlm_wbs_ack_o), .s1_err_i ('0), .s1_rty_i('0),    
        // Slave 2 Interface
        .s2_data_i(mil0_wb_dat_o), .s2_data_o(mil0_wb_dat_i), .s2_addr_o(mil0_wb_adr_i), 
        .s2_sel_o(mil0_wb_sel_i), .s2_we_o(mil0_wb_we_i), .s2_cyc_o(mil0_wb_cyc_i),
        .s2_stb_o(mil0_wb_stb_i), .s2_ack_i(mil0_wb_ack_o), .s2_err_i('0), .s2_rty_i('0),	
        // Slave 3 Interface
        .s3_data_i(mil1_wb_dat_o), .s3_data_o(mil1_wb_dat_i), .s3_addr_o(mil1_wb_adr_i), 
        .s3_sel_o(mil1_wb_sel_i), .s3_we_o(mil1_wb_we_i), .s3_cyc_o(mil1_wb_cyc_i),
        .s3_stb_o(mil1_wb_stb_i), .s3_ack_i(mil1_wb_ack_o), .s3_err_i('0), .s3_rty_i('0),
        // Slave 4 Interface
        .s4_data_i(mil2_wb_dat_o), .s4_data_o(mil2_wb_dat_i), .s4_addr_o(mil2_wb_adr_i), 
        .s4_sel_o(mil2_wb_sel_i), .s4_we_o(mil2_wb_we_i), .s4_cyc_o(mil2_wb_cyc_i),
        .s4_stb_o(mil2_wb_stb_i), .s4_ack_i(mil2_wb_ack_o), .s4_err_i('0), .s4_rty_i('0),
        // Slave 5 Interface
        .s5_data_i(sdram_wb_dat_o), .s5_data_o(sdram_wb_dat_i), .s5_addr_o(sdram_wb_adr_i), 
        .s5_sel_o(sdram_wb_sel_i), .s5_we_o(sdram_wb_we_i), .s5_cyc_o(sdram_wb_cyc_i),
        .s5_stb_o(sdram_wb_stb_i), .s5_ack_i(sdram_wb_ack_o), .s5_err_i('0), .s5_rty_i('0),	
        // Slave 6 Interface
        .s6_data_i(map_wb_dat_o), .s6_data_o(map_wb_dat_i), .s6_addr_o(map_wb_adr_i), 
        .s6_sel_o(map_wb_sel_i), .s6_we_o(map_wb_we_i), .s6_cyc_o(map_wb_cyc_i),
        .s6_stb_o(map_wb_stb_i), .s6_ack_i(map_wb_ack_o), .s6_err_i('0), .s6_rty_i('0),
        // Slave 7 Interface
        .s7_data_i(cbch_wbs.wbs_dat), .s7_data_o(cbch_wbs.wbm_dat), .s7_addr_o(cbch_wbs.wbm_adr), .s7_sel_o(cbch_wbs.wbm_sel), .s7_we_o(cbch_wbs.wbm_we), .s7_cyc_o(cbch_wbs.wbm_cyc),
        .s7_stb_o(cbch_wbs.wbm_stb), .s7_ack_i(cbch_wbs.wbs_ack), .s7_err_i('0), .s7_rty_i('0),
        // Slave 8 Interface
        .s8_data_i(mem_wbs.wbs_dat), .s8_data_o(mem_wbs.wbm_dat), .s8_addr_o(mem_wbs.wbm_adr), .s8_sel_o(mem_wbs.wbm_sel), .s8_we_o(mem_wbs.wbm_we), .s8_cyc_o(mem_wbs.wbm_cyc),
        .s8_stb_o(mem_wbs.wbm_stb), .s8_ack_i(mem_wbs.wbs_ack), .s8_err_i('0), .s8_rty_i('0),
        // Slave 9 Interface
        .s9_data_i(dn_wbs.wbs_dat), .s9_data_o(dn_wbs.wbm_dat), .s9_addr_o(dn_wbs.wbm_adr), .s9_sel_o(dn_wbs.wbm_sel), .s9_we_o(dn_wbs.wbm_we), .s9_cyc_o(dn_wbs.wbm_cyc),
        .s9_stb_o(dn_wbs.wbm_stb), .s9_ack_i(dn_wbs.wbs_ack), .s9_err_i('0), .s9_rty_i('0),
        // Slave 10 Interface
        .s10_data_i(service_wbs.wbs_dat), .s10_data_o(service_wbs.wbm_dat), .s10_addr_o(service_wbs.wbm_adr), .s10_sel_o(service_wbs.wbm_sel), .s10_we_o(service_wbs.wbm_we), .s10_cyc_o(service_wbs.wbm_cyc),
        .s10_stb_o(service_wbs.wbm_stb), .s10_ack_i(service_wbs.wbs_ack), .s10_err_i('0), .s10_rty_i('0),
        // Slave 11 Interface
        .s11_data_i(relay_wbs.wbs_dat), .s11_data_o(relay_wbs.wbm_dat), .s11_addr_o(relay_wbs.wbm_adr), .s11_sel_o(relay_wbs.wbm_sel), .s11_we_o(relay_wbs.wbm_we), .s11_cyc_o(relay_wbs.wbm_cyc),
        .s11_stb_o(relay_wbs.wbm_stb), .s11_ack_i(relay_wbs.wbs_ack), .s11_err_i('0), .s11_rty_i('0),
        // Slave 12 Interface
        .s12_data_i(lm77_wbs.wbs_dat), .s12_data_o(lm77_wbs.wbm_dat), .s12_addr_o(lm77_wbs.wbm_adr), .s12_sel_o(lm77_wbs.wbm_sel), .s12_we_o(lm77_wbs.wbm_we), .s12_cyc_o(lm77_wbs.wbm_cyc),
        .s12_stb_o(lm77_wbs.wbm_stb), .s12_ack_i(lm77_wbs.wbs_ack), .s12_err_i('0), .s12_rty_i('0),
        // Slave 13 Interface
        .s13_data_i('0), .s13_data_o(), .s13_addr_o(), .s13_sel_o(), .s13_we_o(), .s13_cyc_o(),
        .s13_stb_o(), .s13_ack_i('0), .s13_err_i('0), .s13_rty_i('0),
        // Slave 14 Interface
        .s14_data_i(crc_wbs.wbs_dat), .s14_data_o(crc_wbs.wbm_dat), .s14_addr_o(crc_wbs.wbm_adr),
        .s14_sel_o(crc_wbs.wbm_sel), .s14_we_o(crc_wbs.wbm_we), .s14_cyc_o(crc_wbs.wbm_cyc),
        .s14_stb_o(crc_wbs.wbm_stb), .s14_ack_i(crc_wbs.wbs_ack), .s14_err_i('0), .s14_rty_i('0),
        // Slave 15 Interface
        .s15_data_i(debug_regs_wbs.wbs_dat), .s15_data_o(debug_wb_dat_i), .s15_addr_o(debug_wb_adr_i), 
        .s15_sel_o(debug_wb_sel_i), .s15_we_o(debug_wb_we_i), .s15_cyc_o(debug_wb_cyc_i),
        .s15_stb_o(debug_wb_stb_i), .s15_ack_i(debug_regs_wbs.wbs_ack), .s15_err_i('0), .s15_rty_i('0)
    );
    
    larva_overcurr larvaOvercurr(.clk(clk), .nrst(nrst), .overcurr(OVRCURR), .shdn(VST_SHDN));
`ifndef SIMULATION
    larva_orbit_repairer larvaOrbitRepairer(.clk(clk), .nclk(clk_mem), .nrst(nrst), .orbit_in(orbit), .orbit(orbit_repaired), .selftest(selftest), .square(sqout_mode), .inv(new_pulse), .off(record));
`endif
endmodule

