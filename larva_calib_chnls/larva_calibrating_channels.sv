/*
 * Модуль "larva_calibrating_channels" для проекта ft75_main.
 * Формирует константы для калибровочных каналов АНП-1, АНП-2 относительно сигнала sync.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::CALIB_CH0_LOW;
import defines_pkg::CALIB_CH0_HIGH;
import defines_pkg::CALIB_CH1_ZERO;
import defines_pkg::CALIB_CH1_LOW;
import defines_pkg::CALIB_CH1_HIGH;
module larva_calibrating_channels(input bit clk, nrst, // clk is 12.582912 MHz
                                  input logic sync,
                                  wishbone_io.slave cbch_wbs,
                                  input logic [1:0] s_flow_mstb,
                                  input logic selftest);

    logic [8:0] data0, data1;
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            cbch_wbs.wbs_ack <= 1'b0;
            cbch_wbs.wbs_dat <= '0;
        end
        else begin
			cbch_wbs.wbs_ack <= 1'b0;
            if (cbch_wbs.wbm_stb && cbch_wbs.wbm_cyc && !cbch_wbs.wbs_ack) begin
                if (!cbch_wbs.wbm_we && cbch_wbs.wbm_sel[1:0] == '1) begin
					unique case (cbch_wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
						`WB_ADR_INT_WIDTH'd0: begin // АНП-1 калибровочный канал
                            cbch_wbs.wbs_dat <= { data0, (!selftest) ? s_flow_mstb[0] : 1'b0 }; // только СГП1
						end
						`WB_ADR_INT_WIDTH'd1: begin // АНП-2 калибровочный канал
                            cbch_wbs.wbs_dat <= { data1, (!selftest) ? s_flow_mstb[0] : 1'b0 }; // только СГП1, т.к. СГП2 только в ВФБ
						end
					endcase
                end 
                cbch_wbs.wbs_ack <= 1'b1;
            end
        end
    end
    
    logic [1:0] calib_cnt;
    always @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            calib_cnt <= '0;
            data0 <= '0;
            data1 <= '0;
        end
        else begin
            if (sync) begin
                calib_cnt <= calib_cnt + 1'b1;
            end
            case (calib_cnt)
                2'd0: begin
                    data0 <= CALIB_CH0_HIGH; // 9'd365 // АНП-1 МАКС1
                    data1 <= CALIB_CH1_HIGH; // 9'd472 // АНП-2 МАКС2
                end
                2'd1: begin
                    data0 <= CALIB_CH0_LOW; // 9'd50 // АНП-1 МИН1
                    data1 <= CALIB_CH1_ZERO; // 9'd246 // АНП-2 0В
                end
                2'd2: begin
                    data0 <= CALIB_CH0_HIGH; // 9'd365 // АНП-1 МАКС1
                    data1 <= CALIB_CH1_LOW; // 9'd20 // АНП-2 МИН2
                end
                2'd3: begin
                    data0 <= CALIB_CH0_LOW; // 9'd50 // АНП-1 МИН1
                    data1 <= CALIB_CH1_ZERO; // 9'd246 // АНП-2 0В
                end
            endcase
        end
    end
endmodule: larva_calibrating_channels