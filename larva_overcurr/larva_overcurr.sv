/*
 * Модуль защиты от перегрузки источника питания для аналоговых датчиков для проекта larva_main.
 * OVER_CNT_VALUE задает длительность отключения источника питания в случае перегрузки в единицах счетчика частотой 12,582912 МГц.
 * Число 12582912 соответствует длительности 1 секунда.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
module larva_overcurr#(parameter int OVER_CNT_VALUE = 12582912) // 12582912 means that there is the 1 second shutdown signal.
                    (input bit clk, nrst, // clk is 12.582912 MHz
                      input logic overcurr,
                      output logic shdn);
                      
    logic overcurr_mstb , overcurr_locked;
    metastable_chain#(.CHAIN_DEPTH(3),.BUS_WIDTH(1))
    metaChainOvercurr(.clk(clk), .nrst(nrst),
                 .din(overcurr), .dout(overcurr_mstb));

    logic [$clog2(OVER_CNT_VALUE):0] over_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            over_cnt <= '0;
            shdn <= 1'b0;
        end
        else begin
            if (!overcurr_mstb && !shdn) begin // Сигнал overcurr имеет инверсную полярность
                shdn <= 1'b1;
            end
            
            if (shdn) begin
                over_cnt <= over_cnt + 1'b1;
                
                if (over_cnt == OVER_CNT_VALUE) begin // 12582912 = 768 * 16384
                    shdn <= 1'b0;
                    over_cnt <= '0;
                end
            end
        end
    end
endmodule: larva_overcurr