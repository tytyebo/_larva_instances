/*
 * Модуль "larva_orbit_crc" для проекта ft75_main.
 * FSM, читающий из микросхемы флэш-памяти EPCQ16A "карту опроса" для выбранного режима работы.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"

module larva_orbit_crc(input bit clk, nrst, // clk is 12.582912 MHz
                        // Fifo map-interface
                        orbit_io.in orbit,
						wishbone_io.slave wbs,
                        input logic [3:0] mode);
    
    logic [3:0][5:0] boundary;
    logic [31:0] cycle_size;
    always_comb begin
        unique case (mode)
            4'd0: begin // M01
                boundary[0] <= 6'd32;
                boundary[1] <= 6'd16;
                boundary[2] <= 6'd63;
                boundary[3] <= 6'd48;
                cycle_size <= 32'd4095;
            end
            4'd1: begin // M02
                boundary[0] <= 6'd16;
                boundary[1] <= 6'd8;
                boundary[2] <= 6'd31;
                boundary[3] <= 6'd24;
                cycle_size <= 32'd8191;
            end
            4'd2: begin // M04
                boundary[0] <= 6'd8;
                boundary[1] <= 6'd4;
                boundary[2] <= 6'd15;
                boundary[3] <= 6'd12;
                cycle_size <= 32'd16383;
            end
            4'd3: begin // M08
                boundary[0] <= 6'd4;
                boundary[1] <= 6'd2;
                boundary[2] <= 6'd7;
                boundary[3] <= 6'd6;
                cycle_size <= 32'd32767;
            end
            default: begin // M16
                boundary[0] <= 6'd2;
                boundary[1] <= 6'd1;
                boundary[2] <= 6'd3;
                boundary[3] <= 6'd3;
                cycle_size <= 32'd65535;
            end
        endcase    
    end

    logic [5:0] chip_cnt;
    logic [3:0] bit_cnt;
    logic ena, crc_done, clear_crc;
    logic [1:0] sum2, data2;
    logic [23:0] data;
    logic [31:0] crc, bit_errors;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            ena <= 1'b0;
            sum2 <= '0;
            data2 <= '0;
            chip_cnt <= '0;
            bit_cnt <= '0;
            data <= '0;
            crc <= '0;
            bit_errors <= '0;
            crc_done <= 1'b0;
        end
        else begin
            crc_done <= 1'b0;
            if (orbit.strobe_c || ena) begin
                ena <= 1'b1;
                chip_cnt <= chip_cnt + 1'b1;
                if (chip_cnt == '0) begin
                    sum2[1] <= orbit.orbit;
                end
                if (chip_cnt > 6'd0 && chip_cnt < boundary[0]) begin
                    sum2[1] <= sum2[1] ^ orbit.orbit;
                end
                if (chip_cnt == boundary[1]) begin 
                    data2[1] <= orbit.orbit;
                end
                
                if (chip_cnt == boundary[0]) begin
                    sum2[0] <= orbit.orbit;
                end
                if (chip_cnt > boundary[0] && chip_cnt <= boundary[2]) begin
                    sum2[0] <= sum2[0] ^ orbit.orbit;
                end
                if (chip_cnt == boundary[3]) begin 
                    data2[0] <= orbit.orbit;
                end                        
                if (chip_cnt == boundary[2])begin
                    chip_cnt <= '0;
                end
                
                if (chip_cnt == 6'd0 && ena) begin
                    bit_cnt <= bit_cnt + 1'b1;
                    data[1:0] <= data2;
                    for (int i = 24; i > 2; i = i - 2) begin
                        data[i - 1] <= data[i - 3];
                        data[i - 2] <= data[i - 4];
                    end
                    if (bit_cnt == 4'd11) begin
                        crc = next_crc(data, crc);
                        crc_done <= 1'b1;
                        if (sum2 != '0) begin
                            bit_errors <= bit_errors + 1'b1;
                        end
                        bit_cnt <= '0;
                    end
                end
                
                if (clear_crc) begin
                    crc <= '0;
                end
            end
        end
    end
    
    logic [31:0] word_cnt, cycle_cnt, crc_cycle;
    logic [3:0][31:0]crc_1frame;
    logic [2:0] crc_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            word_cnt <= '0;
            clear_crc <= 1'b0;
            crc_cycle <= '0;
            crc_1frame <= '0;
            cycle_cnt <= '0;
            crc_cnt <= '0;
        end
        else begin
            clear_crc <= 1'b0;
            if (crc_done) begin
                word_cnt <= word_cnt + 1'b1;
                if (word_cnt == cycle_size) begin
                    if (crc_cnt != 3'd4) begin
                        crc_cnt <= crc_cnt + 1'b1;
                        crc_1frame[crc_cnt] <= crc;
                    end
                    crc_cycle <= crc;
                    clear_crc <= 1'b1;
                    cycle_cnt <= cycle_cnt + 1'b1;
                    word_cnt <= '0;
                end
            end
        end
    end
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0; 
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                if (!wbs.wbm_we) begin
                    if (wbs.wbm_sel == '1) begin
                        case (wbs.wbm_adr[`WB_ADR_INT_WIDTH - 1:0])
                        `WB_ADR_INT_WIDTH'h0: begin
                            wbs.wbs_dat <= crc_1frame[0];
                        end
                        `WB_ADR_INT_WIDTH'h1: begin
                            wbs.wbs_dat <= crc_1frame[1];
                        end
                        `WB_ADR_INT_WIDTH'h2: begin
                            wbs.wbs_dat <= crc_1frame[2];
                        end
                        `WB_ADR_INT_WIDTH'h3: begin
                            wbs.wbs_dat <= crc_1frame[3];
                        end
                        `WB_ADR_INT_WIDTH'h4: begin 
                            wbs.wbs_dat <= crc_cycle;
                        end
                        `WB_ADR_INT_WIDTH'h5: begin 
                            wbs.wbs_dat <= cycle_cnt;
                        end
                        default: begin
                            wbs.wbs_dat <= '0;
                        end
                    endcase
                    end
                end
                wbs.wbs_ack <= 1'b1;
            end
        end
    end
    
    function logic [31:0] next_crc(input logic [23:0] data, crc);
        logic [23:0] d;
        logic [31:0] c;
        logic [31:0] newcrc;
        d = data;
        c = crc;
        newcrc[0] = d[16] ^ d[12] ^ d[10] ^ d[9] ^ d[6] ^ d[0] ^ c[8] ^ c[14] ^ c[17] ^ c[18] ^ c[20] ^ c[24];
        newcrc[1] = d[17] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[7] ^ d[6] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[25];
        newcrc[2] = d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26];
        newcrc[3] = d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[3] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27];
        newcrc[4] = d[20] ^ d[19] ^ d[18] ^ d[15] ^ d[12] ^ d[11] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[23] ^ c[26] ^ c[27] ^ c[28];
        newcrc[5] = d[21] ^ d[20] ^ d[19] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[21] ^ c[27] ^ c[28] ^ c[29];
        newcrc[6] = d[22] ^ d[21] ^ d[20] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[22] ^ c[28] ^ c[29] ^ c[30];
        newcrc[7] = d[23] ^ d[22] ^ d[21] ^ d[16] ^ d[15] ^ d[10] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[23] ^ c[24] ^ c[29] ^ c[30] ^ c[31];
        newcrc[8] = d[23] ^ d[22] ^ d[17] ^ d[12] ^ d[11] ^ d[10] ^ d[8] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[25] ^ c[30] ^ c[31];
        newcrc[9] = d[23] ^ d[18] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[26] ^ c[31];
        newcrc[10] = d[19] ^ d[16] ^ d[14] ^ d[13] ^ d[9] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[17] ^ c[21] ^ c[22] ^ c[24] ^ c[27];
        newcrc[11] = d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[17] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[28];
        newcrc[12] = d[21] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[17] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[29];
        newcrc[13] = d[22] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[30];
        newcrc[14] = d[23] ^ d[20] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
        newcrc[15] = d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[4] ^ d[3] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[29];
        newcrc[16] = d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[13] ^ d[12] ^ d[8] ^ d[5] ^ d[4] ^ d[0] ^ c[8] ^ c[12] ^ c[13] ^ c[16] ^ c[20] ^ c[21] ^ c[25] ^ c[27] ^ c[29] ^ c[30];
        newcrc[17] = d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[14] ^ d[13] ^ d[9] ^ d[6] ^ d[5] ^ d[1] ^ c[9] ^ c[13] ^ c[14] ^ c[17] ^ c[21] ^ c[22] ^ c[26] ^ c[28] ^ c[30] ^ c[31];
        newcrc[18] = d[23] ^ d[21] ^ d[19] ^ d[15] ^ d[14] ^ d[10] ^ d[7] ^ d[6] ^ d[2] ^ c[10] ^ c[14] ^ c[15] ^ c[18] ^ c[22] ^ c[23] ^ c[27] ^ c[29] ^ c[31];
        newcrc[19] = d[22] ^ d[20] ^ d[16] ^ d[15] ^ d[11] ^ d[8] ^ d[7] ^ d[3] ^ c[11] ^ c[15] ^ c[16] ^ c[19] ^ c[23] ^ c[24] ^ c[28] ^ c[30];
        newcrc[20] = d[23] ^ d[21] ^ d[17] ^ d[16] ^ d[12] ^ d[9] ^ d[8] ^ d[4] ^ c[12] ^ c[16] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[29] ^ c[31];
        newcrc[21] = d[22] ^ d[18] ^ d[17] ^ d[13] ^ d[10] ^ d[9] ^ d[5] ^ c[13] ^ c[17] ^ c[18] ^ c[21] ^ c[25] ^ c[26] ^ c[30];
        newcrc[22] = d[23] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[12] ^ d[11] ^ d[9] ^ d[0] ^ c[8] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[31];
        newcrc[23] = d[20] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[9] ^ d[6] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[14] ^ c[17] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
        newcrc[24] = d[21] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[10] ^ d[7] ^ d[2] ^ d[1] ^ c[0] ^ c[9] ^ c[10] ^ c[15] ^ c[18] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29];
        newcrc[25] = d[22] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[11] ^ d[8] ^ d[3] ^ d[2] ^ c[1] ^ c[10] ^ c[11] ^ c[16] ^ c[19] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
        newcrc[26] = d[23] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[10] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[2] ^ c[8] ^ c[11] ^ c[12] ^ c[14] ^ c[18] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
        newcrc[27] = d[23] ^ d[21] ^ d[20] ^ d[19] ^ d[11] ^ d[7] ^ d[5] ^ d[4] ^ d[1] ^ c[3] ^ c[9] ^ c[12] ^ c[13] ^ c[15] ^ c[19] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
        newcrc[28] = d[22] ^ d[21] ^ d[20] ^ d[12] ^ d[8] ^ d[6] ^ d[5] ^ d[2] ^ c[4] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[20] ^ c[28] ^ c[29] ^ c[30];
        newcrc[29] = d[23] ^ d[22] ^ d[21] ^ d[13] ^ d[9] ^ d[7] ^ d[6] ^ d[3] ^ c[5] ^ c[11] ^ c[14] ^ c[15] ^ c[17] ^ c[21] ^ c[29] ^ c[30] ^ c[31];
        newcrc[30] = d[23] ^ d[22] ^ d[14] ^ d[10] ^ d[8] ^ d[7] ^ d[4] ^ c[6] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[22] ^ c[30] ^ c[31];
        newcrc[31] = d[23] ^ d[15] ^ d[11] ^ d[9] ^ d[8] ^ d[5] ^ c[7] ^ c[13] ^ c[16] ^ c[17] ^ c[19] ^ c[23] ^ c[31];
        next_crc = newcrc;
    endfunction: next_crc
  
endmodule: larva_orbit_crc