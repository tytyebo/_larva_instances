`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::orbitTelemSettings_s;
import defines_pkg::orbitService_s;
module larva_orbit_telemeter#(parameter bit [15:0] MEMORY_ADDR = 16'h0000,
                              parameter int DELAY_ENA = 500, SYNC_DELAY = 60, OUTPUT_DELAY = 60)
                             (input clk,
                              input nrst,

                              // Wishbone master interface
                              output [15:0] wbm_adr_o,
                              output [31:0] wbm_dat_o,	
                              output wbm_stb_o,
                              output wbm_cyc_o,
                              output [3:0] wbm_sel_o,
                              output wbm_we_o,	
                              input [31:0] wbm_dat_i,
                              input wbm_ack_i,
                              
                              // Wishbone slave interface
                              input [15:0] wbs_adr_i,
                              input [31:0] wbs_dat_i,	
                              input wbs_stb_i,
                              input wbs_cyc_i,
                              input [3:0] wbs_sel_i,
                              input wbs_we_i,	
                              output [31:0] wbs_dat_o,
                              output wbs_ack_o,
                              
                              // Fifo map-interface
                              input logic [7:0] map_data, // код адреса опроса - вход адреса памяти larva_orbit_mem
                              input logic map_nempty,
                              output logic map_rrq,
                              output logic [3:0] map_mode,
                              output map_enable,
                              
                              // Local timer enable
                              output logic timer_enable,
                              // Discrete signal one pulse signal input
                              input logic record, 
                              // Global Sync input. There is 4 Hz period. 
                              input logic sync,
                              // sdram stop address
                              output logic [24:0] sdram_stop_adr,
                              // Service information inputs
                              input logic [15:0] local_time_label, // Метка текущего времени (далее - МТВ)
                              input logic [13:0] start_command_label, // Метка прохождения команды (далее - МПК)
                              // input logic [1:0] online, // Команда Исправность // Всегда 2'b11 по ТЗ
                              input logic [1:0] com1, // Команда КОМ1
                              input logic [1:0] com2, // Команда КОМ2
                              input logic [1:0] com3, // Команда КОМ3
                              input logic [1:0] pc1_mode, // Режим ЦФП1
                              input logic [1:0] pc2_mode, // Режим ЦФП2
                              input logic [19:0] system_number, // Номер системы
                              // input logic [7:0] mem_time_rec_label, // Метка времени записи в ЗУ (далее - МВЗ)
                              // input logic [1:0] mem_mode, // Режим ЗУ
                              input logic [5:0] terminal_address, // Адрес ОУ
                              input logic [1:0] mil1_mode, // Режим МК1
                              input logic [1:0] lost_gen_mode, // Режим СГ
                              // input logic [5:0] speed_code, // Код Информативности (И)
                              input logic [3:0] transmitter_power, // Мощность передатчика (М ПР)
                              input logic [7:0] self_temp, // Температура прибора ФТ75 (Т ФТ)
                              input logic [7:0] trans_temp, // Температура передатчика (Т ПР)
                              input logic [7:0] danger_nets, // Напряжение опасных цепей (U оц)
                              input logic [7:0] transmitter_voltage, // Напряжение передатчика (U им)
                              
                              output logic reset_req, cb_ch_sync, selftest,
                              orbit_io.out orbit);
	
    orbitTelemSettings_s telem_sett;
	logic enable_locked, delayed_enable_locked, settings_loaded, telem_stop;
    logic [2:0] telem_state;
    logic [$clog2(DELAY_ENA) - 1:0] delay_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            enable_locked <= 1'b0;
            delayed_enable_locked <= 1'b0;
            delay_cnt <= {$clog2(DELAY_ENA){1'b0}};
        end
        else begin
            if (telem_state[1:0] == 2'd1) begin
                enable_locked <= 1'b1;
            end
            if (enable_locked && !delayed_enable_locked) begin
                delay_cnt <= delay_cnt + 1'b1;
                if (delay_cnt == DELAY_ENA) begin
                    delayed_enable_locked <= 1'b1;
                end
            end
        end    
    end
    assign map_enable = enable_locked;
    assign timer_enable = delayed_enable_locked;
    assign telem_stop = telem_state[1];
    assign settings_loaded = (telem_state[1:0] == 2'd1);
    
    
    logic [3:0] select_mode_num; // Номер текущего режима работы (от 0 до 7)
                                 // ВАЖНО! Режим 0 - М01, 1 - М02, 2 - М04, 3 - М08, 4-9 - М16
    logic memory_mode; // Режим ЗУ
    logic [7:0] mem_time_rec;
    // sdram_stop_adr - это запрограммирование время записи в ЗУ (9'd1 - 0,5 сек; 9'd256 - 128 сек), выраженное в конечном адресе записи sdram.
    // М01: stop_adr = 4096 * 2 * tЗАП − 1;
    // М02: stop_adr = 8192 * 2 * tЗАП − 1;
    // М04: stop_adr = 16384 * 2 * tЗАП − 1;
    // М08: stop_adr = 32768 * 2 * tЗАП − 1;
    // М16: stop_adr = 65536 * 2 * tЗАП − 1;
    // где  tЗАП — время записи в ЗУ в секундах от 0,5 до 128.
    always_comb begin
        unique case (select_mode_num)
            4'd0: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M01_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M01_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd63;
                telem_sett.phrase_size <= 5'd1;
            end
            4'd1: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M02_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M02_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd31;
                telem_sett.phrase_size <= 5'd2;
            end
            4'd2: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M04_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M04_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd15;
                telem_sett.phrase_size <= 5'd4;
            end
            4'd3: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M08_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M08_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd7;
                telem_sett.phrase_size <= 5'd8;
            end
            4'd9: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M08_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M08_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd7;
                telem_sett.phrase_size <= 5'd8;
            end
            default: begin
                sdram_stop_adr <= ({mem_time_rec + 1'b1, 1'b0} << $clog2(`M16_SIZE)) - 1'b1;
                telem_sett.word_clocks_size <= `M16_SHIFT_LN;
                telem_sett.bit_clocks_size <= 6'd3;
                telem_sett.phrase_size <= 5'd16;
            end
        endcase
    end   
    
    logic [9:0] shift_ln_cnt;
    logic sync_loaded, read_word;
    logic sync_delayed, sync_loaded_delayed;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            shift_ln_cnt <= '0;
            read_word <= 1'b0;
            sync_loaded <= 1'b0;
        end
        else begin
            read_word <= 1'b0;
            if (settings_loaded && map_nempty && (sync_delayed || sync_loaded)) begin
                sync_loaded <= 1'b1;
                shift_ln_cnt <= shift_ln_cnt + 1'b1;
                if (shift_ln_cnt == '0) begin
                    read_word <= 1'b1;
                end
                if (shift_ln_cnt == telem_sett.word_clocks_size) begin
                    shift_ln_cnt <= '0;
                end                    
            end
        end
    end
        
    logic wbs_ack;
    logic [31:0] wbs_dat;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            wbs_ack <= 1'b0;
            memory_mode <= 1'b0;
            select_mode_num <= '0;
            wbs_dat <= '0;
            mem_time_rec <= '0;
            telem_state <= '0;
        end
        else begin 
            wbs_ack <= 1'b0;

            if (wbs_stb_i && wbs_cyc_i && !wbs_ack) begin
                if (wbs_we_i) begin
                    case (wbs_adr_i[11:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs_sel_i == 4'hf) begin
                                {mem_time_rec, memory_mode, select_mode_num} <= wbs_dat_i[12:0]; // 8 + 1 + 4 разрядов
                            end                                        
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs_sel_i[0] == 1'b1) begin
                                telem_state <= wbs_dat_i[2:0]; // 3 разряда
                            end                                        
                        end
                    endcase
                end
                else begin
                    case (wbs_adr_i[11:0])
                        `WB_ADR_INT_WIDTH'd0: begin
                            if (wbs_sel_i[1:0] == 2'h3) begin
                                wbs_dat <= {19'd0, mem_time_rec, memory_mode, select_mode_num};
                            end
                        end
                        `WB_ADR_INT_WIDTH'd1: begin
                            if (wbs_sel_i[0] == 1'b1) begin
                                wbs_dat <= {29'd0, telem_state};
                            end
                        end                  
                        default: begin
                            wbs_dat <= 32'd0;
                        end
                    endcase
                end
                wbs_ack <= 1'b1;
            end
        end
    end
    assign map_mode = select_mode_num;

    
    logic is_record, record_sync;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            is_record <= 1'b0;
            record_sync <= 1'b0;
        end
        else begin
            if (!record && memory_mode) begin // Команда ЗАП имеет инверсную полярность
                is_record <= 1'b1;
            end
            if (sync_delayed && is_record) begin
                record_sync <= 1'b1;
            end
            
        end    
    end

    logic [15:0] wbm_adr;
    logic wbm_we, wbm_stb;
    logic [15:0] wbm_data;
    logic data_ready;
    logic [15:0] data;
    logic record_complete;
    logic [24:0] memory_stop_cnt;
    wire [15:0] mem_q;
    
    typedef enum bit [1:0] {WBM_SET_ADR, WBM_GET_DATA, WBM_MEM_WRITE, WBM_MEM_DONE} state_e;
    state_e wbm_state;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            wbm_adr <= 16'd0;
            wbm_we <= 1'b0;
            wbm_stb <= 1'b0;
            data_ready <= 1'b0;
            wbm_data <= 16'd0;
            data <= 16'd0;
            record_complete <= 1'b0;
            memory_stop_cnt <= 25'd0;
			map_rrq <= 1'b0;
            wbm_state <= WBM_SET_ADR;

            cb_ch_sync <= 1'b0;
        end
        else begin
            data_ready <= 1'b0;
            map_rrq <= 1'b0;
            cb_ch_sync <= 1'b0;
            case (wbm_state)
                WBM_SET_ADR: begin
                    if (read_word && !telem_stop) begin
                        wbm_adr <= mem_q;
                        wbm_we <= 1'b0;
                        wbm_stb <= 1'b1;
                        map_rrq <= 1'b1;
                        wbm_state <= WBM_GET_DATA;
                    end             
                end
                WBM_GET_DATA: begin
                    if (wbm_ack_i) begin
                        data <= wbm_dat_i[15:0];
                        wbm_stb <= 1'b0;
                        wbm_state <= WBM_MEM_WRITE;
                    end
                end
                WBM_MEM_WRITE: begin
                    wbm_state <= WBM_SET_ADR;
                    if ((record_sync || selftest) && !record_complete) begin
                        wbm_adr <= MEMORY_ADDR;
                        wbm_we <= 1'b1;
                        wbm_stb <= 1'b1;
                        wbm_data <= data;
                        memory_stop_cnt <= memory_stop_cnt + 1'b1;
                        if (memory_stop_cnt == sdram_stop_adr) begin
                            record_complete <= 1'b1;
                        end
                        wbm_state <= WBM_MEM_DONE;
                    end
                    data_ready <= 1'b1;
                end
                WBM_MEM_DONE: begin
                    if (wbm_ack_i) begin
                        wbm_state <= WBM_SET_ADR;
						wbm_we <= 1'b0;
                        wbm_stb <= 1'b0;
                    end
                end
            endcase
        end
    end 
    localparam [1:0] MEM_OFF = 2'd0, // Коды состояний Реж.ЗУ
                     MEM_WR = 2'd1,
                     MEM_RD = 2'd2;
    logic [1:0] memory_mode_code;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin	
            memory_mode_code <= MEM_OFF;
        end
        else begin
            if (record_sync || selftest) begin
                memory_mode_code <= MEM_WR;
            end
            if (record_complete) begin
                memory_mode_code <= MEM_RD;
            end
            if (!(record_sync || selftest)) begin
                memory_mode_code <= MEM_OFF;
            end
        end
    end
    
    metastable_chain#(.CHAIN_DEPTH(OUTPUT_DELAY), .BUS_WIDTH(1))
    metaChainOutput(.clk(clk), .nrst(nrst),
                 .din(sync_loaded), .dout(sync_loaded_delayed));
                 
    metastable_chain#(.CHAIN_DEPTH(SYNC_DELAY), .BUS_WIDTH(1))
    metaChainSync(.clk(clk), .nrst(nrst),
                 .din(sync), .dout(sync_delayed));
    // Формирование структуры служебной информации
    orbitService_s orbit_service;
    always_comb begin
        orbit_service.local_time_label <= local_time_label; // Метка текущего времени (далее - МТВ)
        orbit_service.online <= 2'b11; // Команда Исправность
        orbit_service.pc1_mode <= pc1_mode; // Режим ЦФП1
        orbit_service.pc2_mode <= pc2_mode; // Режим ЦФП2
        orbit_service.system_number <= system_number; // Номер системы
        orbit_service.mem_time_rec_label <= mem_time_rec; // Метка времени записи в ЗУ (далее - МВЗ)
        orbit_service.mem_mode <= memory_mode_code; // Режим ЗУ
        orbit_service.terminal_address <= terminal_address; // Адрес ОУ
        orbit_service.mil1_mode <= mil1_mode; // Режим МК1
        orbit_service.speed_code <= {2'd0, select_mode_num}; // Код Информативности (И)
        orbit_service.blank_field <= 2'b00; // Пустое поле
        if (!selftest) begin
            orbit_service.start_command_label <= {start_command_label[13:7], 1'b0, start_command_label[6:0], 1'b0}; // Метка прохождения команды (далее - МПК)
            orbit_service.com1 <= com1; // Команда КОМ1
            orbit_service.com2 <= com2; // Команда КОМ2
            orbit_service.com3 <= com3; // Команда КОМ3
            orbit_service.lost_gen_mode <= lost_gen_mode; // Режим СГ
            orbit_service.blank_field <= 2'b00; // Пустое поле
            orbit_service.transmitter_power <= transmitter_power; // Мощность передатчика (М ПР)
            orbit_service.self_temp <= self_temp; // Температура прибора ФТ75 (Т ФТ)
            orbit_service.trans_temp <= trans_temp; // Температура передатчика (Т ПР)
            orbit_service.danger_nets <= danger_nets; // Напряжение опасных цепей (U оц)
            orbit_service.transmitter_voltage <= transmitter_voltage; // Напряжение передатчика (U им)
        end
        else begin
            orbit_service.start_command_label <= '0;
            orbit_service.com1 <= '0;
            orbit_service.com2 <= '0;
            orbit_service.com3 <= '0;
            orbit_service.lost_gen_mode <= '0;
            orbit_service.transmitter_power <= '0;
            orbit_service.self_temp <= '0;
            orbit_service.trans_temp <= '0;
            orbit_service.danger_nets <= '0;
            orbit_service.transmitter_voltage <= '0;
        end
    end
    
    larva_orbit_serviceman larvaOrbitServiceman(
        .clk(clk),
        .nrst(nrst),
        .data_ready(data_ready),
        .data(data[9:0]),
        .settings(telem_sett),
        // Service information inputs
        .orbit_service(orbit_service),
        .orbit(orbit),
        .output_ena(sync_loaded_delayed));
    
    assign wbm_stb_o = wbm_stb;
    assign wbm_cyc_o = wbm_stb;
    assign wbm_sel_o = 4'b0011;
    assign wbm_dat_o = {16'd0, wbm_data};
    assign wbm_we_o = wbm_we;
    assign wbm_adr_o = wbm_adr;
    
    assign wbs_ack_o = wbs_ack;
    assign wbs_dat_o = wbs_dat;
    assign reset_req = (telem_state[1:0] == 2'd3);

    assign selftest = telem_state[2];
    // Память под преобразования карты опроса адресов для 9 базовых режимов, плюс 1 дополнительный из 8 разрядных кодов в 16 разрядные адреса
    larva_orbit_mem larvaOrbitMem(.clock(clk), .address(map_data), .data(16'd0), .wren(1'b0), .q(mem_q));
endmodule: larva_orbit_telemeter