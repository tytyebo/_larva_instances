`include "defines.svh"
`include "interfaces.svh"
module larva_orbit_inserter(input bit clk, nrst,
                            input logic [5:0] bit_repeat,
                            input logic [4:0] phrase_size,
                            input logic data_ready,
                            input logic [10:0] data,
                            input logic ena,
                            orbit_io.out orbit);
    logic [4:0] word_count;
    assign word_count = (phrase_size == 5'd16) ? 5'd31 : { phrase_size[3:0], 1'b0 }; // Количество слов в одной фразе

    localparam bit [15:0] M16 = 16'b1011001_0_00011110; // mirrored
    logic fifo_wrq;
    logic [15:0] fifo_wdata;
    logic [4:0] word_cnt;
    logic marker_sent, even_word;
    logic [3:0] m16_cnt;

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            word_cnt <= 5'd0;
            marker_sent <= 1'b0;
            m16_cnt <= 4'd0;
            fifo_wrq <= 1'b0;
            fifo_wdata <= 16'd0;
            even_word <= 1'b0;
        end
        else begin
            fifo_wrq <= 1'b0;
            if (data_ready) begin
                word_cnt <= word_cnt + 1'b1;
                fifo_wrq <= 1'b1;
                fifo_wdata[15:12] <= 4'd0;
                fifo_wdata[11:1] <= data;
                fifo_wdata[0] <= ~(^data[9:0]);
                            
                if (phrase_size != 5'd16) begin // Не М16
                    if (word_cnt == 5'd0 && !marker_sent) begin
                        fifo_wdata[12] <= 1'b1;
                        marker_sent <= 1'b1; 
                    end
                end
                else begin
                    if (!even_word) begin
                        m16_cnt <= m16_cnt + 1'b1;
                        if (m16_cnt != 8) begin
                            fifo_wdata[11] <= M16[m16_cnt];
                        end
                    end
                    even_word <= ~even_word;  
                end   
            end
                        
            if (word_cnt == word_count) begin
                word_cnt <= 5'd0;
                marker_sent <= 1'b0;
            end     
        end           
    end
    logic [15:0] cycle_size;
    assign cycle_size = {phrase_size[3:0] , 12'd0} - 1'b1;
    larva_orbit_sequencer larvaOrbitSequencer(.clk(clk),
                                              .nrst(nrst),
                                              .bit_repeat(bit_repeat), 
                                              .cycle_size(cycle_size),
                                              .fifo_wrq(fifo_wrq),
                                              .fifo_wdata(fifo_wdata),
                                              .ena(ena),
                                              .data_missed(),
                                              .orbit(orbit));
endmodule: larva_orbit_inserter