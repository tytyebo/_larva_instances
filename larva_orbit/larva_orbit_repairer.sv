`include "defines.svh"
`include "interfaces.svh"
module larva_orbit_repairer(input bit clk, nclk, nrst,
                            orbit_io.in orbit_in,
                            output logic [5:0] orbit,
                            input logic selftest, square, inv, off);
	
    logic [5:0] orbit_clk_q, orbit_nclk_q;
    orbit_io orbit_repaired();
    logic [1:0] inv_q, off_q;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            orbit_clk_q <= '0;
        end
        else begin
            orbit_clk_q[0] <= orbit_in.orbit;
            orbit_clk_q[1] <= orbit_in.orbit;
            orbit_clk_q[2] <= orbit_in.clock_orb;
            orbit_clk_q[3] <= orbit_in.strobe_w;
            orbit_clk_q[4] <= orbit_in.strobe_c;
            orbit_clk_q[5] <= orbit_in.strobe_c;
        end
    end
    always_ff @(posedge nclk or negedge nrst) begin
        if (!nrst) begin				
            orbit_nclk_q <= '0;
        end
        else begin
            orbit_nclk_q[0] <= orbit_in.orbit;
            orbit_nclk_q[1] <= orbit_in.orbit;
            orbit_nclk_q[2] <= orbit_in.clock_orb;
            orbit_nclk_q[3] <= orbit_in.strobe_w;
            orbit_nclk_q[4] <= orbit_in.strobe_c;
            orbit_nclk_q[5] <= orbit_in.strobe_c;
        end
    end
`ifdef SIMULATION
    assign orbit[0] <= orbit_clk_q[0] & orbit_nclk_q[0];
    assign orbit[1] <= orbit_clk_q[1] & orbit_nclk_q[1];
    assign orbit[2] <= orbit_clk_q[2] & orbit_nclk_q[2];
    assign orbit[3] <= orbit_clk_q[3] & orbit_nclk_q[3];
    assign orbit[4] <= orbit_clk_q[4] & orbit_nclk_q[4];
    assign orbit[5] <= orbit_clk_q[5] & orbit_nclk_q[5];
`else   
    always_comb begin
        if (!selftest) begin
            if (!square) begin
                orbit[0] <= orbit_clk_q[0] & orbit_nclk_q[0];
                orbit[1] <= orbit_clk_q[1] & orbit_nclk_q[1];
                orbit[2] <= orbit_clk_q[2] & orbit_nclk_q[2];
                orbit[3] <= orbit_clk_q[3] & orbit_nclk_q[3];
                orbit[4] <= orbit_clk_q[4] & orbit_nclk_q[4];
                orbit[5] <= orbit_clk_q[5] & orbit_nclk_q[5];
            end
            else begin
                orbit[0] <= (!off_q[1]) ? orbit_clk_q[2] & orbit_nclk_q[2] : 1'b1;
                orbit[1] <= (!off_q[1]) ? ((!inv_q[1]) ? orbit_clk_q[2] & orbit_nclk_q[2] : ~(orbit_clk_q[2] & orbit_nclk_q[2])) : 1'b1;
                orbit[2] <= 1'b1;
                orbit[3] <= 1'b1;
                orbit[4] <= 1'b1;
                orbit[5] <= 1'b1;
            end
        end
        else begin
            orbit[0] <= 1'b0;
            orbit[1] <= 1'b0;
            orbit[2] <= 1'b0;
            orbit[3] <= 1'b0;
            orbit[4] <= 1'b0;
            orbit[5] <= 1'b0;
        end
    end

    always_ff @(posedge nclk or negedge nrst) begin
        if (!nrst) begin				
            inv_q <= '0;
            off_q <= '0;
        end
        else begin
            inv_q[0] <= inv;
            off_q[0] <= off;
            if (inv && !inv_q[0]) begin
                inv_q[1] <= 1'b1;
            end
            if (off && !off_q[0]) begin
                off_q[1] <= 1'b1;
            end
        end
    end
`endif
endmodule: larva_orbit_repairer
