module larva_orbit_telemeter
    #(
        parameter MEMORY_ADDR = 16'h0000
    )
    (
        input clk,
        input nrst,

        // Wishbone master interface
        output [15:0] wbm_adr_o,
        output [31:0] wbm_dat_o,	
        output wbm_stb_o,
        output wbm_cyc_o,
        output [3:0] wbm_sel_o,
        output wbm_we_o,	
        input [31:0] wbm_dat_i,
        input wbm_ack_i,
        
        // Wishbone slave interface
        input [15:0] wbs_adr_i,
        input [31:0] wbs_dat_i,	
        input wbs_stb_i,
        input wbs_cyc_i,
        input [3:0] wbs_sel_i,
        input wbs_we_i,	
        output [31:0] wbs_dat_o,
        output wbs_ack_o,
        
        // Service information inputs
        input [15:0] local_time_label, // Метка текущего времени (далее - МТВ)
        input [13:0] start_command_label, // Метка прохождения команды (далее - МПК)
        input [1:0] online, // Команда Исправность
        input [1:0] com1, // Команда КОМ1
        input [1:0] com2, // Команда КОМ2
        input [1:0] com3, // Команда КОМ3
        input [1:0] par_ch1_mode, // Режим ЦФП1
        input [1:0] par_ch2_mode, // Режим ЦФП2
        input [19:0] system_number, // Номер системы
        // input [7:0] mem_time_rec_label, // Метка времени записи в ЗУ (далее - МВЗ)
        // input [1:0] mem_mode, // Режим ЗУ
        input [5:0] terminal_address, // Адрес ОУ
        input [1:0] MK1_mode, // Режим МК1
        input [1:0] lost_gen_mode, // Режим СГ
        input [5:0] speed_code, // Код Информативности (И)
        input [3:0] transmitter_power, // Мощность передатчика (М ПР)
        input [7:0] self_temp, // Температура прибора ФТ75 (Т ФТ)
        input [7:0] trans_temp, // Температура передатчика (Т ПР)
        input [7:0] danger_nets, // Напряжение опасных цепей (U оц)
        input [7:0] transmitter_voltage, // Напряжение передатчика (U им)
        
        output video,
        output debug
    );
reg [15:0] mode_mem[0:4096 * 8 - 1]; // Память адресов опроса для 8 режимов
                                    // Режимы располагаются по порядку от 0 до 7. Размер памяти под каждый режим 4096 16-разрядных слов
`ifdef SIMULATION
reg [7:0] error;
reg [15:0] read_num;
integer file, i;
initial
    begin
        error = 8'd0;
        file = $fopen("mode5.txt", "r");
        if (file == 0)
            begin
                error[4] = 1'b1;
            end
        else
            begin
                for (i = 0; i < 4096; i = i + 1)
                    begin
                        $fscanf(file, "%h", read_num);
                        mode_mem[i + 4096 * 4] = read_num;
                        $display("%h",  mode_mem[i + 4096 * 4]);
                    end
            end
        $fclose(file);
    end
`else
integer i;
initial
    begin
        for (i = 0; i < 4096; i = i + 1)
            begin
                mode_mem[i + 4096 * 4] = 16'd0;
            end
    end
`endif    
reg [2:0] select_mode_num; // Номер текущего режима работы (от 0 до 7)
                           // ВАЖНО! Режим 0 - М01, 1 - М02, 2 - М04, 3 - М08, 4-7 - М16
reg memory_mode; // Режим ЗУ
reg [8:0] memory_timer; // Запрограммирование время записи в ЗУ (9'd1 - 0,5 сек; 9'd256 - 128 сек)
reg [14:0] memory_base_adr; // Базовый адрес для чтения и записи памяти адресов опроса Режимов работы.
reg [14:0] memory_adr_cnt;
reg settings_loaded; // Все настройки загружены
reg wbs_ack;
reg [31:0] wbs_dat;
always @(posedge clk or negedge nrst)
	begin
		if (~nrst)
			begin				
                settings_loaded <= 1'b0;
                wbs_ack <= 1'b0;
                memory_mode <= 1'b0;
                memory_timer <= 9'd0;
                memory_base_adr <= 15'd0;
                memory_adr_cnt <= 15'd0;
                select_mode_num <= 3'd0;
                wbs_dat <= 32'd0;
			end
		else
			begin 
                wbs_ack <= 1'b0;
                if (wbs_stb_i && wbs_cyc_i && ~wbs_ack)
                    begin
                        if (wbs_we_i)
                            begin
                                case (wbs_adr_i[11:0])
                                    12'h000:
                                        begin
                                            {memory_timer, memory_mode, select_mode_num, settings_loaded} <= wbs_dat_i[13:0]; // 9 + 1 + 3 + 1 разрядов                                          
                                        end
                                    12'h010:
                                        begin
                                            memory_base_adr <= wbs_dat_i[14:0];
                                            memory_adr_cnt <= 15'd0;
                                        end
                                    12'h011:
                                        begin
                                            if (memory_base_adr == 15'h7000)
                                                begin
                                                    mode_mem[memory_adr_cnt] <= wbs_dat_i[15:0];
                                                    memory_adr_cnt <= memory_adr_cnt + 1'b1;
                                                end
                                        end
                                    12'h012:
                                        begin
                                            if (memory_base_adr == 15'h7000)
                                                begin
                                                    mode_mem[memory_adr_cnt] <= { wbs_dat_i[31:24], wbs_dat_i[23:16] };
                                                    mode_mem[memory_adr_cnt + 1'b1] <= { wbs_dat_i[15:8], wbs_dat_i[7:0] };
                                                    memory_adr_cnt <= memory_adr_cnt + 2'd2;
                                                end
                                        end
                                endcase
                            end
                        else
                            begin
                                case (wbs_adr_i[11:0])
                                    12'h000:
                                        wbs_dat <= {18'd0, memory_timer, memory_mode, select_mode_num, settings_loaded};
                                    12'h010:
                                        wbs_dat <= {17'd0, memory_base_adr};
                                    12'h011:
                                        begin
                                            wbs_dat <= {16'd0, mode_mem[memory_adr_cnt]};
                                            memory_adr_cnt <= memory_adr_cnt + 1'b1;
                                        end
                                    12'h012:
                                        begin
                                            wbs_dat <= { mode_mem[memory_adr_cnt], mode_mem[memory_adr_cnt + 1'b1] };
                                            memory_adr_cnt <= memory_adr_cnt + 2'd2;
                                        end
                                    12'h013:
                                        begin
                                            wbs_dat <= {17'd0, memory_adr_cnt};
                                        end
                                endcase
                            end
                        wbs_ack <= 1'b1;
                    end
            end
    end

reg [2:0] wbm_state;
reg [15:0] wbm_adr;
reg [11:0] ard_cnt;
reg wbm_we, wbm_stb;
reg [15:0] wbm_data;
reg data_ready;
reg [15:0] data;

wire fifo_full;
wire ready;
localparam [2:0] WBM_IDLE = 3'd0,
                 WBM_SET_ADR = 3'd1,
                 WBM_GET_DATA = 3'd2,
                 WBM_MEM_WRITE = 3'd3,
                 WBM_MEM_DONE = 3'd4;
always @(posedge clk or negedge nrst)
	begin
		if (~nrst)
			begin				
                wbm_adr <= 16'd0;
                ard_cnt <= 12'd0;
                wbm_we <= 1'b0;
                wbm_stb <= 1'b0;
                data_ready <= 1'b0;
                wbm_data <= 16'd0;
                data <= 16'd0;
                wbm_state <= WBM_IDLE;
			end
		else
			begin
                data_ready <= 1'b0;
                case (wbm_state)
                    WBM_IDLE:
                        begin
                            if (settings_loaded && ready)
                                wbm_state <= WBM_SET_ADR;
                        end
                    WBM_SET_ADR:
                        begin
                            if (~fifo_full)
                                begin
                                    wbm_adr <= mode_mem[{ select_mode_num, ard_cnt }];
                                    wbm_we <= 1'b0;
                                    wbm_stb <= 1'b1;
                                    ard_cnt <= ard_cnt + 1'b1;
                                    wbm_state <= WBM_GET_DATA;
                                end
                        end
                    WBM_GET_DATA:
                        begin
                            if (wbm_ack_i)
                                begin
                                    data <= wbm_dat_i[15:0];
                                    wbm_stb <= 1'b0;
                                    wbm_state <= WBM_MEM_WRITE;
                                end
                        end
                    WBM_MEM_WRITE:
                        begin
                            wbm_state <= WBM_SET_ADR;
                            if (memory_mode)
                                begin
                                    wbm_adr <= MEMORY_ADDR;
                                    wbm_we <= 1'b1;
                                    wbm_stb <= 1'b1;
                                    wbm_data <= data;
                                    wbm_state <= WBM_MEM_DONE;
                                end
                            data_ready <= 1'b1;
                        end
                    WBM_MEM_DONE:
                        begin
                            if (wbm_ack_i)
                                wbm_state <= WBM_SET_ADR;                        
                        end
                endcase
            end
    end        
    larva_orbit_serviceman larvaOrbitServiceman
    (
        .clk(clk),
        .nrst(nrst),
        
        .enable(settings_loaded),        
        .mode(select_mode_num),
        
        .data_ready(data_ready),
        .data(data),
                
        // Service information inputs
        .local_time_label(local_time_label), // Метка текущего времени (далее - МТВ)
        .start_command_label(start_command_label), // Метка прохождения команды (далее - МПК)
        .online(), // Команда Исправность
        .com1(), // Команда КОМ1
        .com2(), // Команда КОМ2
        .com3(), // Команда КОМ3
        .par_ch1_mode(), // Режим ЦФП1
        .par_ch2_mode(), // Режим ЦФП2
        .system_number(), // Номер системы
        .mem_time_rec_label(), // Метка времени записи в ЗУ (далее - МВЗ)
        .mem_mode(), // Режим ЗУ
        .terminal_address(), // Адрес ОУ
        .MK1_mode(), // Режим МК1
        .lost_gen_mode(), // Режим СГ
        .speed_code(), // Код Информативности (И)
        .transmitter_power(), // Мощность передатчика (М ПР)
        .self_temp(), // Температура прибора ФТ75 (Т ФТ)
        .trans_temp(), // Температура передатчика (Т ПР)
        .danger_nets(), // Напряжение опасных цепей (U оц)
        .transmitter_voltage(), // Напряжение передатчика (U им)
        
        .fifo_full(fifo_full),     
        .video(video),
        .ready(ready),
        .error(),
        
        .debug(debug)
    );
    
    assign wbm_stb_o = wbm_stb;
    assign wbm_cyc_o = wbm_stb;
    assign wbm_sel_o = 4'b0011;
    assign wbm_dat_o = {16'd0, wbm_data};
    assign wbm_we_o = wbm_we;
    assign wbm_adr_o = wbm_adr;
    
    assign wbs_ack_o = wbs_ack;
    assign wbs_dat_o = wbs_dat;
endmodule