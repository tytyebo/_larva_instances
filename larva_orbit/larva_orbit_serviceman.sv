`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::orbitTelemSettings_s;
module larva_orbit_serviceman(input bit clk, nrst,
                              input logic data_ready,
                              input logic [9:0] data,
                              input orbitTelemSettings_s settings,
                              // Sync inputs
                              input logic output_ena,
                              // Service information inputs
                              input logic [127:0] orbit_service,
                              orbit_io.out orbit);

    logic [4:0] phrase_word_cnt;
    logic phrase_start, phrase_even, service;
    logic [1:0] data_ready_d;
    // Service information section - Data
    localparam bit FRAME_LABEL = 1'b1;
    localparam bit [7:0] GROUP_LABEL = 8'b01110010, // Маркер группы (далее - МГ) = 0x72
                         CYCLE_LABEL = 8'b10001101; // Маркер цикла (далее - МЦ) = 0x8D

    // Service information section - Counters
    logic [6:0] group_cnt;    // Счетчик групп
    logic [5:0] phrase_pair_cnt;   // Счетчик фраз (т.к. служебная информация добавляется только в четные фразы, то счетчик фраз от 0 до 63)
    logic [63:0] group;
    logic new_group;
    always_ff @(posedge clk or negedge nrst) begin
		if (!nrst) begin				
            phrase_word_cnt <= 5'd0;
            phrase_start <= 1'b0;
            phrase_even <= 1'b0;
            service <= 1'b0;
            data_ready_d <= '0;
                   
            // Service information section - Counters
            group_cnt <= 7'd0;
            phrase_pair_cnt <= 6'd0;
            new_group <= 1'b0;
        end
		else begin
            phrase_start <= 1'b0;
            data_ready_d[0] <= data_ready;
            data_ready_d[1] <= data_ready_d[0];
            if (data_ready) begin
                phrase_word_cnt <= phrase_word_cnt + 1'b1;
                if (phrase_word_cnt == settings.phrase_size - 1'b1) begin
                    phrase_word_cnt <= 5'd0;
                end
                if (phrase_word_cnt == '0) begin
                    phrase_start <= 1'b1;
                end
                service <= 1'b0;
            end
            
            new_group <= 1'b0;
            if (phrase_start) begin
                phrase_even <= ~phrase_even;
                if (!phrase_even) begin
                    if (phrase_pair_cnt == '0) begin
                        new_group <= 1'b1;
                    end                                     
                end
                else begin
                    phrase_pair_cnt <= phrase_pair_cnt + 1'b1;    
                    if (phrase_pair_cnt == 6'd63) begin
                        group_cnt <= group_cnt + 1'b1;
                    end
                    service <= group[6'd63 - phrase_pair_cnt];
                    // $display("Service: 1'b%b", service);
                    // $display("Service group: 1'b%b", group[6'd63 - phrase_pair_cnt]);
                    // $display("phrase_pair_cnt: 6'b%b", phrase_pair_cnt);
                end
            end
        end
    end
    

    always_ff @(posedge clk or negedge nrst) begin
		if (!nrst) begin
            group <= 64'd0;
        end
        else begin
            if (new_group) begin // Синхронизация служебной информации по началу новой группы
                group[63:57] <= group_cnt;
                group[56] <= 1'b0;
                
                if (group_cnt == 7'd0)
                    group[56] <= FRAME_LABEL;
                    
                group[55:24] <= 32'd0;
                group[23:8] <= 16'd0;   
              
                group[7:0] <= GROUP_LABEL;
                case (group_cnt[4:0])
                    5'd0: begin
                        group[55:24] <= orbit_service[127:96];
                    end
                    5'd1: begin 
                        group[55:24] <= orbit_service[95:64];
                    end
                    5'd2: begin
                        group[55:24] <= orbit_service[63:32];
                    end
                    5'd3: begin
                        group[55:24] <= orbit_service[31:0];
                    end
                    5'd31: begin
                        group[7:0] <= CYCLE_LABEL;
                    end
                endcase
            end
        end
    end

    larva_orbit_inserter larvaOrbitInserter(
        .clk(clk),
        .nrst(nrst),
        .bit_repeat(settings.bit_clocks_size),
        .phrase_size(settings.phrase_size),
        .ena(output_ena),        
        .data_ready(data_ready_d[1]),
        .data({service, data}),   
        .orbit(orbit));
endmodule