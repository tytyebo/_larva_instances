`include "defines.svh"
`include "interfaces.svh"
module larva_orbit_sequencer(input bit clk, nrst, // clk is 12.582912 MHz
        input [5:0] bit_repeat, 
        input logic [15:0] cycle_size,
        input fifo_wrq,
        input [15:0] fifo_wdata,
        input logic ena,
        output logic [1:0] data_missed,
        orbit_io.out orbit);
    
    logic next_bit, marker_zero, clko_rise, clko_down, video_ena, data_chooser;
    logic [5:0] next_bit_cnt, insert_value;
    logic [15:0] orbit_word;
    logic [1:0][15:0] data;
    logic [1:0] data_filled;
    logic [3:0] bit_cnt;
    logic data_switcher, word_sent, insert_marker;
    assign insert_value = ((bit_repeat[5:1]) + 1'b1);
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            next_bit <= 1'b0;
            next_bit_cnt <= 6'd0;
            marker_zero <= 1'b0;
            clko_rise <= 1'b0; 
            clko_down <= 1'b0;
            video_ena <= 1'b0;
            orbit_word <= '0;
            data_missed <= '0;
            data <= '0;
            data_filled <= '0;
            data_switcher <= 1'b0;
        end
        else begin
            if (fifo_wrq) begin 
                if (!data_switcher) begin
                    data[0] <= fifo_wdata;
                    data_filled[0] <= 1'b1;
                end
                else begin
                    data[1] <= fifo_wdata;
                    data_filled[1] <= 1'b1;
                end
                data_switcher <= ~data_switcher;
            end
            
            next_bit <= 1'b0;   
            marker_zero <= 1'b0;
            clko_rise <= 1'b0; 
            clko_down <= 1'b0;
            if (ena) begin            
                if (next_bit_cnt == 6'd0) begin
                    if (video_ena) begin
                        next_bit <= 1'b1;
                        if (bit_cnt == '0) begin
                            if (data_chooser) begin
                                orbit_word <= data[1];
                                data_filled[1] <= 1'b0;
                                if (!data_filled[1]) begin
                                    data_missed[1] <= 1'b1;
                                end
                            end
                            else begin
                                orbit_word <= data[0];
                                data_filled[0] <= 1'b0;
                                if (!data_filled[0]) begin
                                    data_missed[0] <= 1'b1;
                                end
                            end
                        end
                    end
                    clko_down <= 1'b1;
                end

                next_bit_cnt <= next_bit_cnt + 1'b1;
                if (next_bit_cnt == bit_repeat) begin
                    next_bit_cnt <= 6'd0;
                    video_ena <= 1'b1;
                end
                
                if (next_bit_cnt == bit_repeat[5:1] + 1'b1) begin
                    clko_rise <= 1'b1;
                end
                
                if (next_bit_cnt == insert_value) begin
                    if (video_ena) begin
                        marker_zero <= 1'b1;
                    end
                end
            end
        end
    end

        
    logic phrase_marker, marker_sent;
    assign insert_marker = orbit_word[12];
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            bit_cnt <= 4'd0;
            phrase_marker <= 1'b0;
            orbit.orbit <= 1'b0;
            marker_sent <= 1'b0;
            data_chooser <= 1'b0;
            word_sent <= 1'b0;
        end
        else begin
            word_sent <= 1'b0;
            if (next_bit) begin
                bit_cnt <= bit_cnt + 1'b1;
                orbit.orbit <= orbit_word[4'd11 - bit_cnt];
                
                if (insert_marker && !marker_sent) begin
                    orbit.orbit <= 1'b1;
                    phrase_marker <= 1'b1;
                end
            end

            if (marker_zero && insert_marker) begin
                if (phrase_marker) begin
                    phrase_marker <= 1'b0;
                    orbit.orbit <= 1'b0;
                    marker_sent <= 1'b1;
                end
            end

            if (bit_cnt == 4'd12) begin
                bit_cnt <= 4'd0;
                word_sent <= 1'b1;
                data_chooser <= ~data_chooser;
            end

            if (word_sent)
                marker_sent <= 1'b0;
        end
    end
    
    logic [3:0] strobe_w_cnt;
    logic [15:0] strobe_c_cnt;
    logic strobe_ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            strobe_ena <= 1'b0;
            orbit.strobe_w <= 1'b0;
            orbit.strobe_c <= 1'b0;
            orbit.clock_orb <= 1'b0;
            strobe_w_cnt <= '0;
            strobe_c_cnt <= '0;
        end
        else begin               
            if (clko_rise) begin
                orbit.clock_orb <= 1'b1;
                strobe_w_cnt <= strobe_w_cnt + 1'b1;
                if (!strobe_ena) begin
                    strobe_ena <= 1'b1;
                    orbit.strobe_w <= 1'b1;
                end
                
                if (strobe_w_cnt == 4'd0 && strobe_ena) begin
                    orbit.strobe_w <= 1'b1;
                end
                if (strobe_w_cnt == 4'd6) begin
                    orbit.strobe_w <= 1'b0;
                end
                if (strobe_w_cnt == 4'd11) begin
                    strobe_w_cnt <= '0;
                end
            
            end
            if (clko_down) begin
                orbit.clock_orb <= 1'b0;
            end
            if (strobe_ena) begin
                if (word_sent) begin
                    strobe_c_cnt <= strobe_c_cnt + 1'b1;
                    if (strobe_c_cnt == cycle_size) begin
                        strobe_c_cnt <= '0;
                    end
                end
                if (strobe_c_cnt == '0 && strobe_w_cnt == 12'd1 && clko_down) begin
                    orbit.strobe_c <= 1'b1;
                end
                if (strobe_w_cnt == 12'd7 && clko_down) begin
                    orbit.strobe_c <= 1'b0;
                end
            end
        end
    end
endmodule: larva_orbit_sequencer
    