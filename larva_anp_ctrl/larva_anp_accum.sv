/*
 * Модуль реализующий скользящее среднее для проекта larva_analog.
 * LENGTH - длина сдвигового регистра для скользяшего среднего;
 * DATA_WIDTH - разрядность входных данных для сдвигового регистра.
 * До полного заполнения сдвигового регистра выход ready будет в неактивном состоянии 0.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */

module larva_anp_accum#(parameter int LENGTH = 32, DATA_WIDTH = 16)
                       (input bit clk, nrst,
                        input logic clear, we,
                        input logic [DATA_WIDTH - 1:0] data_in,
                        output logic [$clog2(LENGTH) + DATA_WIDTH - 1:0] data_out,
                        output logic ready);
                                              
    logic [LENGTH - 1:0][DATA_WIDTH - 1:0] shift_reg;
    logic [$clog2(LENGTH) - 1:0] init_valid_cnt;
    logic init_valid;
    always_ff @(posedge clk or negedge nrst) begin
		if (!nrst) begin				
            shift_reg <= '0;
            init_valid_cnt <= '0;
            init_valid <= 1'b0;
            data_out <= '0;
        end
		else begin
            if (clear) begin
                shift_reg <= '0;
                init_valid_cnt <= '0;
                init_valid <= 1'b0;
                data_out <= '0;
            end
            else begin
                if (we) begin
                    shift_reg[0] <= data_in;
                    for (int i = LENGTH - 1; i > 0; i--) begin
                        shift_reg[i] <= shift_reg[i - 1];
                    end
                    data_out <= data_out + data_in - shift_reg[LENGTH - 1];
                    // $display("@%0t: %m init_valid_cnt %d data_in %h", $time, init_valid_cnt, data_in);                
                    if (init_valid_cnt != '1) begin
                        init_valid_cnt <= init_valid_cnt + 1'b1;
                    end
                    else begin
                        init_valid <= 1'b1;
                    end
                end
            end
        end
    end 
    assign ready = ~we & init_valid;
endmodule: larva_anp_accum