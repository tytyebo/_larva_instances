`include "interfaces.svh"
`include "defines.svh"
module larva_anp_ctrl(input bit clk, nrst,
                      input bit [7:0] base_adr,
                      input bit [7:0] speed_ch_a, // 0 - 120 Гц
                      input bit [15:0] speed_ch_b,
                      input bit [15:0] speed_ch_c,
                      // internal bus side
                      generic_bus_io.slave_hndshk data_bus,
                      anp_io.master anp);
    typedef enum bit [1:0] {ST_ADC_DELAY, ST_ADC_SKIP, ST_ADC_ACCUM, ST_SWITCH_ON} state_e;
    state_e state;
    
    // ((2^22 * 3) / 32) / 8 / 192 = 256 Гц
    localparam  ADC_SKIP_CNT = 60 + 64 + 32, // (156)
                ADC_ACCUM_CNT = 32,
                ADC_ACCUM_CNT_WIDTH = 5,                
                DELAY_MUX2ADC_CNT = 8,
                DELAY_SWITCH2ADC_CNT = 32 * 4 - DELAY_MUX2ADC_CNT,
                F120_CIC_LEN = 2,
                F15_CIC_LEN = 8,
                OVERLOAD_LEN = 8;

    localparam bit [11:0] OVERLOAD_LOW_LEVEL = 12'h004, 
                          OVERLOAD_HIGH_LEVEL = 12'hFDF;
    logic adc_start;
    logic phy_busy_a, phy_busy_b, phy_busy_c;
    logic [4:0][11:0] adc_data; // 0 - a0
                               // 1 - b0 
                               // 2 - b1 
                               // 3 - c0 
                               // 4 - c1                            
    logic [$clog2(DELAY_MUX2ADC_CNT) - 1:0] mux2adc_cnt;
    logic [$clog2(DELAY_SWITCH2ADC_CNT) - 1:0] switch2adc_cnt;

    logic [$clog2(ADC_SKIP_CNT) - 1:0] adc_skip_cnt;
    logic [$clog2(ADC_ACCUM_CNT) - 1:0] adc_accum_cnt;

    logic [4:0][7:0][11 + $clog2(ADC_ACCUM_CNT):0] data_acc32;

    logic [4:0] adc_overhigh, adc_overlow, overhigh_ready, overlow_ready;
    logic [4:0][$clog2(OVERLOAD_LEN) + 11:0] adc_overhigh_acc, adc_overlow_acc;
    
    logic switch_ready;
    logic [4:0] anp_disch;
    assign anp.ndisch = { 1'b0, ~anp_disch};
    logic mux_ready;
    logic [7:0] adc_result_done; // 7..0 - adc_a, 23.. 8 - adc_b, 39..24 - adc_c
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            anp.mux <= '0;
            mux2adc_cnt <= '0;
            switch2adc_cnt <= '0;
            
            adc_skip_cnt <= '0;
            adc_accum_cnt <= '0;
            
            state <= ST_ADC_DELAY;
            data_acc32 <= '0;
            adc_start <= 1'b0;

            adc_overhigh <= '0;
            adc_overlow <= '0;
            anp_disch <= '0;
                      
            switch_ready <= 1'b0;
            mux_ready <= 1'b0;
            adc_result_done <= '0;
        end
        else begin
            adc_result_done <= '0;
            adc_start <= 1'b0;
            case (state)
                // ожидание заряда тракта на входе АЦП, после запуск преобразования
                // 8 тактов
                ST_ADC_DELAY: begin
                    mux2adc_cnt <= mux2adc_cnt + 1'b1;
                    if (mux2adc_cnt == DELAY_MUX2ADC_CNT - 1'b1) begin
                        adc_start <= 1'b1;
                        state <= ST_ADC_SKIP;
                        mux2adc_cnt <= '0;
                    end                           
                    data_acc32 <= '0;
                    adc_overhigh <= '0;
                    adc_overlow <= '0;                    
                end
                // Пропуск первых 156 преобразований АЦП
                // 156 * 32 тактов
                ST_ADC_SKIP: begin
                    if (!phy_busy_a && !phy_busy_b && !phy_busy_c && !adc_start) begin
                        adc_skip_cnt <= adc_skip_cnt + 1'b1;
                        adc_start <= 1'b1;
                        
                        if (adc_skip_cnt == ADC_SKIP_CNT - 1'b1) begin
                            state <= ST_ADC_ACCUM;
                            adc_skip_cnt <= '0;
                        end
                        
                        for (int i = 0; i < 5; i = i + 1) begin // контроль обрыва и перегрузки канала                                       
                            if ((adc_overhigh_acc[i][$clog2(OVERLOAD_LEN) + 11:$clog2(OVERLOAD_LEN)] >= OVERLOAD_HIGH_LEVEL) && overhigh_ready[i]) begin
                                adc_overhigh[i] <= 1'b1;
                                anp_disch[i] <= 1'b1;
                            end
                                
                            if ((adc_overlow_acc[i][$clog2(OVERLOAD_LEN) + 11:$clog2(OVERLOAD_LEN)]  <= OVERLOAD_LOW_LEVEL) && overlow_ready[i] && (adc_skip_cnt > 5'd16)) begin // (adc_skip_cnt > 5'd16) - подстройка под переходной процесс аналоговой схемы
                                adc_overlow[i] <= 1'b1;
                                anp_disch[i] <= 1'b1;
                            end
                        end
                    end
                end
                // Накопление 32х преобразований АЦП
                // 32 * 32 тактов
                ST_ADC_ACCUM: begin 
                    if (!phy_busy_a && !phy_busy_b && !phy_busy_c && !adc_start) begin
                        adc_accum_cnt <= adc_accum_cnt + 1'b1;
                        adc_start <= 1'b1;
                    
                        if (adc_accum_cnt == ADC_ACCUM_CNT - 1'b1) begin
                            state <= ST_SWITCH_ON;    
                            adc_accum_cnt <= '0;
                            adc_start <= 1'b0;
                            adc_result_done[anp.mux] <= 1'b1;                               
                        end
                        
                        for (int i = 0; i < 5; i++) begin
                            data_acc32[i][anp.mux] <= data_acc32[i][anp.mux] + adc_data[i];
                            if ((adc_overhigh_acc[i][$clog2(OVERLOAD_LEN) + 11:$clog2(OVERLOAD_LEN)] >= OVERLOAD_HIGH_LEVEL) && overhigh_ready[i]) begin                            
                                adc_overhigh[i] <= 1'b1;
                                anp_disch[i] <= 1'b1;
                            end
                                
                            if ((adc_overlow_acc[i][$clog2(OVERLOAD_LEN) + 11:$clog2(OVERLOAD_LEN)]  <= OVERLOAD_LOW_LEVEL) && overlow_ready[i]) begin
                                    adc_overlow[i] <= 1'b1;
                                    anp_disch[i] <= 1'b1;
                            end
                        end
                    end
                end
                ST_SWITCH_ON: begin // включение ключей для зазряда канала 
                    if (!switch_ready) begin
                        anp_disch <= '1;
                    end
                    
                    switch2adc_cnt <= switch2adc_cnt + 1'b1;
                    if (switch2adc_cnt == (DELAY_SWITCH2ADC_CNT - 1'b1) && !switch_ready) begin                             
                        anp_disch <= '0;					
                        switch_ready <= 1'b1;
                    end 
                    
                    mux2adc_cnt <= mux2adc_cnt + 1'b1;
                    if (mux2adc_cnt == (DELAY_MUX2ADC_CNT - 1'b1) && !mux_ready) begin                            
                        anp.mux <= anp.mux + 1'b1;
                        mux_ready <= 1'b1;
                    end
                    
                    if (switch_ready & mux_ready) begin                            
                        state <= ST_ADC_DELAY;
                        switch2adc_cnt <= '0;
                        switch_ready <= 1'b0;
                        mux_ready <= 1'b0;
                    end
                end
            endcase
        end
    end

    logic [4:0][7:0][31:0] overhigh_f120, overlow_f120;
    logic [4:0][7:0][7:0] overhigh_f15, overlow_f15;
    logic accum_result_done;
    logic [2:0] mux_saved;       
    logic [4:0][7:0] valid_f120, valid_f15;
    logic [4:0][7:0] is_overhigh_f120, is_overlow_f120, is_overhigh_f15, is_overlow_f15;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            for (int i = 0; i < 8; i++) begin
                for (int j = 0; j < 5; j++) begin
                    overhigh_f120[j][i] <= '0;
                    overlow_f120[j][i] <= '0;
                    overhigh_f15[j][i] <= '0;
                    overlow_f15[j][i] <= '0;   
                end
            end
            accum_result_done <= 1'b0;
            mux_saved <= '0;
        end
        else begin
            accum_result_done <= |adc_result_done;
            
            for (int i = 0; i < 8; i++) begin
                if (adc_result_done[i]) begin // Происходит одновременно для всех каналов
                    for (int j = 0; j < 5; j++) begin						
                        mux_saved <= anp.mux; // поэтому 1 раз сохраняем anp.mux
                        overhigh_f120[j][i] <= { overhigh_f120[j][i][F120_CIC_LEN - 2:0], adc_overhigh[j] };
                        overlow_f120[j][i] <= { overlow_f120[j][i][F120_CIC_LEN - 2:0], adc_overlow[j] };
                        overhigh_f15[j][i] <= { overhigh_f15[j][i][F15_CIC_LEN - 2:0], is_overhigh_f120[j][i] };
                        overlow_f15[j][i] <= { overlow_f15[j][i][F15_CIC_LEN - 2:0], is_overlow_f120[j][i] };					
                    end
                end
            end
        end
    end
    
    logic [4:0][7:0][$clog2(F120_CIC_LEN) + 17 - 1:0] data_f120;
    logic [4:0][7:0][$clog2(F15_CIC_LEN) + $clog2(F120_CIC_LEN) + 17 - 1:0] data_f15;
    logic [4:0][7:0] ena_f15;
    genvar k, l;
    generate 
        begin: generate_accums
            for (l = 0; l < 5; l++) begin: gen_l
                for (k = 0; k < 8; k++) begin: gen_k
                    larva_anp_accum#(.LENGTH(F120_CIC_LEN), .DATA_WIDTH($bits(data_acc32[l][k])))
                    larvaAnpAccum_f120(.clk(clk), .nrst(nrst), .clear(1'b0), .we(adc_result_done[k]), .data_in(data_acc32[l][k]), .data_out(data_f120[l][k]), .ready(valid_f120[l][k]));
                    
                    larva_anp_accum#(.LENGTH(F15_CIC_LEN), .DATA_WIDTH($clog2(F120_CIC_LEN) + $bits(data_acc32[l][k])))
                    larvaAnpAccum_f15(.clk(clk), .nrst(nrst), .clear(1'b0), .we(adc_result_done[k] & ena_f15[l][k]), .data_in(data_f120[l][k]), .data_out(data_f15[l][k]), .ready(valid_f15[l][k]));
                    
                    always_ff @(posedge clk or negedge nrst) begin
                        if (!nrst) begin
                            ena_f15[l][k] <= 1'b0;
                        end
                        else begin
                            if (valid_f120[l][k]) begin
                                ena_f15[l][k] <= 1'b1;
                            end
                        end
                    end
                end
                
                larva_anp_accum#(.LENGTH(OVERLOAD_LEN), .DATA_WIDTH($bits(adc_data[l])))
                larvaAnpAccum_overhigh(.clk(clk), .nrst(nrst), .clear(anp_disch[l]), .we(adc_start), .data_in(adc_data[l]), .data_out(adc_overhigh_acc[l]), .ready(overhigh_ready[l]));
                
                larva_anp_accum#(.LENGTH(OVERLOAD_LEN), .DATA_WIDTH($bits(adc_data[l])))
                larvaAnpAccum_overlow(.clk(clk), .nrst(nrst), .clear(anp_disch[l]), .we(adc_start), .data_in(adc_data[l]), .data_out(adc_overlow_acc[l]), .ready(overlow_ready[l]));
            end
        end
    endgenerate

    generate
        begin: generate_assigns
            for (l = 0; l < 5; l++) begin: gen_l
                for (k = 0; k < 8; k++) begin: gen_k 
                    assign is_overhigh_f120[l][k] = |overhigh_f120[l][k];
                    assign is_overlow_f120[l][k] = |overlow_f120[l][k];
                    assign is_overhigh_f15[l][k] = |overhigh_f15[l][k];
                    assign is_overlow_f15[l][k] = |overlow_f15[l][k];
                end
            end
        end
    endgenerate

    logic [4:0][7:0][15:0] result_f120, result_f15;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            result_f120 <= '0;
            result_f15 <= '0;
        end
        else begin
            if (accum_result_done) begin
                for (int i = 0; i < 5; i++) begin
					if (valid_f120[i][mux_saved]) begin
						if (is_overhigh_f120[i][mux_saved] == 1'b1) begin
							result_f120[i][mux_saved] <= '1;
						end
						else begin
							if (is_overlow_f120[i][mux_saved] == 1'b1) begin
								result_f120[i][mux_saved] <= '0;
							end
							else begin
								result_f120[i][mux_saved] <= data_f120[i][mux_saved] >> 2; // 2 is the right value.
							end
						end
					end
					else begin
						result_f120[i][mux_saved] <= '0;
					end
					
					if (valid_f15[i][mux_saved] && valid_f120[i][mux_saved]) begin
						if (is_overhigh_f15[i][mux_saved] == 1'b1) begin
							result_f15[i][mux_saved] <= '1;
						end
						else begin
							if (is_overlow_f15[i][mux_saved] == 1'b1) begin
								result_f15[i][mux_saved] <= '0;
							end
							else begin
								result_f15[i][mux_saved] <= data_f15[i][mux_saved] >> 5;
							end
						end
					end
					else begin
						result_f15[i][mux_saved] <= '0;
					end
				end
			end
        end
    end
    
    logic [39:0][9:0] debug_cnt;
    bit [5:0] init_cnt;
    logic init_debug;
    logic [1:0] ctrl_mode;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            data_bus.dat_miso <= '0;
            data_bus.ready <= 1'b0;
            ctrl_mode <= '0;
            init_cnt = '0;
            debug_cnt <= '0;
            init_debug <= 1'b0;
        end
        else begin
            data_bus.ready <= 1'b0;
            if (data_bus.valid && !data_bus.ready) begin
                data_bus.ready <= 1'b1;
                if (!data_bus.we) begin
                    unique case (ctrl_mode)
                        2'd0: begin
                            if (data_bus.adr >= base_adr && data_bus.adr < base_adr + 8'd8) begin
                                if (speed_ch_a[data_bus.adr - base_adr]) begin
                                    data_bus.dat_miso <= result_f15[0][data_bus.adr - base_adr];
                                end
                                else begin
                                    data_bus.dat_miso <= result_f120[0][data_bus.adr - base_adr];
                                end
                            end
                            
                            if (data_bus.adr >= base_adr + 8'd8 && data_bus.adr < base_adr + 8'd16) begin
                                if (speed_ch_b[data_bus.adr - base_adr - 8'd8]) begin
                                    data_bus.dat_miso <= result_f15[1][data_bus.adr - base_adr - 8'd8];
                                end
                                else begin
                                    data_bus.dat_miso <= result_f120[1][data_bus.adr - base_adr - 8'd8];
                                end
                            end
                            
                            if (data_bus.adr >= base_adr + 8'd16 && data_bus.adr < base_adr + 8'd24) begin
                                if (speed_ch_b[data_bus.adr - base_adr - 8'd16])
                                    data_bus.dat_miso <= result_f15[2][data_bus.adr - base_adr - 8'd16];
                                else
                                    data_bus.dat_miso <= result_f120[2][data_bus.adr - base_adr - 8'd16];
                            end
                            
                            if (data_bus.adr >= base_adr + 8'd24 && data_bus.adr < base_adr + 8'd32) begin
                                if (speed_ch_c[data_bus.adr - base_adr - 8'd24]) begin
                                    data_bus.dat_miso <= result_f15[3][data_bus.adr - base_adr - 8'd24];
                                end
                                else begin
                                    data_bus.dat_miso <=  result_f120[3][data_bus.adr - base_adr - 8'd24];
                                end
                            end
                            
                            if (data_bus.adr >= base_adr + 8'd32 && data_bus.adr < base_adr + 8'd40) begin
                                if (speed_ch_c[data_bus.adr - base_adr - 8'd32]) begin
                                    data_bus.dat_miso <= result_f15[4][data_bus.adr - base_adr - 8'd32];
                                end
                                else begin
                                    data_bus.dat_miso <= result_f120[4][data_bus.adr - base_adr - 8'd32];
                                end
                            end
                            
                            if (data_bus.adr < base_adr || data_bus.adr >= base_adr + 8'd40) begin
                                data_bus.dat_miso <= '0;
                                data_bus.ready <= 1'b0;
                            end
                        end
                        2'd1: begin
                            if (data_bus.adr >= base_adr && data_bus.adr < base_adr + 8'd40) begin
                                data_bus.dat_miso <= '0;
                            end
                            else begin
                                data_bus.dat_miso <= '0;
                                data_bus.ready <= 1'b0;
                            end
                        end
                        2'd2: begin
                            if (data_bus.adr >= base_adr && data_bus.adr < base_adr + 8'd40) begin
                                data_bus.dat_miso <= '1;
                            end
                            else begin
                                data_bus.dat_miso <= '0;
                                data_bus.ready <= 1'b0;
                            end
                        end
                        2'd3: begin
                            if (data_bus.adr >= base_adr && data_bus.adr < base_adr + 8'd40) begin
                                data_bus.dat_miso <= debug_cnt[data_bus.adr - base_adr];
                            end
                            else begin
                                data_bus.dat_miso <= '0;
                                data_bus.ready <= 1'b0;
                            end
                        end
                    endcase
                end
                else begin
                    if (data_bus.adr == base_adr) begin
                        ctrl_mode <= data_bus.dat_mosi[1:0];
                        if (!init_debug) begin
                            for (int i = 0; i < 40; i++) begin
                                debug_cnt[i] <= base_adr + init_cnt;
                                init_cnt++;
                            end
                            init_debug <= 1'b1;
                        end
                    end
                end
            end   
        end
    end
    
    larva_ad7476_phy ad7476Phy0(.clk(clk), .nrst(nrst), .read(adc_start), .busy(phy_busy_a),                           
                                .data0(adc_data[0]), // a0
                                .data1(), // not connected
                                .sclk(anp.sck[0]),
                                .sdata0(anp.miso[0]),
                                .sdata1(1'b0),
                                .ncs(anp.ncs[0]));
    
    larva_ad7476_phy ad7476Phy1(.clk(clk), .nrst(nrst), .read(adc_start), .busy(phy_busy_b),
                                .data0(adc_data[1]),
                                .data1(adc_data[2]),        
                                .sclk(anp.sck[1]),
                                .sdata0(anp.miso[1]), // b0
                                .sdata1(anp.miso[2]), // b1
                                .ncs(anp.ncs[1]));
    
    larva_ad7476_phy ad7476Phy2(.clk(clk), .nrst(nrst), .read(adc_start), .busy(phy_busy_c),
                                .data0(adc_data[3]),
                                .data1(adc_data[4]),        
                                .sclk(anp.sck[2]),
                                .sdata0(anp.miso[3]), // c0
                                .sdata1(anp.miso[4]), // c1
                                .ncs(anp.ncs[2]));
endmodule: larva_anp_ctrl