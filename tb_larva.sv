`include "defines.svh"
`include "interfaces.svh"
// 12.582912 MHz

import defines_pkg::*;

module tb_larva();
    //-------------------------------------------------------------------------------------//
    //--------------------------------- Global things -------------------------------------//
    //-------------------------------------------------------------------------------------//
    bit clk, clk_uart, mil_clk;
    logic nrst;
    always #20.345 clk = ~clk; // clk is 24.576 i.e. there is 40.69 ns period
    always #39.736 clk_uart = ~clk_uart; 
    always #20.833 mil_clk = ~mil_clk; // 24 MHz

    initial begin
        nrst <= 0;
		wb_reset();
        #406.901
        nrst <= 1;            
    end  
//-------------------------------------------------------------------------------------//
//----------------------------------- PC side -----------------------------------------//
//-------------------------------------------------------------------------------------//
    logic uart_rx, uart_tx;
    logic [7:0] pc_tx_data = 8'd0;
    logic pc_tx_start = 1'b0;
    logic pc_uart_busy;
        
    task pc_wr_byte(input [7:0] wr_byte);
        pc_tx_data <= wr_byte;
        @(posedge clk_uart) pc_tx_start <= 1'b1; 
        @(posedge clk_uart) pc_tx_start <= 1'b0;
        @(posedge clk_uart);
        do
            @(posedge clk_uart);
        while (pc_uart_busy);     
    endtask
    
	
	wishbone_io tb_wbm();
	task wb_reset()
		tb_wbm.wbm_we <= 1'b0;
        tb_wbm.wbm_stb <= 1'b0;
        tb_wbm.wbm_cyc <= 1'b0;
        tb_wbm.wbm_sel <= '0;
        tb_wbm.wbm_adr <= '0;
        tb_wbm.wbm_dat <= '0;
	endtask: wb_reset
	
	task wb_write(input bit [15:0] adr, bit [31:0] data, bit [3:0] sel);
        while (tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_we <= 1'b1;
        tb_wbm.wbm_stb <= 1'b1;
        tb_wbm.wbm_cyc <= 1'b1;
        tb_wbm.wbm_sel <= sel;
        tb_wbm.wbm_adr <= adr;
        tb_wbm.wbm_dat <= data;
        while (!tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_stb <= 1'b0;
        tb_wbm.wbm_cyc <= 1'b0;
    endtask: wb_write

    task wb_read(input bit [15:0] adr, bit [3:0] sel);
        while (tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wbm.wbm_we <= 1'b0;
        tb_wbm.wbm_stb <= 1'b1;
        tb_wbm.wbm_cyc <= 1'b1;
        tb_wbm.wbm_sel <= sel;
        tb_wbm.wbm_adr <= adr;
        while (!tb_wbm.wbs_ack) begin
            @(posedge clk);
        end
        tb_wb_data <= tb_wbm.wbs_dat;
        tb_wbm.wbm_stb <= 1'b0;
        tb_wbm.wbm_cyc <= 1'b0;
    endtask: wb_read
	
    logic [7:0] pc_rx_data;
    logic pc_rx_ready;
    

    // MIL-STD-1553
    milStd_io tb_mil_tx();
    logic tb_mil_dr, tb_mil_busy;
    milTxWord_s tb_mil_data;
    mil_tx_phy tbMilTxPhy(
        .nrst(nrst),
        .clk(mil_clk),
        .mil(tb_mil_tx),
        .mil_data(tb_mil_data),
        .data_ready(tb_mil_dr),
        .busy(tb_mil_busy));
                   
    task mil_send_word(input milTxWord_s mil_data); // needed 24 MHz clock frequency
        if (tb_mil_busy) begin
            @(negedge tb_mil_busy);
        end
        tb_mil_dr <= 1'b1;
        tb_mil_data <= mil_data;
        @(posedge mil_clk);
        tb_mil_dr <= 1'b0;
        @(posedge mil_clk);
    endtask: mil_send_word
    
    // Main initial
    initial begin
        automatic milCmdWord_s mil_cmd_word;
        @(posedge nrst)
        #20000
        pc_wr_byte(8'h00); // Проверка тестовых регистров uart
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h03);
        pc_wr_byte(8'h02);         
        pc_wr_byte(8'h92);
        pc_wr_byte(8'h03);
        
        pc_wr_byte(8'h13); // Чтение тестового регистра 16'h55AA в модуле Analog;
        pc_wr_byte(8'h00);
        pc_wr_byte(8'h00);
        
        pc_wr_byte(8'h13); // Чтение тестового регистра 16'hAA55 в модуле Analog;
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h00);

        pc_wr_byte(8'h12); // Запись тестового регистра в модуле Analog;
        pc_wr_byte(8'h02);
        pc_wr_byte(8'h00);
        pc_wr_byte(8'h65);
        pc_wr_byte(8'h78);

        pc_wr_byte(8'h13); // Чтение предыдущего записанного тестового регистра в модуле Analog;
        pc_wr_byte(8'h02);
        pc_wr_byte(8'h00);

        pc_wr_byte(8'h13); // Чтение аналоговых датчиков (2 байта)
        pc_wr_byte(8'h04);
        pc_wr_byte(8'h00);
        
        /*pc_wr_byte(8'h13); // Чтение датчиков температуры
        pc_wr_byte(8'h05);
        pc_wr_byte(8'h00);
        pc_wr_byte(8'h13);
        pc_wr_byte(8'h06);
        pc_wr_byte(8'h00);*/
        
        pc_wr_byte(8'h10); // Записать 1 байт: Настройки mil_responder. Выбор resp_ena = 1'b1 и resp_addr = 5'd0
        pc_wr_byte(8'h00); //
        pc_wr_byte(8'h20); // Адрес 0x2000
        pc_wr_byte(8'h20);

        // Отправка данных в МКИО
        mil_cmd_word = '{addr:5'd0, dir:RECEIVE, sub_addr:5'd0,words_cnt:5'd1};
        mil_send_word('{\type :MIL_CMD, data:mil_cmd_word});
        mil_send_word('{\type :MIL_DATA, data:16'h1234});
        
        @(negedge tb_mil_busy);
        
        pc_wr_byte(8'h13); // Чтение данных из МКИО;
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h20); // Адрес 0x2001
        
        pc_wr_byte(8'h13); // Чтение данных из МКИО;
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h20); // Адрес 0x2001
        
        pc_wr_byte(8'h13); // Чтение данных из МКИО;
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h20); // Адрес 0x2001
        
        pc_wr_byte(8'h13); // Чтение данных из МКИО;
        pc_wr_byte(8'h01);
        pc_wr_byte(8'h20); // Адрес 0x2001
        
        /*pc_wr_byte(8'h16); // Записать 4 байта в память: debug_adr
        pc_wr_byte(8'h01); //
        pc_wr_byte(8'h50); // Адрес 0x5001
        pc_wr_byte(8'h04);
        pc_wr_byte(8'h03);
        pc_wr_byte(8'h02);
        pc_wr_byte(8'h01);
        
        pc_wr_byte(8'h12); // Записать 2 байта в память: debug_data
        pc_wr_byte(8'h02); //
        pc_wr_byte(8'h50); // Адрес 0x5001
        pc_wr_byte(8'h89);
        pc_wr_byte(8'hA5);

        pc_wr_byte(8'h13); // Чтение 2 байта в память: debug_data
        pc_wr_byte(8'h02); //
        pc_wr_byte(8'h50); // Адрес 0x5001   */  
		
		/* pc_wr_byte(8'h12); // Записать 2 байта: Настройки telemeter. Выбор mode = 4'b0000 и enable = 1'b1
        pc_wr_byte(8'h00); //
        pc_wr_byte(8'h10); // Адрес 0x1000
        pc_wr_byte(8'h01); // Данные 0x0001
        pc_wr_byte(8'h00); */
    end

	uart_ctrl#(.FREQ(24), .RATE(12), .MAJ_DIV(1))
    uart_pc(
        .clock(mil_clk),
        .reset_n(nrst),
        .data_tx(pc_tx_data),
        .data_recieved(pc_rx_data),
        .trans_en(pc_tx_start),
        .rec_succ(pc_rx_ready),
        .error(),
        .uart_busy(pc_uart_busy),
        .rxd(uart_rx),
        .txd(uart_tx),
        .state_rx());
        	
    sdram_io #(.ADR_WIDTH(13), .DATA_WIDTH(16)) sdram();
    logic [12:0] sdram_a;
    wire [15:0] sdram_dq;
    logic [1:0] sdram_ba;
    logic sdram_clk, sdram_cke, sdram_cs_n, sdram_ras_n, sdram_cas_n, sdram_we_n, sdram_dqmh, sdram_dqml;
    sdr mem(
        .Dq(sdram_dq),
        .Addr(sdram_a),
        .Ba(sdram_ba),
        .Clk(sdram_clk),
        .Cke(sdram_cke),
        .Cs_n(sdram_cs_n),
        .Ras_n(sdram_ras_n),
        .Cas_n(sdram_cas_n),
        .We_n(sdram_we_n),
        .Dqm({sdram_dqmh, sdram_dqml}));
//-------------------------------------------------------------------------------------//
//----------------------------------- DEV side ----------------------------------------//
//-------------------------------------------------------------------------------------//
    logic debug_orbita_word_sent, debug_video;    
    wire [15:0] UPR_DATA;
    logic UPR_RD, UPR_WR, UPR_FLAG;
    logic orbita_clk;
    logic [15:0] debug;
    ft75_main larvaMain(
        // RAM pins
        .RAM_A(sdram_a),
        .RAM_DQ(sdram_dq),
        .RAM_BA(sdram_ba),
        .RAM_nCS(sdram_cs_n),
        .RAM_CLK(sdram_clk),
        .RAM_CKE(sdram_cke),
        .RAM_nWE(sdram_we_n),
        .RAM_nRAS(sdram_ras_n),
        .RAM_nCAS(sdram_cas_n),
        .RAM_DQMH(sdram_dqmh),
        .RAM_DQML(sdram_dqml),
        
        // MCU pins
        .A(16'd0),
        .D(8'd0),
        .MCU_nWR(1'bz),
        .MCU_nRD(1'bz),
        .MCU_ALE(1'bz),
        .MCU_nRST_C2CK(1'bz),
        .MCU_PO(4'd0),
        
        // BUS pins
        .UPR_DATA(UPR_DATA),
        .UPR_CLK(1'bz),
        .UPR_RD(UPR_RD),
        .UPR_WR(UPR_WR),
        .UPR_SGP1(1'bz),
        .UPR_FLAG(UPR_FLAG),
        .UPR_MISC(2'bzz),

        // MKO buf
        .MSP_nMSTCLR(),
        .MSP_TAG_CLK(),
        .MSP_STRBD(),
        .MSP_nSELECT(),
        .MSP_MEM_REG(),
        .MSP_RD_WR(),
        .MSP_CLK_IN(),
        .MSP_MEMENA_OUT(),
        .MSP_INCMD(),
        .MSP_IOEN(),
        .MSP_READYD(),
        .MSP_INT(),

        .RTAD(5'd0),
        .RTADP(1'bz),
        
        .CLK_24M576(clk),
        .CLK_24M(mil_clk),
        
        // Debug pins
        // .DEBUG(debug),
        
        // UART
        .MCU_RXD(uart_tx),
        .MCU_TXD(uart_rx),

        // RK pins
        .NOV(1'bz),
        .KOM1(1'bz),
        
        // MKO buf pins
        .MPK0_RX_P(tb_mil_tx.txp),
        .MPK0_RX_N(tb_mil_tx.txn),
        .MPK1_RX_P(1'bz),
        .MPK1_RX_N(1'bz),
        .MPK2_RX_P(1'bz),
        .MPK2_RX_N(1'bz),
        
        // TIMER pins
        .TMR_IO(1'bz),
        .TMR_SCLK(),
        .TMR_CE(),

        // TEMP pins
        .TEMP_SCL(),
        .TEMP_SDA(),

        // LDO RESET pin
        .nRES(1'b1),

        // VIDEO pins
        .VIDEO_MOD_A(debug_video),
        .VIDEO_MOD_B());    
    
    assign debug_orbita_word_sent = debug[1];
    assign orbita_clk = debug[0];
    
    logic act_adca_sdata_reg;    
    logic act_adca_sclk;
    logic act_adca_ncs;
    ft75_analog larvaAnalog(  
        // BUS pins
        .UPR_DATA(UPR_DATA), // adata
        .UPR_CLK(1'bz),
        .UPR_RD(UPR_RD),     // nwrs
        .UPR_WR(UPR_WR),     // nwrm
        .UPR_SGP1(1'bz),
        .UPR_FLAG(UPR_FLAG), // nale
        .UPR_MISC(2'bzz),
        
        .CLK_24M576(clk),
        
        // Debug pins
        .DEBUG(),

        // LDO RESET pin
        .nRES(1'b1),

        .BOARD_ID(1'bz),

        // ACT pins

        .ACT_MUXA_A(),
        .ACT_ADCA_SDATA(act_adca_sdata_reg),
        .ACT_ADCA_SCLK(act_adca_sclk),
        .ACT_ADCA_nCS(act_adca_ncs),
        .ACT_nDISCH_A(),

        .ACT_MUXB_A(),
        .ACT_ADCB_SDATA(2'bzz),
        .ACT_ADCB_SCLK(),
        .ACT_ADCB_nCS(),
        .ACT_nDISCH_B(),

        .SGP1_ACT(1'bz),
        .SGP2_ACT(1'bz),

        .ACT_MUXC_A(),
        .ACT_ADCC_SDATA(2'bzz),
        .ACT_ADCC_SCLK(),
        .ACT_ADCC_nCS(),
        .ACT_nDISCH_C());
        
    logic act_adca_sclk_reg;
    logic [15:0] adc_data;
    logic [3:0] adc_sdata_cnt;
    logic [12:0] adc_data_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (~nrst) begin
            act_adca_sclk_reg <= 1'b0;
            adc_sdata_cnt <= 4'd15;
            act_adca_sdata_reg <= 1'b0;
            adc_data_cnt <= 13'd0;
        end
        else begin
            act_adca_sclk_reg <= act_adca_sclk;
            if (~act_adca_ncs) begin
                if (act_adca_sclk_reg & ~act_adca_sclk & (adc_sdata_cnt != 4'd0)) begin
                        adc_sdata_cnt <= adc_sdata_cnt - 1'b1;
                        act_adca_sdata_reg <= adc_data[adc_sdata_cnt - 1];
                        if (adc_sdata_cnt == 4'd1) begin
                            adc_data_cnt <= adc_data_cnt + 1'b1;
                        end
                end
            end
            else begin
                adc_sdata_cnt <= 4'd15;
            end
                
            if (adc_data_cnt == 13'd6144)
                adc_data_cnt <= 13'd0;
        end
    end
    
    always_comb begin
        if (adc_data_cnt >= 13'd0 && adc_data_cnt < 13'd3072) begin
            adc_data <= 16'h0555;
        end 
        if (adc_data_cnt >= 13'd3072 && adc_data_cnt < 13'd6143) begin
            adc_data <= 16'h0777;
        end
        if (adc_data_cnt == 13'd6143) begin
            adc_data <= 16'h0000;
        end
    end
    
    logic [5:0] debug_serviceman_phrase_cnt;
    logic [6:0] debug_serviceman_group_cnt;
    logic [6:0] debug_group_number;
    logic [4:0] debug_serviceman_ddd_cnt;
    logic debug_sync_ready, debug_frame_label;
    logic debug_orbita_word_sent_d, debug_orbita_word_sent_2d, debug_orbita_word_sent_3d, debug_orbita_word_sent_4d;
    logic [15:0] debug_sync;
    logic [7:0] debug_group_label;
    logic [63:0] debug_group;
    logic debug_group_ready;
    logic [15:0] debug_local_time;
    logic [13:0] debug_second_shares;
    always_ff @(posedge orbita_clk or negedge nrst) begin
        if (~nrst) begin
            debug_serviceman_phrase_cnt <= 6'd0;
            debug_serviceman_group_cnt <= 7'd0;
            
            debug_group_number <= 7'd0;
            debug_sync_ready <= 1'b0;
            debug_orbita_word_sent_d <= 1'b0;
            debug_orbita_word_sent_2d <= 1'b0;
            debug_orbita_word_sent_3d <= 1'b0;
            debug_orbita_word_sent_4d <= 1'b0;
            debug_serviceman_ddd_cnt <= 5'd0;
            debug_sync <= 16'd0;
            debug_frame_label <= 1'b0;
            debug_group_label <= 8'd0;
            debug_group <= 64'd0;
            debug_group_ready <= 1'b0;
            debug_local_time <= 16'd0;
            debug_second_shares <= 14'd0;
        end
        else begin
            debug_orbita_word_sent_d <= debug_orbita_word_sent;
            debug_orbita_word_sent_2d <= debug_orbita_word_sent_d;
            debug_orbita_word_sent_3d <= debug_orbita_word_sent_2d;
            debug_orbita_word_sent_4d <= debug_orbita_word_sent_3d;

            debug_sync_ready <= 1'b0;
            debug_group_ready <= 1'b0;
            if (debug_orbita_word_sent) begin
                debug_sync_ready <= (debug_serviceman_ddd_cnt == 5'd31);
            end
            if (debug_orbita_word_sent_4d) begin
                debug_serviceman_ddd_cnt <= debug_serviceman_ddd_cnt + 1'b1;
                if (debug_serviceman_ddd_cnt[0] == 1'b1) begin
                    debug_sync <= {debug_sync[14:0], debug_video};
                end
                    
                if (debug_serviceman_ddd_cnt == 5'd14) begin
                    debug_serviceman_phrase_cnt <= debug_serviceman_phrase_cnt + 1'b1;
                    if (debug_serviceman_phrase_cnt == 6'd63) begin
                        debug_group_ready <= 1'b1;
                        debug_serviceman_group_cnt <= debug_serviceman_group_cnt + 1'b1;
                    end 
                                                     
                    debug_group <= debug_group << 1'b1;
                    debug_group[0] <= debug_video;
                end
            end
                
            if (debug_group_ready) begin
                debug_group_number <= debug_group[63:57];
                debug_frame_label <= debug_group[56];
                debug_group_label <= debug_group[7:0];
                debug_local_time <= debug_group[55:40];
                debug_second_shares <= {debug_group[39:33], debug_group[31:25]};
            end
        end
    end
endmodule

